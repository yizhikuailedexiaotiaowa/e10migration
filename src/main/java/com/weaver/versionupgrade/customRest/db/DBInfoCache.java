package com.weaver.versionupgrade.customRest.db;

import com.alibaba.druid.pool.DruidDataSource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cx
 * @date 2023/9/1
 **/
public class DBInfoCache {
    public static Map<String, MigrationDBInfoBean> targetDbInfoCache = new ConcurrentHashMap<>();
    public static Map<String, MigrationDBInfoBean> sourceDbInfoCache = new ConcurrentHashMap<>();

    private static ReentrantLock lockPool = new ReentrantLock();

    //连接池
    private static volatile ConcurrentHashMap<DBUTKey, DBPools> pools = new ConcurrentHashMap();
    /**
     * url地址
     */
    private static volatile ConcurrentHashMap<DBUTKey, DBUrl> urlMap = new ConcurrentHashMap();
    /**
     * 数据库类型
     */
    private static volatile ConcurrentHashMap<DBUTKey, String> dbTypeMap = new ConcurrentHashMap();
    /**
     * 无法连接的微服务
     */
    private static volatile ConcurrentHashMap<DBUTKey,String> noConServices = new ConcurrentHashMap<>();




    public static MigrationDBInfoBean getSDbByUseType(String useType) {
        return sourceDbInfoCache.get(useType);
    }
    public static void setSDbByUseType(String useType, MigrationDBInfoBean migrationDBInfoBean) {
        sourceDbInfoCache.put(useType, migrationDBInfoBean);
    }
    public static MigrationDBInfoBean getTDbByUseType(String useType) {
        return targetDbInfoCache.get(useType);
    }
    public static void setTDbByUseType(String useType, MigrationDBInfoBean migrationDBInfoBean) {
        targetDbInfoCache.put(useType, migrationDBInfoBean);
    }

    public static ConcurrentHashMap<DBUTKey, DBPools> getPools() {
        return pools;
    }

    public static void setPools(ConcurrentHashMap<DBUTKey, DBPools> pools) {
        DBInfoCache.pools = pools;
    }

    public static ConcurrentHashMap<DBUTKey, DBUrl> getUrlMap() {
        return urlMap;
    }

    public static void setUrlMap(ConcurrentHashMap<DBUTKey, DBUrl> urlMap) {
        DBInfoCache.urlMap = urlMap;
    }

    public static ConcurrentHashMap<DBUTKey, String> getDbTypeMap() {
        return dbTypeMap;
    }

    public static void setDbTypeMap(ConcurrentHashMap<DBUTKey, String> dbTypeMap) {
        DBInfoCache.dbTypeMap = dbTypeMap;
    }

    public static ConcurrentHashMap<DBUTKey, String> getNoConServices() {
        return noConServices;
    }

    public static void setNoConServices(ConcurrentHashMap<DBUTKey, String> noConServices) {
        DBInfoCache.noConServices = noConServices;
    }

    /**
     * 添加Pools
     * @param dbutKey
     * @param url
     * @param dataSource
     */
    public static void addDBPools(DBUTKey dbutKey,String url,DruidDataSource dataSource) {
        if(!pools.containsKey(dbutKey)){
            initPoolsByDBkey(dbutKey);
        }
        pools.get(dbutKey).addPools(url,dataSource );
    }

    public static void addDBUrl(DBUTKey dbutKey,String servicename,String url) {
        System.out.println("设置"+dbutKey.toString()+"----"+servicename);
        if(!pools.containsKey(dbutKey)){
            initPoolsByDBkey(dbutKey);
        }
        urlMap.get(dbutKey).addUrlMaping(servicename,url );
    }


    /**
     * 初始化连接池对象
     * @param dbutKey
     */
    public static  void initPoolsByDBkey(DBUTKey dbutKey){
            if(!pools.containsKey(dbutKey)){
                //缓存内容
                assert ! lockPool.isHeldByCurrentThread();
                lockPool.lock();
                try {
                    if(!DBInfoCache.getPools().containsKey(dbutKey)){
                        DBInfoCache.getPools().put(dbutKey,new DBPools());
                    }
                    if(!DBInfoCache.getUrlMap().containsKey(dbutKey)){
                        DBInfoCache.getUrlMap().put(dbutKey,new DBUrl());
                    }

                 }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    lockPool.unlock();
                }
            }
    }


}

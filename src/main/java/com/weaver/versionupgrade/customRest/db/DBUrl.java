package com.weaver.versionupgrade.customRest.db;


import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cx
 * @date 2023/9/7
 **/
public class DBUrl {
    private ConcurrentHashMap<String, String> urlMap = new ConcurrentHashMap();

    public ConcurrentHashMap<String, String> getUrlMap() {
        return urlMap;
    }

    public void setUrlMap(ConcurrentHashMap<String, String> urlMap) {
        this.urlMap = urlMap;
    }

    public void addUrlMaping(String servicename  ,String url ) {
        if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(url)){
            urlMap.put(servicename, url);
        }
    }
}

package com.weaver.versionupgrade.customRest.db;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerSchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.weaver.versionupgrade.MiDBExecutor;
import com.weaver.versionupgrade.customRest.util.Constant;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import com.weaver.versionupgrade.util.SyncConstant;
import com.weaver.versionupgrade.util.Util;
import org.apache.commons.lang3.StringUtils;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cx
 * @date 2023/8/17
 **/
public class DBUtil {
    //数据库是否已配置
    public static volatile boolean dbConfigSuc = false;
    public static String baseServiceName = null;
    public static String useType = null;
    private static  Pattern pFieldPattern = Pattern.compile(":new\\.(.*?) ");
    /**
     * MYSQL 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] MYSQL_KEYS = new String[]{"ADD", "ALL", "ALTER", "ANALYZE", "AND", "AS", "ASC", "ASENSITIVE", "BEFORE", "BETWEEN", "BIGINT", "BINARY", "BLOB", "BOTH", "BY", "CALL", "CASCADE", "CASE", "CHANGE", "CHAR", "CHARACTER", "CHECK", "COLLATE", "COLUMN", "CONDITION", "CONSTRAINT", "CONTINUE", "CONVERT", "CREATE", "CROSS", "CUBE", "CUME_DIST", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER", "CURSOR", "DATABASE", "DATABASES", "DAY_HOUR", "DAY_MICROSECOND", "DAY_MINUTE", "DAY_SECOND", "DEC", "DECIMAL", "DECLARE", "DEFAULT", "DELAYED", "DELETE", "DENSE_RANK", "DESC", "DESCRIBE", "DETERMINISTIC", "DISTINCT", "DISTINCTROW", "DIV", "DOUBLE", "DROP", "DUAL", "EACH", "ELSE", "ELSEIF", "EMPTY", "ENCLOSED", "ESCAPED", "EXCEPT", "EXISTS", "EXIT", "EXPLAIN", "FALSE", "FETCH", "FIRST_VALUE", "FLOAT", "FLOAT4", "FLOAT8", "FOR", "FORCE", "FOREIGN", "FROM", "FULLTEXT", "FUNCTION", "GENERATED", "GET", "GRANT", "GROUP", "GROUPING", "GROUPS", "HAVING", "HIGH_PRIORITY", "HOUR_MICROSECOND", "HOUR_MINUTE", "HOUR_SECOND", "IF", "IGNORE", "IN", "INDEX", "INFILE", "INNER", "INOUT", "INSENSITIVE", "INSERT", "INT", "INT1", "INT2", "INT3", "INT4", "INT8", "INTEGER", "INTERVAL", "INTO", "IO_AFTER_GTIDS", "IO_BEFORE_GTIDS", "IS", "ITERATE", "JOIN", "JSON_TABLE", "KEY", "KEYS", "KILL", "LAG", "LAST_VALUE", "LATERAL", "LEAD", "LEADING", "LEAVE", "LEFT", "LIKE", "LIMIT", "LINEAR", "LINES", "LOAD", "LOCALTIME", "LOCALTIMESTAMP", "LOCK", "LONG", "LONGBLOB", "LONGTEXT", "LOOP", "LOW_PRIORITY", "MASTER_BIND", "MASTER_SSL_VERIFY_SERVER_CERT", "MATCH", "MAXVALUE", "MEDIUMBLOB", "MEDIUMINT", "MEDIUMTEXT", "MIDDLEINT", "MINUTE_MICROSECOND", "MINUTE_SECOND", "MOD", "MODIFIES", "NATURAL", "NOT", "NO_WRITE_TO_BINLOG", "NTH_VALUE", "NTILE", "NULL", "NUMERIC", "OF", "ON", "OPTIMIZE", "OPTIMIZER_COSTS", "OPTION", "OPTIONALLY", "OR", "ORDER", "OUT", "OUTER", "OUTFILE", "OVER", "PARTITION", "PARTITIONING", "PARTITIONS", "PASSWORD", "PASSWORD_LOCK_TIME", "PATH", "PERCENT_RANK", "PERSIST6", "PERSIST_ONLY6", "PHASE", "PLUGIN", "PLUGINS", "PLUGIN_DIR", "POINT", "POLYGON", "PORT", "PRECEDES", "PRECEDING", "PRECISION", "PREPARE", "PRESERVE", "PREV", "PRIMARY", "PRIVILEGES", "PRIVILEGE_CHECKS_USER", "PROCEDURE", "PROCESS", "PROCESSLIST", "PROFILE", "PROFILES", "PROXY", "PURGE", "RANGE", "RANK", "READ", "READS", "READ_WRITE", "REAL", "RECURSIVE", "REFERENCES", "REGEXP", "RELEASE", "RENAME", "REPEAT", "REPLACE", "REQUIRE", "RESIGNAL", "RESTRICT", "RETURN", "REVOKE", "RIGHT", "RLIKE", "ROW", "ROWS", "ROW_NUMBER", "SCHEMA", "SCHEMAS", "SECOND_MICROSECOND", "SELECT", "SENSITIVE", "SEPARATOR", "SET", "SHOW", "SIGNAL", "SMALLINT", "SPATIAL", "SPECIFIC", "SQL", "SQLEXCEPTION", "SQLSTATE", "SQLWARNING", "SQL_BIG_RESULT", "SQL_CALC_FOUND_ROWS", "SQL_NO_CACHE", "SQL_SMALL_RESULT", "SSL", "STARTING", "STORED", "STRAIGHT_JOIN", "SYSTEM", "TABLE", "TERMINATED", "THEN", "TINYBLOB", "TINYINT", "TINYTEXT", "TO", "TRAILING", "TRIGGER", "TRUE", "UNDO", "UNION", "UNIQUE", "UNLOCK", "UNSIGNED", "UPDATE", "USAGE", "USE", "USING", "UTC_DATE", "UTC_TIME", "UTC_TIMESTAMP", "VALUES", "VARBINARY", "VARCHAR", "VARCHARACTER", "VARYING", "VIRTUAL", "WHEN", "WHERE", "WHILE", "WINDOW", "WITH", "WRITE", "XOR", "YEAR_MONTH", "ZEROFILL"};
    /**
     * POSTGRESQL 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] POSTGRESQL_KEYS = new String[]{"ALL", "ANALYSE", "ANALYZE", "AND", "ANY", "ARRAY", "AS", "ASC", "BINARY", "BOTH", "CASE", "CHECK", "COLLATE", "COLLATION", "COLUMN", "CONSTRAINT", "CREATE", "CROSS", "CURRENT_DATE", "CURRENT_TIMESTAMP", "CURRENT_USER", "DEFAULT", "DESC", "DISTINCT", "DO", "ELSE", "END", "EXCEPT", "FALSE", "FETCH", "FOR", "FOREIGN", "FROM", "FULL", "GRANT", "GROUP", "HAVING", "IN", "INNER", "INTO", "IS", "JOIN", "LATERAL", "LEADING", "LEFT", "LIKE", "LIMIT", "LOCALTIME", "LOCALTIMESTAMP", "NATURAL", "NOT", "NULL", "OFFSET", "ON", "ONLY", "OR", "ORDER", "OUTER", "PRIMARY", "REFERENCES", "RETURNING", "RIGHT", "SELECT", "SOME", "TABLE", "THEN", "TO", "TRAILING", "TRUE", "UNION", "UNIQUE", "USER", "USING", "WHEN", "WHERE", "WINDOW", "WITH", "ARRAY", "EXCEPT", "LATERAL"};
    /**
     * ORACLE 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] ORACLE_KEYS = new String[]{"ACCESS", "ADD", "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "AUDIT", "BEGIN", "BETWEEN", "BY", "CHAR", "CHECK", "CLUSTER", "COLUMN", "COMMENT", "COMPRESS", "CONNECT", "CREATE", "CURRENT", "DATE", "DECIMAL", "DECLARE", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", "ELSE", "EXCLUSIVE", "EXISTS", "EXTERNAL", "FILE", "FLOAT", "FOR", "FROM", "GRANT", "GROUP", "HAVING", "IDENTIFIED", "IMMEDIATE", "IN", "INCREMENT", "INDEX", "INITIAL", "INSERT", "INTEGER", "INTERSECT", "INTO", "IS", "LEVEL", "LIKE", "LOCK", "LONG", "MAXEXTENTS", "MINUS", "MLSLABEL", "MODE", "MODIFY", "NOAUDIT", "NOCOMPRESS", "NOT", "NOWAIT", "NULL", "NUMBER", "OF", "OFF", "OFFLINE", "ON", "ONLINE", "OPTION", "OR", "ORA_ROWSCN", "ORDER", "OVERFLOW", "PCTFREE", "PRIOR", "PUBLIC", "RAW", "RENAME", "RESOURCE", "REVOKE", "ROW", "ROWID", "ROWNUM", "ROWS", "SELECT", "SESSION", "SET", "SHARE", "SIZE", "SMALLINT", "START", "SUCCESSFUL", "SYNONYM", "SYSDATE", "TABLE", "THEN", "TO", "TRIGGER", "UID", "UNION", "UNIQUE", "UPDATE", "USER", "VALIDATE", "VALUES", "VARCHAR", "VARCHAR2", "VIEW", "WHENEVER", "WHERE", "WITH", "VERSIONS"};

    /**
     * SQLSERVER 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] SQLSERVER_KEYS = new String[]{"TOP","ADD", "EXTERNAL", "PROCEDURE", "ALL", "FETCH", "PUBLIC", "ALTER", "FILE", "RAISERROR", "AND", "FILLFACTOR", "READ", "ANY", "FOR", "READTEXT", "AS", "FOREIGN", "RECONFIGURE", "ASC", "FREETEXT", "REFERENCES", "AUTHORIZATION", "FREETEXTTABLE", "BACKUP", "FROM", "RESTORE", "BEGIN", "FULL", "RESTRICT", "BETWEEN", "FUNCTION", "RETURN", "BREAK", "GOTO", "REVERT", "BROWSE", "GRANT", "REVOKE", "BULK", "GROUP", "RIGHT", "BY", "HAVING", "ROLLBACK", "CASCADE", "HOLDLOCK", "ROWCOUNT", "CASE", "IDENTITY", "ROWGUIDCOL", "CHECK", "IDENTITY_INSERT", "RULE", "CHECKPOINT", "IDENTITYCOL", "SAVE", "CLOSE", "IF", "SCHEMA", "CLUSTERED", "IN", "SECURITYAUDIT", "COALESCE", "INDEX", "SELECT", "COLLATE", "INNER", "COLUMN", "INSERT", "COMMIT", "INTERSECT", "COMPUTE", "INTO", "SESSION_USER", "CONSTRAINT", "IS", "SET", "CONTAINS", "JOIN", "SETUSER", "CONTAINSTABLE", "KEY", "SHUTDOWN", "CONTINUE", "KILL", "SOME", "CONVERT", "LEFT", "STATISTICS", "CREATE", "LIKE", "SYSTEM_USER", "CROSS", "LINENO", "TABLE", "CURRENT", "TABLESAMPLE", "CURRENT_DATE", "MERGE", "TEXTSIZE", "CURRENT_TIME", "NATIONAL", "THEN", "CURRENT_TIMESTAMP", "NOCHECK", "TO", "CURRENT_USER", "TRANSACTION", "CURSOR", "DEALLOCATE", "UNION", "NATIONAL", "UNIQUE", "DECLARE", "UPDATE", "DEFAULT", "DELETE", "USER", "DESC", "NOT", "NULL", "NULLIF", "VALUES", "VARYING", "DISTINCT", "OF", "VIEW", "ON", "WHEN", "DOUBLE", "DROP", "OPEN", "WHERE", "ELSE", "OPTION", "WITH", "END", "OR", "END-EXEC", "ORDER", "ESCAPE", "OUTER", "EXCEPT", "ABSOLUTE", "NULL", "PRIMARY", "REWRITE_OR_ERROR", "SIZE", "TRIGGER", "TRUNCATE", "USE"};

    public static List<String> numberTypes = new ArrayList<String>(){
        {
            this.add("TINYINT");
            this.add("SMALLINT");
            this.add("MEDIUMINT");
            this.add("INT");
            this.add("BIGINT");
            this.add("FLOAT");
            this.add("DOUBLE");
            this.add("DECIMAL");
            this.add("BIT");
            this.add("NUMBER");
            this.add("INTEGER");

        }
    };
    public static List<String> dataTimeTypes = new ArrayList<String>(){
        {
            this.add("DATE");
            this.add("TIMESTAMP");
            this.add("DATETIME");
            this.add("TIME");
        }
    };

    /**
     * 大字段
     */
    public static List<String> largerColumnTypes = new ArrayList<String>(){
        {
            this.add("CLOB");
            this.add("NCLOB");
            this.add("BLOB");
            this.add("LONGBLOB");
            this.add("LONGVARBINARY");
            this.add("IMAGE");
            this.add("LONG");
        }
    };


    /**
     * 去掉括号，注意调用一次只能去掉最外层的一组
     * @param columnValue
     * @return
     */
    public static String repalceBracket(String columnValue) {
        Pattern pattern = Pattern.compile("[(](.+)[)]");
        Matcher matcher = pattern.matcher(columnValue);
        if (matcher.find()) {
            columnValue = matcher.group(1);
        }
        return columnValue;
    }
    /**
     * 格式化完成的sql
     * @param fromsql
     * @return
     */
    public static String formatIntegratedSql(String fromsql) {
        if(!fromsql.startsWith("select")){
            if(!fromsql.startsWith("from")){
                fromsql="from "+fromsql;
            }
            fromsql="select * "+fromsql;
        }
        return fromsql;
    }
    /**
     * 解析fromsql得到表名
     * @param fromsql
     * @return
     */
    public static ArrayList<String> getTableNamesByFromSql(String fromsql, String dbType){//兼容性,表名全为小写
        ArrayList<String> tableNameList = new ArrayList<>();
        if("sqlserver".equals(dbType)){
            List<SQLStatement> stmtList = SQLUtils.parseStatements(fromsql, "sqlserver");
            for(SQLStatement sqlStatement:stmtList){
                SQLServerSchemaStatVisitor visitor=new SQLServerSchemaStatVisitor();
                sqlStatement.accept(visitor);
                Map<TableStat.Name,TableStat> tables=visitor.getTables();
                Set<TableStat.Name> tableNameSet=tables.keySet();
                for(TableStat.Name name:tableNameSet){
                    String tableName=name.getName();
                    if(StringUtils.isNotBlank(tableName)){
                        tableNameList.add(tableName);
                    }
                }
            }
        }else if("mysql".equals(dbType)){
            List<SQLStatement> stmtList = SQLUtils.parseStatements(fromsql,"mysql");
            for(SQLStatement sqlStatement:stmtList){
                MySqlSchemaStatVisitor visitor=new MySqlSchemaStatVisitor();
                sqlStatement.accept(visitor);
                Map<TableStat.Name,TableStat> tables=visitor.getTables();
                Set<TableStat.Name> tableNameSet=tables.keySet();
                for(TableStat.Name name:tableNameSet){
                    String tableName=name.getName();
                    if(StringUtils.isNotBlank(tableName)){
                        tableNameList.add(tableName);
                    }
                }
            }
        }else if("oracle".equals(dbType)){
            List<SQLStatement> stmtList = SQLUtils.parseStatements(fromsql,"oracle");
            for(SQLStatement sqlStatement:stmtList){
                OracleSchemaStatVisitor visitor=new OracleSchemaStatVisitor();
                sqlStatement.accept(visitor);
                Map<TableStat.Name,TableStat> tables=visitor.getTables();
                Set<TableStat.Name> tableNameSet=tables.keySet();
                for(TableStat.Name name:tableNameSet){
                    String tableName=name.getName();
                    if(StringUtils.isNotBlank(tableName)){
                        tableNameList.add(tableName);
                    }
                }
            }
        }else{//其他默认使用mysql的解析方式
            List<SQLStatement> stmtList = SQLUtils.parseStatements(fromsql,"mysql");
            for(SQLStatement sqlStatement:stmtList){
                MySqlSchemaStatVisitor visitor=new MySqlSchemaStatVisitor();
                sqlStatement.accept(visitor);
                Map<TableStat.Name,TableStat> tables=visitor.getTables();
                Set<TableStat.Name> tableNameSet=tables.keySet();
                for(TableStat.Name name:tableNameSet){
                    String tableName=name.getName();
                    if(StringUtils.isNotBlank(tableName)){
                        tableNameList.add(tableName);
                    }
                }
            }
        }
        return tableNameList;
    }


    /**
     * 获取数据库名字的sql
     * @param dbtype
     * @return
     */
    public  static  String  getDbnameSql(String dbtype){
        String selectsql="select database() as dbname";
        if(dbtype.equals("oracle")){
            selectsql="SELECT USER as dbname FROM DUAL";
        }else if(dbtype.equals("sqlserver")){
            selectsql="Select Name as dbname From Master..SysDataBases Where DbId=(Select Dbid From Master..SysProcesses Where Spid = @@spid)";
        }else if(dbtype.equals("mysql")){
            selectsql="select database() as dbname";
        }else if(dbtype.equals("postgresql")){
            selectsql="select current_database() as dbname" ;
        }
        return selectsql;
    }

    /**
     * 获得数据库类型
     * @param URL
     */
    public  static  Map<String,String> getDBtypeByUrl(String URL){

        Map<String,String> result=new HashMap<>();
        if(StringUtils.isBlank(URL)){
            result.put("dbtype","" );
            result.put("realDbtype","" );
            return result;
        }
        String dbtype="oracle";
        String realDbtype = "oracle";
        if (URL.indexOf(":oracle") >= 0) {
            dbtype = "oracle";
            realDbtype = "oracle";
        } else if (URL.indexOf(":sqlserver") >= 0) {
            dbtype = "sqlserver";
            realDbtype = "sqlserver";
        } else if (URL.indexOf(":mysql") >= 0) {
            dbtype = "mysql";
            realDbtype = "mysql";
        } else if (URL.indexOf("oscar") >= 0) {//神通国产数据库高度兼容oracle，设置dbtype为oracle并保留原始类型在orgindbtype属性
            dbtype = "oracle";
            realDbtype = "st";
        } else if (URL.indexOf("kingbase") >= 0) {//人大金仓国产数据库高度兼容oracle，设置dbtype为oracle并保留原始类型在orgindbtype属性中
            dbtype = "oracle";
            realDbtype = "jc";
        } else if (URL.indexOf("gbasedbt") >= 0) {
            dbtype = "mysql";
            realDbtype = "nt";
        } else if (URL.indexOf("gbase") >= 0) {
            dbtype = "mysql";
            realDbtype = "ntold";
        } else if (URL.contains("zenith")) {
            dbtype = "oracle";
            realDbtype = "gs";
        } else if (URL.indexOf("oceanbase") >= 0) {
            dbtype = "oracle";
            realDbtype = "ob";
        } else if (URL.indexOf("postgresql") >= 0) {
            dbtype = "postgresql";
            realDbtype = "pg";
        } else if (URL.indexOf("highgo") >= 0) {
            dbtype = "postgresql";
            realDbtype = "hg";
        } else if (URL.indexOf("gaussdb") >= 0) {
            dbtype = "postgresql";
            realDbtype = "hg";
        } else if (URL.indexOf("uxdb") >= 0) {
            dbtype = "postgresql";
            realDbtype = "ux";
        } else if (URL.indexOf("db2") >= 0) {
            dbtype = "db2";
            realDbtype = "db2";
        } else if (URL.indexOf("dm") >= 0) {//dm国产数据库高度兼容oracle，设置dbtype为oracle并保留原始类型在orgindbtype属性中
            dbtype = "oracle";
            realDbtype = "dm";
        }

        result.put("dbtype", dbtype);
        result.put("realDbtype", realDbtype);
        return result;
    }


    public static String getScriptbreak(String dbtype) {
        String var1 = ";";
        if(dbtype.equals("oracle")){
            var1="/";
        }else  if(dbtype.equals("sqlserver")){
            var1="GO";
        }else  if(dbtype.equals("mysql")){
            var1=";";
        }else  if(dbtype.equals("postgresql")){
            var1=";";
        }
//        boolean var2 = dbtype.equalsIgnoreCase("oracle");
//        boolean var3 = dbtype.equals("db2");
//        boolean var4 = dbtype.equals("mysql");
//        boolean var5 = dbtype.equals("dm");
//        boolean var6 = dbtype.equals("st");
//        if (var2) {
//            var1 = "/";
//        } else if (var3) {
//            var1 = ";";
//        } else if (var4) {
//            var1 = ";";
//        } else if (var5) {
//            var1 = "/";
//        } else if (var6) {
//            var1 = "/";
//        } else {
//            var1 = "GO";
//        }

        return var1;
    }



    /**
     * 获取所有表名的sql
     * @return
     */
    public static String  getTableNamesSql(String dbtype) {
        String sql="select table_name as tablename from information_schema.tables where TABLE_SCHEMA = (SELECT DATABASE ()) AND TABLE_TYPE = 'BASE TABLE' order by TABLE_NAME";
        if("mysql".equals(dbtype)){
            sql = "select table_name as tablename from information_schema.tables where TABLE_SCHEMA = (SELECT DATABASE ()) AND TABLE_TYPE = 'BASE TABLE' order by TABLE_NAME";
        }else if("sqlserver".equals(dbtype)){
            sql = "SELECT Name as tablename FROM SysObjects Where XType='U' ORDER BY Name";
        }else if("oracle".equals(dbtype)){
            sql = "select t.table_name as tablename from user_tables t order by t.table_name";
        }else if("postgresql".equals(dbtype)){
            sql = "select tablename from pg_tables where schemaname= (SELECT current_schema()) ";
        }
        return sql;
    }



    /**
     * 获取表所有的字段名
     * @param tableName
     * @return
     */
    public static  String getTablefieldsNamesSqlOrderBYId(String tableName,String dbtype) {
        String sql = "";

        if(dbtype.equals("sqlserver")){
            sql="SELECT b.name as column_name, c.name as data_type ,b.max_length as data_length,\n" +
                " b.is_nullable as isnullable, b.is_identity as inc_info, e.definition as column_default,b.precision as num_precision,b.scale as num_scale \n" +
                " FROM sys.tables a\n" +
                "INNER JOIN sys.columns b ON a.object_id=b.object_id\n" +
                "LEFT JOIN sys.types c ON b.system_type_id=c.system_type_id\n" +
                "LEFT JOIN sys.extended_properties d ON a.object_id=d.major_id AND b.column_id=d.minor_id\n" +
                "LEFT JOIN sys.default_constraints e ON e.name=OBJECT_NAME(b.default_object_id)\n"+
                "WHERE a.name='"+tableName+"' " +
				"order by b.column_id ";
        }else if(dbtype.equals("oracle")) {
            sql = "select column_name,data_type,data_length,data_default as column_default,nullable as isnullable,data_precision as num_precision,data_scale as num_scale,'' as inc_info" +
                " from user_tab_columns A where TABLE_NAME='"+tableName.toUpperCase()+"'  order by column_id";
        }else if(dbtype.equals("mysql")){
            sql = "select column_name,data_type,character_maximum_length as data_length,EXTRA as inc_info," +
                "IS_NULLABLE as isnullable,column_default,NUMERIC_PRECISION as num_precision,NUMERIC_SCALE as num_scale from information_schema.columns where" +
                " TABLE_SCHEMA= (select database()) and  table_name='"+tableName.toLowerCase()+"'  ORDER BY ORDINAL_POSITION ";
        }else if("postgresql".equals(dbtype)){
			sql = "select column_name,data_type,character_maximum_length as data_length,is_identity as inc_info ," +
                "IS_NULLABLE as isnullable,column_default,numeric_precision as num_precision ,numeric_scale as num_scale from information_schema.columns where table_schema=(SELECT current_schema()) and table_name='"+tableName.toLowerCase()+"'  ORDER BY ORDINAL_POSITION";
       }

        return sql;
    }


 /**
     * 获取表所有的字段名
     * @param tableName
     * @return
     */
    public static  String getTablefieldsNamesSql(String tableName,String dbtype) {
        String sql = "";

        if(dbtype.equals("sqlserver")){
            sql="SELECT b.name as column_name, c.name as data_type ,b.max_length as data_length,\n" +
                " b.is_nullable as isnullable\n" +
                "\n" +
                " FROM sys.tables a\n" +
                "INNER JOIN sys.columns b ON a.object_id=b.object_id\n" +
                "LEFT JOIN sys.types c ON b.system_type_id=c.system_type_id\n" +
                "LEFT JOIN sys.extended_properties d ON a.object_id=d.major_id AND b.column_id=d.minor_id\n" +
                "WHERE a.name='"+tableName+"' " +
                "ORDER BY b.name ";
        }else if(dbtype.equals("oracle")) {
            sql = "select column_name,data_type,data_length from user_tab_columns A where TABLE_NAME='"+tableName.toUpperCase()+"' ORDER BY column_name";
        }else if(dbtype.equals("mysql")){
            sql = "select column_name,data_type,character_maximum_length as data_length from information_schema.columns where table_name='"+tableName.toLowerCase()+"' and TABLE_SCHEMA = (SELECT DATABASE ())  ORDER BY column_name";
        }else if("postgresql".equals(dbtype)){
            sql = "select column_name,data_type,character_maximum_length as data_length from information_schema.columns where table_schema=(SELECT current_schema()) and table_name='"+tableName.toLowerCase()+"'  ORDER BY column_name";
        }

        return sql;
    }



    /**
     * 获取判断表是否存在的sql
     * @param tablename
     * @return
     */
    public static String getCheckTableExistSql(String tablename,String dbtype){
        if(dbtype.equals("sqlserver")){
            return getSqlserverTableExistSql(tablename);
        }else if(dbtype.equals("oracle")) {
           return getOracleTableExistSql(tablename);
        }else if(dbtype.equals("mysql")){
           return  getMysqlTableExistSql(tablename);
        }else if("postgresql".equals(dbtype)){
            return getPostgresqlTableExistSql(tablename);
        }
        return getOracleTableExistSql(tablename);
    }

    /**
     * 数据库列名关键字处理
     * @param sqlField
     * @param dbType
     * @return
     */
    public static String formateSqlField(String sqlField, String dbType) {
        if(sqlField != null) {
            String sqlFieldtemp=sqlField.toUpperCase();
            if(dbType.equalsIgnoreCase(SyncConstant.ORACLE)) {
                if(sqlField.startsWith("_")) {//oracle不支持下划线开头的字段
                    sqlField = "\"" + sqlField.toUpperCase() + "\"";
                    return sqlField;
                }
            }
            if(getDBKeysArray(dbType).contains(sqlFieldtemp)){
                if(dbType.equalsIgnoreCase(SyncConstant.SQLSERVER)){
                    sqlField="["+sqlField+"]";
                }else if(dbType.equalsIgnoreCase(SyncConstant.ORACLE)){
                    sqlField = "\"" + sqlField.toUpperCase() + "\"";
                }else if(dbType.equalsIgnoreCase(Constant.POSTGRESQL)){
					sqlField = "\"" + sqlField.toLowerCase() + "\"";
                }else{
                    sqlField="`"+sqlField+"`";
                }
            }
        }
        return sqlField;
    }

    /**
     * 获取各个数据的关键字信息
     * @param dbtype
     * @return
     */
    public static List<String> getDBKeysArray(String dbtype){

        if ("oracle".equalsIgnoreCase(dbtype)) {
            return Arrays.asList(ORACLE_KEYS);
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return Arrays.asList(MYSQL_KEYS);
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return Arrays.asList(POSTGRESQL_KEYS);
        } else {
            return Arrays.asList(SQLSERVER_KEYS);
        }
    }


    /**
     * 获取判断列是否存存在的sql
     * @param tablename
     * @param fieldName
     * @return
     */
    public static String getCheckFieldExistSql(String tablename,String fieldName,String dbtype){
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select 1 from  USER_TAB_COLUMNS WHERE TABLE_NAME = UPPER('"+tablename+"') AND COLUMN_NAME = '"+fieldName+"' ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return "select count(*) as cnt from information_schema.columns " +
                "        where table_name ='"+tablename+"' and column_name ='"+fieldName+"' and TABLE_SCHEMA = (select database())";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return "select column_name,TABLE_SCHEMA from information_schema.columns WHERE table_schema=(SELECT current_schema()) " +
                "and table_name = lower('"+tablename+"') and column_name = lower('"+fieldName+"') ";
        } else {
            return "select 1 from syscolumns  where id=object_id('"+tablename+"') and name='"+fieldName+"'";
        }
    }



    /**
     * 判断表是否存在的sql
     * @param tableName
     * @return
     */
    public static String getMysqlTableExistSql(String tableName){
        return "select count(*) as count from information_schema.TABLES where TABLE_SCHEMA = (SELECT DATABASE ()) and TABLE_NAME = '"+tableName+"';";
    }


    /**
     * 判断表是否存在的sql
     * @param tableName
     * @return
     */
    public static String getSqlserverTableExistSql(String tableName){
        return "select count(*) as count from sysObjects where Id=OBJECT_ID(N'"+tableName+"')";
    }

    /**
     * 判断表是否存在的sql
     * @param tableName
     * @return
     */
    public static String getOracleTableExistSql(String tableName){
        return "select count(*) as count from user_tables where table_name =upper('"+tableName+"')";
    }

    /**
     * 判断表是否存在的sql
     * @param tableName
     * @return
     */
    public static String getPostgresqlTableExistSql(String tableName){
        return "select count(*) as count from information_schema.tables where table_schema=(SELECT current_schema())\n" +
            " and table_type='BASE TABLE' and table_name=lower('"+tableName+"')";
    }


    /**
     * 判断索引是否存在的sql
     * @param
     * @return
     */
    public static String getPostgresqlIndexExistSql(String indexname,String tablename){
      return "select  * from pg_class where relname=lower('"+indexname+"') and  relkind in ( 'i')";
        //return "SELECT * FROM pg_indexes WHERE indexname = '"+indexname+"' AND schemaname = (SELECT current_database())";
    }


 /**
     * 数据库连接格式化
     * @param databaseType
     * @param databaseIP
     * @param databasePort
     * @param databaseName
     * @return
     */
    public static Map<String, String> transDatabaseType(
        String databaseType, String databaseIP, String databasePort, String databaseName) {
        databaseIP=Util.null2String(databaseIP).trim();
        StringBuilder jdbcurl = new StringBuilder("jdbc:");
        String jdbcdriver = "";
        if ("1".equals(databaseType)) {
            //jdbc:mysql://127.0.0.1:3306/ecanaly
            jdbcurl.append("mysql://").append(databaseIP).append(":")
                .append(databasePort).append("/").append(databaseName)
                .append("?characterEncoding=utf8&useSSL=false&autoReconnect=true&failOverReadOnly=false&serverTimezone=Asia/Shanghai");
            jdbcdriver = "com.mysql.cj.jdbc.Driver";
        } else if ("2".equals(databaseType)) {
            jdbcurl.append("oracle:thin:@")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName);
            jdbcdriver = "oracle.jdbc.driver.OracleDriver";
        } else if ("3".equals(databaseType)) {
//            jdbcurl.append(":microsoft:sqlserver://")
//                .append(databaseIP)
//                .append(":")
//                .append(databasePort)
//                .append(";")
//                .append("DatabaseName=")
//                .append(databaseName);
//            jdbcdriver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
//        } else if ("sqlserver2005".equals(databaseType)||"sqlserver2008".equals(databaseType)) {
            jdbcurl.append("sqlserver://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append(";")
                .append("DatabaseName=")
                .append(databaseName);
            jdbcdriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        } else if ("dm".equals(databaseType)) {//DM
            jdbcurl.append("dm://")
                .append(databaseIP)
                .append(":")
                .append(databasePort);
//                .append("/")
//                .append(databaseName);
            jdbcdriver = "dm.jdbc.driver.DmDriver";

        } else if ("gs".equals(databaseType)) {//GS
            jdbcurl.append("zenith:@")
                .append(databaseIP)
                .append(":")
                .append(databasePort);
//                .append("/")
//                .append(databaseName)
//                .append("?connectTimeout=0&socketTimeout=0");
            jdbcdriver = "com.huawei.gauss.jdbc.ZenithDriver";
        } else if ("jc".equals(databaseType)) {//JC
            jdbcurl.append("kingbase8://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?prepareThreshold=0&clientEncoding=UTF-8")
            ;
            jdbcdriver = "com.kingbase8.Driver";
        } else if ("pg".equals(databaseType)) {//PG
            jdbcurl.append("postgresql://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?connectTimeout=0")
            ;
            jdbcdriver = "org.postgresql.Driver";

        } else if ("st".equals(databaseType)) {//神通
            jdbcurl.append("oscar://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?useUnicode=true&characterEncoding=utf-8;useOldAliasMetadataBehavior=true");
            ;
            jdbcdriver = "com.oscar.Driver";

        } else if ("hg".equals(databaseType)) {
            jdbcurl.append("highgo://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName);
            jdbcdriver = "com.highgo.jdbc.Driver";
        } else if ("og".equals(databaseType)) {
            jdbcurl.append("gaussdb://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?connectTimeout=0&socketTimeout=0")
            ;
            jdbcdriver = "com.huawei.gauss200.jdbc.Driver";

        } else if ("ux".equals(databaseType)) {
            jdbcurl.append("uxdb://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?connectTimeout=0&socketTimeout=0")
            ;
            jdbcdriver = "com.uxsino.uxdb.Driver";
        } else if ("db2".equals(databaseType)) {//暂不处理
            jdbcurl.append("db2://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append(":")
                .append(databaseName)
            ;
            jdbcdriver = "com.ibm.db2.jdbc.net.DB2Driver";
        } else if ("nt".equals(databaseType)) {
            jdbcurl.append("gbasedbt-sqli://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append(";SQLMODE=Oracle;DB_LOCALE=zh_CN.57372;DELIMIDENT=y;IFX_LOCK_MODE_WAIT=100;OPT_USE_PUT=true;OPTOFC=1;IFX_AUTOFREE=1;direct_query=0;CASE_SENSITIVE=N;BLOB_VARCHAR2_OPT=0 ")
            ;
            jdbcdriver = "com.gbasedbt.jdbc.Driver";
        } else if ("ob".equals(databaseType)) {
            jdbcurl.append("oceanbase://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
            ;
            jdbcdriver = "com.alipay.oceanbase.jdbc.Driver";

        } else if ("obmysql".equals(databaseType)) {
            jdbcurl.append("oceanbase://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append("/")
                .append(databaseName)
                .append("?obtype=mysql")
            ;
            jdbcdriver = "com.alipay.oceanbase.jdbc.Driver";
            try
            {
                Class.forName("com.alipay.oceanbase.jdbc.Driver");
            } catch (ClassNotFoundException e){
                jdbcurl.append("mysql://")
                    .append(databaseIP)
                    .append(":")
                    .append(databasePort)
                    .append("/")
                    .append(databaseName)
                    .append("?characterEncoding=utf8&useSSL=false&autoReconnect=true&failOverReadOnly=false&serverTimezone=Asia/Shanghai&obtype=mysql")
                ;
                jdbcdriver = "com.mysql.cj.jdbc.Driver";
            }


        } else {
            jdbcurl.append("sqlserver://")
                .append(databaseIP)
                .append(":")
                .append(databasePort)
                .append(";")
                .append("DatabaseName=")
                .append(databaseName);
            jdbcdriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        }

        HashMap<String, String> jdbcInfo = new HashMap(16);
        jdbcInfo.put("url", jdbcurl.toString());
        jdbcInfo.put("DriverClasses", jdbcdriver);
        return jdbcInfo;
    }


    /**
     * 直接源数据库  根据表名获取建表语句  弃用，要兼容不同种类数据库互转
     * @param tableName
     * @return
     */
    public static String getSourceCreateTableSql(String tableName, SourceDBUtil rs) {
        String result = "";
        String sql = "";
        String dbtype = rs.getDbtype();
        switch(dbtype){
            case Constant.MYSQL:
                sql = "show create table "+tableName;
                rs.executeSql(sql);
                if(rs.next()){
                    //第二列为建表语句
                    result = rs.getString(2);
                }
                break;
            case Constant.SQLSERVER:
                sql  = "select 'create table [' + so.name + '] (' + o.list + ')' + CASE\n"
                    + "                                                                       WHEN tc.Constraint_Name IS NULL THEN ''\n"
                    + "                                                                       ELSE 'ALTER TABLE ' + so.Name +\n"
                    + "                                                                            ' ADD CONSTRAINT ' + tc.Constraint_Name +\n"
                    + "                                                                            ' PRIMARY KEY ' +\n"
                    + "                                                                            ' (' + LEFT(j.List, Len(j.List)-1) + ')' END\n"
                    + "        from sysobjects so\n"
                    + "            cross apply\n"
                    + "            (SELECT\n"
                    + "            '  ['+ column_name +'] ' +\n"
                    + "            data_type + case data_type\n"
                    + "            when 'sql_variant' then ''\n"
                    + "            when 'text' then ''\n"
                    + "            when 'ntext' then ''\n"
                    + "            when 'xml' then ''\n"
                    + "            when 'image' then ''\n"
                    + "            when 'decimal' then '(' + cast (numeric_precision as varchar) + ', ' + cast (numeric_scale as varchar) + ')'\n"
                    + "            else coalesce ('('+ case when character_maximum_length = -1 then 'MAX' else cast (character_maximum_length as varchar) end +')', '') end + ' ' +\n"
                    + "            case when exists (\n"
                    + "            select id from syscolumns\n"
                    + "            where object_name(id)=so.name\n"
                    + "            and name = column_name\n"
                    + "            and columnproperty(id, name, 'IsIdentity') = 1\n"
                    + "            ) then\n"
                    + "            'IDENTITY(' +\n"
                    + "            cast (ident_seed(so.name) as varchar) + ',' +\n"
                    + "            cast (ident_incr(so.name) as varchar) + ')'\n"
                    + "            else ''\n"
                    + "            end + ' ' +\n"
                    + "            (case when IS_NULLABLE = 'No' then 'NOT ' else '' end ) + 'NULL ' +\n"
                    + "            case when information_schema.columns.COLUMN_DEFAULT IS NOT NULL THEN 'DEFAULT '+ information_schema.columns.COLUMN_DEFAULT ELSE '' END + ', '\n"
                    + "            from information_schema.columns where table_name = so.name\n"
                    + "            order by ordinal_position\n"
                    + "            FOR XML PATH ('')) o (list)\n"
                    + "            left join\n"
                    + "            information_schema.table_constraints tc\n"
                    + "        on tc.Table_name = so.Name\n"
                    + "            AND tc.Constraint_Type = 'PRIMARY KEY'\n"
                    + "            cross apply\n"
                    + "            (select '[' + Column_Name + '], '\n"
                    + "            FROM information_schema.key_column_usage kcu\n"
                    + "            WHERE kcu.Constraint_Name = tc.Constraint_Name\n"
                    + "            ORDER BY\n"
                    + "            ORDINAL_POSITION\n"
                    + "            FOR XML PATH ('')) j (list)\n"
                    + "        where xtype = 'U'\n"
                    + "          AND name = '"+tableName+"' ";
                rs.executeSql(sql);
                if(rs.next()){
                    //第二列为建表语句
                    result = rs.getString(1);
                }
                break;
            case Constant.ORACLE:
                sql = "select dbms_metadata.get_ddl('TABLE','"+tableName+"') from dual";
                rs.executeSql(sql);
                if(rs.next()){
                    //第二列为建表语句
                    result = rs.getString(1);
                }
                result = result.substring(result.indexOf(".")+1);
                result = "CREATE TABLE "+result;
                break;
            case Constant.POSTGRESQL:
                sql = "SELECT\n" +
                    "      'CREATE TABLE "+tableName+"(' || array_to_string(ARRAY (\n" +
                    "          \n" +
                    "          SELECT SQL1\n" +
                    "          FROM\n" +
                    "           (\n" +
                    "            (--字段信息\n" +
                    "             SELECT array_to_string(ARRAY (\n" +
                    "             SELECT A.attname || ' ' || concat_ws ( '', T.typname, SUBSTRING ( format_type ( A.atttypid, A.atttypmod ) FROM '\\(.*\\)' ) ) || ' ' ||\n" +
                    "                CASE A.attnotnull  WHEN 't' THEN 'NOT NULL' ELSE '' END || ' ' ||\n" +
                    "                CASE WHEN D.adbin IS NOT NULL THEN ' DEFAULT ' || pg_get_expr ( D.adbin, A.attrelid ) ELSE'' END \n" +
                    "             FROM\n" +
                    "              pg_attribute\n" +
                    "              A LEFT JOIN pg_description b ON A.attrelid = b.objoid \n" +
                    "              AND A.attnum = b.objsubid\n" +
                    "              LEFT JOIN pg_attrdef D ON A.attrelid = D.adrelid \n" +
                    "              AND A.attnum = D.adnumW,\n" +
                    "              pg_type T \n" +
                    "             WHERE\n" +
                    "              A.attstattarget =- 1 \n" +
                    "              AND A.attrelid = '"+tableName+"' :: REGCLASS :: OID \n" +
                    "              AND A.attnum > 0 \n" +
                    "              AND NOT A.attisdropped \n" +
                    "              AND A.atttypid = T.OID \n" +
                    "             ORDER BY A.attnum  ),',' || CHR( 10 )  )  SQL1, 1 seri \n" +
                    "            ) \n" +
                    "            UNION\n" +
                    "            (--约束（含主外键）信息\n" +
                    "             SELECT 'CONSTRAINT ' || conname || ' ' || pg_get_constraintdef ( OID ) SQL1, 2 seri \n" +
                    "             FROM pg_constraint T \n" +
                    "           WHERE conrelid = '"+tableName+"' :: REGCLASS :: OID \n" +
                    "             ORDER BY contype DESC \n" +
                    "            ) \n" +
                    "            ORDER BY seri  \n" +
                    "           ) T \n" +
                    "           \n" +
                    "           \n" +
                    "           \n" +
                    "         ),',' || CHR( 10 )  ) || ')' AS ret";
                rs.executeSql(sql);
                if(rs.next()){
                    //第二列为建表语句
                    result = rs.getString(1);
                }
                break;
        }

        return result;
    }

    /**
     * 表主键信息
     * @param miDBExecutor
     * @return
     */
    public static HashMap<String, String> getPriColMap(MiDBExecutor miDBExecutor, String[] pTableNames) {
        HashMap<String, String> priColMap = new HashMap<>();
        String dbType = miDBExecutor.getDbtype();

        String pTableNameStr = "";
        if(pTableNames != null && pTableNames.length > 0 && pTableNames.length < 1000) {//查询太多表的时候，直接查询全部的，不拼接语句
            pTableNameStr = String.join("','", pTableNames);
            pTableNameStr = "('"+pTableNameStr+"')";
        }

        if(SyncConstant.MYSQL.equals(dbType)) {
            String sql = "SELECT table_name as tablename,index_name, GROUP_CONCAT(column_name order by seq_in_index) as columnname,non_unique\n" +
                "FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = (select database()) and index_name='PRIMARY'\n";
            if(StringUtils.isNotBlank(pTableNameStr)) {
                sql = sql + " and TABLE_NAME in " + pTableNameStr + "";
            }
            sql = sql + " GROUP BY table_schema,index_name,table_name";
            miDBExecutor.executeQuery(sql);
            while (miDBExecutor.next()) {
                String tableName = miDBExecutor.getString("tablename");
                String columnName = miDBExecutor.getString("columnname");
                priColMap.put(tableName.toLowerCase(), columnName.toLowerCase());
            }
        } else if(SyncConstant.ORACLE.equals(dbType)) {
            String sql = "SELECT aic.table_name as tablename, aic.index_name,ai.uniqueness,\n" +
                "LISTAGG(aic.column_name, ',') WITHIN GROUP (ORDER BY aic.COLUMN_POSITION) as columnname\n" +
                "FROM USER_IND_COLUMNS aic \n" +
                "left join USER_INDEXES ai on aic.index_name = ai.index_name\n" +
                "left join user_constraints au on aic.index_name = au.index_name\n" +
                "where  "+
                " au.constraint_type = 'P' \n";

            if(StringUtils.isNotBlank(pTableNameStr)) {
                sql = sql + "and aic.table_name in " + pTableNameStr.toUpperCase() +" ";
            }
            sql = sql + " GROUP BY aic.table_name,aic.index_name,ai.uniqueness";

            miDBExecutor.executeQuery(sql);
            while (miDBExecutor.next()) {
                String tableName = miDBExecutor.getString("tablename");
                String columnName = miDBExecutor.getString("columnname");
                priColMap.put(tableName.toLowerCase(), columnName.toLowerCase());
            }

        } else if(SyncConstant.SQLSERVER.equals(dbType)) {//联合主键的问题未解决
            String sql = "SELECT i.name,t.name as tablename,  c.name AS columnname FROM  sys.tables t\n" +
                "LEFT OUTER JOIN  sys.columns c  ON t.object_id = c.object_id \n" +
                "LEFT OUTER JOIN   sys.index_columns ic  ON ic.object_id = c.object_id AND ic.column_id = c.column_id \n" +
                "INNER JOIN  sys.indexes i  ON ic.object_id = i.object_id   AND ic.index_id = i.index_id \n" +
                "WHERE  i.is_primary_key = 1 \n";
            if(StringUtils.isNotBlank(pTableNameStr)) {
                sql = sql + " and t.name in " + pTableNameStr + "";
            }
            sql = sql + " order by i.name,ic.key_ordinal";//注意排序

            miDBExecutor.executeQuery(sql);
            while (miDBExecutor.next()) {
                String tableName = miDBExecutor.getString("tablename");
                String columnName = miDBExecutor.getString("columnname");
                tableName  = tableName.toLowerCase();
                columnName  = columnName.toLowerCase();
                if(priColMap.containsKey(tableName)) {
                    String existCols = priColMap.get(tableName);
                    existCols = existCols + "," + columnName;
                    priColMap.put(tableName, existCols);
                } else {
                    priColMap.put(tableName, columnName);
                }
            }

        } else if(SyncConstant.POSTGRESQL.equals(dbType)) {
            String sql = "SELECT tc.constraint_name,tc.table_name as tablename, string_agg(kcu.column_name, ',' ORDER BY kcu.ordinal_position) as columnname FROM information_schema.table_constraints AS tc \n" +
                "JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name \n" +
                "WHERE constraint_type = 'PRIMARY KEY' ";
            if(StringUtils.isNotBlank(pTableNameStr)) {
                sql = sql + " and tc.table_name in " + pTableNameStr + "";
            }
            sql = sql + " group by tc.table_name,tc.constraint_name ";

            miDBExecutor.executeQuery(sql);
            while (miDBExecutor.next()) {
                String tableName = miDBExecutor.getString("tablename");
                String columnName = miDBExecutor.getString("columnname");
                priColMap.put(tableName.toLowerCase(), columnName.toLowerCase());
            }
        }

        return priColMap;
    }


    /**
     * 时间字段默认值
     * @param timeValue
     * @param tdbType
     * @return
     */
    public static String getDefaultDate(String timeValue, String tdbType) {
        if(timeValue.contains("-") && timeValue.contains(":")) {
            timeValue = "CURRENT_TIMESTAMP";//默认是当前时间，所有的数据库都支持
            return timeValue;
        }
        if(tdbType.equalsIgnoreCase(SyncConstant.SQLSERVER)){
            if(timeValue.contains("-")) {
                timeValue = "CAST(GETDATE() AS DATE)";
            }
            if(timeValue.contains(":")) {
                timeValue = "CAST(GETDATE() AS TIME)";
            }
        }else if(tdbType.equalsIgnoreCase(SyncConstant.ORACLE)){
            /*这种设置方式会报错
            if(timeValue.contains("-")) {
                timeValue = "TO_DATE(SYSDATE, 'YYYY-MM-DD')";
            }
            if(timeValue.contains(":")) {
                timeValue = "TO_DATE(SYSDATE, 'HH24:MI:SS')";
            }*/
            timeValue = "SYSDATE";
        }else if(tdbType.equalsIgnoreCase(Constant.POSTGRESQL)){
            if(timeValue.contains("-")) {
                timeValue = "CURRENT_DATE";
            }
            if(timeValue.contains(":")) {
                timeValue = "CURRENT_TIME";
            }
        }else{

        }
        return timeValue;
    }

    /**
     * 获取所有表名字段名的sql
     * @param dbtype
     * @return
     */
    public static String getAllTableColumns(String dbtype){
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select TABLE_NAME as tablename,COLUMN_NAME as fieldname from  USER_TAB_COLUMNS ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return "select table_name as tablename,column_name as fieldname from information_schema.columns  where  TABLE_SCHEMA = (select database())  ";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return "select table_name as  tablename,column_name as fieldname from information_schema.columns WHERE table_schema=(SELECT current_schema())  order by tablename ";
        } else {
            return " select object_name(id) as tablename,name as fieldname  from syscolumns ";
        }
    }

    /**
     * 获取所有索引的名称
     * @param dbtype
     * @return
     */
    public static String getAllIndex(String dbtype){
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select table_name as tablename,index_name as indexname from user_indexes";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {
            return "SELECT table_name as tablename, index_name as indexname FROM information_schema.statistics WHERE table_schema = (select database())";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            //schemaname不是真实数据库名
            return "SELECT schemaname as dbname, tablename, indexname FROM pg_indexes WHERE schemaname = (SELECT current_schema())";
        } else {
            return "SELECT \n" +
                "    OBJECT_NAME(i.object_id) AS tablename,\n" +
                "    i.name AS indexname\n" +
                "FROM \n" +
                "    sys.indexes AS i\n" +
                "INNER JOIN \n" +
                "    sys.index_columns AS ic ON i.object_id = ic.object_id AND i.index_id = ic.index_id\n" +
                "INNER JOIN \n" +
                "    sys.columns AS c ON i.object_id = c.object_id AND ic.column_id = c.column_id\n" +
                "WHERE \n" +
                "    i.type_desc <> 'HEAP'\n" +
                "ORDER BY \n" +
                "    tablename,indexname,ic.key_ordinal";
        }
    }

    /**
     * 判断字段类型是否是数值类型
     * @param typeName
     * @return
     */
    public static boolean isNumberType(String typeName) {
        if(DBUtil.numberTypes.contains(typeName)) {
            return true;
        }
        return false;
    }

    /**
     * 判断字段类型是否是时间类型
     * @param typeName
     * @return
     */
    public static boolean isDateTimeType(String typeName) {
        if(DBUtil.dataTimeTypes.contains(Util.null2String(typeName).toUpperCase())) {
            return true;
        }
        return false;
    }


    /**
     * 是否是大字段类型
     * @param typeName
     * @return
     */
    public static boolean isLargeClomunType(String typeName) {
        if(DBUtil.largerColumnTypes.contains(Util.null2String(typeName).toUpperCase())) {
            return true;
        }
        return false;
    }


    public static String getBaseServiceName() {
        return baseServiceName;
    }

    public static void setBaseServiceName(String baseServiceName) {
        DBUtil.baseServiceName = baseServiceName;
    }




}

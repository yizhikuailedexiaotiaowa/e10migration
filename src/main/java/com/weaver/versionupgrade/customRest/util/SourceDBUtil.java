package com.weaver.versionupgrade.customRest.util;


import com.alibaba.druid.pool.DruidDataSource;


import com.alibaba.druid.support.ibatis.DruidDataSourceFactory;
import com.weaver.versionupgrade.MiDBExecutor;
import com.weaver.versionupgrade.util.Util;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;


public class SourceDBUtil extends MiDBExecutor {


    private static Logger logger = LoggerFactory.getLogger(SourceDBUtil.class);

    protected static volatile   ConcurrentHashMap<String, DruidDataSource> poolstem = new ConcurrentHashMap();


    /**
     * 构造函数
     *
     */
    public SourceDBUtil(){
        super("monomer","e9",Constant.DB_SOURCE);
    }



    @Override
    public void writeLog(String str){
        logger.error(""+str);
    }


    public void writeLog(String str,Object o){
        logger.error("" + str, o);
    }


    public  static   Connection getStaticSourceConnection() {
        SourceDBUtil rs1=new SourceDBUtil();
       return  rs1.getSourceConnection("monomer");
    }

    /**
     * 执行查询sql
     * @param sql
     * @param args
     * @return
     */
    @Override
    public boolean executeQuery(String sql, Object... args){                                         //只能执行select语句
        //缓存
        if( ruleId>0 && args.length>0 && sql.indexOf("*")<0){
            String key=getCacheKeys(args);
            if(key!=null){
                boolean isexists= getCahcheByKey(ruleId,sql,key);
                if(isexists){//如果获取到的话，直接返回
                    return true;
                }else{
                }
            }
        }

        long startTime=System.currentTimeMillis();
        boolean result= false;
        try {
            result = executeSql(sql,true,args);


            //如果规则大于0 且有参数 && sql查询执行成功就缓存  && sql语句中不允许含有*
            if(ruleId>0 && args.length>0 && result && sql.indexOf("*")<0){
                String key=getCacheKeys(args);
                if(isBlank(key)){
                    return result;
                }
                WeaverCacheObject weaverCacheObject=new WeaverCacheObject();
                weaverCacheObject.setArray((Vector) array.clone());
                weaverCacheObject.setColumnName(columnName);
                weaverCacheObject.setColumnType(columnType);
                WeaverCacheInfo.setCahcheByKey(ruleId,sql,key,weaverCacheObject);
            }
        } catch (Exception e) {
            if(isPritErrorStack){
                e.printStackTrace();
            }
            writeLog(e.getMessage());
        }
        return result;
    }

    /**
     * 批量执行
     * @param sqlList
     * @return
     */
    @Override
    public boolean executeUpdateBatch(ArrayList<String> sqlList){
        boolean success = false;
        if(sqlList==null || sqlList.size() == 0){
            return success;
        }
        String result ="";

        conn = getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if(conn!=null) {
            try {
                statement = conn.createStatement();
                for(String sql : sqlList) {
                    List<String> sqllist = TablePaginationOperation.paginationPrepareStatementSql(sql, dbtype);
                    for(String tempsql:sqllist){
                        statement.addBatch(tempsql);
                    }
                }
                statement.executeBatch();
                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                e.printStackTrace();
                writeLog(e.getMessage());
                for(String tempsql:sqlList){
                    writeLog("errorsql:"+tempsql);
                    this.executeUpdate2("insert into transerrorinfo(ruleid,message,updatetime) values (?,?,?)"
                            ,9999121, "错误sql:"+tempsql+"------错误信息："+e.getMessage(),Util.getCurrentTime());
                }
            } finally {
                if(conn!=null) {
                    try {conn.close();} catch (Exception e) {}
                }
                if(statement!=null) {
                    try {statement.close();} catch (Exception e) {}
                }
                if(resultset!=null) {
                    try {resultset.close();} catch (Exception e) {}
                }
            }
        }
        return success;
    }

    /**
     * 执行sql语句
     * @param s
     * @return
     */
    @Override
    public boolean execute(String s) {
        List<String>  splitsqls=new ArrayList<>();
        try {
            splitsqls = TablePaginationOperation.paginationSql(s, dbtype);
        } catch (Exception e) {
            if(isPritErrorStack){
                e.printStackTrace();
            }
            return false;
        }
        boolean success = false;
        conn = this.getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if(conn!=null) {
            try {
                statement = conn.createStatement();

                //执行全部的sql
                for(String sql:splitsqls){
                    statement.execute(sql);
                    resultset = statement.getResultSet();

                    if(resultset != null) {
                        parseResultSet(resultset);
                        resultset.close();
                    }
                }

                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if(isPritErrorStack){
                    e.printStackTrace();
                }
                writeLog("ERROR:"+s);
                writeLog(e.getMessage());
                this.executeUpdate2("insert into transerrorinfo(ruleid,message,updatetime) values (?,?,?)"
                        ,9999121, "错误sql:"+s+"------错误信息："+e.getMessage(), Util.getCurrentTime());
            } finally {
                if(conn!=null) {
                    try {conn.close();} catch (Exception e) {}
                }
                if(statement!=null) {
                    try {statement.close();} catch (Exception e) {}
                }
                if(resultset!=null) {
                    try {resultset.close();} catch (Exception e) {}
                }
            }

        }

        return success;
    }




    /**
     * 获取缓存的行数据对象
     * @return
     */
    public WeaverCacheObject getWeaverCacheObject(){
        if(!array.isEmpty() && curpos >= 0 && curpos < array.size())
        {
            WeaverCacheObject  cacheObject=new WeaverCacheObject();
            cacheObject.setColumnType(columnType);
            cacheObject.setColumnName(columnName);
            Object as[] =(Object[]) array.get(curpos);
            Vector rowvalue= new Vector();
            rowvalue.add(as);
            cacheObject.setArray(rowvalue);
            return cacheObject;
        }
        return null;
    }


    /**
     * 在缓存中查询,查询成功返回true,
     * @param sql
     * @param args
     * @return
     */
    public boolean executeQueryOnlyInCache(String sql,Object... args) {
        //缓存
        if (ruleId > 0 && args.length > 0 && sql.indexOf("*") < 0) {
            String key = getCacheKeys(args);
            if (key != null) {
                boolean isexists = getCahcheByKey(ruleId, sql, key);
                return isexists;
            }
        }
        return false;
    }


    /**
     * @param selectDataSql  查询数据的sql
     * @param ruleId  规则id
     * @param cachesql  缓存sql   后续执行这个sql查询的话会先从缓存中取
     * @param paramsClumnNames   缓存sql的参数列
     */
    public synchronized void  initSqlCacheData(String selectDataSql,int ruleId,String cachesql,String[] paramsClumnNames){
        //如果已经缓存过，直接return
        if(WeaverCacheInfo.checkSelectDataSqlIsInit(ruleId,cachesql,selectDataSql)){
            return ;
        }
        SourceDBUtil rs=new SourceDBUtil();
        rs.executeQuery(selectDataSql);
        while(rs.next()){
            WeaverCacheObject weaverCacheObject=rs.getWeaverCacheObject();//获取行数据缓存对象
            if(weaverCacheObject!=null){
                List<Object>  params=new ArrayList<>();
                for(String clumnName:paramsClumnNames){
                    String clumnvalue= rs.getString(clumnName);
                    params.add(clumnvalue);
                }
                String key =getCacheKeys(params);//缓存的key
                WeaverCacheInfo.setCahcheByKey(ruleId,cachesql,key,weaverCacheObject);
            }
        }
    }

    public static final String DATA_SEP = ",_,";

    /**
     * 查询transidmap专用
     * @param
     * @param
     * @param
     * @return
     */
    public void executeQueryTransSql(HashMap<String, String> sqlMap){
        boolean success = false;
        HashMap<String, String> map = new HashMap<>();
        if(sqlMap == null || sqlMap.size() == 0){
            return;
        }
        String result ="";
        conn = getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        PreparedStatement preparedStatement=null;
        if(conn!=null) {
            try {
                final StringBuffer querySql = new StringBuffer();
                int totalSize = sqlMap.size();
                int i = 0;
                Iterator iterator = sqlMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, String> entry = (Map.Entry<String, String>) iterator.next();
                    String key = entry.getKey();
                    String sql = entry.getValue();
                    if(sql != null) {
                        sql = sql.trim();
                    }

//                    String tableName = TablePaginationOperation.getPaginationTable("transidmap", key, null);
//                    sql = sql.replaceAll("(?i)[ ]*?transidmap[ ]*?", " "+tableName+" ");
                    List<String> sqllist = TablePaginationOperation.paginationPrepareStatementSql(sql, dbtype);//解析sql
                    if(sqllist.size()>=1){
                        sql=sqllist.get(0);
                        if(i == totalSize - 1) {
                            querySql.append(sql);
                        } else {
                            querySql.append(sql);
                            //524288
                            if(querySql.length() > 500000) {
                                if(querySql.length() > 0) {
                                    preparedStatement=conn.prepareStatement(querySql.toString());
                                    List<Object> paramList = new ArrayList();
                                    preparedStatement.executeQuery();
                                    resultset=preparedStatement.getResultSet();
                                    if(resultset != null) {
                                        parseDataResultSet(resultset);
                                        resultset.close();
                                    }
                                    preparedStatement.close();
                                    querySql.setLength(0);
                                }
                            } else {
                                querySql.append(" union all ");
                            }

                        }
                    }
                    i++;
                }
                //导致回表 先注释掉
/*                if(querySql.length() > 0) {
                    querySql.append(" order by id desc");
                }*/
                //System.out.println("===querySql.toString():"+querySql.toString());
                if(querySql.length() > 0) {
                    preparedStatement=conn.prepareStatement(querySql.toString());
                    List<Object> paramList = new ArrayList();
                    preparedStatement.executeQuery();
                    resultset=preparedStatement.getResultSet();
                    if(resultset != null) {
                        parseDataResultSet(resultset);
                        resultset.close();
                    }
                    preparedStatement.close();
                }

                success = true;
            } catch (Exception e) {
                success = false;
                if(isPritErrorStack){
                    e.printStackTrace();
                }
                writeLog(e.getMessage());
            } finally {
                if(conn!=null) {
                    try {conn.close();} catch (Exception e) {}
                }
                if(preparedStatement!=null){
                    try {preparedStatement.close();} catch (Exception e) {}
                }
                if(statement!=null) {
                    try {statement.close();} catch (Exception e) {}
                }
                if(resultset!=null) {
                    try {resultset.close();} catch (Exception e) {}
                }
            }
        }
    }







    /**
     * 执行存储过程，采用默认的链接池
     *
     * @param s    存储过程名称
     * @param s1   存储过程参数，多个参数中间用Util.getSeparator()分开
     *
     * @return boolean   如果执行成功，返回true，否则返回false
     */
    public boolean executeProc(String s, String s1) {
        return executeProc(s, s1, null);
    }

    /**
     * 执行存储过程，采用指定的链接池    青春版  功能不完善
     *
     * @param s 存储过程名称
     * @param s1 存储过程参数，多个参数中间用Util.getSeparator()分开
     * @param poolname 指定的链接池名称，如果为null 则使用默认的链接池
     *
     * @return boolean   如果执行成功，返回true，否则返回false
     */
    public boolean executeProc(String s, String s1, String poolname) {
        parseArgument(s1);

        return executeProc(s, Lists.newArrayList(args), poolname);
    }

    public boolean executeProc(String s, List<String> arguments) {

        return executeProc(s, arguments, null);
    }

    public boolean executeProc(String s, List<String> arguments, String poolname) {
        final String separator = "2";
        boolean bSuccess;
        s = s.trim();
        String sqlTmp = s;
        long l = 0L;
        long l2 = 0L;
        StringBuilder sb = new StringBuilder();
//        ResultMap result = null;
        conn = getSourceConnection();
        dbtype = this.getDbtype();
//        String orgindbtype=conn.getOrgindbtype();
        boolean oldautocommit = true;//神通数据库必须把autocommit设置为false才能返回游标
        if (arguments == null) {
            arguments = Lists.newArrayList();
        }
        int i = arguments.size();
        for (int j = 0; j < i; j++) {
            sb.append(arguments.get(j));
            if (j < i - 1) {
                sb.append(separator);
            }
        }
        String arg_str = sb.toString();
        sb.delete(0, sb.length());

        try {
            if (dbtype.equals("st") || dbtype.equals("jc") || dbtype.equals("gs")
                || dbtype.equalsIgnoreCase("postgresql")) {
                oldautocommit = conn.getAutoCommit();
                conn.setAutoCommit(false);
            }
            sb.append("{call ").append(s).append("(");
            if (dbtype.equals("db2")) {
                for (int j = 0; j < i; j++) {
                    if (j == 0) sb.append("?");
                    else sb.append(",?");
                }
                sb.append(")}").toString();
            } else {
                for (int j = 0; j < i + 1; j++)
                    sb.append("?,");
                if (dbtype.equals("oracle") || dbtype.equalsIgnoreCase("postgresql"))
                    sb.append("?,");
                sb.append("?)}").toString();
            }
            String s2 = sb.toString();
            sb.delete(0, sb.length());
            writeLog(s2);
            CallableStatement callablestatement = conn.prepareCall(s2);
            for (int k = 0; k < i; k++) {
                //if(databaseType.equals("sqlserver")) args[k] = (new String(args[k].getBytes("UTF-8"),"ISO8859_1")) ;
                callablestatement.setString(k + 1, arguments.get(k));
            }

            if (!dbtype.equals("db2")) {
                if (dbtype.equals("jc") || dbtype.equalsIgnoreCase("gs") || dbtype.equalsIgnoreCase(
                    "postgresql")) {
                    callablestatement.registerOutParameter(i + 1, Types.INTEGER);
                } else {
                    callablestatement.registerOutParameter(i + 1, Types.NUMERIC);
                }

                callablestatement.registerOutParameter(i + 2, Types.VARCHAR);

                if (dbtype.equals("jc") || dbtype.equals("gs") || dbtype.equalsIgnoreCase(
                    "postgresql")) {
                    callablestatement.registerOutParameter(i + 3, Types.REF_CURSOR);
                } else if (dbtype.equals("oracle"))
                    callablestatement.registerOutParameter(i + 3, -10);
            }
            Date beginTime = new Date();
            // long l1 = System.currentTimeMillis();

            callablestatement.execute();
            // long l3 = System.currentTimeMillis();
            Date endTime = new Date();
            try {
                ResultSet resultset;
                if (dbtype.equals("oracle") || dbtype.equals("postgresql")) {
                    try {
                        resultset = (ResultSet) callablestatement.getObject(i + 3);
                        //增加flag,msg输出参数支持,xiaofeng
                        try {
                            flag = callablestatement.getInt(i + 1);
                        } catch (SQLException e) {
                            //ignore
                        }
                        try {
                            msg = callablestatement.getString(i + 2);
                        } catch (SQLException e) {
                            //ignore
                        }
                    } catch (SQLException cursorinvalidate) {
                        resultset = null;
                    }
                } else {
                    while ((resultset = callablestatement.getResultSet()) == null) {
                        if (!callablestatement.getMoreResults()
                            && callablestatement.getUpdateCount() == -1)
                            break;
                    }
                }

                if (resultset != null) {
                    parseResultSet(resultset);
                    try {
                        //DeleteSqlCountMonitorUtil.writeLog(getCounts(),s+"("+arg_str+")");
                    } catch (Exception e) {
                    }
                    resultset.close();
                } else {
                    //DeleteSqlCountMonitorUtil.writeLog(-1,s+"("+arg_str+")");
                }
                //增加flag,msg输出参数支持,xiaofeng
                try {
                    flag = callablestatement.getInt(i + 1);
                } catch (SQLException e) {
                    //ignore
                }
                try {
                    msg = callablestatement.getString(i + 2);
                } catch (SQLException e) {
                    //ignore
                }
            } catch (SQLException sqlexception) {
                writeLog(sqlexception.getMessage());
            }

            callablestatement.close();
            bSuccess = true;
        } catch (Exception exception) {
            bSuccess = false;
            if(isPritErrorStack){
                exception.printStackTrace();
            }
            writeLog(s + " " + arg_str);
            writeLog(exception.toString());
        } finally {

            try {
                if (dbtype.equals("st") || dbtype.equals("jc") || dbtype.equals("gs")
                    || dbtype.equalsIgnoreCase("postgresql")) {
                    conn.setAutoCommit(oldautocommit);
                }
                conn.commit();
                conn.close();
            } catch (Exception exception2) {
            }
        }
        return bSuccess;
    }

    private String args[];
    private char separator = 2 ;

    private void parseArgument(String s) {
        boolean needTransCode = false;
//        if (poolname == null || GCONST.getServerName().equals(poolname) || EncodingUtils.containsNativeDB(poolname) || "".equals(poolname)) {
//            if (EncodingUtils.encodingStrategy == EncodingUtils.UNICODEENCODING_STRATEGY) {
//                needTransCode = true;
//            }
//        }
        int i = 0;
        int j = 0;
        if (s.trim().equals("")) {
            args = new String[0];
            return;
        }
        for (j = 0; j < s.length(); j++)
            if (s.charAt(j) == separator)
                i++;

        args = new String[i + 1];
        j = 0;
        i = 0;
        String databaseType = dbtype ;
        while ((j = s.indexOf(separator)) != -1) {
            // fix by caoyun 支持Mysql数字类型传入null
            String a = s.substring(0, j);
            if (Constant.MYSQL.equals(databaseType)) {
                if (org.apache.commons.lang3.StringUtils.isEmpty(a)) {
                    a = null;
                }
            }
            args[i++] = a;
            s = s.substring(j + 1);
        }
        if (Constant.MYSQL.equals(databaseType)) {
            if (StringUtils.isEmpty(s)) {
                s = null;
            }
        }
        args[i] = s;
    }




}

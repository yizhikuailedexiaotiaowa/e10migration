package com.weaver.versionupgrade.customRest.util;


import com.alibaba.druid.pool.DruidDataSource;
import com.weaver.versionupgrade.customRest.db.DBUtil;
import com.weaver.versionupgrade.util.PropertiesUtil;
import com.weaver.versionupgrade.util.Util;
import org.apache.commons.lang3.StringUtils;


import java.text.SimpleDateFormat;

import java.util.Date;



/**
 * @author cx
 * @date 2022/4/25
 **/
public class DruidDataSourceFactory {

    private static final long serialVersionUID = 1L;
//    private static final Logger LOG = LoggerFactory.getLogger(DruidDataSourceFactory.class);


    private String username;
    private String password;
    private String drivername;
    private String dbURL;
    private int maxConn;
    private static DruidDataSource dataSource;
    private static DruidDataSourceFactory druidDataSourceFactory;




    private DruidDataSourceFactory() {

    }



    /**
     * 单例
     * @return
     */
    public synchronized static DruidDataSourceFactory getInstance() {
        if(druidDataSourceFactory == null) {
            druidDataSourceFactory = new DruidDataSourceFactory();
        }
        return druidDataSourceFactory;
    }


    //Druid连接池的加入
    public DruidDataSource getDataSource(){
        dataSource = new DruidDataSource();
        dataSource.setDriverClassName(drivername);
        if(dbURL.toLowerCase().indexOf(":sqlserver")>-1){
            dbURL = dbURL +";SelectMethod=cursor";
        }
        dataSource.setUrl(dbURL);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setInitialSize(10);   //初始化时建立物理连接的个数。初始化发生在显示调用init方法，或者第一次getConnection时
        dataSource.setMinIdle(10);  //最小连接池数量
        dataSource.setMaxActive(maxConn);  //最大连接池数量
        dataSource.setMaxWait(-1); //获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
        dataSource.setTimeBetweenEvictionRunsMillis(1000 * 60);  //有两个含义：1) Destroy线程会检测连接的间隔时间2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明
        // 设置最大空闲时间为15分钟
//        dataSource.setMaxWait(15 * 60 * 1000);
        dataSource.setConnectionErrorRetryAttempts(10);//失败重连次数

        //minEvictableIdleTimeMillis连接空闲时间大于该值并且池中空闲连接大于minIdle则关闭该连接
        //maxEvictableIdleTimeMillis连接空闲时间大于该值，不管minIdle都关闭该连接
        dataSource.setMaxEvictableIdleTimeMillis(1000 * 60 * 60 * 10);  //<!-- 配置一个连接在池中最大生存的时间，单位是毫秒 -->
        dataSource.setMinEvictableIdleTimeMillis(1000 * 60 * 30);  //<!-- 配置一个连接在池中最小生存的时间，单位是毫秒 -->
        dataSource.setTestWhileIdle(true);  // <!-- 这里建议配置为TRUE，防止取到的连接不可用 -->
        dataSource.setQueryTimeout(0);
        dataSource.setTestOnBorrow(true);
        dataSource.setTestOnReturn(false);

//
//        try {
//            //默认关闭sql打印日志开关
//            String isopenSqlLog= Util.null2String(getLogSqlProp("isopen"),"0");
//
//            if(isopenSqlLog.equals("1")){
//                //日志输出间隔
//                int  sqlLogPrintSplitTime=Util.getIntValue(getLogSqlProp("sqlLogPrintSplitTime"),10)*1000;
//                LOG.info("日志输出间隔："+sqlLogPrintSplitTime);
//                dataSource.setTimeBetweenLogStatsMillis(sqlLogPrintSplitTime);
//
//                String currentDate=getCurrentDate();
//                // 如果已經有了一個Logger實例返回現有的
//                String logname="slowSql_"+currentDate+"_log";
//                MyStatLogger statLogger=new MyStatLogger();
//                statLogger.setLoggerName(logname);
//
//                dataSource.setStatLogger(statLogger);
//                dataSource.setFilters("stat,log4j");
//                List<Filter> proxyFileters=new ArrayList<>();
//
//
//                Log4jFilter log4jFilter=new Log4jFilter();
//                log4jFilter.setResultSetLogEnabled(false);
//                log4jFilter.setStatementExecutableSqlLogEnable(true);
////
//                Logger logger1=getLogger();
//                log4jFilter.setDataSourceLogger(logger1);
//                proxyFileters.add(log4jFilter);
//
//
//                StatFilter statFilter=new StatFilter();
//                statFilter.setMergeSql(true);
//                statFilter.setSlowSqlMillis(1000);
//                statFilter.setLogSlowSql(true);
//                statFilter.setDbType(DBUtil.getDBtypeByUrl(dbURL).get("dbtype"));
//                proxyFileters.add(statFilter);
//
////            logger1.info("1321321");
//                dataSource.setProxyFilters(proxyFileters);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String  dbtype=DBUtil.getDBtypeByUrl(dbURL).get("dbtype");
        if(dbtype.equals(Constant.SQLSERVER) || dbtype.equals(Constant.MYSQL)) {
            dataSource.setValidationQuery("select 1");
        } else if(dbtype.equals(Constant.ORACLE)){
            dataSource.setValidationQuery("SELECT 'x'FROM DUAL");
        } else if(dbtype.equals(Constant.POSTGRESQL)){
            dataSource.setValidationQuery("SELECT 'x'");
        }

        return dataSource;
    }


    /**
     * 获取sql日志配置
     * @param propname
     * @return
     */
    public static String getLogSqlProp(String propname){
        //flink自定义参数设置
        PropertiesUtil propertiesUtil = new PropertiesUtil();


        String evnPath = getConfigFile("sqllog.properties");
        propertiesUtil.load(evnPath);
        if(propertiesUtil.getKeys().contains(propname)) {
            return Util.null2String(propertiesUtil.get(propname));
        }
        return "";
    }


    /**
     * 配置文件路径
     */
    public static String getConfigFile(String fileName) {
        String evnPath = ConfigUtil.getConfigDir() + fileName;

        return evnPath;
    }



    /**
     * 获取当前日期
     *
     * @return
     */
    public static String getCurrentDate() {
        Date date = new Date();// 获得当前的时间戳
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");// 定义格式
        String currentdate = dateFormat.format(date);// 格式化时间
        return currentdate;
    }

    /**
     * 获取flink根路径
     * @return
     */
    public static String  getFlinkRootPath(){
        String rootpath="";
        rootpath = ConfigUtil.getConfigDir();
        if (StringUtils.isNotBlank(rootpath)){
            rootpath=rootpath.replaceAll("\\\\","/" );
            if(rootpath.indexOf("/conf")>0){
                rootpath=  rootpath.replace("/conf","" );
            }
        }
//        rootpath=rootpath.replaceAll("/", File.separatorChar+"" );
        return rootpath;
    }


    /**
     * 参数设置
     * @return
     */
    public static JDBCInputFormatBuilder buildJDBCInputFormat() {
        return new JDBCInputFormatBuilder();
    }

    public static class JDBCInputFormatBuilder {
        private DruidDataSourceFactory format;

        public JDBCInputFormatBuilder() {
            this.format = new DruidDataSourceFactory();
        }

        public JDBCInputFormatBuilder setUsername(String username) {
            format.username = username;
            return this;
        }


        public JDBCInputFormatBuilder setPassword(String password) {
            format.password = password;
            return this;
        }

        public JDBCInputFormatBuilder setDrivername(String drivername) {
            format.drivername = drivername;
            return this;
        }

        public JDBCInputFormatBuilder setDBUrl(String dbURL) {
            format.dbURL = dbURL;
            return this;
        }

        public JDBCInputFormatBuilder setMaxConn(int maxConn) {
            format.maxConn = maxConn;
            return this;
        }


        public DruidDataSourceFactory finish() {
            if (format.username == null) {
//                LOG.info("Username was not supplied separately.");
            }
            if (format.password == null) {
//                LOG.info("Password was not supplied separately.");
            }
            if (format.dbURL == null) {
                throw new IllegalArgumentException("No database URL supplied");
            }
/*            if (format.queryTemplate == null) {
                throw new IllegalArgumentException("No query supplied");
            }*/
            if (format.drivername == null) {
                throw new IllegalArgumentException("No driver supplied");
            }
            /*if (format.rowTypeInfo == null) {
                throw new IllegalArgumentException("No " + RowTypeInfo.class.getSimpleName() + " supplied");
            }
            if (format.parameterValues == null) {
                LOG.debug("No input splitting configured (data will be read with parallelism 1).");
            }*/
            return format;
        }

    }
}

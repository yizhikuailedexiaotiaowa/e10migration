package com.weaver.versionupgrade.customRest.util;

import com.weaver.versionupgrade.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;


/**
 * @author cx
 * @date 2022/7/19
 **/
public class WeaverCacheInfo {
    //规则与服务名对应关系
    private static ConcurrentHashMap<Integer, String> servicesMap = new ConcurrentHashMap();
    //多语言标签缓存，减少重复数据插入
    private static Set<String> lanIndexSet = ConcurrentHashMap.newKeySet();
    //单体部署et库对应的服务
    private static Set<String> etServiceSet = ConcurrentHashMap.newKeySet();
    //组合部署服务
    private static ConcurrentHashMap<String, String> coGroupServiceMap = new ConcurrentHashMap();




    private static final Logger LOG = LoggerFactory.getLogger(WeaverCacheInfo.class);
    /**
     *每条sql缓存集合 最大值，超过将清空
     */
    public static int cacheMapSize=30000;


    public static int maxBatchCacheSize=50000;



    /**
     * 规则缓存，<ruleid,<sql字符串,<查询的条件字符串,WeaverCacheObject>>
     */
    private static ConcurrentHashMap<Integer,ConcurrentHashMap<String,ConcurrentHashMap<String,WeaverCacheObject>>>  sqlValueMap=new ConcurrentHashMap<>();



    /**
     * 执行缓存数据的sql 记录
     * ruleid,  sql字符串：带？的     List<String>  初始化数据的集合的sql
     */
    private static ConcurrentHashMap<Integer,ConcurrentHashMap<String,List<String>>> initDataSqlInfo=new ConcurrentHashMap();


    /**
     * 规则临时字符串数据缓存
     */
    private static ConcurrentHashMap<Integer,ConcurrentHashMap<String,Object>>  ruleDataCacheString=new ConcurrentHashMap<>();





    /**
     * 人员对应E10id
     */
    public static ConcurrentHashMap<String,String> hrmresourceNewidMap=new ConcurrentHashMap<>();


    /**
     * 分部对应E10id
     */
    public static ConcurrentHashMap<String,String> subcompanyNewidMap=new ConcurrentHashMap<>();

    /**
     * 部门对应E10id
     */
    public static ConcurrentHashMap<String,String> departmentNewidMap=new ConcurrentHashMap<>();

    /**
     * 虚拟分部对应E10id
     */
    public static ConcurrentHashMap<String,String> subcompanyVirtualNewidMap=new ConcurrentHashMap<>();

    /**
     * 虚拟部门对应E10id
     */
    public static ConcurrentHashMap<String,String> departmentVirtualNewidMap=new ConcurrentHashMap<>();

    /**
     * 岗位对应E10id
     */
    public static ConcurrentHashMap<String,String> jobtitleNewidMap=new ConcurrentHashMap<>();

    /**
     * 角色对应E10id
     */
    public static ConcurrentHashMap<String,String> hrmroleNewidMap=new ConcurrentHashMap<>();


    /**
     * 流程路径对应E10id
     */
    public static ConcurrentHashMap<String,String> wrokflowbaseNewidMap=new ConcurrentHashMap<>();


    /**
     * 流程节点对应E10id
     */
    public static ConcurrentHashMap<String,String> wrokflownodebaseNewidMap=new ConcurrentHashMap<>();


    /**
     * 是否初始化次账号
     */
    public static boolean isInitSubAccount = false;


    /**
     * E9次账号id
     */
    public static HashSet<String> e9SubAccount=new HashSet<>();




    /**
     * 设置缓存数据的查询sql信息
     */
    public static synchronized void setInitDataSqlInfo(int ruleid,String sql,String selectDataSql){
        ConcurrentHashMap<String,List<String>>  dataSqlMap=initDataSqlInfo.get(ruleid);
        if(dataSqlMap==null){
            dataSqlMap=new ConcurrentHashMap<>();
            initDataSqlInfo.put(ruleid,dataSqlMap);
        }

       List<String> selectDataSqlList = dataSqlMap.get(sql);
        if(selectDataSqlList==null){
            selectDataSqlList=new ArrayList<>();
            dataSqlMap.put(sql,selectDataSqlList);
        }

        if(!selectDataSqlList.contains(selectDataSql)){
            selectDataSqlList.add(selectDataSql);
        }

    }


    /**
     *
     * @param ruleid  规则id
     * @param sql   查询sql 待？
     * @param key  参数生成的key  可以使用getCacheKeys  来获取
     * @param weaverCacheObject  缓存的结果对象
     */
    public static void setCahcheByKey(int ruleid,String sql,String key,WeaverCacheObject weaverCacheObject){
        setCahcheByKey( ruleid, sql, key, weaverCacheObject, false);
    }

    /**
     * 缓存对象
     * @param ruleid
     * @param sql
     * @param key
     */
    public static void setCahcheByKey(int ruleid,String sql,String key,WeaverCacheObject weaverCacheObject,boolean isBatchCache){
        if(StringUtils.isBlank(key)){
            return ;
        }
        ConcurrentHashMap<String, ConcurrentHashMap<String, WeaverCacheObject>> rulesqlmap= WeaverCacheInfo.getSqlValueMap().get(ruleid);
        if(rulesqlmap==null){
            //如果没有缓存过rule对象，就新建一个
            rulesqlmap=new ConcurrentHashMap<String, ConcurrentHashMap<String, WeaverCacheObject>>();
             WeaverCacheInfo.getSqlValueMap().put(ruleid,rulesqlmap);
        }

        ConcurrentHashMap<String, WeaverCacheObject>  valueMap=rulesqlmap.get(sql);
        if(valueMap==null){
            //如果没有缓存过sql对象，就新建一个
            valueMap=new  ConcurrentHashMap<String, WeaverCacheObject>();
            rulesqlmap.put(sql,valueMap);
        }else{
            //超过最大集合数量将清空
            if(valueMap.size()>WeaverCacheInfo.cacheMapSize){
                if(!isBatchCache){//如果不是批量缓存的话
                    valueMap.clear();
                }else{
                    if(valueMap.size()>WeaverCacheInfo.maxBatchCacheSize){//批量缓存数据如果大于50000直接清理
                        initDataSqlInfo.get(ruleid).get(sql).clear();//
                    }
                }
            }
        }

        if(!valueMap.containsKey(key)){
            valueMap.put(key,weaverCacheObject);
        }
    }


    /**
     * 根据ruleid来移除缓存
     * @param ruleid
     */
    public static void clearCacheByRuleId(int ruleid){
        if(sqlValueMap.containsKey(ruleid)){
            sqlValueMap.remove(ruleid);
        }

        if(initDataSqlInfo.containsKey(ruleid)){
            initDataSqlInfo.remove(ruleid);
        }
    }


    /**
     * 判断查询数据的sql是否缓存过
     * @param ruleId
     * @param sql
     * @param selectDataSql
     * @return
     */
    public static  boolean checkSelectDataSqlIsInit(int ruleId,String sql,String selectDataSql){
        ConcurrentHashMap<String,List<String>>  dataSqlMap=WeaverCacheInfo.getInitDataSqlInfo().get(ruleId);
        if(dataSqlMap==null){
            return false;
        }

        List<String> selectDataSqlList = dataSqlMap.get(sql);
        if(selectDataSqlList==null){
            return false;
        }

        if(!selectDataSqlList.contains(selectDataSql)){
            return false;
        }
        return true;
    }

    /**
     * 根据参数获取key
     * @param args
     * @return
     */
    public static String getCacheKeys(Object... args){
        List<Object> paramList = new ArrayList();
        for (Object param : args) {
            if ((param instanceof Vector)) {
                paramList.addAll(Collections.list(((Vector) param).elements()));
                break;
            } else if ((param instanceof List)) {
                paramList.addAll((List) param);
            } else {//动态参数
                if (param == null) {
                    paramList.add(null);
                } else {
                    paramList.add(param);
                }
            }
        }
        if(paramList.size()<=0){
            return null;
        }

        return paramList.toString();
    }

    /**
     * 获取打缓存的数据对象
     * @return
     */
    public static ConcurrentHashMap<Integer, ConcurrentHashMap<String, ConcurrentHashMap<String, WeaverCacheObject>>> getSqlValueMap() {
        return sqlValueMap;
    }


    /**
     * 获取微服务集合
     * @return
     */
    public static ConcurrentHashMap<Integer, String> getServicesMap() {
        return servicesMap;
    }

    public static void setServicesMap(ConcurrentHashMap<Integer, String> servicesMap) {
        WeaverCacheInfo.servicesMap = servicesMap;
    }

    public static Set<String> getLanIndexSet() {
        return lanIndexSet;
    }

    public static void setLanIndexSet(Set<String> lanIndexSet) {
        WeaverCacheInfo.lanIndexSet = lanIndexSet;
    }

    public static ConcurrentHashMap<Integer, ConcurrentHashMap<String, List<String>>> getInitDataSqlInfo() {
        return initDataSqlInfo;
    }


    /**
     E9表数量
     */
    private static ConcurrentHashMap<String,Integer>  e9TableCount=new ConcurrentHashMap<>();

    protected static ReentrantLock lockPool = new ReentrantLock();

    /**
     * 获取表数据数量
     * @return
     */
    public static  int getE9TableDataNum(String tablename){
        if(StringUtils.isBlank(tablename)){
            return 0;
        }
        tablename=tablename.toLowerCase().trim();
        if(e9TableCount.containsKey(tablename)){
            return  e9TableCount.get(tablename);
        }

        assert ! lockPool.isHeldByCurrentThread();
        lockPool.lock();
        try {
            SourceDBUtil rs=new SourceDBUtil();
            rs.executeQuery(" select count(*) as num from  "+tablename);
            if(rs.next()){
                int count=Util.getIntValue(Util.null2String(rs.getString("num")),0);
                e9TableCount.put(tablename,count);
            }else{
                e9TableCount.put(tablename,0);
            }
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            lockPool.unlock();
        }
        return Util.getIntValue(Util.null2String(e9TableCount.get(tablename)),0);
    }





    /**
     * 缓存人员E10  Id的方法
     */
    public static synchronized void  initHrmResourceNewIdCache(){
        if(hrmresourceNewidMap.size()>=getE9TableDataNum("hrmresource")){
            return;
        }

        ConcurrentHashMap<String,String> newidmaptemp=new ConcurrentHashMap<String,String>();
        SourceDBUtil rs=new SourceDBUtil();
        rs.executeQuery(" select * from rulequeue where ruleid in(select distinct(ruleid) from transidmap where tablename='employee'and oldtable='hrmresource') and status in(0,1,3) ");
        if (rs.next()){//正在迁移中就不缓存
            LOG.info("人员规则："+rs.getString("ruleid")+"-----未执行，暂不缓存：employee");
        }else{
            rs.executeQuery(" select * from rulequeue where ruleid in(select distinct(ruleid) from transidmap where tablename='employee_manager') and status in(0,1,3) ");
            if(rs.next()){//如果迁移规则不结束 就不缓存
                LOG.info("分权管理员规则："+rs.getString("ruleid")+"-----未执行，暂不缓存：employee_manager");
            }else{
                rs.executeQuery(" select newcontent,oldcontent from transidmap  where tablename = 'employee' ");
                while(rs.next()){
                    String newcontent=Util.null2String(rs.getString("newcontent")).trim();
                    String oldcontent=Util.null2String(rs.getString("oldcontent")).trim();
                    if(StringUtils.isNotBlank(oldcontent) && StringUtils.isNotBlank(newcontent)){
                        newidmaptemp.put(oldcontent,newcontent);
                    }
                }

                rs.executeQuery(" select newcontent,oldcontent from transidmap  where tablename = 'employee_manager' ");
                while(rs.next()){
                    String newcontent=Util.null2String(rs.getString("newcontent")).trim();
                    String oldcontent=Util.null2String(rs.getString("oldcontent")).trim();
                    if(StringUtils.isNotBlank(oldcontent) && StringUtils.isNotBlank(newcontent)){
                        newidmaptemp.put(oldcontent,newcontent);
                    }
                }
                hrmresourceNewidMap.putAll(newidmaptemp);
            }
        }
        iniSubAccount();
    }


    /**
     * 缓存E9次账号id
     */
    public static synchronized void  iniSubAccount(){
        if(isInitSubAccount){
            isInitSubAccount=false;
            SourceDBUtil rs=new SourceDBUtil();
            rs.executeQuery("select distinct(id) from hrmresource  where accounttype=1  and accounttype is not null");
            while (rs.next()){
                e9SubAccount.add(rs.getString("id"));
            }
        }
    }



    /**
     * 根据E9和E10表名缓存对应的类
     * @param e10tablename
     * @param e9tablename
     */
    public synchronized static void initNewIdCacheByTable(String e10tablename,String e9tablename){

        if(e10tablename.equals("employee") ||e10tablename.equals("employee_manager")){//人员
            initHrmResourceNewIdCache();
        }else if(e10tablename.equals("department") && e9tablename.equals("hrmsubcompany")){//分部
            if(subcompanyNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(subcompanyNewidMap,"department","hrmsubcompany");
        }else if(e10tablename.equals("department") && e9tablename.equals("hrmsubcompanyvirtual")){//虚拟分部
            if(subcompanyVirtualNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(subcompanyVirtualNewidMap,"department","hrmsubcompanyvirtual");
        }else if(e10tablename.equals("department") && e9tablename.equals("hrmdepartment")){//部门
            if(departmentNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(departmentNewidMap,"department","hrmdepartment");
        }else if(e10tablename.equals("department") && e9tablename.equals("hrmdepartmentvirtual")){//虚拟部门
            if(departmentVirtualNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(departmentVirtualNewidMap,"department","hrmdepartmentvirtual");

        }else if(e10tablename.equals("position") && e9tablename.equals("hrmjobtitles")){//岗位对应E10id
            if(jobtitleNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(jobtitleNewidMap,"position","hrmjobtitles");
        }else if(e10tablename.equals("auth_role") && e9tablename.equals("hrmroles")){//角色对应E10id
            if(hrmroleNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(hrmroleNewidMap,"auth_role","hrmroles");
        }else if(e10tablename.equals("wfp_base") && e9tablename.equals("workflow_base")){//流程路径
            if(wrokflowbaseNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(wrokflowbaseNewidMap,"wfp_base","workflow_base");
        }else if(e10tablename.equals("wfp_node") && e9tablename.equals("workflow_nodebase")){//流程节点
            if(wrokflownodebaseNewidMap.size()>0){
                return;
            }
            initNewIdCacheByTableExceute(wrokflownodebaseNewidMap,"wfp_node","workflow_nodebase");
        }
    }


    /**
     *根据表名缓存类
     */
    public static void  initNewIdCacheByTableExceute(ConcurrentHashMap<String,String> newidmap,String e10tablename,String e9tablename){
        if(newidmap.size()>0){//已缓存
            return;
        }
        if(StringUtils.isBlank(e10tablename)){
            return;
        }
        if(StringUtils.isBlank(e10tablename)){
            return;
        }
        SourceDBUtil rs=new SourceDBUtil();

        rs.executeQuery(" select * from rulequeue where ruleid in(select distinct(ruleid) from transidmap where tablename='"+e10tablename+"'and oldtable='"+e9tablename+"') and status in(0,1,3) ");
        if(rs.next()){
            LOG.info("规则："+rs.getString("ruleid")+"-----未执行，暂不缓存："+e10tablename+"---e9:"+e9tablename);
        }else{
            rs.executeQuery(" select newcontent,oldcontent from transidmap  where tablename='"+e10tablename+"'  and oldtable='"+e9tablename+"' ");

            ConcurrentHashMap<String,String> newidmaptemp=new ConcurrentHashMap<String,String>();
            while(rs.next()){
                String newcontent=Util.null2String(rs.getString("newcontent")).trim();
                String oldcontent=Util.null2String(rs.getString("oldcontent")).trim();
                if(StringUtils.isNotBlank(oldcontent) && StringUtils.isNotBlank(newcontent)){
                    newidmaptemp.put(oldcontent,newcontent);
                }
            }
            if(newidmaptemp.size()>0){
                newidmap.putAll(newidmaptemp);
            }
            LOG.info("表名："+e10tablename+"-----"+e9tablename+"已缓存成功："+newidmap.size());
        }
    }



    public static ConcurrentHashMap<String, String> getHrmresourceNewidMap() {
        return hrmresourceNewidMap;
    }

    public static void setHrmresourceNewidMap(ConcurrentHashMap<String, String> hrmresourceNewidMap) {
        WeaverCacheInfo.hrmresourceNewidMap = hrmresourceNewidMap;
    }

    public static ConcurrentHashMap<String, String> getSubcompanyNewidMap() {
        return subcompanyNewidMap;
    }

    public static void setSubcompanyNewidMap(ConcurrentHashMap<String, String> subcompanyNewidMap) {
        WeaverCacheInfo.subcompanyNewidMap = subcompanyNewidMap;
    }

    public static ConcurrentHashMap<String, String> getDepartmentNewidMap() {
        return departmentNewidMap;
    }

    public static void setDepartmentNewidMap(ConcurrentHashMap<String, String> departmentNewidMap) {
        WeaverCacheInfo.departmentNewidMap = departmentNewidMap;
    }

    public static ConcurrentHashMap<String, String> getSubcompanyVirtualNewidMap() {
        return subcompanyVirtualNewidMap;
    }

    public static void setSubcompanyVirtualNewidMap(ConcurrentHashMap<String, String> subcompanyVirtualNewidMap) {
        WeaverCacheInfo.subcompanyVirtualNewidMap = subcompanyVirtualNewidMap;
    }

    public static ConcurrentHashMap<String, String> getDepartmentVirtualNewidMap() {
        return departmentVirtualNewidMap;
    }

    public static void setDepartmentVirtualNewidMap(ConcurrentHashMap<String, String> departmentVirtualNewidMap) {
        WeaverCacheInfo.departmentVirtualNewidMap = departmentVirtualNewidMap;
    }

    public static ConcurrentHashMap<String, String> getJobtitleNewidMap() {
        return jobtitleNewidMap;
    }

    public static void setJobtitleNewidMap(ConcurrentHashMap<String, String> jobtitleNewidMap) {
        WeaverCacheInfo.jobtitleNewidMap = jobtitleNewidMap;
    }

    public static ConcurrentHashMap<String, String> getHrmroleNewidMap() {
        return hrmroleNewidMap;
    }

    public static void setHrmroleNewidMap(ConcurrentHashMap<String, String> hrmroleNewidMap) {
        WeaverCacheInfo.hrmroleNewidMap = hrmroleNewidMap;
    }

    public static ConcurrentHashMap<String, String> getWrokflowbaseNewidMap() {
        return wrokflowbaseNewidMap;
    }

    public static void setWrokflowbaseNewidMap(ConcurrentHashMap<String, String> wrokflowbaseNewidMap) {
        WeaverCacheInfo.wrokflowbaseNewidMap = wrokflowbaseNewidMap;
    }

    public static ConcurrentHashMap<String, String> getWrokflownodebaseNewidMap() {
        return wrokflownodebaseNewidMap;
    }

    public static void setWrokflownodebaseNewidMap(ConcurrentHashMap<String, String> wrokflownodebaseNewidMap) {
        WeaverCacheInfo.wrokflownodebaseNewidMap = wrokflownodebaseNewidMap;
    }

    public static Set<String> getEtServiceSet() {
        return etServiceSet;
    }

    public static void setEtServiceSet(Set<String> etServiceSet) {
        WeaverCacheInfo.etServiceSet = etServiceSet;
    }

    public static ConcurrentHashMap<String, String> getCoGroupServiceMap() {
        return coGroupServiceMap;
    }

    public static void setCoGroupServiceMap(ConcurrentHashMap<String, String> coGroupServiceMap) {
        WeaverCacheInfo.coGroupServiceMap = coGroupServiceMap;
    }
}

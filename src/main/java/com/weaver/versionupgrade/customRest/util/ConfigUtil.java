package com.weaver.versionupgrade.customRest.util;

import com.weaver.versionupgrade.customRest.db.DBUtil;
import com.weaver.versionupgrade.util.Util;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author cx
 * @date 2022/4/25
 **/
public class ConfigUtil {
    private static ConfigUtil configUtil;
    private ConfigUtil() {

    }

    public static void initConfig() {

        String confpath = "";
        File tFile = new File(".");
        String filePath = tFile.getAbsolutePath();

        if (filePath.indexOf("bin") > -1) {
            int binTag = filePath.lastIndexOf("bin");
            confpath = filePath.substring(0, binTag) + "conf/";
        } else {
            URL evnPathUrl = ConfigUtil.class.getClassLoader().getResource("application.properties");
            if (evnPathUrl != null) {
                confpath = evnPathUrl.getFile();
                if (confpath.indexOf("lib") > -1) {
                    int binTag = confpath.lastIndexOf("lib");
                    confpath = confpath.substring(0, binTag) + "conf/";
                    confpath = confpath.replace("file:", "");
                } else if (confpath.indexOf("classes") > -1) {
                    // 设置配置文件目录地址
                    configDir = confpath.substring(0, confpath.indexOf("classes") + 7);
                    configDir = configDir + File.separatorChar;
                    confpath = configDir;

                    ConfigUtil.setConfigDir(configDir);
                }
            } else {
                confpath = confpath;
            }
        }

        setConfigDir(confpath);

    }

    /**
     * 分表开关
     */
    private static int usePagination = 0;
    /**
     * 分表数量
     */
    private static int paginationSize = 16;

    public static String excelname = "ecologyForE10List.zip";

    /**
     * 大表独立分表
     */
    private static int useLargeTableSeparatePagination=0;

    /**
     * 大表独立分表没张表数据量
     */
    private static int largeTableDataNumber =0;


    /**
     *最长锁定时间
     */
    private static int  maxLockTimes =180*1000;

    /**
     * 开启租户单独建表--未使用
     */
    private static boolean aloneTable;




    /**
     *大表表名
     */
    private static Set<String> largeTableNames=ConcurrentHashMap.newKeySet();

    /**
     *E9大表和E10映射关系
     */
    public static ConcurrentHashMap<String,HashSet<String>> e10ToE9LargerTableMaping=new ConcurrentHashMap<>();



    private static int numberSlots = 1;
    private static String configDir;
    //判断是否微服务分库
    private static boolean useMiDB = false;
    //判断是否服务组合部署
    private static boolean serviceCoGroup = false;
    //判断是否微服务分库--源库
    private static boolean sourceMiDB = false;
    //判断是否微服务分库--目标库
    private static boolean targetMiDB = false;

    public static int getUsePagination() {
        return usePagination;
    }

    public static void setUsePagination(int usePagination) {
        ConfigUtil.usePagination = usePagination;
    }

    public static int getPaginationSize() {
        return paginationSize;
    }

    public static void setPaginationSize(int paginationSize) {
        ConfigUtil.paginationSize = paginationSize;
    }

    public static String getExcelDir() {
        return Util.getRootPath() + File.separatorChar + "excel";
    }
    public static String getExcelPath() {
        return Util.getRootPath() + File.separatorChar + "excel" + File.separatorChar + excelname;
    }

    /**
     * 配置文件路径
     */
    public static String getConfigFile(String fileName) {
        String evnPath = ConfigUtil.getConfigDir() + fileName;

        return evnPath;
    }

    public static int getNumberSlots() {
        return numberSlots;
    }

    public static void setNumberSlots(int numberSlots) {
        ConfigUtil.numberSlots = numberSlots;
    }

    /**
     * 老版本，给目标库专用的
     * @return
     */
    @Deprecated
    public static boolean isUseMiDB() {
        return useMiDB;
    }
    /**
     * 老版本，给目标库专用的；新的代码请使用setTargetUseMiDB
     * @return
     */
    @Deprecated
    public static void setUseMiDB(boolean useMiDB) {
        ConfigUtil.useMiDB = useMiDB;
    }

    public static boolean isSourceMiDB() {
        return sourceMiDB;
    }

    public static void setSourceMiDB(boolean sourceMiDB) {
        ConfigUtil.sourceMiDB = sourceMiDB;
    }

    public static boolean isTargetMiDB() {
        return targetMiDB;
    }

    public static void setTargetMiDB(boolean targetMiDB) {
        ConfigUtil.targetMiDB = targetMiDB;
        //setUseMiDB(targetMiDB);
    }

    public static String getConfigDir() {
        return configDir;
    }

    public static int getUseLargeTableSeparatePagination() {
        return useLargeTableSeparatePagination;
    }

    public  static void setUseLargeTableSeparatePagination(int useLargeTableSeparatePagination) {
        ConfigUtil.useLargeTableSeparatePagination = useLargeTableSeparatePagination;
    }

    public static int getLargeTableDataNumber() {
        return largeTableDataNumber;
    }

    public static void setLargeTableDataNumber(int largeTableDataNumber) {
        ConfigUtil.largeTableDataNumber = largeTableDataNumber;
    }

    public static Set<String> getLargeTableNames() {
        return largeTableNames;
    }

    public static void setLargeTableNames(Set<String> largeTableNames) {
        ConfigUtil.largeTableNames = largeTableNames;
    }

    public  ConcurrentHashMap<String, HashSet<String>> getE10ToE9LargerTableMaping() {
        return e10ToE9LargerTableMaping;
    }

    public  void setE10ToE9LargerTableMaping(ConcurrentHashMap<String, HashSet<String>> e10ToE9LargerTableMaping) {
        ConfigUtil.e10ToE9LargerTableMaping = e10ToE9LargerTableMaping;
    }

    public static int getMaxLockTimes() {
        return maxLockTimes;
    }

    public static boolean isServiceCoGroup() {
        return serviceCoGroup;
    }

    public static void setServiceCoGroup(boolean serviceCoGroup) {
        ConfigUtil.serviceCoGroup = serviceCoGroup;
    }

    public static void setConfigDir(String configDir) {
        ConfigUtil.configDir = configDir;
        File file = new File(configDir);
        if(file.exists()) {
            String rootPath = file.getParentFile().getAbsolutePath() + File.separatorChar;
            System.out.println("###rootPath:"+rootPath);
            Util.setRootPath(rootPath);
        }
    }

    /**
     * 组合服务下，判断不同的微服务是否组合在一个服务
     * @return
     */
    public static boolean isSameService(int ruleId, String serviceName2) {
        String serviceName1 = WeaverCacheInfo.getServicesMap().get(ruleId);
        return isSameService(serviceName1, serviceName2);
    }
    /**
     * 组合服务下，判断不同的微服务是否组合在一个服务
     * @return
     */
    public static boolean isSameService(String serviceName1, String serviceName2) {
        boolean same = false;
        if(serviceCoGroup) {
            String coServiceName1 = WeaverCacheInfo.getCoGroupServiceMap().get(serviceName1);
            String coServiceName2 = WeaverCacheInfo.getCoGroupServiceMap().get(serviceName2);
            same = coServiceName1.equals(coServiceName2)? true:false;
        } else {
            same = serviceName2.equals(serviceName1)? true:false;
        }
        return same;
    }


}

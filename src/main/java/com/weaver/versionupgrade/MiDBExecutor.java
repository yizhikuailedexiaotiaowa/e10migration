package com.weaver.versionupgrade;

import com.alibaba.druid.pool.DruidDataSource;
import com.weaver.versionupgrade.customRest.db.*;
import com.weaver.versionupgrade.customRest.util.*;
import com.weaver.versionupgrade.util.PropertiesUtil;
import com.weaver.versionupgrade.util.SyncConstant;

import com.weaver.versionupgrade.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.*;
import java.net.URL;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cx
 * @date 2023/9/6
 **/
public class MiDBExecutor {

    protected Connection conn;
    protected int curpos;
    protected int flag;
    protected String msg;
    protected Vector array;
    protected String columnName[];
    protected int columnType[];

    /**
     * 数据库类型
     */
    protected String dbtype = "";

    /**
     * 真实数据库类型
     */
    protected String realDbtype = "";

    //连接池
    protected ConcurrentHashMap<String, DruidDataSource> pools = new ConcurrentHashMap();
    protected ConcurrentHashMap<String, String> urlMap = new ConcurrentHashMap();
    protected ConcurrentHashMap<String, Integer> tableHitMap = new ConcurrentHashMap();

    protected static Logger logger = LoggerFactory.getLogger(MiDBExecutor.class);
    //规则id
    public int ruleId = -1;
    //服务名
    public String serviceName = "monomer";
    protected String useType = "";
    protected String type = "";

    public static boolean isPritErrorStack = true;

    /**
     * 无法连接的微服务
     */
    private static ConcurrentHashMap<String, String> noConectServices = new ConcurrentHashMap<String, String>();



    public MiDBExecutor() {

    }


    /**
     * 构造方法
     *
     * @param serviceName
     * @param useType
     * @param type
     */
    public MiDBExecutor(String serviceName, String useType, String type) {

        array = new Vector();
        this.serviceName = serviceName;
        this.useType = useType;
        this.type = type;
        initPoolsAndUrlMap(useType, type);
    }

    /**
     * 初始化类中的pools和urlmap
     *
     * @param useType
     * @param type
     */
    public void initPoolsAndUrlMap(String useType, String type) {

        DBUTKey dbutKey = new DBUTKey(useType, type);
        DBInfoCache.initPoolsByDBkey(dbutKey);
        DBPools dbPools = DBInfoCache.getPools().get(dbutKey);
        if (dbPools != null) {
            pools = dbPools.getPoolsMap();
//        } else {
//            pools = new ConcurrentHashMap<String, DruidDataSource>();
        }
        DBUrl dbUrl = DBInfoCache.getUrlMap().get(dbutKey);
        if (dbPools != null) {
            urlMap = dbUrl.getUrlMap();
        }
        if (!checkPoolExistByService(serviceName)) {
            Connection connection = getSourceConnection();
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("查询错误：", e);
                }
            }
        } else {
            if (StringUtils.isBlank(dbtype)) {
                setDbtype(getDataSourceByServicename(this.serviceName).getUrl());
            }
        }
    }


    public void clear() {
        array.clear();
    }

    public void writeLog(String str) {
        logger.error(str);
    }

    /**
     * 执行sql语句
     *
     * @param
     * @return
     */
    public void executeSql(String s) {
        List<String> splitsqls = new ArrayList<>();
        try {
            splitsqls = TablePaginationOperation.paginationSql(s, dbtype);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (splitsqls.size() <= 0) {
            splitsqls.add(s);
        }
        String result = "";
        boolean success = false;
        conn = getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if (conn != null) {
            try {
                statement = conn.createStatement();
                //执行全部的sql
                for (String sql : splitsqls) {
                    statement.execute(sql);
                    resultset = statement.getResultSet();
                    if (resultset != null) {
                        parseResultSet(resultset);
                        resultset.close();
                    }
                }
                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                writeLog("ERROR:" + s);
                writeLog(e.getMessage());
                result = "ERROR:" + e.toString();
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                    }
                }
                if (resultset != null) {
                    try {
                        resultset.close();
                    } catch (Exception e) {
                    }
                }
            }
        } else {
//            loggerer.error("###connect is null!!!");
            try {
                throw new Exception("连接为空");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 执行查询sql
     *
     * @param sql
     * @param args
     * @return
     */
    public boolean executeQuery(String sql, Object... args) {
        try {
            boolean result = executeSql(sql, true, args);
            return result;
        } catch (Exception e) {
            logger.error("查询错误：", e);
            logger.error(sql);
            if (isPritErrorStack) {
                e.printStackTrace();
            }
            return false;
        }
    }


    /**
     * 使用缓存查询sql
     *
     * @param sql
     * @param args
     * @return
     */
    public boolean executeCacheQuery(String sql, Object... args) {
        //缓存
        if (ruleId > 0 && args.length > 0 && sql.indexOf("*") < 0) {
            String key = getCacheKeys(args);
            if (key != null) {
                boolean isexists = getCahcheByKey(ruleId, sql, key);
                if (isexists) {//如果获取到的话，直接返回
                    return true;
                } else {
                }
            }
        }
        boolean result = false;
        try {
            result = executeSql(sql, true, args);
        } catch (Exception e) {
            logger.error("查询错误：", e);
            logger.error(sql);
            return false;
        }

        //如果规则大于0 且有参数 && sql查询执行成功就缓存 sql中不允许含有*
        if (ruleId > 0 && args.length > 0 && result && sql.indexOf("*") < 0) {
            String key = getCacheKeys(args);
            if (isBlank(key)) {
                return result;
            }
            WeaverCacheObject weaverCacheObject = new WeaverCacheObject();
            weaverCacheObject.setArray((Vector) array.clone());
            weaverCacheObject.setColumnName(columnName);
            weaverCacheObject.setColumnType(columnType);
            WeaverCacheInfo.setCahcheByKey(ruleId, sql, key, weaverCacheObject);

        }
        return result;

    }


    /**
     * 执行sql语句
     *
     * @param sql
     * @return
     */
    public boolean executeUpdate(String sql, Object... args) {
        try {
            return executeSql(sql, false, args);
        } catch (Exception e) {
            logger.error(sql + args.toString());
            logger.error("查询错误：", e);
            return false;
        }
    }

    /**
     * @param sql
     * @param isQuerySql 是否查询sql
     * @param args
     * @return
     */
    public boolean executeSql(String sql, boolean isQuerySql, Object... args) throws Exception {
        boolean success = false;

        if (sql == null || sql.trim().equals("")) {
            return success;
        }

        String result = "";

        conn = getSourceConnection(serviceName);
        Statement statement = null;
        ResultSet resultset = null;
        PreparedStatement preparedStatement = null;
        if (conn != null) {
            try {

                if (Constant.DB_TARGET.equals(type) && !checkSqlSytax(sql)) {//如果校验不通过，抛出异常
                    throw new Exception("不允许执行delete语句，update语句中条件字段需要有tenant_key字段");
                }

                List<Object> paramList = new ArrayList();
                List<String> sqllist = TablePaginationOperation.paginationPrepareStatementSql(sql, dbtype, paramList, args);
                for (String executesql : sqllist) {
                    preparedStatement = conn.prepareStatement(executesql);
                    for (int i = 0; i < paramList.size(); i++) {
                        preparedStatement.setObject(i + 1, paramList.get(i));
                    }
                    if (isQuerySql) {
                        preparedStatement.executeQuery();
                        resultset = preparedStatement.getResultSet();
                        if (resultset != null) {
                            parseResultSet(resultset);
                            resultset.close();
                        }
                    } else {
                        int i = preparedStatement.executeUpdate();
                    }
                }

                preparedStatement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                writeLog("ERROR:" + sql);
                writeLog(e.getMessage());
            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (Exception e) {
                    }
                }
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                    }
                }
                if (resultset != null) {
                    try {
                        resultset.close();
                    } catch (Exception e) {
                    }
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return success;
    }

//    public static void main(String[] args) {
//        String sql="update set stao=1 where a.id=111 and tenantkey='231321'";
//        System.out.println(checkSqlSytax(sql));
//    }

    /**
     * 校验sql是否合规
     *
     * @return
     */
    public boolean checkSqlSytax(String sql) {
        if (!useType.equals("e9")) {
            if (StringUtils.isNotBlank(sql)) {
                String lowersql = sql.toLowerCase().trim();
                if (lowersql.startsWith("update")) {
                    if (lowersql.indexOf("tenant_key") < 0 && lowersql.indexOf("tenantkey") < 0) {
                        return false;
                    }
                } else if (lowersql.startsWith("delete ")) {
                    return false;
                }
            }
        }
        return true;
    }

    public synchronized static void clearPools(String useType, String type) {
        DBUTKey dbutKey = new DBUTKey(useType, type);
        DBPools dbPools = DBInfoCache.getPools().get(dbutKey);
        DBUrl dbUrl = DBInfoCache.getUrlMap().get(dbutKey);
        ConcurrentHashMap<String, DruidDataSource> pools = null;
        ConcurrentHashMap<String, String> urlMap = null;
        if (dbPools != null) {
            pools = dbPools.getPoolsMap();
        }
        if (dbUrl != null) {
            urlMap = dbUrl.getUrlMap();
        }
        if (pools != null) {
            pools.forEach((key, druidDataSource) -> {
                druidDataSource.close();
            });
            pools.clear();
        }
        if (urlMap != null) {
            urlMap.clear();
        }
        //清理无法链接的缓存数据
        noConectServices = new ConcurrentHashMap<String, String>();

    }

    /**
     * 执行sql语句
     *
     * @param sql
     * @return
     */
    public int executeUpdate2(String sql, Object... args) {
        String result = "";
        int cnt = -1;
        boolean success = false;
        conn = this.getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        PreparedStatement preparedStatement = null;
        if (conn != null) {
            try {
                List<Object> paramList = new ArrayList();
                List<String> sqllist = TablePaginationOperation.paginationPrepareStatementSql(sql, dbtype, paramList, args);
                for (String executesql : sqllist) {
                    preparedStatement = conn.prepareStatement(executesql);
                    for (int i = 0; i < paramList.size(); i++) {
                        preparedStatement.setObject(i + 1, paramList.get(i));
                    }
                    int tempcnt = preparedStatement.executeUpdate();
                    if (tempcnt > 0) {
                        cnt = tempcnt;
                    }
                }
                preparedStatement.close();
            } catch (Exception e) {
                cnt = -1;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                writeLog("ERROR:" + sql);
                writeLog(e.getMessage());
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return cnt;
    }

    /**
     * 批量执行
     *
     * @param sqlList
     * @return
     */
    public boolean executeUpdateBatch(ArrayList<String> sqlList) {
        boolean success = false;
        if (sqlList == null || sqlList.size() == 0) {
            return success;
        }
        String result = "";

        conn = getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if (conn != null) {
            try {
                statement = conn.createStatement();
                for (String sql : sqlList) {
                    List<String> sqllist = TablePaginationOperation.paginationPrepareStatementSql(sql, dbtype);
                    for (String temp : sqllist) {
                        statement.addBatch(temp);
                    }
                }
                statement.executeBatch();
                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                logger.error("查询错误：", e);
            } finally {
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                    }
                }
                if (resultset != null) {
                    try {
                        resultset.close();
                    } catch (Exception e) {
                    }
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return success;
    }

    /**
     * 批量执行
     *
     * @param sqlList
     * @return
     */
    public boolean executeBatch(ArrayList<String> sqlList) {
        boolean success = false;
        if (sqlList == null || sqlList.size() == 0) {
            return success;
        }
        String result = "";
        conn = getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if (conn != null) {
            try {
                statement = conn.createStatement();
                for (String sql : sqlList) {
                    statement.addBatch(sql);
                }
                statement.executeBatch();
                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                logger.error("查询错误:", e);
                writeLog(e.getMessage());
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                    }
                }
                if (resultset != null) {
                    try {
                        resultset.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return success;
    }

    /**
     * 执行sql语句
     *
     * @param s
     * @return
     */
    public boolean execute(String s) {
        boolean success = false;
        conn = this.getSourceConnection();
        Statement statement = null;
        ResultSet resultset = null;
        if (conn != null) {
            try {
                statement = conn.createStatement();
                statement.execute(s);
                resultset = statement.getResultSet();

                if (resultset != null) {
                    parseResultSet(resultset);
                    resultset.close();
                }
                statement.close();
                success = true;
            } catch (Exception e) {
                success = false;
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                writeLog("查询错误:" + s);
                writeLog(e.getMessage());
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Exception e) {
                    }
                }
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                    }
                }
                if (resultset != null) {
                    try {
                        resultset.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return success;
    }

    /**
     * 初始化
     */
    private void init() {
        curpos = -1;
        flag = 1;
        msg = "数据库异常";
        if (array != null) {
            array.clear();
        }

    }

    /**
     * 解析查询结果
     */
    protected void parseResultSet(ResultSet resultset) throws Exception {
        init();
        parseDataResultSet(resultset);
    }

    /**
     * 解析查询结果
     */
    protected void parseDataResultSet(ResultSet resultset) throws Exception {
        curpos = -1;
        flag = 1;
        msg = "数据库异常";
        ResultSetMetaData resultsetmetadata = resultset.getMetaData();
        int i1 = resultsetmetadata.getColumnCount();

        columnName = new String[i1];
        columnType = new int[i1];

        for (int i2 = 0; i2 < i1; i2++) {
            if (dbtype.equals(Constant.MYSQL)) {
                columnName[i2] = resultsetmetadata.getColumnLabel(i2 + 1);
            } else {
                columnName[i2] = resultsetmetadata.getColumnName(i2 + 1);
            }
            columnType[i2] = resultsetmetadata.getColumnType(i2 + 1);
        }
        int j1;
        for (j1 = 0; resultset.next(); j1++) {
            Object as[] = new Object[i1];
            for (int k1 = 1; k1 <= i1; k1++) {
                Object tempobj = resultset.getObject(k1);
                //System.out.println(tempobj.toString());
                if (tempobj == null) {
                    as[k1 - 1] = "";


                } else if (columnType[k1 - 1] == Types.BLOB) {
                    Blob blob = null;
                    try {
                        blob = (Blob) tempobj;
                        if (blob != null) {
                            String readline = "";
                            StringBuffer nlobStrBuff = new StringBuffer("");
                            InputStream in = blob.getBinaryStream();
                            byte[] buff = new byte[(int) blob.length()];
                            for (int i = 0; (i = in.read(buff)) > 0; ) {
                                nlobStrBuff = nlobStrBuff.append(new String(buff));
                            }
                            in.close();
                            as[k1 - 1] = nlobStrBuff.toString();
                        } else {
                            as[k1 - 1] = "";
                        }
                    } catch (Exception se) {
                        as[k1 - 1] = "";
                    }

                } else if (columnType[k1 - 1] == Types.CLOB || columnType[k1 - 1] == Types.NCLOB) {
                    Clob clob = null;
                    try {
                        clob = (Clob) tempobj;
                        if (clob != null) {
                            String readline = "";
                            StringBuffer clobStrBuff = new StringBuffer("");
                            BufferedReader clobin = new BufferedReader(clob.getCharacterStream());
                            while ((readline = clobin.readLine()) != null) {
                                clobStrBuff = clobStrBuff.append(readline);
                            }
                            clobin.close();
                            as[k1 - 1] = clobStrBuff.toString();
                        } else {
                            as[k1 - 1] = "";
                        }
                    } catch (Exception se) {
                        as[k1 - 1] = "";
                    }
                } else {
                    as[k1 - 1] = tempobj;
                }
            }
            array.add(as);
        }
    }


    /**
     * 将查询结果的游标从当前位置移至下一个位置，游标最初位于第一条记录之前。
     *
     * @return boolean   如果下一个位置有值，返回true，否则返回false
     */
    public boolean next() {
        if (array.isEmpty())
            return false;
        boolean flag1 = true;
        if (curpos < array.size() - 1) {
            curpos++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值为字符串或者将被转换为字符串类型
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return String   该字段的值
     */
    public String getString(int columnIndex) {
        columnIndex--;
        String s = "";
        if (!array.isEmpty() && curpos >= 0 && curpos < array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            if (columnIndex >= 0 && columnIndex < as.length) {
                try {
                    s = as[columnIndex].toString();
                    s = s.trim();
                } catch (Exception e) {
                    s = "";
                }
            }
        }
        return s;

    }

    public String getStringNoTrim(int columnIndex) {
        columnIndex--;
        String s = "";
        if (!array.isEmpty() && curpos >= 0 && curpos < array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            if (columnIndex >= 0 && columnIndex < as.length) {
                try {
                    s = as[columnIndex].toString();
                } catch (Exception e) {
                    s = "";
                }
            }
        }
        return s;

    }

    /**
     * 获得当前记录中某一数据库字段的值，该值为字符串或者将被转换为字符串类型
     *
     * @param columnname 数据库字段的名称
     * @return String   该字段的值
     */
    public String getString(String columnname) {

        return getString(getColumnIndex(columnname));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值为boolean 类型
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return boolean   该字段的值
     */
    public boolean getBoolean(int columnIndex) {
        columnIndex--;
        boolean bool = false;
        if (!array.isEmpty() && curpos >= 0 && curpos <= array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            try {
                bool = ((Boolean) as[columnIndex]).booleanValue();
            } catch (Exception e) {
                throw new ClassCastException();
            }
        }
        return bool;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值为boolean 类型
     *
     * @param columnName 数据库字段的名称
     * @return boolean   该字段的值
     */
    public boolean getBoolean(String columnName) {
        return getBoolean(getColumnIndex(columnName));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值为int 类型
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return int   该字段的值
     */
    public int getInt(int columnIndex) {
        columnIndex--;
        int integer = -1;
        if (!array.isEmpty() && curpos >= 0 && curpos < array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            if (columnIndex >= 0 && columnIndex < as.length) {
                String v = as[columnIndex].toString();
                if (v != null) {
                    v = v.trim();
                }
                integer = Util.getIntValue(v, -1);
            }
        }
        return integer;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值为int 类型
     *
     * @param columnname 数据库字段的名称
     * @return int   该字段的值
     */
    public int getInt(String columnname) {
        return getInt(getColumnIndex(columnname));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值以二进制输出流形式输出 InputStream
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return InputStream   该字段的输出
     */
    public InputStream getInputStream(int columnIndex) {
        BufferedInputStream bis = null;
        if (!array.isEmpty() && curpos >= 0 && curpos <= array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            if (columnType[columnIndex] == Types.LONGVARCHAR) {
                byte inputByte[] = new byte[as[columnIndex].toString().length()];
                inputByte = as[columnIndex].toString().getBytes();

                bis = new BufferedInputStream(new ByteArrayInputStream(inputByte));
            }
        } else {
            throw new ClassCastException();
        }
        return bis;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值以二进制输出流形式输出 InputStream
     *
     * @param columnName 数据库字段的名称
     * @return InputStream   该字段的输出
     */
    public InputStream getInputStream(String columnName) {
        return getInputStream(getColumnIndex(columnName));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值为float 类型
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return float   该字段的值
     */
    public float getFloat(int columnIndex) {
        columnIndex--;
        float f = 0.0f;
        if (!array.isEmpty() && curpos >= 0 && curpos <= array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            try {
                f = Util.getFloatValue(as[columnIndex].toString(), -1);
            } catch (ClassCastException cce) {
                throw new ClassCastException();
            }
        }
        return f;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值为float 类型
     *
     * @param columnName 数据库字段的名称
     * @return float   该字段的值
     */
    public float getFloat(String columnName) {
        return getFloat(this.getColumnIndex(columnName));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值为double 类型
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return double   该字段的值
     */
    public double getDouble(int columnIndex) {
        columnIndex--;
        double d = 0.0;
        if (!array.isEmpty() && curpos >= 0 && curpos <= array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            try {
                String v = as[columnIndex].toString();
                if (v != null) {
                    v = v.trim();
                }
                d = Util.getDoubleValue(v, -1.0);
            } catch (ClassCastException cce) {
                throw new ClassCastException();
            }
        }
        return d;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值为double 类型
     *
     * @param columnName 数据库字段的名称
     * @return double   该字段的值
     */
    public double getDouble(String columnName) {
        return getDouble(getColumnIndex(columnName));
    }

    /**
     * 获得当前记录中指定位置的数据库字段的值，在查询语句中第一个字段的位置为1，该值以日期类型输出
     *
     * @param columnIndex 数据库字段在查询语句中的位置
     * @return Date   该字段的值
     */
    public Date getDate(int columnIndex) {
        columnIndex--;
        Date date = null;
        if (!array.isEmpty() && curpos >= 0 && curpos <= array.size()) {
            Object as[] = (Object[]) array.get(curpos);
            try {
                if (as[columnIndex] instanceof String) {
                    Timestamp sqlDate = Timestamp.valueOf(as[columnIndex].toString());
                    return new Date(sqlDate.getTime());
                } else if (as[columnIndex] instanceof java.sql.Date) {
                    date = (java.sql.Date) as[columnIndex];
                    if (date != null) {
                        Timestamp sqlDate = new Timestamp(date.getTime());
                        return new Date(sqlDate.getTime());
                    }
                } else {
                    date = (Date) as[columnIndex];
                }
            } catch (Exception e) {
                throw new ClassCastException();
            }
        }
        return date;
    }

    /**
     * 获得当前记录中某一数据库字段的值，该值以日期类型输出
     *
     * @param columnName 数据库字段的名称
     * @return Date   该字段的值
     */
    public Date getDate(String columnName) {
        return getDate(getColumnIndex(columnName));
    }


    /**
     * 获得查询的字段在字段排列中的位置
     */
    private int getColumnIndex(String columnname) {
        for (int i1 = 0; i1 < columnName.length; i1++) {
            if (columnName[i1].equalsIgnoreCase(columnname)) {
                return i1 + 1;
            }
        }
        try {
            throw new RuntimeException(columnname + ":列明不存在");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 重置连接
     */
    public void initConnection() {
        array = new Vector();
        pools.clear();
        init();
        Connection connection = getSourceConnection();
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 重置连接
     */
    public void initConnection(String fileName) {
        array = new Vector();
        pools.clear();
        init();
        Connection connection = getSourceConnection();
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                logger.error("查询错误:", e);
            }

        }
    }

    /**
     * 从连接池获取连接
     *
     * @return
     */
    public Connection getSourceConnection() {
        return getSourceConnection(serviceName);
    }

    /**
     * 从连接池获取连接
     *
     * @return
     */
    public Connection getSourceConnection(String serviceName) {

        Connection connection = null;
        try {
            if (checkPoolExistByService(serviceName)) {
                DruidDataSource dataSource = getDataSourceByServicename(serviceName);
                if (StringUtils.isBlank(dbtype)) {
                    setDbtype(dataSource.getUrl());
                }
                connection = dataSource.getConnection();
            } else if (noConectServices != null && noConectServices.containsKey(serviceName)) {//如果连接不上的集合中存在该微服务，直接pass
                return null;
            } else {
                synchronized (MiDBExecutor.class) {
                    if (checkPoolExistByService(serviceName)) {
                        DruidDataSource dataSource = getDataSourceByServicename(serviceName);
                        if (StringUtils.isBlank(dbtype)) {
                            setDbtype(dataSource.getUrl());
                        }
                        connection = dataSource.getConnection();
                    } else if (noConectServices != null && noConectServices.containsKey(serviceName)) {//如果连接不上的集合中存在该微服务，直接pass
                        return null;
                    } else {
                        Map<String, String> dbMap = getDBInfo(serviceName, useType);
                        if (dbMap != null) {
                            String driver = Util.null2String(dbMap.get("driver"));
                            String url = Util.null2String(dbMap.get("url"));
                            String username = Util.null2String(dbMap.get("user"));
                            String password = Util.null2String(dbMap.get("password"));
                            int maxconn = Util.getIntValue(dbMap.get("maxconn"));
                            if (StringUtils.isNotBlank(url)) {
                                boolean isconnect = false;
                                for (int i = 0; i < 3; i++) {
                                    boolean tempconnect = testDBConnection(dbMap);
                                    if (tempconnect) {
                                        isconnect = true;
                                        break;
                                    }
                                }
                                if (StringUtils.isBlank(dbtype)) {
                                    setDbtype(url);
                                }
                                if (!isconnect) {
                                    noConectServices.put(serviceName, "0");
                                    return connection;
                                }

                                //获取连接
                                DruidDataSource dataSource = getSourcePool(url, username, password, driver, maxconn, serviceName);
                                connection = dataSource.getConnection();
                                if (StringUtils.isBlank(dbtype)) {
                                    setDbtype(url);
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            if (isPritErrorStack) {
                e.printStackTrace();
            }
            logger.error("SQLException: " + e.getMessage());
            logger.error("SQLState: " + e.getSQLState());
            logger.error("VendorError: " + e.getErrorCode());
            Throwable cause = e.getCause();
            if (cause != null) {
                logger.error("RootCause: " + cause.getMessage());
            }
            logger.error(e.getMessage() + "微服务名：" + serviceName);
            System.out.println(e.getMessage() + "微服务名：" + serviceName);
            e.printStackTrace();
            connection = null;
            if (pools.containsKey(serviceName)) {
                pools.remove(serviceName);
            }
        } catch (Exception e) {
            logger.error(e.getMessage() + "微服务名：" + serviceName);
            System.out.println(e.getMessage() + "微服务名：" + serviceName);
            logger.error("查询错误:", e);
            connection = null;
            if (pools.containsKey(serviceName)) {
                pools.remove(serviceName);
            }
        }
        return connection;

    }


    /**
     * 测试数据库连接是否正常
     */
    public static boolean testDBConnection(Map<String, String> dbMap) {
        boolean isconnect = false;
        Connection conn = null;
        try {
            String driver11 = Util.null2String(dbMap.get("driver"));
            String url = Util.null2String(dbMap.get("url"));
            String username = Util.null2String(dbMap.get("user"));
            String password = Util.null2String(dbMap.get("password"));
            //第一步注册驱动
            Class.forName(driver11);
            conn = DriverManager.getConnection(url, username, password);

            if (conn != null && conn.createStatement() != null) {
                isconnect = true;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }
        }
        return isconnect;
    }



    /**
     * 获取结果数量
     *
     * @return
     */
    public int getCounts() {
        if (array == null) {
            return 0;
        } else {
            return array.size();
        }
    }

    /**
     * 连接池
     *
     * @param
     * @param
     * @param
     * @param
     * @return
     */
    public DruidDataSource getSourcePool(String serviceName, String useType) {

        DruidDataSource dataSource = null;
        if (serviceName != null && checkPoolExistByService(serviceName)) {
            return getDataSourceByServicename(serviceName);
        }

        synchronized (MiDBExecutor.class) {
            if (serviceName != null && checkPoolExistByService(serviceName)) {
                return getDataSourceByServicename(serviceName);
            }
            String relservicename = serviceName;
            Map<String, String> dbMap = getDBInfo(relservicename, useType);
            if (dbMap != null) {
                String driver = Util.null2String(dbMap.get("driver"));
                String url = Util.null2String(dbMap.get("url"));
                String username = Util.null2String(dbMap.get("user"));
                String password = Util.null2String(dbMap.get("password"));
                int maxconn = Util.getIntValue(dbMap.get("maxconn"));

                //获取连接
                dataSource = getSourcePool(url, username, password, driver, maxconn, serviceName);
            }
        }

        return dataSource;

    }

    /**
     * 连接池
     *
     * @param url
     * @param username
     * @param password
     * @param
     * @return
     */
    public DruidDataSource getSourcePool(String url, String username, String password, String driver, int maxconn, String serviceName) {
        synchronized (MiDBExecutor.class) {
            String otherParam = "";
            DBUTKey dbutKey = new DBUTKey(useType, type);
            setDbtype(url);
            if (Constant.MYSQL.equals(dbtype) && StringUtils.isNotBlank(url)) {
                otherParam = "&rewriteBatchedStatements=true&tinyInt1isBit=false";
            }
            if (serviceName == null) {
                serviceName = "monomer";
            }

//            if(!DBInfoCache.getPools().containsKey(dbutKey)){
//                System.out.println("初始化异常1229");
//                DBInfoCache.getPools().put(dbutKey,new DBPools());
//            }
//            if(!DBInfoCache.getUrlMap().containsKey(dbutKey)){
//                System.out.println("初始化异常1233");
//                DBInfoCache.getUrlMap().put(dbutKey,new DBUrl());
//            }
//            DBPools dbPools=DBInfoCache.getPools().get(dbutKey);
//            pools=dbPools.getPoolsMap();
//            DBUrl dbUrl = DBInfoCache.getUrlMap().get(dbutKey);
//            urlMap = dbUrl.getUrlMap();

//            if(dbUrl != null) {
//                urlMap = dbUrl.getUrlMap();
//            }
//            if(urlMap == null) {
//                urlMap = new ConcurrentHashMap<>();
//            }

            if (checkPoolExistByService(serviceName)) {
                return getDataSourceByServicename(serviceName);
//            } else if(urlMap.containsKey(url)) {//判断该连接是否已存在，从已存在中获取
//                //   String existName = urlMap.get(url);
//                //  DruidDataSource druidDataSource = pools.get(existName);
//                DruidDataSource druidDataSource=DruidDataSourceFactory.buildJDBCInputFormat().setDrivername(driver).setDBUrl(url+otherParam).setUsername(username).setPassword(password)
//                    .setMaxConn(maxconn).finish().getDataSource();
//                if(druidDataSource != null) {
//                    pools.put(serviceName, druidDataSource);
//                    DBPools dbPools = new DBPools();
//                    dbPools.setPoolsMap(pools);
//                    DBInfoCache.getPools().put(dbutKey, dbPools);
//                    return druidDataSource;
//                }
            }

            DruidDataSourceFactory jdbcInputFormatBuilder = DruidDataSourceFactory.buildJDBCInputFormat()
                    // 数据库连接驱动名称
                    .setDrivername(driver)
                    // 数据库连接地址
                    .setDBUrl(url + otherParam)
                    // 数据库连接用户名
                    .setUsername(username)
                    // 数据库连接密码
                    .setPassword(password)
                    .setMaxConn(maxconn)
                    .finish();
            String jdbcKey = Util.null2String(url).trim() + Util.null2String(username).trim() + Util.null2String(password).trim();
            DruidDataSource dataSource = jdbcInputFormatBuilder.getDataSource();
            if (dataSource != null) {
                DBInfoCache.addDBPools(dbutKey, jdbcKey, dataSource);
                DBInfoCache.addDBUrl(dbutKey, serviceName, jdbcKey);
//                urlMap.put(serviceName, jdbcKey);
            }

//            DBPools dbPools = new DBPools();
//            dbPools.setPoolsMap(pools);
//            DBInfoCache.getPools().put(dbutKey, dbPools);

//            DBUrl nDbUrl = new DBUrl();
//            nDbUrl.setUrlMap(urlMap);
//            DBInfoCache.getUrlMap().put(dbutKey, nDbUrl);

            return dataSource;
        }
    }


    /**
     * 获得数据库类型
     *
     * @param url
     */
    private void setDbtype(String url) {
        if (StringUtils.isBlank(url)) {
            return;
        }
        Map<String, String> dbinfo = DBUtil.getDBtypeByUrl(url);
        dbtype = dbinfo.get("dbtype");
        realDbtype = dbinfo.get("realDbtype");
    }

    /**
     * 获取数据库类型
     *
     * @return
     */
    public String getDbtype() {
        return dbtype;
    }
    /**
     * 获取数据库类型
     *
     * @return
     */
    public String getDBType() {
        return dbtype;
    }

    /**
     * 获取真实数据类型
     *
     * @return
     */
    public String getRealDbtype() {
        return realDbtype;
    }

    /**
     * 获取数据库分割符
     *
     * @return
     */
    public String getScriptbreak() {
        return DBUtil.getScriptbreak(dbtype);
    }


    /**
     * 空判断
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 非空判断
     *
     * @param str
     * @return
     */
    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    /**
     * 根据key 获取缓存对象，如果获取到了 返回true,没获取到 返回false
     *
     * @param ruleid
     * @param sql
     * @param key
     * @return
     */
    protected boolean getCahcheByKey(int ruleid, String sql, String key) {

        if (ruleId < 0) {
            return false;
        }
        if (isBlank(key)) {
            return false;
        }

        //如果规则没有缓存对象
        ConcurrentHashMap<String, ConcurrentHashMap<String, WeaverCacheObject>> rulesqlmap = WeaverCacheInfo.getSqlValueMap().get(ruleid);
        if (rulesqlmap == null) {
            return false;
        }
        ConcurrentHashMap<String, WeaverCacheObject> valueMap = rulesqlmap.get(sql);
        if (valueMap == null) {
            return false;
        }

        WeaverCacheObject cacheObject = valueMap.get(key);
        if (cacheObject == null) {
            return false;
        }
        init();//初始化数据

        columnName = cacheObject.getColumnName();
        columnType = cacheObject.getColumnType();
        this.array.addAll(cacheObject.getArray());

        return true;
    }

    /**
     * 根据参数获取key
     *
     * @param args
     * @return
     */
    public static String getCacheKeys(Object... args) {
        return WeaverCacheInfo.getCacheKeys(args);
    }

    /**
     * 获取所有的列名
     *
     * @return
     */
    public String[] getColumnName() {
        return columnName;
    }

    /**
     * @return
     */
    public int[] getColumnType() {
        return columnType;
    }

    /**
     * 关闭连接池
     */
    public void closePool() {
        pools.forEach((key, druidDataSource) -> {
            druidDataSource.close();
        });
        urlMap = new ConcurrentHashMap<>();
        pools = new ConcurrentHashMap<>();
    }


    public Map<String, String> getDBInfo() {
        return getDBInfo(serviceName, useType, type);
    }

    /**
     * 获取数据库连接信息
     *
     * @return
     */
    public Map<String, String> getDBInfo(String serviceName) {
        return getDBInfo(serviceName, useType, type);
    }

    /**
     * 获取数据库连接信息
     *
     * @return
     */
    public Map<String, String> getDBInfo(String serviceName, String useType) {
        return getDBInfo(serviceName, useType, type);
    }

    /**
     * 获取数据库连接信息
     *
     * @return
     */
    public static Map<String, String> getDBInfo(String serviceName, String useType, String type) {


        {
            try {
                PropertiesUtil propertiesUtil = new PropertiesUtil();
                String password = "";
                String username = "";
                String url = "";
                String driver = "";
                String maxconn = "";
                String confFileName = "weaver.properties";


                String propertiespath = ConfigUtil.getConfigDir() + confFileName;


                propertiesUtil.load(new File(propertiespath));
                boolean isEncrypt = "1".equals(propertiesUtil.get("isEncrypt"));

                password = isEncrypt ? AESUtil.Decrypt(propertiesUtil.get("ecology.password"), Util.AES_ACCOUNT_KEY) : propertiesUtil.get("ecology.password");
                username = isEncrypt ? AESUtil.Decrypt(propertiesUtil.get("ecology.user"), Util.AES_ACCOUNT_KEY) : propertiesUtil.get("ecology.user");
                url = propertiesUtil.get("ecology.url");
                driver = propertiesUtil.get("DriverClasses");
                maxconn = Util.null2String(propertiesUtil.get("ecology.maxconn"));

                if (maxconn.equals("")) {
                    maxconn = url.indexOf("mysql") > -1 ? "300" : "50";
                }

                //开启游标
                if (url.indexOf(SyncConstant.MYSQL) > -1) {
                    if (url.indexOf("useCursorFetch") < 0) {
                        url = url + "&useCursorFetch=true";
                    }
                    if (url.indexOf("rewriteBatchedStatements") < 0) {
                        url = url + "&rewriteBatchedStatements=true";
                    }
                }
                HashMap<String, String> jdbcInfo = new HashMap(8);
                jdbcInfo.put("url", url);
                jdbcInfo.put("driver", driver);
                jdbcInfo.put("user", username);
                jdbcInfo.put("password", password);
                jdbcInfo.put("maxconn", maxconn);

                return jdbcInfo;
            } catch (Exception e) {
                if (isPritErrorStack) {
                    e.printStackTrace();
                }
                logger.error("查询错误:", e);
            }
            return null;
        }


    }


    public static ConcurrentHashMap<String, String> getNoConectServices() {
        return noConectServices;
    }

    public static void setNoConectServices(ConcurrentHashMap<String, String> noConectServices) {
        MiDBExecutor.noConectServices = noConectServices;
    }

    /**
     * 判断微服务对应的连接是否存在
     *
     * @param serviceName
     * @return
     */
    public boolean checkPoolExistByService(String serviceName) {
        if (urlMap != null && urlMap.containsKey(serviceName)) {
            String jdbcKey = Util.null2String(urlMap.get(serviceName));
            if (pools != null && pools.containsKey(jdbcKey)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据微服务名获取datasource
     *
     * @param serviceName
     * @return
     */
    private DruidDataSource getDataSourceByServicename(String serviceName) {
        if (urlMap != null && urlMap.containsKey(serviceName)) {
            String jdbcKey = Util.null2String(urlMap.get(serviceName));
            if (pools != null && pools.containsKey(jdbcKey)) {
                return pools.get(jdbcKey);
            }
        }
        return null;
    }


    public DBUTKey getDButkey() {
        DBUTKey dbutKey = new DBUTKey(useType, type);
        return dbutKey;
    }

    public String getUseType() {
        return useType;
    }

    public String getType() {
        return type;
    }
}

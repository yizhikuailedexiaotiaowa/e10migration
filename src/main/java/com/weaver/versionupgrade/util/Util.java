package com.weaver.versionupgrade.util;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleSchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.alibaba.druid.util.JdbcConstants;
import com.weaver.versionupgrade.customRest.util.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

/**
 * @author cx
 * @date 2022/3/1
 */
public class Util {


    /**
     * 为了线程安全
     * 使用ThreadLocal，令每个线程创建一个当前线程的SimpleDateFormat的实例对象。
     */
    public static ThreadLocal<DateFormat> SDFTimeLocal = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        }
    };

    public static ThreadLocal<DateFormat> SDFTimeLocal2 = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd HH:mm");

        }
    };
    /**
     * 为了线程安全
     * 使用ThreadLocal，令每个线程创建一个当前线程的SimpleDateFormat的实例对象。
     */
    public static ThreadLocal<DateFormat> SDFDateLocal = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd");

        }
    };
    private static Logger logger = LoggerFactory.getLogger(Util.class);
    public static final String IS_USE = "1";
    private static String rootPath = null;
    private static final String listSep = "@SEP@";
    private static Pattern p = Pattern.compile("\\s*|\t*|\r*|\n*");

    private volatile static boolean dbConnectionFlag = false;//只有成功获取数据库连接时候，才更新该字段
    //需要为16位
    public static final String AES_KEY = "weaverE10propsIn";
    public static final String AES_ACCOUNT_KEY = "weaverAccountKey";
    public static final String LANG_CONTENT_PREFIX = "~`~`";
    public static final int LANG_CONTENT_PREFIX_LEN = LANG_CONTENT_PREFIX.length();
    public static final String LANG_CONTENT_SPLITTER1 = "`~`";
    public static final String LANG_CONTENT_SUFFIX = "`~`~";
    public static final String LANG_INPUT_PREFIX = "__multilangpre_";
    public static final String langRegP = "(?<!value {0,3}= {0,3}('|\"))(~`~`(([\\d ]{1,2}[^`]{0,2000}?`~`)+[\\d ]{1,2}[^`]{0,2000}?)`~`~).{0,2000}?";
    public static final Pattern langRegPattern = Pattern.compile(langRegP, Pattern.CASE_INSENSITIVE);
    //初始化配置文件中 ecologyhome,resinVersion,JDKHome

    /**
     * 获得项目路径
     */
    public static String getRootPath(){

        return rootPath;

    }


    public static boolean isDbConnectionFlag() {
        return dbConnectionFlag;
    }

    public static void setDbConnectionFlag(boolean dbConnectionFlag) {
        Util.dbConnectionFlag = dbConnectionFlag;
    }

    /**
     * String空值校验
     */
    public  static boolean stringIsNotNUll(String str){
        if(null != str && !"".equals(str.trim())){
            return true;
        }
        return false;
    }

    public static void setRootPath(String rootPath) {
        Util.rootPath = rootPath;
    }

    public static int getIntValue(String v) {
        return getIntValue(v, -1);
    }

    public static int getIntValue(String v, int def) {
        try {
            return Integer.parseInt(v);
        } catch (Exception ex) {
            return def;
        }
    }

    public static float getFloatValue(String v) {
        return getFloatValue(v, -1);
    }

    public static float getFloatValue(String v, float def) {
        try {
            return Float.parseFloat(v);
        } catch (Exception ex) {
            return def;
        }
    }

    public static double getDoubleValue(String v) {
        return getDoubleValue(v, -1);
    }

    public static double getDoubleValue(String v, double def) {
        try {
            return Double.parseDouble(v);
        } catch (Exception ex) {
            return def;
        }
    }



    public static String null2String(String s) {
        return s == null ? "" : s.trim();
    }

    public static String null2String(Object s) {
        return s == null ? "" : s.toString();

    }


    /**
     * String 如果为null 返回默认值
     *
     * @param s
     * @param def
     * @return
     */
    public static String null2String(String s, String def) {
        return s == null ? (def == null ? "" : def) : s;
    }

    /**
     * Object 如果为null 返回默认值
     *
     * @param o
     * @param def
     * @return
     */
    public static String null2String(Object o, String def) {
        return o == null ? (def == null ? "" : def) : o.toString();
    }


    /**
     * 替换注释和空格
     * @param str
     * @return
     */
    public static StringBuffer replaceXmlNotes(StringBuffer str) {
        if(str != null) {
            str = new StringBuffer(str.toString().replaceAll("(?s)<\\!\\-\\-.+?\\-\\->", ""));//替换注释
        }

        return str;
    }
    /**
     * 替换注释和空格
     * @param str
     * @return
     */
    public static String replaceJavaNotes(String str) {
        if(str != null) {
            //替换注释
            str = str.replaceAll("//.*", "");
        }

        return str;
    }

    /**
     * 判断字符串是否是乱码
     *
     * @param strName 字符串
     * @return 是否是乱码
     */

    public static boolean isMessyCode(String strName) {
        try {

            Matcher m = p.matcher(strName);
            String after = m.replaceAll("");
            String temp = after.replaceAll("\\p{P}", "");
            char[] ch = temp.trim().toCharArray();

            int length = (ch != null) ? ch.length : 0;
            for (int i = 0; i < length; i++) {
                char c = ch[i];
                if (!Character.isLetterOrDigit(c)) {
                    String str = "" + ch[i];
                    if (!str.matches("[\u4e00-\u9fa5]+")) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }


    /**
     * 获得磁盘空间 单位M
     * @param file
     * @return
     */
    public static long getDiskSpace(File file) {
        long size = (long)Math.ceil(file.getFreeSpace()*1.000000/(1024*1024));//单位M
        return size;
    }
    /**
     * @return 返回当前时间字符，格式为 yyyy'-'MM'-'dd
     *
     * 返回当前时间字符，默认格式为yyyy'-'MM'-'dd
     *
     * 如 2004-09-07
     */
    public static String getCurrentDateString() {
        String timestrformart = "yyyy'-'MM'-'dd" ;
        SimpleDateFormat SDF = new SimpleDateFormat(timestrformart) ;
        Calendar calendar = Calendar.getInstance() ;

        return SDF.format(calendar.getTime()) ;
    }

    /**
     * @return 返回当前时间字符，格式为 yy'-'MM'-'dd
     *
     * 返回当前时间字符，默认格式为yy'-'MM'-'dd
     *
     * 如 2004-09-07
     */
    public static String getCurrentDateString2() {
        String today = getCurrentDateString();
        //获得月份+日期部分环境的日期格式为20-08-25的格式
        try {
            today = today.substring(today.indexOf("-") + 1);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return today ;
    }

    /**
     * 获得当前日期
     * @return
     */
    public static String getCurrentTime() {
        String timestrformart = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat SDF = new SimpleDateFormat(timestrformart);
        Calendar calendar = Calendar.getInstance();

        return SDF.format(calendar.getTime());
    }

    /**
     * @return 返回当前时间字符，格式为 yyyy'-'MM'-'dd
     *
     * 返回当前时间字符，默认格式为yyyy'-'MM'-'dd
     *
     * 如 2004-09-07
     */
    public static String getCurrentDateString(Date date) {
        String timestrformart = "yyyy'-'MM'-'dd" ;
        SimpleDateFormat SDF = new SimpleDateFormat(timestrformart) ;

        return SDF.format(date) ;
    }


    public static String getJdkVersion() {
        return System.getProperty("java.version");
    }

    /**
     * 获取Linux下的IP地址
     * @return IP地址
     * @throws SocketException
     */
    public static String getLinuxLocalIp() {
        String ip = "";
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                String name = intf.getName();
                if (!name.contains("docker") && !name.contains("lo")) {
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            String ipaddress = inetAddress.getHostAddress().toString();
                            if (!ipaddress.contains("::") && !ipaddress.contains("0:0:") && !ipaddress.contains("fe80")) {
                                ip = ipaddress;
                                System.out.println(ipaddress);
                            }
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            ip = "127.0.0.1";
            ex.printStackTrace();
        }
        return ip;
    }

    /**
     * 获取linux本地Host名称
     * @return String hostname
     * @throws UnknownHostException
     */
    public static  String getLocalHostName() {
        String localHostName="";
        try{
            localHostName=InetAddress.getLocalHost().getHostName();
        }catch(Exception e){
            e.printStackTrace();
        }
        return localHostName;
    }

    /**
     * 判断当前操作系统的类型
     * @return   boolean  linux放回false  window 返回true
     */
    public static   boolean isWindowsOS(){
        boolean flag=true;
        String os = System.getProperty("os.name");
        if(!os.toLowerCase().startsWith("win")){
            flag=false;
        }
        return flag;
    }

    /**
     * 获取本机IP列表（包含内网ip，外网ip），存储在集合中<br>
     * @return
     * @throws SocketException
     */
    public static ArrayList<String> getRealIp()  {
        String localip = null; // 本地IP
        String netip = null;  // 外网IP
        ArrayList<String> ips = new ArrayList<String>();//用于存储所有获取到的本机IP
        Enumeration<NetworkInterface> netInterfaces = null;
        try {
            netInterfaces = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        } //返回此机器上的所有网络接口，如果所有网络接口都被禁止 则返回null
        InetAddress ip = null;
        while (netInterfaces.hasMoreElements()) { // 遍历多网卡
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> address = ni.getInetAddresses(); //获取所有的inetaddresses或绑定到该网络接口的子集枚举对象
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()&& ip.getHostAddress().indexOf(":") == -1) {// 外网IP(三个条件分别为 不为局域网IP，不为环路IP,不为ipv6 IP)
                    netip = ip.getHostAddress();
                    if(netip != null && !"".equals(netip)){
                        ips.add(netip);
                    }
                } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()&& ip.getHostAddress().indexOf(":") == -1) {// 内网IP(三个条件分别为 是局域网IP，不是环路IP，不是ipv6 Ip)
                    localip = ip.getHostAddress();
                    ips.add(localip);
                }
            }
        }
        return ips;
    }




    /**
     * list转string
     */
    public static String list2String(List list) {
        return StringUtils.join(list, listSep);
    }


    /**
     * 获得方法体等
     * @param content
     * @param
     * @param
     * @return
     */
    public static String getContentBody(String content, char startChar, char endChar) {
        char[] chars = content.toCharArray();
        int cnt = 0;
        int index = 0;
        boolean match = false;
        for(int i = 0; i < chars.length; i++) {
            index++;
            char c = chars[i];
            if(c == startChar) {
                if(match == false) {
                    match = true;
                }
                cnt++;
            }
            if(cnt > 0 && c == endChar) {
                cnt--;
            }
            if(match && cnt == 0) {
                break;
            }
        }
        if(index > 0) {
            content = content.substring(0, index);
        }
        return content;
    }

    public static String getPercent(int cnt, int total) {
        if(total != 0) {
            double per = (double) cnt*100/total;
            String rounded = "0";
            if(per != (int)per) {
                BigDecimal bg = new BigDecimal(per);
                double f1 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                rounded = String.valueOf(f1);
            } else {
                rounded = ""+(int)per;
            }
            return rounded;
        } else {
            return "100";
        }
    }

    public static void main(String[] args) {
        String url="jdbc:mysql://10.10.42.247:3306/ecologytest0908?characterEncoding=utf8&useSSL=false&autoReconnect=true&failOverReadOnly=false&serverTimezone=Asia/Shanghai";;

        System.out.println(getDatabaseName(url));

        System.out.println(getDatabaseName("jdbc:oracle:thin:@10.12.1.34:1521/ecology"));


        System.out.println(getDatabaseName("jdbc:sqlserver://10.12.2.18:1433;DatabaseName=flow"));
//
//
//
//

    }
    //解析url获取库名
    public static String getDatabaseName(String jdbcUrl){
        String database = null;
        int pos, pos1;
        String connUri;

        if (StringUtils.isBlank(jdbcUrl)) {
            throw new IllegalArgumentException("Invalid JDBC url.");
        }

        jdbcUrl = jdbcUrl.toLowerCase();

        if (jdbcUrl.startsWith("jdbc:impala")) {
            jdbcUrl = jdbcUrl.replace(":impala", "");
        }

        if (!jdbcUrl.startsWith("jdbc:")
                || (pos1 = jdbcUrl.indexOf(':', 5)) == -1) {
            throw new IllegalArgumentException("Invalid JDBC url.");
        }

        connUri = jdbcUrl.substring(pos1 + 1);

        if (connUri.startsWith("//")) {
            if ((pos = connUri.indexOf('/', 2)) != -1) {
                database = connUri.substring(pos + 1);
            }else if(  (pos = connUri.indexOf('=')) != -1 ){
                database = connUri.substring(pos + 1);
            }else{
                database=connUri;
            }
        } else {
            if(connUri.indexOf("thin")>-1){
                if ((pos = connUri.indexOf('/')) != -1) {
                    database = connUri.substring(pos + 1);
                }else{
                    database = connUri;
                }
            }else{
                database = connUri;
            }

        }

        if (database.contains("?")) {
            database = database.substring(0, database.indexOf("?"));
        }

        if (database.contains(";")) {
            database = database.substring(0, database.indexOf(";"));
        }

        if (StringUtils.isBlank(database)) {
            throw new IllegalArgumentException("Invalid JDBC url.");
        }
        return database;
    }

    /**
     * 生成随机6位后缀
     * @return
     */
    public static String getlinkNo() {
        String linkNo = "";
        // 用字符数组的方式随机
        String model = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[] m = model.toCharArray();
        for (int j = 0; j < 6; j++) {
            char c = m[(int) (Math.random() * 36)];
            if (linkNo.contains(String.valueOf(c))) {
                j--;
                continue;
            }
            linkNo = linkNo + c;
        }
        return linkNo;
    }

    /**
     * 获取逗号分割的两个字符串中相同的元素
     * 元素少的为str1
     */
    public static String getDifferentStrs(String str1,String str2){

        // 先转换成list
        List<String> list = Arrays.asList(str2.split(","));
        // 转换成ArrayList
        List arrayList = new ArrayList(list);
        // 遍历ArrayList
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String x = it.next();
            // 如果存在，直接删除
            if (str1.indexOf(x) != -1) {
                it.remove();
            }
        }
        return StringUtils.join(arrayList.toArray(), ",");
    }


    /**
     * 格式化获得第一条记录的sql
     * @param sql
     * @return
     */
    public static String formatTop1Sql(String sql, String dbType) {
        if(SyncConstant.MYSQL.equalsIgnoreCase(dbType)) {
            sql = sql + " limit 1";
        }else if(SyncConstant.POSTGRESQL.equalsIgnoreCase(dbType)) {
                sql = sql + " limit 1";
        } else if(SyncConstant.ORACLE.equalsIgnoreCase(dbType)) {
            //需要sql中有order by
            sql = sql.replaceFirst("(?i)order by ", " and rownum=1 order by ");
        } else if(Constant.SQLSERVER.equalsIgnoreCase(dbType)) {
            sql = sql.replaceFirst("(?i)select ", "select top 1 ");
        }
        return sql;
    }



    public static Connection initConnection(String jdbcdriver, String jdbcurl, String username, String password) {
        Connection conn = null;
        try {
            Class.forName(jdbcdriver);
            // 连接数据库
            conn = DriverManager.getConnection(jdbcurl, username, password);
        } catch (Exception se) {
            se.printStackTrace();
            return null;
        }
        return conn;
    }


    /**
     * 数据库关键字
     */
    private static HashSet<String> dbKeywordSet = new HashSet<String>(){
        {
            this.add("call");
            this.add("option");
            this.add("index");
            this.add("order");
            this.add("key");
            this.add("desc");
            this.add("describe");
            this.add("rank");
            this.add("left");
            this.add("right");
            this.add("top");
            this.add("row");
            this.add("system");
            this.add("condition");
            this.add("explain");
            this.add("require");
            this.add("range");
            this.add("sql");
            this.add("read");
            this.add("mode");
        }
    };



	 /**
     * 计算crc32值
     * @param content
     * @return
     */
    public static long getCRC32(String content) {
        CRC32 crc32 = new CRC32();
        crc32.update(content.getBytes());
        return crc32.getValue();
    }

    /**
     * 解析sql获取表名
     * @param sql
     * @return
     */
    public static List<String> getTablenamesBysql(String sql){
        List<String> result=new ArrayList<>();
        sql=sql.toLowerCase();
        if(sql.indexOf("where")>0){
            sql=sql.substring(0,sql.indexOf("where"));
        }

        if(sql.indexOf(" having ")>0){
            sql=sql.substring(0,sql.indexOf(" having "));
        }

        String dbType = JdbcConstants.ORACLE.toString();
        String lowersql=sql.toLowerCase().trim();
        List<SQLStatement> stmtList =null;
        try{
            stmtList = SQLUtils.parseStatements(sql, dbType);
            //解析出的独立语句的个数
            for (int i = 0; i < stmtList.size(); i++) {
                SQLStatement stmt = stmtList.get(i);
                OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
                stmt.accept(visitor);
                if (visitor!=null) {
                    Map<TableStat.Name, TableStat> tables = visitor.getTables();
                    Set<TableStat.Name> keys = tables.keySet();
                    for (TableStat.Name key : keys) {
                        String temptablename = Util.null2String(key.getName()).trim();//如果不为空的话
                        if (!temptablename.equals("")) {
                            result.add(temptablename.toLowerCase());
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }


        return result;
    }


    public static long pareTime2Long(String datetime) throws ParseException {
//        return SDFTime.parse(datetime).getTime();
        if (datetime.indexOf(":") == datetime.lastIndexOf(":")) {
            return SDFTimeLocal2.get().parse(datetime).getTime();
        } else {
            return SDFTimeLocal.get().parse(datetime).getTime();
        }


    }

    public static long pareDate2Long(String datetime) throws ParseException {
//        return SDFDate.parse(datetime).getTime();
        return SDFDateLocal.get().parse(datetime).getTime();
    }

    public static long pareDateTime2Long(String datetime) throws ParseException {
        if (datetime.indexOf(":") > -1) {
            return pareTime2Long(datetime);
        } else {
            return pareDate2Long(datetime);
        }
    }
    public static long getLongValue(String v) {
        return getLongValue(v, -1L);
    }

    public static long getLongValue(String v, long def) {
        try {
            if (v != null && v.indexOf(".") > 0) {
                v = v.substring(0, v.indexOf("."));
            }
            return Long.parseLong(v);
        } catch (Exception ex) {
            return def;
        }
    }




    private static String formatMultiLangOld(String s, String lang) {
        if (s == null || "".equals(s) || s.indexOf(LANG_CONTENT_PREFIX) == -1) return s;
        Matcher m = langRegPattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        boolean result = m.find();
        if (lang.equals("")) lang = "7";
        while (result) {
            String var = m.group(2);
            String value = transLang(var, lang);
            String sb2 = value;
            value = value.replace("\\", "\\\\").replace("$", "\\$");
            try {
                m.appendReplacement(sb, value);
            } catch (Exception e) {
            }
            result = m.find();
        }
        m.appendTail(sb);
        return sb.toString();
    }

    private static String transLang(String var, String lang) {
        ////System.out.println(var);
        boolean isExsit = false;
        //如果没有设过相应语言的翻译，返回中翻译作为默认值
        String defaultValue = "";

        String oriLang = lang;
        if (lang.length() < 2) lang += " ";
        var = var.replace("&nbsp;", " ");
        ////System.out.println("2::"+var+"::"+lang+"::"+ThreadVarManager.getMultiLang());
        int from = var.indexOf(LANG_CONTENT_PREFIX);
        int to = var.lastIndexOf(LANG_CONTENT_SUFFIX);
        if (from != -1 && to != -1 && from + 4 < to) {
            String[] ls = var.substring(from + 4, to).split(LANG_CONTENT_SPLITTER1);
            for (String lg : ls) {
                if (lg.startsWith("7 ")) {
                    defaultValue = lg.substring(lang.length());
                }
                if (lg.startsWith(lang)) {
                    isExsit = true;
                    return lg.substring(lang.length());
                } else if (lg.startsWith(oriLang)) {
                    isExsit = true;
                    return lg.substring(oriLang.length());
                }
            }
        }
        if (!isExsit) {
            return defaultValue;
        }
        return var;
    }
    public static String formatInValue(String values) {
        if (values != null) {
            if (values.endsWith(",")) {
                values = values.substring(0, values.length() - 1);
                //递归遍历
                formatInValue(values);
            }
        }

        return values;
    }


    /**
     * 判断空值
     *
     * @param v
     * @return
     */
    public static boolean checkValNull(String v) {
        if (v == null || "".equals(v) || "null".equals(v.toLowerCase())) {
            return true;
        } else {
            return false;
        }

    }

    public static Short getShortValue(String v) {
        return getShortValue(v, Short.valueOf("-1"));
    }

    public static Short getShortValue(String v, Short def) {
        try {
            if (v.indexOf(".") > -1) {
                v = v.substring(0, v.indexOf("."));
            }
            return Short.valueOf(v);
        } catch (Exception ex) {
            return def;
        }
    }

}

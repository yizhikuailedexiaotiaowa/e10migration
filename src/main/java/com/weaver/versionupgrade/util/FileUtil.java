package com.weaver.versionupgrade.util;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
	public final static String SEP = "/";
	public String getPath(String path) {
		// 为了在Linux环境下找到路径
		if (path == null) {
			return "";
		}
		String osname = System.getProperty("os.name").toLowerCase();
		if (!osname.startsWith("windows")) {
			return path.replaceAll("\\\\+", File.separator);
		}
		return path;

	}
	/**
	 * 读取文件
	 * @param file
	 * @return
	 */
	public static StringBuffer readFile(File file) {
		//读取文件  获取文件编码
		String encode = "UTF-8";
		InputStreamReader read = null;
		BufferedReader bufferedReader = null;
		StringBuffer text = new StringBuffer();
		try {

			if(file.exists() && file.isFile()) {
				//编码格式
				read= new InputStreamReader(new FileInputStream(file),encode);
				bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while((lineTxt = bufferedReader.readLine()) != null){
					text.append(lineTxt).append("\r\n");
				}
			} else {

			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (read != null) {
				try {
					read.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}


		return text;
	}

	/**
	 * 读取文件
	 * @param file
	 * @return
	 */
	public static StringBuffer readFileWithLineNo(File file) {
		//读取文件  获取文件编码
		String encode = "UTF-8";
		InputStreamReader read = null;
		BufferedReader bufferedReader = null;
		StringBuffer text = new StringBuffer();
		try {

			if(file.exists()) {
				//编码格式
				read= new InputStreamReader(new FileInputStream(file),encode);
				bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				int lineNo = 1;
				while((lineTxt = bufferedReader.readLine()) != null){
					lineTxt = String.format("行号%d:%s", lineNo, lineTxt);
					text.append(lineTxt).append("\r\n");
					lineNo++;
				}
			} else {

			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (read != null) {
				try {
					read.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		return text;
	}

	/**
	 *文件最后修改日期
	 * @param path
	 * @return
	 */
	public static String getLastModifyDate(String path) {
		File file = new File(path);
		if(file.exists()) {
			long lastModified = file.lastModified();
			String timeFormat = "yyyy'-'MM'-'dd" ;
			SimpleDateFormat sdf = new SimpleDateFormat(timeFormat) ;
			String  lastModifiedDate = sdf.format(new Date(lastModified));
			 return lastModifiedDate;
		} else {
			return "";
		}
	}

	/**
	 *
	 * @param path
	 * @return
	 */
	public static String delComments(String content) {
		//替换注释
		content = content.replaceAll("(?s)<\\!\\-\\-.+?\\-\\->", "");
		content = content.replaceAll("(?s)/\\*.+?\\*/", "");
		content = content.replaceAll("//.*", "");
		return content;
	}

	/**
	 * 格式化路径
	 * @param path
	 * @return
	 */
	public static String formatePath(String path) {
		path = Util.null2String(path);
		path = path.replace("\\", FileUtil.SEP);
		if(!path.endsWith(FileUtil.SEP) && path.indexOf(".") < 0) {
			path = path + FileUtil.SEP;
		}

		return path;
	}



}

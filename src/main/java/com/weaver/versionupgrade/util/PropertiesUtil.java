package com.weaver.versionupgrade.util;


import com.weaver.versionupgrade.customRest.util.ConfigUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class PropertiesUtil {
	/** Keys*/
	private List<String> keys = new ArrayList<String>();

	/** ValueMap*/
	private Map<String, String> valueMap = new HashMap<String, String>();
	/** notesmap**/
	private Map<String, String> notesMap = new HashMap<String, String>();
	
	public String  filepath = "";
	
	FileUtil fileUtil = new FileUtil();
	
	public PropertiesUtil() {
		
	}


	/**
	 * 根据key获得属性值
	 * @param key
	 * @return
	 */
	public String getPropertyVal(String key) {
		return Util.null2String(valueMap.get(key));
	}
	/**
	 * 根据key获得注释
	 */
	public String getPropertyNotes(String key) {
		return Util.null2String(notesMap.get(key));
	}
	
	/**
	 * 设置属性值
	 * @param key
	 * @param value
	 */
	public void put(String key, String value)
	{
		if (keys.contains(key))
		{
			keys.remove(key);
		}
		keys.add((String)key);
		valueMap.put((String)key, (String)value);
	}
	
	public void put(String key, String value, String notes)
	{
		if (keys.contains(key)) {//key已存在  做更新操作
		} else {
			keys.add((String)key);
		}
		valueMap.put((String)key, (String)value);
		
		//判断是否有换行;有换行需要插入换行
		if(notes.indexOf("\n")>-1){
			notes = notes.replaceAll("\n","\r\n");
			//System.out.println("notes===="+notes);
			notesMap.put((String)key, (String)notes);
		} else {
			notesMap.put((String)key, (String)notes);
		}
		
	}
	/**
	 * 判断是否包含key
	 * @param key
	 * @return
	 */
	public boolean containsKey(String key)
	{
		return keys.contains(key);
	}
	/**
	 * 根据key获得属性值
	 * @param key
	 * @return
	 */
	public String get(String key)
	{
		return Util.null2String(valueMap.get(key));
	}
	/**
	 * 删除指定的属性
	 * @param key
	 * @return
	 */
	public void remove(String key)
	{
		keys.remove(key);
		valueMap.remove(key);
		notesMap.remove(key);
	}
	/**
	 * 根据正则表达式获得属性值，并返回列表
	 * @return
	 */
	public List<String> getKeys(String keyPattern) {
		Pattern pat = Pattern.compile(keyPattern);
		List<String> kl = new ArrayList<String>();
		for (String k : keys) {
			if (pat.matcher(k).matches()) {
				kl.add(k);
			}
		}
		return kl;
	}
	
	
	public List<String> getKeys() {
		return keys;
	}

	@Override
	public String toString() {
		return valueMap.toString();
	}

	/** 
	 * 加载Properties文件
	 */
	public synchronized void load(String filepath){
		File file=new File(fileUtil.getPath(filepath));
		load(file);
	}

	/**
	 *
	 * @param propname
	 * @return
	 */
	public static String getPropValue(String fname,String propname){
		//flink自定义参数设置
		PropertiesUtil propertiesUtil = new PropertiesUtil();


		String evnPath = ConfigUtil.getConfigFile(fname + ".properties");
		propertiesUtil.load(evnPath);
		if(propertiesUtil.getKeys().contains(propname)) {
			return Util.null2String(propertiesUtil.get(propname));
		}
		return "";
	}

	/**
	 * 加载Properties文件
	 * @param file
	 */
	public synchronized void load(File file){
		FileInputStream fis = null;
		InputStreamReader insreader = null;
		if(!file.exists()) {
		    return;
        }
		this.filepath = file.getPath();
		try {
			fis = new FileInputStream(file);
            insreader = new InputStreamReader(fis, "GBK");
	        BufferedReader bin = new BufferedReader(insreader);
	        List<String> lines = new ArrayList<String>();
	        StringBuffer fileinfo=new StringBuffer();
	        String line;  
	        while ((line = bin.readLine()) != null) {  
	            lines.add(line);
				fileinfo.append(line);
	        }
	        //迁移过来时有问题，先注释掉
	      /*  if(SVNUtil.isMessyCode(fileinfo.toString())){//是否乱码
				insreader = new InputStreamReader(fis, "UTF-8");
				 bin = new BufferedReader(insreader);
				 lines = new ArrayList<String>();
				while ((line = bin.readLine()) != null) {
					lines.add(line);
				}
			}*/
			bin.close();
			insreader.close();

			// parse key-value
	        StringBuffer notebuffer = new StringBuffer();
			for (String l : lines) {
				if(l==null || "".equals(l.trim())) {
					continue;
				}
				if (l.trim().startsWith("#")||l.trim().startsWith("<!--")) {//以#开头或者<!---->是文件的注释
					notebuffer.append(l).append("\r\n");
				} else {
					if (l.indexOf("=") > -1 && !l.trim().startsWith("{")) {//有等号的情况，键值对
						String k = l.substring(0, l.indexOf("=")).trim();
						String val = l.substring(l.indexOf("=") + 1).trim();
						String v = val;
						String notes = "";
						//判断值的后面是否有#开头的注释
                        if(val.indexOf(" #")>-1) {
                            v = val.substring(0, l.indexOf(" #"));
                            v = v.trim();
                            notes = val.substring(l.indexOf(" #") + 2);
                            notebuffer.append(notes.trim()).append("\r\n");
                        }
						keys.add(k);
						valueMap.put(k, v);
						if(notebuffer.length()>0) {//判断是否有注释
							notesMap.put(k, notebuffer.toString());
							notebuffer = new StringBuffer();
						}
						
					} else {
						if(keys.size() == 0) {
							keys.add(l);//没有等号的情况，值为空
						} else {
							int k = keys.size() - 1;
							String key  = keys.get(k);
							String value = valueMap.get(key);
							StringBuffer buffer = new StringBuffer(value);
							buffer.append("\r\n").append(l);
							valueMap.put(key, buffer.toString());
							//keys.add(l);//没有等号的情况，值为空
						}

					}
				}
			}
		} catch (Exception e) {
			//writeLog("文件加载失败："+filepath);
		//	e.printStackTrace();
		}finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

    public synchronized void loadInCustomCharset(String filepath,String charset){
        File file=new File(fileUtil.getPath(filepath));
        FileInputStream fis = null;
        InputStreamReader insreader = null;
        this.filepath = file.getPath();
        try {
            fis = new FileInputStream(file);
            insreader = new InputStreamReader(fis, charset);
            BufferedReader bin = new BufferedReader(insreader);
            List<String> lines = new ArrayList<String>();
            StringBuffer fileinfo=new StringBuffer();
            String line;
            while ((line = bin.readLine()) != null) {
                lines.add(line);
                fileinfo.append(line);
            }
            //迁移过来时有问题，先注释掉
	      /*  if(SVNUtil.isMessyCode(fileinfo.toString())){//是否乱码
				insreader = new InputStreamReader(fis, "UTF-8");
				 bin = new BufferedReader(insreader);
				 lines = new ArrayList<String>();
				while ((line = bin.readLine()) != null) {
					lines.add(line);
				}
			}*/
            bin.close();
            insreader.close();

            // parse key-value
            StringBuffer notebuffer = new StringBuffer();
            for (String l : lines) {
                if(l==null || "".equals(l.trim())) {
                    continue;
                }
                if (l.trim().startsWith("#")||l.trim().startsWith("<!--")) {//以#开头或者<!---->是文件的注释
                    notebuffer.append(l).append("\r\n");
                } else {
                    if (l.indexOf("=") > -1 && !l.trim().startsWith("{")) {//有等号的情况，键值对
                        String k = l.substring(0, l.indexOf("=")).trim();
                        String val = l.substring(l.indexOf("=") + 1).trim();
                        String v = val;
                        String notes = "";
                        //判断值的后面是否有#开头的注释
                        if(val.indexOf(" #")>-1) {
                            v = val.substring(0, l.indexOf(" #"));
                            v = v.trim();
                            notes = val.substring(l.indexOf(" #") + 2);
                            notebuffer.append(notes.trim()).append("\r\n");
                        }
                        keys.add(k);
                        valueMap.put(k, v);
                        if(notebuffer.length()>0) {//判断是否有注释
                            notesMap.put(k, notebuffer.toString());
                            notebuffer = new StringBuffer();
                        }

                    } else {
                        if(keys.size() == 0) {
                            keys.add(l);//没有等号的情况，值为空
                        } else {
                            int k = keys.size() - 1;
                            String key  = keys.get(k);
                            String value = valueMap.get(key);
                            StringBuffer buffer = new StringBuffer(value);
                            buffer.append("\r\n").append(l);
                            valueMap.put(key, buffer.toString());
                            //keys.add(l);//没有等号的情况，值为空
                        }

                    }
                }
            }
        } catch (Exception e) {
            //writeLog("文件加载失败："+filepath);
            //	e.printStackTrace();
        }finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

	/**
	 * 保存Properties文件
	 * @param filepath
	 */
	public synchronized boolean store(String filepath)
	{
		BufferedWriter out = null;
		boolean result = false;
		try {
			List em = keys;
			File fout = new File(fileUtil.getPath(filepath));
			//文件不存在直接返回
			if(!fout.exists()) {
				return false;
			}
			
			//不可编辑的文件设置成可编辑
			if(!fout.canWrite()) {
				fout.setWritable(true);
			}
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fout),"GBK"));
			for(int i = 0;i<em.size();i++)
			{
				String key = Util.null2String((String)em.get(i));
				String tempstring = key;
				if(!"".equals(key)) {
					tempstring += "=" + Util.null2String((String)valueMap.get(key));
				}
				//先写注释
				if(notesMap.get(key) != null) {
					out.write(notesMap.get(key));
					//System.out.println("content:"+notesMap.get(key));
					out.newLine();//换行
				}
				//写入键值对
				out.write(tempstring);
				out.newLine();
			}
			out.flush();
			result = true;
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			//writeLog("文件保存失败："+filepath);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {		
				}
			}
		}
		
		return result;
	}
	
	/** 新建properties文件
	 * 
	 **/
	public synchronized String saveFile(String filepath, String content){
		File file = new File(fileUtil.getPath(filepath));
		try {
			//创建目录
			filepath = file.getPath();
			
			String filedir = filepath.substring(0,filepath.lastIndexOf(File.separatorChar)); 
			File dir = new File(fileUtil.getPath(filedir));
			if(!dir.exists()) {
				dir.mkdirs();
			}
			//创建文件
			if(!file.exists()) {
				file.createNewFile();
			} else {
				return "error";
			}
			
			//以行分隔
			String[] contentArr = content.split("\n");
			
	        StringBuffer notebuffer = new StringBuffer();//注释
	        //将字符串按行读取放入到变量中；再通过store方法来把变量的值写入到本地文件
			for(int i = 0; i < contentArr.length; i++) {
				String l = contentArr[i];
				if(l==null || "".equals(l.trim())) {
					continue;
				}
				if (l.trim().startsWith("#")||l.trim().startsWith("<!--")) {//以#开头或者<!---->是文件的注释
					notebuffer.append(l).append("\r\n");
				} else {
					if (l.indexOf("=") > -1) {//有等号的情况，键值对
						String k = l.substring(0, l.indexOf("=")).trim();
						String val = l.substring(l.indexOf("=") + 1).trim();
						String v = val;
						String notes = "";
						//判断值的后面是否有#开头的注释
						if(val.indexOf("#")>-1) {
							v = val.substring(0, l.indexOf("#"));
							notes = val.substring(l.indexOf("#") + 1);
							notebuffer.append(notes).append("\r\n");
						}
						keys.add(k);
						valueMap.put(k, v);
						if(notebuffer.length()>0) {//判断是否有注释
							notesMap.put(k, notebuffer.toString());
							notebuffer = new StringBuffer();
						}
						
					} else {
						keys.add(l);//没有等号的情况，值为空
					}
				}
			}
			//存储文件
			store(filepath);
			return "ok";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}  

	/**
	 * 根据文件路径，获得属性列表
	 * @param otherparams
	 * @param request
	 * @param response
	 * @return
	 */
	public List<Map<String,String>> getPropList( Map<String,String> otherparams, HttpServletRequest request, HttpServletResponse response) {
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		String fpath = otherparams.get("fpath");
		String excludestatus = otherparams.get("excludestatus");
		//判断是否能编辑该文件，没有编辑权限直接返回空的内容
		if("false".equals(excludestatus)) {
			return list;
		}
		fpath = fpath.replace("#",":");
		//System.out.println("===fpath:"+fpath);
		//加载properties文件
		if(fpath != null) {
			load(fpath);
		}
		for(int i = 0;i < keys.size(); i++) {
			Map<String,String> map = new HashMap<String,String>();
			String tkey = keys.get(i);//键
			String value = valueMap.get(tkey);//值
			String notes = notesMap.get(tkey);//注释
			map.put("attrname",tkey);
			if(value != null) {
				map.put("attrvalue",value);
			}
			if(notes != null) {
				map.put("attrnotes",notes);
			}
			list.add(map);
		}
		return list;
	}
	public void clearKeys(){
	    this.keys.clear();
    }
}

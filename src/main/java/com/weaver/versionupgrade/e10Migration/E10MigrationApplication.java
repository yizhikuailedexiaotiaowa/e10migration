package com.weaver.versionupgrade.e10Migration;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import weaver.init.InitWeaverServer;

@SpringBootApplication
public class E10MigrationApplication {

    public static void main(String[] args) {

        InitWeaverServer initWeaverServer = new InitWeaverServer();
        initWeaverServer.init();

        SpringApplication.run(E10MigrationApplication.class, args);
    }

}

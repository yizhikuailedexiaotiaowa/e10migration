package com.weaver.versionupgrade.e10Migration.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ygd2020
 * @date 2024-1-18 10:21
 * Description:
 **/
@Controller
public class IndexController {


    @ResponseBody
    @GetMapping("/")
    public void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
         response.sendRedirect("/e10Migration/migration.jsp");
    }
}
package com.weaver.versionupgrade.e10Migration.controller;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
 * @author ygd2020
 * @date 2024-1-18 10:21
 * Description:
 **/
@Controller
public class DownloadController {


    @ResponseBody
    @GetMapping("/downloadExcel")
    public  void downLoad( HttpServletResponse response) throws UnsupportedEncodingException {


        File file = new File(ConfigUtil.getExcelPath());
        String fileName= file.getName();

        response.reset();
        response.setHeader("Content-Disposition","attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
        response.setCharacterEncoding("utf-8");
        response.setContentLength((int)file.length());
        response.setContentType("application/octet-stream");
        System.out.println("filename:"+fileName);
        try(BufferedInputStream bis=new BufferedInputStream(new FileInputStream(file)); OutputStream outputStream = response.getOutputStream();)
        {

            byte[] bytes = new byte[1024];
            int i=0;
            while((i=bis.read(bytes))!=-1)
            {
                outputStream.write(bytes,0,i);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
package weaver.e10Migration.excel.file;


import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import weaver.e10Migration.excel.file.bean.ExcelFunctionBean;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.Util;

import java.io.FileOutputStream;

import java.util.*;

/**
 * @param
 * @Author ygd2020
 * @Date 9:27 2023-1-15
 * @Return
 **/
public class ExcelWrite {


    String PATH = "";
    String MODULE = "";
    int rows = 0;//当前行号
    int rowLen = 5;//每个功能之间空多少

    public ExcelWrite(String path, String module) {
        this.PATH = path;
        this.MODULE = module;
        execlInit();
    }

    public void execlInit() {
        Sheet sheet = workbook.createSheet("目录");
        formatColumnWidth(sheet);
        resetRows();
    }

    HashSet<String> allheet = new HashSet<>();
    LinkedHashMap<String, String> validsheet = new LinkedHashMap<>();


    CellStyle bodystyle = null;
    CellStyle headerstyle = null;

    public void setAllSheetnames(String sheetname) {
        allheet.add(sheetname);
    }


    Workbook workbook = new XSSFWorkbook();


    public String removeSpecialChar(String sheetname) {

        String rex = "$";
        sheetname = sheetname.replace("[", rex).replace("]", rex).replace("/", rex);

        return sheetname;
    }

    public Row createRow(Sheet sheet) {
        Row row = sheet.createRow(rows);
        rows++;
        return row;
    }

    public void formatColumnWidth(Sheet sheet) {

        for (int i = 0; i < 100; i++) {
            sheet.setColumnWidth((short) i, (short) (6 * 1000));
        }

    }

    public void createHeaderRow(Sheet sheet, List<String> colsNames) {
        Row row = createRow(sheet);


        for (int i = 0; i < colsNames.size(); i++) {
            row.setHeight((short) 312);
            Cell headCell = row.createCell(i);
            headCell.setCellValue(colsNames.get(i));
            headCell.setCellStyle(getHeaderStyle(HorizontalAlignment.CENTER));
        }

    }


    public CellStyle getHeaderStyle(HorizontalAlignment horizontalAlignment) {


        Font font = workbook.createFont();
        //设置字体大小
        font.setFontHeightInPoints((short) 14);
        //字体加粗
        font.setBold(true);
        //设置字体名字
        font.setFontName("宋体");
        //设置样式;
        CellStyle style = workbook.createCellStyle();
        //设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        //设置底边框颜色;
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        //设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        //设置左边框颜色;
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        //设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        //设置右边框颜色;
        style.setRightBorderColor(IndexedColors.BLACK.index);
        //设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        //设置顶边框颜色;
        style.setTopBorderColor(IndexedColors.BLACK.index);
        //在样式用应用设置的字体;
        style.setFont(font);
        //设置自动换行;
        style.setWrapText(false);
        //设置水平对齐的样式为居中对齐;
        style.setAlignment(horizontalAlignment);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;
    }


    public CellStyle getBodyStyle() {

        if (bodystyle == null) {


            Font font = workbook.createFont();
//        //设置字体大小
//        font.setFontHeightInPoints((short) 14);
//        字体加粗
//        font.setBold(true);
            //设置字体名字
            font.setFontName("宋体");
            //设置样式;
            CellStyle style = workbook.createCellStyle();
            //设置底边框;
            style.setBorderBottom(BorderStyle.THIN);
            //设置底边框颜色;
            style.setBottomBorderColor(IndexedColors.BLACK.index);
            //设置左边框;
            style.setBorderLeft(BorderStyle.THIN);
            //设置左边框颜色;
            style.setLeftBorderColor(IndexedColors.BLACK.index);
            //设置右边框;
            style.setBorderRight(BorderStyle.THIN);
            //设置右边框颜色;
            style.setRightBorderColor(IndexedColors.BLACK.index);
            //设置顶边框;
            style.setBorderTop(BorderStyle.THIN);
            //设置顶边框颜色;
            style.setTopBorderColor(IndexedColors.BLACK.index);
            //在样式用应用设置的字体;
            style.setFont(font);
            //设置自动换行;
            style.setWrapText(true);
            //设置水平对齐的样式为居中对齐;
            style.setAlignment(HorizontalAlignment.CENTER);
            //设置垂直对齐的样式为居中对齐;
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            bodystyle = style;
        }

        return bodystyle;
    }


    public void addMergedRegion(Sheet sheet, List<String> headerNames, String modelName) {

        Row row = createRow(sheet);

        Cell cell = row.createCell(0);
        cell.setCellValue(modelName);
        cell.setCellStyle(getHeaderStyle(HorizontalAlignment.LEFT));


        CellRangeAddress region = new CellRangeAddress(rows - 1, rows - 1, 0, headerNames.size() - 1);
        sheet.addMergedRegion(region);


    }

    public void addNewModel(Sheet sheet, String modelName, List<String> headerNames, List<List<String>> values) {

        allheet.remove(sheet.getSheetName());//有内容就把sheet页删了

        validsheet.put(sheet.getSheetName(), sheet.getSheetName());// 有内容就是需要保留 加链接的

        addMergedRegion(sheet, headerNames, modelName);

        createHeaderRow(sheet, headerNames);

        for (List<String> list : values) {

            Row row = createRow(sheet);


            for (int i1 = 0; i1 < list.size(); i1++) {

                Cell cell = row.createCell(i1);
                cell.setCellStyle(getBodyStyle());
                String value = Util.null2String(list.get(i1));
                if (value.length() > 32767) {
                    System.out.println(value);

                    value = "内容过长 请在系统自行查看";
                }
                cell.setCellValue(value);

            }


        }


        for (int i = 0; i < rowLen; i++) {
            createRow(sheet);//放空行
        }

    }

    private void insertDesc(List<String> headerNames, List<List<String>> values) {
        headerNames.add("业务");
        headerNames.add("e10");
        headerNames.add("是否已处理/无需处理");


        for (List<String> value : values) {

            value.add("");
            value.add("");
            value.add("");
        }

    }


    public CellStyle getLinkStyle(HorizontalAlignment horizontalAlignment) {


        Font font = workbook.createFont();
        //设置字体大小
//        font.setFontHeightInPoints((short) 14);


        font.setUnderline((byte) 1);
        font.setColor(IndexedColors.BLUE.index);
        //设置样式;
        CellStyle style = workbook.createCellStyle();
        //在样式用应用设置的字体;
        style.setFont(font);

        //设置水平对齐的样式为居中对齐;
        style.setAlignment(horizontalAlignment);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;
    }


    public void formatSheet(Sheet sheet) {

        allheet.add(sheet.getSheetName());
        formatColumnWidth(sheet);
        resetRows();
        Row row = createRow(sheet);//用来放返回目录
        Cell cell = row.createCell(0);

        getLinkCell(cell, "目录", "返回目录");

        row.setHeight((short) 312);

    }


    public Sheet getSeet(String getSheetName) {
        getSheetName = removeSpecialChar(getSheetName);
        Sheet sheet = workbook.createSheet(getSheetName);

        System.out.println(getSheetName);

        formatSheet(sheet);


        return sheet;
    }


    public void resetRows() {
        rows = 0;
    }

    public String writeFile() throws Exception {

        //生成一张表(IO流)，03版本就是使用xlsx结尾
        String fileName = this.MODULE + Util.getRandom() + ".xlsx";
        String filePath = PATH + fileName;
        FileOutputStream fos = new FileOutputStream(filePath);
        //输出
        workbook.write(fos);
        //关闭流
        fos.close();
        System.out.println("文件生成完毕");
        return filePath;
    }

    public List<String> getDirectoryHeaderNames() {

        ArrayList<String> strings = new ArrayList<>();

        strings.add("链接");
        return strings;

    }

    public List<List<String>> getDirectoryvalues() {
        List<List<String>> values = new ArrayList<>();

        for (String sheetName : validsheet.keySet()) {

            List<String> strings = new ArrayList<>();
            strings.add(sheetName);
            values.add(strings);
        }

        return values;

    }


    private void dealdirectoryLInk() {
        Sheet sheet = workbook.getSheet("目录");

        resetRows();

        createHeaderRow(sheet, getDirectoryHeaderNames());

        for (List<String> list : getDirectoryvalues()) {

            Row row = createRow(sheet);


            for (int i1 = 0; i1 < list.size(); i1++) {

                Cell cell = row.createCell(i1);
                String value = list.get(i1);

                getLinkCell(cell, value, value);
                row.setHeight((short) 312);


            }


        }

    }

    public String excelWrite(ExcelWriteEntity entity) {
        String filePath = "";
        Map<String, List<ExcelFunctionBean>> sheets = entity.getSheets();

        for (Map.Entry<String, List<ExcelFunctionBean>> entry : sheets.entrySet()) {
            String sheetname = entry.getKey();
            List<ExcelFunctionBean> functionBeanList = entry.getValue();
            Sheet sheet = getSeet(sheetname);
            for (ExcelFunctionBean bean : functionBeanList) {
                String functionName = bean.getFunctionName();

                if (!bean.getValues().isEmpty()) {
                    addNewModel(sheet, functionName, bean.getHeaderNames(), bean.getValues());
                }
            }
        }

        for (String name : allheet) {//没被移出的sheet名字说明没有二开
            if (name.equals("目录")) {
                continue;
            }
            workbook.removeSheetAt(workbook.getSheetIndex(name));
        }

        dealdirectoryLInk();

        try {
            filePath = writeFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }


    public void getLinkCell(Cell cell, String sheetname, String showname) {

        CreationHelper createHelper = workbook.getCreationHelper();
        XSSFHyperlink hyperlink = (XSSFHyperlink) createHelper.createHyperlink(HyperlinkType.DOCUMENT);

        // "#"表示本文档    "明细页面"表示sheet页名称  "A10"表示第几列第几行
        hyperlink.setAddress("#" + sheetname + "!A1");
        cell.setHyperlink(hyperlink);
        // 点击进行跳转
        cell.setCellValue(showname);

        cell.setCellStyle(getLinkStyle(HorizontalAlignment.CENTER));
    }


    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {


        //创建一个模块 功能项
        ExcelFunctionBean functionBean = new ExcelFunctionBean(functionName);

        //功能有多少列，指定每个列的列名
        functionBean.setHeaderNames(headerNames);

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        functionBean.setValues(values);

        List<ExcelFunctionBean> list = new ArrayList<>();//一个sheet页有多少功能项
        list.add(functionBean);
        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            sheets.get(sheetname).addAll(list);
        } else { //不存在当前sheet页，维护新的
            sheets.put(sheetname, list);
        }
        writeEntity.setSheets(sheets);
    }


}

package weaver.e10Migration.excel.file.bean;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author ygd2020
 * @date 2023-7-4 10:51
 * Description:
 **/
public class ExcelWriteEntity {

    public String module = "";// 模块名称  excel名称
    public String path = "";


    public LinkedHashMap<String, List<ExcelFunctionBean>> sheets;// sheet名称 +sheet页中的功能项集合


    public ExcelWriteEntity(String module, String path) {
        this.module = module;
        this.path = path;
    }

    public LinkedHashMap<String, List<ExcelFunctionBean>> getSheets() {
        if (sheets == null) {
            return new LinkedHashMap<>();
        }
        return sheets;
    }

    public ExcelWriteEntity setSheets(LinkedHashMap<String, List<ExcelFunctionBean>> sheets) {
        this.sheets = sheets;
        return this;
    }

    public String getModule() {
        return module;
    }

    public ExcelWriteEntity setModule(String module) {
        this.module = module;
        return this;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

package weaver.e10Migration.excel.file.bean;

import java.util.Collections;
import java.util.List;

/**
 * @author ygd2020
 * @date 2023-7-4 10:53
 * Description:
 **/
public class ExcelFunctionBean {

    public String functionName;// 功能项名称


    public List<String> headerNames; // 功能列名


    public List<List<String>> values; // 功能行数+功能详细描述

    public ExcelFunctionBean(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public ExcelFunctionBean setFunctionName(String functionName) {
        this.functionName = functionName;
        return this;
    }

    public List<String> getHeaderNames() {
        if (headerNames == null) {
            return Collections.emptyList();
        }

        return headerNames;
    }

    public ExcelFunctionBean setHeaderNames(List<String> headerNames) {
        this.headerNames = headerNames;
        return this;
    }

    public List<List<String>> getValues() {
        if (values == null) {
            return Collections.emptyList();
        }

        return values;
    }

    public ExcelFunctionBean setValues(List<List<String>> values) {
        this.values = values;
        return this;
    }
}

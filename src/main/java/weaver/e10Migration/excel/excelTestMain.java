package weaver.e10Migration.excel;

import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelFunctionBean;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class excelTestMain {
    public static void main(String[] args) {
  // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("表单建模","");

        //创建一个模块 功能项
        ExcelFunctionBean functionBean = new ExcelFunctionBean("权限修改");

        //功能有多少列，指定每个列的列名
        functionBean.setHeaderNames(modeShareDatarefactorHeaderNames1());

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        functionBean.setValues(modeShareDatarefactorValue1(""));

        List<ExcelFunctionBean> list = new ArrayList<>();//一个sheet页有多少功能项

        //四项
        list.add(functionBean);
        list.add(functionBean);
        list.add(functionBean);
        list.add(functionBean);


        LinkedHashMap<String, List<ExcelFunctionBean>> map = new LinkedHashMap<>();


        map.put("权限相关", list);// key是sheet页名称，list 是sheet有多少功能项
        map.put("权限相关2", list);
        map.put("权限相关3", list);

        writeEntity.setSheets(map);
        ExcelWrite excelWrite= new ExcelWrite("","");
        excelWrite.excelWrite(writeEntity);

    }
	
	

    public static List<List<String>> modeShareDatarefactorValue1(String appid) {

        List<List<String>> values = new ArrayList<>();


        for (int i = 0; i < 10; i++) {


            List<String> valueList = new ArrayList<>();

            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            valueList.add("permission_id");
            values.add(valueList);

        }


        return values;

    }


    public static List<String> modeShareDatarefactorHeaderNames1() {

        ArrayList<String> headerNames = new ArrayList<>();
        headerNames.add("权限id");
        headerNames.add("权限类型");
        headerNames.add("权限类型-翻译");
        headerNames.add("org_id");
        headerNames.add("org_id-翻译");
        headerNames.add("创建时间");
        headerNames.add("链接");
        headerNames.add("创建人");
        headerNames.add("创建人id");
        headerNames.add("查询名称");
        headerNames.add("表单名");
        headerNames.add("表名");
        headerNames.add("ob_id");


        return headerNames;
    }


}

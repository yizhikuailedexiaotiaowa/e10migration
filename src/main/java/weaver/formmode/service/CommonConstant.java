package weaver.formmode.service;

import weaver.conn.RecordSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonConstant {
	/*布局类型*/
	public final static Integer ENTITY_VIEWLAYOUT_TYPE = 0;
	public final static Integer ENTITY_CREATELAYOUT_TYPE = 1;
	public final static Integer ENTITY_EDITLAYOUT_TYPE = 2;
	public final static Integer ENTITY_MONITORLAYOUT_TYPE = 3;
	public final static Integer ENTITY_PRINTLAYOUT_TYPE = 4;
	
	public final static String DB_TYPE;
	//数据库连接符号
	public final static String DB_CONCAT_SYMBOL;
	public final static String DB_ISNULL_FUN;
	public final static String DB_SUBSTR_FUN;
	
	public static String DB_MYSQL_SCHEMA = "";

	static{
		RecordSet rs = new RecordSet();
		String dbtype = rs.getDBType();
		DB_TYPE = dbtype;
		if("oracle".equals(dbtype)){
			DB_CONCAT_SYMBOL = "||";
			DB_ISNULL_FUN="nvl";
			DB_SUBSTR_FUN="substr";
		}else if("mysql".equals(dbtype)){//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
			DB_CONCAT_SYMBOL = "concat";//请使用getConcatSql(List<String> list,String dbtype)代替处理字符串链接
			DB_ISNULL_FUN="ifnull";
			DB_SUBSTR_FUN="substr";
			String sql = "select database() as dbname";//当前链接的数据库名
			rs.execute(sql);
			if(rs.next()){
				DB_MYSQL_SCHEMA = rs.getString("dbname");
			}
		}
		else if("postgresql".equals(dbtype.toLowerCase())){
			DB_CONCAT_SYMBOL = "||";
			DB_ISNULL_FUN="COALESCE";
			DB_SUBSTR_FUN="substr";
		}
		else{
			DB_CONCAT_SYMBOL = "+";
			DB_ISNULL_FUN="isnull";
			DB_SUBSTR_FUN="substring";
		}
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("','");
		list.add("allSuperFieldId");
		list.add("','");
		String sqlConcat = CommonConstant.getConcatSql(list, "mysql");
	}
	
	public static String getConcatSql(String ...params){
		String sql = "";
		if(params.length>0){
			String con = "";
			if("oracle".equals(DB_TYPE)){
				con = "||";
				for(int i=0;i<params.length;i++){
					if(i==params.length-1){
						con = "";
					}
					sql += params[i]+con;
				}
			}
			else if("postgresql".equals(DB_TYPE)){
				con = "||";
				for(int i=0;i<params.length;i++){
					if(i==params.length-1){
						con = "";
					}
					sql += params[i]+con;
				}
			}
			else if("mysql".equals(DB_TYPE)){//mysql使用concat拼接字符串，这个使用的时候需要注意
				sql = "concat(";
				for(int i=0;i<params.length;i++){
					con = ",";
					if(i==params.length-1){
						con = "";
					}
					sql += params[i]+con;
				}
				sql += ")";
			}else if("sqlserver".equals(DB_TYPE)){
				con = "+";
				for(int i=0;i<params.length;i++){
					if(i==params.length-1){
						con = "";
					}
					sql += params[i]+con;
				}
			}
		}
		return sql;
	}

	/**
	 * 用来拼接数据库的字符串链接
	 * @param list 多个字符串放在此list中
	 * @param dbtype
	 * @return
	 */
	public static String getConcatSql(List<String> list,String dbtype){
		String sql = "";
		if(list.size()>0){
			String con = "";
			if("oracle".equals(dbtype)||"postgresql".equals(dbtype)){
				con = "||";
				for(int i=0;i<list.size();i++){
					if(i==list.size()-1){
						con = "";
					}
					sql += list.get(i)+con;
				}
			}else if("mysql".equals(dbtype)){//mysql使用concat拼接字符串，这个使用的时候需要注意
				sql = "concat(";
				for(int i=0;i<list.size();i++){
					con = ",";
					if(i==list.size()-1){
						con = "";
					}
					sql += list.get(i)+con;
				}
				sql += ")";
			}else if("sqlserver".equals(dbtype)){
				con = "+";
				for(int i=0;i<list.size();i++){
					if(i==list.size()-1){
						con = "";
					}
					sql += list.get(i)+con;
				}
			}else if("gs".equals(dbtype)){//gs使用concat拼接字符串
				sql = "concat(";
				for(int i=0;i<list.size();i++){
					con = ",";
					if(i==list.size()-1){
						con = "";
					}
					sql += list.get(i)+con;
				}
				sql += ")";
			}
		}
		return sql;
	}

	public static String toChar(String fieldname){
		String str = "";
		if("oracle".equals(DB_TYPE)){
			str="to_char("+fieldname+")";
		}else if("mysql".equals(DB_TYPE)){//mysql使用concat拼接字符串，这个使用的时候需要注意
			str="CAST("+fieldname+" AS CHAR)";
		}else if("sqlserver".equals(DB_TYPE)){
			str="cast("+fieldname+" as varchar)";
		}
		else if("postgresql".equals(DB_TYPE)){
			str="to_char("+fieldname+")";
		}
	
		return str;
	}
	
	
	public final static Map<String, String> SOURCECODE_PATH_MAP = new HashMap<String, String>();
	static{
		SOURCECODE_PATH_MAP.put("1", "formmode/sourcecode/pk/");
		SOURCECODE_PATH_MAP.put("2", "formmode/sourcecode/customsearch/");
		SOURCECODE_PATH_MAP.put("3", "formmode/sourcecode/browser/");
		SOURCECODE_PATH_MAP.put("4", "formmode/sourcecode/mobile/numremind/");
		SOURCECODE_PATH_MAP.put("5", "formmode/sourcecode/modeexpand/");
		SOURCECODE_PATH_MAP.put("6", "formmode/sourcecode/moderightinfo/");
		SOURCECODE_PATH_MAP.put("7", "formmode/sourcecode/remindjob/");
		SOURCECODE_PATH_MAP.put("8", "formmode/sourcecode/formlayout/");
		SOURCECODE_PATH_MAP.put("9", "formmode/sourcecode/batchImport/");
		SOURCECODE_PATH_MAP.put("10", "formmode/sourcecode/customButton/");
		SOURCECODE_PATH_MAP.put("11", "formmode/sourcecode/remindaction/");
		SOURCECODE_PATH_MAP.put("12", "formmode/sourcecode/batchImport/");
		SOURCECODE_PATH_MAP.put("13", "formmode/sourcecode/customsearch/");
		SOURCECODE_PATH_MAP.put("15", "formmode/sourcecode/interface/");
		SOURCECODE_PATH_MAP.put("16", "formmode/sourcecode/modeexpand/");
	}

	public final static Map<String, String> SOURCECODE_PACKAGENAME_MAP = new HashMap<String, String>();
	static{
		SOURCECODE_PACKAGENAME_MAP.put("2", "weaver.formmode.customjavacode.customsearch");
		SOURCECODE_PACKAGENAME_MAP.put("3", "weaver.formmode.customjavacode.browser");
		SOURCECODE_PACKAGENAME_MAP.put("4", "weaver.formmode.customjavacode.mobile.numremind");
		SOURCECODE_PACKAGENAME_MAP.put("5", "weaver.formmode.customjavacode.modeexpand");
		SOURCECODE_PACKAGENAME_MAP.put("6", "weaver.formmode.customjavacode.moderightinfo");
		SOURCECODE_PACKAGENAME_MAP.put("7", "weaver.formmode.customjavacode.remindjob");
		SOURCECODE_PACKAGENAME_MAP.put("8", "weaver.formmode.customjavacode.formlayout");
	}
}

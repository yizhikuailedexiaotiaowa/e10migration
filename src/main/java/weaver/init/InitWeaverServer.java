package weaver.init;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import weaver.conn.RecordSet;

/**
 * @author ygd2020
 * @date 2024-1-18 9:10
 * Description:
 **/
public class InitWeaverServer {
    public void init() {

        ConfigUtil.initConfig();

        initDb();

    }

    private void initDb() {
//
//        RecordSet rs = new RecordSet();
//        String dbtype = rs.getDBType();
//        DB_TYPE = dbtype;
//        if("oracle".equals(dbtype)){
//            DB_CONCAT_SYMBOL = "||";
//            DB_ISNULL_FUN="nvl";
//            DB_SUBSTR_FUN="substr";
//        }else if("mysql".equals(dbtype)){//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
//            DB_CONCAT_SYMBOL = "concat";//请使用getConcatSql(List<String> list,String dbtype)代替处理字符串链接
//            DB_ISNULL_FUN="ifnull";
//            DB_SUBSTR_FUN="substr";
//            String sql = "select database() as dbname";//当前链接的数据库名
//            rs.execute(sql);
//            if(rs.next()){
//                DB_MYSQL_SCHEMA = rs.getString("dbname");
//            }
//        }
//        else if("postgresql".equals(dbtype.toLowerCase())){
//            DB_CONCAT_SYMBOL = "||";
//            DB_ISNULL_FUN="COALESCE";
//            DB_SUBSTR_FUN="substr";
//        }
//        else{
//            DB_CONCAT_SYMBOL = "+";
//            DB_ISNULL_FUN="isnull";
//            DB_SUBSTR_FUN="substring";
//        }

    }
}

package weaver.systeminfo;

import weaver.conn.RecordSet;

/**
 * @author ygd2020
 * @date 2024-1-18 9:28
 * Description:
 **/
public class SystemEnv {

    public static String getHtmlLabelName(String labelid) {
        return getHtmlLabelName(labelid, 7);
    }

    public static String getHtmlLabelName(int  labelid, int deflang) {
        return getHtmlLabelName(labelid+"", deflang);
    }

    public static String getHtmlLabelName(String labelid, int deflang) {
        RecordSet rs = new RecordSet();

        rs.executeQuery("select labelname  from htmllabelinfo where indexid = ? and languageid = ?", labelid, deflang);

        if (rs.next()) {
            return rs.getString("labelname");
        }
        return "";
    }
}

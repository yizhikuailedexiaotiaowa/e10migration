package weaver.hrm.company;

import weaver.conn.RecordSet;

/**
 * @author ygd2020
 * @date 2024-1-18 9:24
 * Description:
 **/
public class SubCompanyComInfo {

    public String getSubCompanyname(String id) {

        RecordSet recordSet = new RecordSet();

        recordSet.executeQuery("select id,subcompanyname\n" +
                "from hrmsubcompany\n" +
                "union all\n" +
                "select id,subcompanyname\n" +
                "from hrmsubcompanyvirtual\n" +
                "where id = ?", id);

        if (recordSet.next()) {
            return recordSet.getString("subcompanyname");
        }
        return "";
    }
}

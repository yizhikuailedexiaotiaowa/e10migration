package weaver.hrm.resource;

import weaver.conn.RecordSet;

/**
 * @author ygd2020
 * @date 2024-1-18 9:16
 * Description:
 **/
public class ResourceComInfo {
    public String getLastname(String userid) {

        RecordSet rs = new RecordSet();

        rs.executeQuery("\n" +
                "select id,lastname\n" +
                "from hrmresource\n" +
                "union all\n" +
                "select id,lastname\n" +
                "from hrmresourcemanager\n" +
                " where id = ?", userid);

        if (rs.next()) {
            return rs.getString("lastname");
        }
        return "";
    }

}

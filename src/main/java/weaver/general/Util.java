package weaver.general;

import com.weaver.versionupgrade.customRest.util.Constant;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

/**
 * @author cx
 * @date 2022/3/3
 **/
public class Util {
    //    public static SimpleDateFormat SDFTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
//        public static SimpleDateFormat SDFDate = new SimpleDateFormat("yyyy-MM-dd") ;
    private static Random random = new Random();
    /**
     * 为了线程安全
     * 使用ThreadLocal，令每个线程创建一个当前线程的SimpleDateFormat的实例对象。
     */
    public static ThreadLocal<DateFormat> SDFTimeLocal = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        }
    };

    public static ThreadLocal<DateFormat> SDFTimeLocal2 = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd HH:mm");

        }
    };
    /**
     * 为了线程安全
     * 使用ThreadLocal，令每个线程创建一个当前线程的SimpleDateFormat的实例对象。
     */
    public static ThreadLocal<DateFormat> SDFDateLocal = new ThreadLocal<DateFormat>() {

        @Override

        protected DateFormat initialValue() {

            return new SimpleDateFormat("yyyy-MM-dd");

        }
    };


    public static String null2String(Object s) {
        return s == null ? "" : s.toString();

    }

    public static String null2String(String s) {
        return s == null ? "" : s;

    }

    public static String null2String(String s, String def) {

        return (s == null || s.trim().length() <= 0) ? (def == null ? "" : def) : s;

    }


    // add by wangwei

    public static int getIntValue(String v) {
        return getIntValue(v, -1);
    }

    public static int getIntValue(String v, int def) {
        try {
            if (v.indexOf(".") > -1) {
                v = v.substring(0, v.indexOf("."));
            }
            return Integer.parseInt(v);
        } catch (Exception ex) {
            return def;
        }
    }


    public static Short getShortValue(String v) {
        return getShortValue(v, Short.valueOf("0"));
    }

    public static Short getShortValue(String v, Short def) {
        try {
            if (v.indexOf(".") > -1) {
                v = v.substring(0, v.indexOf("."));
            }
            return Short.valueOf(v);
        } catch (Exception ex) {
            return def;
        }
    }


    public static long getLongValue(String v) {
        return getLongValue(v, -1L);
    }

    public static long getLongValue(String v, long def) {
        try {
            if (v != null && v.indexOf(".") > 0) {
                v = v.substring(0, v.indexOf("."));
            }
            return Long.parseLong(v);
        } catch (Exception ex) {
            return def;
        }
    }

    public static int getIntValue(int v) {
        return v;
    }


    public static float getFloatValue(String v) {
        return getFloatValue(v, -1);
    }

    public static float getFloatValue(String v, float def) {
        try {
            return Float.parseFloat(v);
        } catch (Exception ex) {

            return def;
        }
    }

    public static double getDoubleValue(String v) {
        return getDoubleValue(v, -1);
    }

    public static double getDoubleValue(String v, double def) {
        try {
            return Double.parseDouble(v);
        } catch (Exception ex) {
            return def;
        }
    }

    public static String formatInValue(String values) {
        if (values != null) {
            if (values.endsWith(",")) {
                values = values.substring(0, values.length() - 1);
                //递归遍历
                formatInValue(values);
            }
        }

        return values;
    }

    public static final String LANG_CONTENT_PREFIX = "~`~`";
    public static final int LANG_CONTENT_PREFIX_LEN = LANG_CONTENT_PREFIX.length();
    public static final String LANG_CONTENT_SPLITTER1 = "`~`";
    public static final String LANG_CONTENT_SUFFIX = "`~`~";
    public static final String LANG_INPUT_PREFIX = "__multilangpre_";
    public static final String langRegP = "(?<!value {0,3}= {0,3}('|\"))(~`~`(([\\d ]{1,2}[^`]{0,2000}?`~`)+[\\d ]{1,2}[^`]{0,2000}?)`~`~).{0,2000}?";
    public static final Pattern langRegPattern = Pattern.compile(langRegP, Pattern.CASE_INSENSITIVE);


    public static void main(String args[]) {
//
//        try {
//            String fieldValue = "2023-02-08 11:43:00:1";
//            System.out.println(Util.pareDateTime2Long(fieldValue));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        System.out.println(getShortValue("-1"));

//        String str11="~`~`7 代码认证管理员`~`8 Code certification administrator`~`9 代碼認證管理員`~`~";
//        Pattern  pattern= Pattern.compile("(~`~`7)(.*?)(`~`8.*)");
//       Matcher matcher= pattern.matcher(str11);
//        if(matcher.find()){
////            System.out.println(matcher.group(2));
//        }

//        System.out.println(str11.replaceAll("(~`~`7)(.*?)(`~`8.*)","$2"));


//                System.out.println(formatMultiLang("~`~`7 出纳`~`8 cashier`~`9 出納`~`~"));
//        String str=" ~`~`7 延安`~`8 Yan'an`~`9 延安`~`~";
//        String str1="123~`~`7 北京`~`8 Beijing`~`9 北京`~`~";
//        String str2="KH071206:申请-&gt;~`~`7 典型客户`~`8 Typical customers`~`9 典型客戶`~`~";
//
////        System.out.println(formatMultiLang(str2));
//
//        long currentime=System.currentTimeMillis();
//          int a=0;
//        while(a<1000000){
//            formatMultiLang2Map(a+str2);
//            a++;
//        }
//
//        long currentime2=System.currentTimeMillis();
//
//        int i=0;
//        while(i<1000000){
//            formatMultiLang(i+str2);
//            i++;
//        }
//        long currentime3=System.currentTimeMillis();
//
//        System.out.println(currentime);
//        System.out.println(currentime2);
//        System.out.println(currentime3);
//
//        System.out.println("formatMultiLang2Map:::"+(currentime2-currentime));
//        System.out.println("formatMultiLang:::"+(currentime3-currentime2));

        //        System.out.println(formatMultiLang2Map(str2));
//
    }

    public static String formatMultiLang(String s) {
        return formatMultiLang(s, "7");

    }

    public static String processBody(String body) {
        return processBody(body, "7");
    }

    public static String processBody(String body, String deflang) {
        //~`~`7 转发收回zs`~`8 转发收回zs`~`9 转发收回zs`~`~
        if (body != null && body.contains("~`~`" + deflang)) {

            int start = body.indexOf("~`~`" + deflang) + 5;
            int end = body.indexOf("`~`8");
            //存在没有8的情况
            if (end < 0) {
                end = body.indexOf("`~`9");
            }
            if (end > start) {
                body = body.substring(start, end).trim();
            }
        }
        return body;
    }

    public static String formatMultiLang(String s, String deflang) {
        String lang = Util.null2String(deflang);
        if (s == null || "".equals(s) || s.indexOf(LANG_CONTENT_PREFIX) == -1) return s;
        int count = 0;
        while (s.indexOf(LANG_CONTENT_PREFIX) > -1 && s.indexOf(LANG_CONTENT_SUFFIX) > -1) {
            count++;
            if (count > 10000) {  //错误数据，有前后缀标识，但没有中缀等异常情况
                break;
            }
            int pos = s.indexOf(LANG_CONTENT_PREFIX);
            String s1 = s.substring(pos + LANG_CONTENT_PREFIX.length(), s.length());
            int pos1 = s1.indexOf(LANG_CONTENT_PREFIX);
            int pos2 = s1.indexOf(LANG_CONTENT_SUFFIX);
            if (pos1 > -1 && (pos1 < pos2)) {
                String s2 = s1.substring(pos1, pos2 + LANG_CONTENT_SUFFIX.length());
                String s_replace = Util.formatMultiLangOld(s2, lang);
                String s_pre = s.substring(0, pos + LANG_CONTENT_PREFIX.length() + pos1);
                String s_suffix = s.substring((pos + LANG_CONTENT_PREFIX.length() + pos2 + LANG_CONTENT_SUFFIX.length()), s.length());
                s = s_pre + s_replace + s_suffix;
            } else {
                s = Util.formatMultiLangOld(s, lang);
            }
        }
        return s;
    }


    private static String formatMultiLangOld(String s, String lang) {
        if (s == null || "".equals(s) || s.indexOf(LANG_CONTENT_PREFIX) == -1) return s;
        Matcher m = langRegPattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        boolean result = m.find();
        if (lang.equals("")) lang = "7";
        while (result) {
            String var = m.group(2);
            String value = transLang(var, lang);
            String sb2 = value;
            value = value.replace("\\", "\\\\").replace("$", "\\$");
            try {
                m.appendReplacement(sb, value);
            } catch (Exception e) {
            }
            result = m.find();
        }
        m.appendTail(sb);
        return sb.toString();
    }

    private static String transLang(String var, String lang) {
        ////System.out.println(var);
        boolean isExsit = false;
        //如果没有设过相应语言的翻译，返回中翻译作为默认值
        String defaultValue = "";

        String oriLang = lang;
        if (lang.length() < 2) lang += " ";
        var = var.replace("&nbsp;", " ");
        ////System.out.println("2::"+var+"::"+lang+"::"+ThreadVarManager.getMultiLang());
        int from = var.indexOf(LANG_CONTENT_PREFIX);
        int to = var.lastIndexOf(LANG_CONTENT_SUFFIX);
        if (from != -1 && to != -1 && from + 4 < to) {
            String[] ls = var.substring(from + 4, to).split(LANG_CONTENT_SPLITTER1);
            for (String lg : ls) {
                if (lg.startsWith("7 ")) {
                    defaultValue = lg.substring(lang.length());
                }
                if (lg.startsWith(lang)) {
                    isExsit = true;
                    return lg.substring(lang.length());
                } else if (lg.startsWith(oriLang)) {
                    isExsit = true;
                    return lg.substring(oriLang.length());
                }
            }
        }
        if (!isExsit) {
            return defaultValue;
        }
        return var;
    }

    /**
     * 多语言转换
     *
     * @param content
     * @return
     */
    public static HashMap<String, String> formatMultiLang2Map(String content) {
        //~`~`7 转发收回`~`8 转发收回`~`9 转发收回`~`~
        HashMap<String, String> multLangMap = new HashMap<String, String>();
        if (content != null && content.indexOf(LANG_CONTENT_PREFIX) > -1
                && content.trim().startsWith(LANG_CONTENT_PREFIX)) {
            int from = content.indexOf(LANG_CONTENT_PREFIX);
            int to = content.lastIndexOf(LANG_CONTENT_SUFFIX);
            if (from != -1 && to != -1 && from + LANG_CONTENT_PREFIX_LEN < to) {
                String preContent = "";
                if (from > 0) {
                    preContent = content.substring(0, from);
                }
                content = content.substring(from + LANG_CONTENT_PREFIX_LEN, to);
                String[] multArray = content.split(LANG_CONTENT_SPLITTER1);
                for (int i = 0; i < multArray.length; i++) {
                    String item = multArray[i];
                    item = item.trim();
                    if (item.length() > 2) {
                        //前两位预留给语言类型
                        String lang = item.substring(0, 2).trim();
                        String langContent = item.substring(2);
                        multLangMap.put(lang, langContent);
                    }
                }
            } else {//格式错误的，当作普通字符串处理
                //返回所有值的话，不玩map中插入数据，直接获得元数据即可
                multLangMap.put("all", content);
            }
        } else {
            //返回所有值的话，不玩map中插入数据，直接获得元数据即可
            multLangMap.put("all", content);
        }
        return multLangMap;
    }


    public static long pareTime2Long(String datetime) throws ParseException {
//        return SDFTime.parse(datetime).getTime();
        if (datetime.indexOf(":") == datetime.lastIndexOf(":")) {
            return SDFTimeLocal2.get().parse(datetime).getTime();
        } else {
            return SDFTimeLocal.get().parse(datetime).getTime();
        }


    }

    public static long pareDate2Long(String datetime) throws ParseException {
//        return SDFDate.parse(datetime).getTime();
        return SDFDateLocal.get().parse(datetime).getTime();
    }

    public static long pareDateTime2Long(String datetime) throws ParseException {
        if (datetime.indexOf(":") > -1) {
            return pareTime2Long(datetime);
        } else {
            return pareDate2Long(datetime);
        }
    }

    /**
     * 判断空值
     *
     * @param v
     * @return
     */
    public static boolean checkValNull(String v) {
        if (v == null || "".equals(v) || "null".equals(v.toLowerCase())) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 格式化获得第一条记录的sql
     *
     * @param sql
     * @return
     */
    public static String formatTop1Sql(String sql, String dbType) {
        if (Constant.MYSQL.equalsIgnoreCase(dbType)) {
            sql = sql + " limit 1";
        } else if (Constant.POSTGRESQL.equalsIgnoreCase(dbType)) {
            sql = sql + " limit 1";
        } else if (Constant.ORACLE.equalsIgnoreCase(dbType)) {
            //需要sql中有order by
            sql = sql.replaceFirst("(?i)order by ", " and rownum=1 order by ");
        } else if (Constant.SQLSERVER.equalsIgnoreCase(dbType)) {
            sql = sql.replaceFirst("(?i)select ", "select top 1 ");
        }
        return sql;
    }


    public static String StringReplace(String sou, String s1, String s2) {
        sou = null2String(sou);
        s1 = null2String(s1);
        s2 = null2String(s2);
        try {
            sou = sou.replace(s1, s2);
        } catch (Exception e) {
            //System.out.println(e);//将未知异常打印出来，便于检查错误。
        }
        return sou;
    }

    /**
     * 格式化错误信息
     *
     * @param e
     * @return
     */
    public static String formatException(Exception e) {
        StringBuffer stringBuffer = new StringBuffer();
        StackTraceElement stackTraceElement = e.getStackTrace()[0];// 得到异常棧的首个元素
        stringBuffer.append("File=" + stackTraceElement.getFileName()).append("\r\n");// 打印文件名
        stringBuffer.append("Line=" + stackTraceElement.getLineNumber()).append("\r\n");// 打印出错行号
        stringBuffer.append("Method=" + stackTraceElement.getMethodName()).append("\r\n");// 打印出错方法
        stringBuffer.append("Error=" + e.toString());// 打印出错方法


        return stringBuffer.toString();
    }


    /**
     * 提供精确的小数位四舍五入处理。
     *
     * @param v     需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */

    public static double round(double v, int scale) {

        if (scale < 0) {

            throw new IllegalArgumentException("The scale must be a positive integer or zero");

        }

        BigDecimal b = new BigDecimal(Double.toString(v));

        BigDecimal one = new BigDecimal("1");

        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();

    }


    /**
     * 替换首个内容 解决Illegal group reference问题
     *
     * @param content
     * @param regex
     * @param replacement
     * @return
     */
    public static String replaceFirst(String content, String regex, String replacement) {
        //if(content.indexOf("$") > -1) {
        content = content.replaceAll("\\$", "RDS_CHAR_DOLLAR");// encode replacement;
        content = content.replaceFirst(regex, replacement);
        content = content.replaceAll("RDS_CHAR_DOLLAR", "\\$");// decode replacement;
        //}

        return content;
    }


    /**
     * 计算crc32值
     *
     * @param content
     * @return
     */
    public static long getCRC32(String content) {
        CRC32 crc32 = new CRC32();
        crc32.update(content.getBytes());
        return crc32.getValue();
    }

    /**
     * 日期
     *
     * @return
     */
    public static String getNowCurrentDate() {
        String timestrformart = "yyyy-MM-dd";
        SimpleDateFormat SDF = new SimpleDateFormat(timestrformart);
        Calendar calendar = Calendar.getInstance();
        return SDF.format(calendar.getTime());
    }

    /**
     * 格式化值
     * 保留两位小数
     *
     * @param value
     * @return
     */
    public static String format2(double value) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value);
    }

    /**
     * 时间
     *
     * @return
     */
    public static String getNowCurrentTime() {
        String timestrformart = "HH:mm:ss";
        SimpleDateFormat SDF = new SimpleDateFormat(timestrformart);
        Calendar calendar = Calendar.getInstance();
        return SDF.format(calendar.getTime());
    }


    public static String getRandom() {
        int randomInt = 1000000000 + random.nextInt(1000000000);
        while (randomInt == 0) {
            randomInt = 1000000000 + random.nextInt(1000000000);
        }
        return String.valueOf(randomInt);
    }

    public static char getSeparator() {
        return 2;
    }
}

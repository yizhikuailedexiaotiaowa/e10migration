package weaver.general;

/**
 * Title:        weaversales
 * Description:  weaver sales for mrp (order , inventory , sales)
 * Copyright:    Copyright (c) 1995
 * Company:      weaver
 * @author liuyu
 * @version 1.0
 */


import com.weaver.versionupgrade.util.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class BaseBean {


	private int maxModuleNameLen = 20;
    public String getPropValue(String fname , String key)  {
		return PropertiesUtil.getPropValue(fname , key) ;
    }



    public void writeLog(Object obj) {
        writeLog(getClass().getName(),obj);
    }


    public void writeLog(String classname, Object obj) {
        Logger log = LoggerFactory.getLogger(getClass());
        if (obj instanceof Exception)
            log.error(classname, (Exception) obj);
        else {
            log.error(obj + "");
        }
    }

   /* private void infoLogNew(String a2, Object obj) {
        WLog4jFactory.getMoudleLogger(a2).info(obj);
    }

    private void errorLogNew(String a2, Object obj) {
        WLog4jFactory.getMoudleLogger(a2).info(obj);
    }*/


    /**
     * 此处是获取线程号
     * @return
     */
    private  Object getThreadInfo(){
        return  String.valueOf(Thread.currentThread().getName()+"-"+Thread.currentThread().getId());
    }

}
package wscheck;


import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;
import weaver.file.FileSecurityUtil;
import weaver.general.BaseBean;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.GZIPOutputStream;


public class ZipUtils
{
	private static int BUFFER = 1024 * 4; // 缓冲大小
	private static byte[] B_ARRAY = new byte[BUFFER];
	/**
	 * 压缩文件
	 * @param sourceDir
	 * @param zipFile
	 */
	public static void zip(String sourceDir, String zipFile) throws Exception
	{
		OutputStream os = null;
		BufferedOutputStream bos = null;
		ZipOutputStream zos = null;
		try
		{
			File zip = new File(zipFile);
			if (!zip.getParentFile().exists())
				zip.getParentFile().mkdirs();
			os = new FileOutputStream(zipFile);
			bos = new BufferedOutputStream(os);
			zos = new ZipOutputStream(bos);
			File file = new File(sourceDir);
			String basePath = null;
			if (file.isDirectory())
			{
				basePath = file.getPath();
			}
			else
			{
				basePath = file.getParent();
			}
			basePath = basePath+""+File.separatorChar;
			zipFile(file, basePath, zos);
			zos.closeEntry();
		}
		catch (Exception e)
		{
			new BaseBean().writeLog("压缩文件出错:"+e);
			throw e;
		}
		finally
		{
			try
			{
				if(null!=zos)
				{
					zos.close();
				}
			}
			catch(Exception e)
			{
				
			}
			try
			{
				if(null!=bos)
				{
					bos.close();
				}
			}
			catch(Exception e)
			{
				
			}
			try
			{
				if(null!=os)
				{
					os.close();
				}
			}
			catch(Exception e)
			{
				
			}
		}
	}

	/**
	 * 递归压缩目录下的所有文件
	 * @param source
	 * @param basePath
	 * @param zos
	 */
	private static void zipFile(File source, String basePath, ZipOutputStream zos) throws Exception
	{
		File[] files = new File[0];
		if (source.isDirectory())
		{
			files = source.listFiles();
		}
		else
		{
			files = new File[1];
			files[0] = source;
		}
		String pathName;
		byte[] buf = new byte[1024];
		int length = 0;
		try
		{
			for (File file : files)
			{
				if (file.isDirectory())
				{
					pathName = file.getPath().substring(basePath.length())+""+File.separatorChar;
					//System.out.println("pathName 1 : "+pathName);
					//zos.putNextEntry(new ZipEntry(pathName));
					zipFile(file, basePath, zos);
				}
				else
				{
					pathName = file.getPath().substring(basePath.length());
					ZipEntry zipEntry = new ZipEntry(pathName);

					zipEntry.setSize(file.length());
					zipEntry.setTime(file.lastModified());
					//System.out.println("pathName 2 : "+pathName);
					InputStream is = new FileInputStream(file);
					BufferedInputStream bis = new BufferedInputStream(is);
					zos.putNextEntry(zipEntry);
					while ((length = bis.read(buf)) > 0)
					{
						zos.write(buf, 0, length);
					}
					bis.close();
					is.close();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			new BaseBean().writeLog("压缩文件出错 zipFile : "+e);
			throw e;
		}
	}

	/**
	 * 解压 zip 文件，注意不能解压 rar 文件哦，只能解压 zip 文件 解压 rar 文件 会出现 java.io.IOException:
	 * Negative seek offset 异常 
	 * 
	 * @param zipfile zip 文件，注意要是正宗的 zip 文件哦，不能是把 rar 的直接改为 zip 这样会出现 java.io.IOException: Negative seek offset 异常
	 * @param destDir
	 * @throws IOException
	 */
	public static void unZip(String zipfile, String destDir) throws Exception
	{
		destDir = destDir.endsWith(""+File.separatorChar) ? destDir : destDir + File.separatorChar;
		byte b[] = new byte[1024];
		int length;
		ZipFile zipFile = null;
		try
		{
			zipFile = new ZipFile(new File(zipfile));
			Enumeration enumeration = zipFile.getEntries();
			ZipEntry zipEntry = null;
			while (enumeration.hasMoreElements())
			{
				zipEntry = (ZipEntry) enumeration.nextElement();
				File loadFile = new File(destDir + zipEntry.getName());
				if (zipEntry.isDirectory())
				{
					// 这段都可以不要，因为每次都貌似从最底层开始遍历的
					loadFile.mkdirs();
				}
				else
				{
					if (!loadFile.getParentFile().exists())
						loadFile.getParentFile().mkdirs();
					OutputStream outputStream = new FileOutputStream(loadFile);
					InputStream inputStream = zipFile.getInputStream(zipEntry);
					while ((length = inputStream.read(b)) > 0)
						outputStream.write(b, 0, length);
					
					outputStream.close();
					inputStream.close();
				}
				loadFile.setLastModified(zipEntry.getTime());
			}
			//System.out.println(" 文件解压成功 ");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			new BaseBean().writeLog("文件解压成功 unZip : "+e);
			throw new Exception(e);
		}
		finally
		{
			try
			{
				if(null!=zipFile)
					zipFile.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	

	/*
	 * 方法功能：打包单个文件或文件夹 参数：inputFileName 要打包的文件夹或文件的路径 zipgetFileName 打包后的文件路径
	 */
	public static void execute(String inputFileName, String zipgetFileName) {
		File inputFile = new File(inputFileName);
		String base = inputFileName.substring(inputFileName.lastIndexOf(""+File.separatorChar) + 1);
		ZipOutputStream out = getZipOutputStream(zipgetFileName);
		zipPack(out, inputFile, base);
		try {
			if (null != out) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//compress(new File(zipgetFileName));
	}

	/**
	 * 方法功能：打包多个文件或文件夹 参数：inputFileNameList 要打包的文件夹或文件的路径的列表 zipgetFileName
	 * 打包后的文件路径
	 * @param inputFileNameList 需要打包的文件
	 * @param zipgetFileName 打包后的文件名
	 * @param rootPath 源的全根目录（一定要以/结束）
	 * @param renameRoot 重命名首目录
	 */
	public static void execute(List<String> inputFileNameList, String zipgetFileName,String rootPath,String renameRoot) {
		ZipOutputStream out = getZipOutputStream(zipgetFileName);

		for (String inputFileName : inputFileNameList) {
			File inputFile = new File(inputFileName);
			//System.out.println("inputFileName : "+inputFileName);
			if(inputFile.exists())
			{
				String base = renameRoot+File.separatorChar+inputFileName.substring(rootPath.length());
				zipPack(out, inputFile, base);
			}
		}

		try {
			if (null != out) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//compress(new File(zipgetFileName));
	}

	/*
	 * 方法功能：打包成zip文件 参数：out 打包后生成文件的流 inputFile 要压缩的文件夹或文件 base 打包文件中的路径
	 */

	private static void zipPack(ZipOutputStream out, File inputFile, String base) {
		if (inputFile.isDirectory()) // 打包文件夹
		{
			packFolder(out, inputFile, base);
		} else // 打包文件
		{
			packFile(out, inputFile, base);
		}
	}

	/*
	 * 方法功能：遍历文件夹下的内容，如果有子文件夹，就调用zipPack方法 参数：out 打包后生成文件的流 inputFile 要压缩的文件夹或文件
	 * base 打包文件中的路径
	 */
	private static void packFolder(ZipOutputStream out, File inputFile, String base) {
		File[] fileList = inputFile.listFiles();
		try {
			// 在打包文件中添加路径
			out.putNextEntry(new ZipEntry(base + ""+File.separatorChar));
		} catch (IOException e) {
			e.printStackTrace();
		}
		base = base.length() == 0 ? "" : base + ""+File.separatorChar;
		for (File file : fileList) {
			zipPack(out, file, base + file.getName());
		}
	}

	/*
	 * 方法功能：打包文件 参数：out 压缩后生成文件的流 inputFile 要压缩的文件夹或文件 base 打包文件中的路径
	 */
	private static void packFile(ZipOutputStream out, File inputFile, String base) {
		
		ZipEntry zipEntry = new ZipEntry(base);

		// 设置打包文件的大小，如果不设置，打包有内容的文件时，会报错
		zipEntry.setSize(inputFile.length());
		zipEntry.setTime(inputFile.lastModified());
		try {
			out.putNextEntry(zipEntry);
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileInputStream in = null;
		try {
			in = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int b = 0;

		try {
			while ((b = in.read(B_ARRAY, 0, BUFFER)) != -1) {
				out.write(B_ARRAY, 0, b);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.err
					.println("NullPointerException info ======= [FileInputStream is null]");
		} finally {
			try {
				if (null != in) {
					in.close();
				}
				if (null != out) {
					out.closeEntry();
				}
			} catch (IOException e) {

			}
		}
	}

	/*
	 * 方法功能：把打包的zip文件压缩成gz格式 参数：srcFile 要压缩的zip文件路径
	 */
	private static void compress(File srcFile) {
		File zipget = new File(srcFile.getAbsolutePath());
		FileInputStream in = null;
		GZIPOutputStream out = null;
		try {
			in = new FileInputStream(srcFile);
			out = new GZIPOutputStream(new FileOutputStream(zipget));
			int number = 0;
			while ((number = in.read(B_ARRAY, 0, BUFFER)) != -1) {
				out.write(B_ARRAY, 0, number);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}

				if (out != null) {
					out.close();
				}
				//删除压缩前的文件
				deleteFile(srcFile.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

	/*
	 * 方法功能：获得打包后文件的流 参数：zipgetFileName 打包后文件的路径
	 */
	private static ZipOutputStream getZipOutputStream(String zipgetFileName) {
		// 如果打包文件没有.zip后缀名，就自动加上
		zipgetFileName = zipgetFileName.endsWith(".zip") ? zipgetFileName
				: zipgetFileName + ".zip";
		FileOutputStream fileOutputStream = null;
		try {
			File zipget = new File(zipgetFileName);
			if (!zipget.getParentFile().exists())
				zipget.getParentFile().mkdirs();
			fileOutputStream = new FileOutputStream(zipgetFileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
				fileOutputStream);
		ZipOutputStream out = new ZipOutputStream(bufferedOutputStream);

		// 如果不加下面这段，当压缩包中的路径字节数超过100 byte时，就会报错
		//out.setLongFileMode(ZipOutputStream.LONGFILE_GNU);
		return out;
	}
	/**
	 * 删除文件
	 * @param targetPath
	 * @throws IOException
	 */
	public static void deleteFile(String targetPath)
	{
		try
		{

			FileSecurityUtil.deleteFile(new File(targetPath));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			new BaseBean().writeLog("删除文件失败 : "+e);
		}
	}
	/**
	 * 删除文件
	 * @param targetPath
	 * @throws IOException
	 */
	public static void deleteFile(String targetPath,boolean _self)
	{
		try
		{
			if(_self)
			{
				deleteFile(targetPath);
			}
			else
			{
				File targetFile = new File(targetPath);
				if (targetFile.exists()&&targetFile.isDirectory()) 
				{
					String[] files = targetFile.list();
					if(null!=files&&files.length>0)
					{
						for(int i = 0;i<files.length;i++)
						{
							deleteFile(targetFile.getAbsolutePath()+File.separatorChar+files[i]);
						}
					}
					
				} 
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			new BaseBean().writeLog("删除文件失败 : "+e);
		}
	}
	/**
	 * 检查上传文件的格式是否为zip
	 * @param filename
	 * @return
	 */
	public static boolean checkZip(String filename)
	{
		boolean iszip = false;
		File f = new File(filename);
		if(f.exists())
		{
			if(f.isFile()&&filename.lastIndexOf(".zip")>-1)
			{
				iszip = true;
			}
		}
		return iszip;
	}
	public static void main(String[] args) 
	{
        // 把 E 盘正则表达式文件夹下的所有文件压缩到 E 盘 stu 目录下，压缩后的文件名保存为 正则表达式 .zip 
        // zip ("E:// 正则表达式 ", "E://stu // 正则表达式 .zip "); 
        // 把 E 盘 stu 目录下的正则表达式 .zip 压缩文件内的所有文件解压到 E 盘 stu 目录下面 
		try
		{
			unZip ( "D://FileUpload.zip" , "D://stu" ); 
			//zip ( "D:"+File.separatorChar+"stu" , "D:"+File.separatorChar+"FileUpload.zip" );
		}
		catch(Exception e)
		{
			
		}
	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="org.bouncycastle.crypto.BufferedBlockCipher" %>
<%@ page import="org.bouncycastle.crypto.engines.AESFastEngine" %>
<%@ page import="org.bouncycastle.crypto.modes.CBCBlockCipher" %>
<%@ page import="org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher" %>
<%@ page import="org.bouncycastle.crypto.params.KeyParameter" %>
<%@ page import="org.bouncycastle.crypto.params.ParametersWithIV" %>
<%@ page import="org.bouncycastle.util.encoders.Hex" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>


<%!
    public static String decodeStr(String content) {
        byte[] keybytes = "WEAVER E-DESIGN.".getBytes();
        byte[] iv = "weaver e-design.".getBytes();
        try {
            BufferedBlockCipher engine = new PaddedBufferedBlockCipher(
                    new CBCBlockCipher(new AESFastEngine()));
            engine.init(true, new ParametersWithIV(new KeyParameter(keybytes), iv));
            byte[] deByte = Hex.decode(content);
            engine.init(false, new ParametersWithIV(new KeyParameter(keybytes), iv));
            byte[] dec = new byte[engine.getOutputSize(deByte.length)];
            int size1 = engine.processBytes(deByte, 0, deByte.length, dec, 0);
            int size2 = engine.doFinal(dec, size1);
            byte[] decryptedContent = new byte[size1 + size2];
            System.arraycopy(dec, 0, decryptedContent, 0,
                    decryptedContent.length);
            return new String(decryptedContent, "GBK");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String getRealFromName(String virtualFormName) {
        if (virtualFormName.length() > 33 && virtualFormName.charAt(32) == '_') {
            virtualFormName = virtualFormName.substring(33);
        }
        return virtualFormName;
    }


    public static String getModeFieldLinkageSql(String entryid) {
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        RecordSet rs2 = new RecordSet();
        RecordSet rs3 = new RecordSet();
        String finalSql = "select ";

        rs1.executeQuery("select * from modeDataInputmain where ENTRYID=?", entryid);
        while (rs1.next()) {
            String backfields = "";
            String sqlfrom = "";
            String sqlwhere = "";

            String mainId = Util.null2String(rs1.getString("id"));
            rs2.executeQuery("select * from modeDataInputtable  where DATAINPUTID=?", mainId);
            while (rs2.next()) {
                String tableid = Util.null2String(rs2.getString("id"));
                String tablename = Util.null2String(rs2.getString("TABLENAME"));
                String alias = Util.null2String(rs2.getString("ALIAS"));
                if ("".equals(alias)) {
                    alias = tablename;
                    sqlfrom = sqlfrom + tablename + ",";
                } else {
                    sqlfrom = sqlfrom + tablename + " " + alias + ",";
                }


                rs3.executeQuery("select * from modeDataInputfield  where DATAINPUTID=? and TABLEID=?", mainId, tableid);
                while (rs3.next()) {
                    String type = Util.null2String(rs3.getString("type"));// 1为条件字段，2为查询字段
                    String dbfieldname = Util.null2String(rs3.getString("DBFIELDNAME"));
                    String pagefieldname = Util.null2String(rs3.getString("PAGEFIELDNAME"));
                    if ("1".equals(type)) {// 拼接条件
                        sqlwhere = sqlwhere + alias + "." + dbfieldname + "='" + pagefieldname + "'" + " and ";
                    }
                    if ("2".equals(type)) {
                        backfields = backfields + alias + "." + dbfieldname + ",";
                    }
                }
            }
            backfields = backfields.trim();
            sqlfrom = sqlfrom.trim();
            sqlwhere = sqlwhere.trim();
            if (backfields.endsWith(",")) {
                backfields = backfields.substring(0, backfields.length() - 1);
            }
            if (sqlfrom.endsWith(",")) {
                sqlfrom = sqlfrom.substring(0, sqlfrom.length() - 1);
            }
            if (sqlwhere.endsWith("and")) {
                sqlwhere = sqlwhere.substring(0, sqlwhere.length() - 3);
            }

            String whereclause = Util.null2String(rs1.getString("WHERECLAUSE")).trim();
            if (whereclause.toLowerCase().startsWith("where")) {
                whereclause.substring(5);
            }

            if ("".equals(backfields)) {
                backfields = "*";
            }
            finalSql = finalSql + backfields + " from " + sqlfrom + " where 1=1 ";
            if (!"".equals(whereclause)) {
                finalSql = finalSql + " and " + whereclause;
            }
            if (!"".equals(sqlwhere)) {
                finalSql = finalSql + " and " + sqlwhere;
            }
        }
        return finalSql;
    }

    public static String getDmlActionName(String dmlactionid) {

        RecordSet rs = new RecordSet();

        String sqldml = "select  * from mode_dmlactionset a,mode_dmlactionsqlset b where a.id = ? and a.id=b.ACTIONID";
        rs.executeQuery(sqldml, dmlactionid);

        String dmlactionname = "";

        if (rs.next()) {

            dmlactionname = Util.null2String(rs.getString("DMLACTIONNAME"));

        }

        return dmlactionname;
    }

    public static boolean checkCodeHasDevCode(String code) {



        boolean haskaifa = false;
        try {
            org.jsoup.nodes.Element body = org.jsoup.Jsoup.parse(code);
            org.jsoup.select.Elements scripts = body.select("script");
            for (int i = 0; i < scripts.size(); i++) {
                org.jsoup.nodes.Element script = scripts.get(i);
                if (script != null) {
                    String str = removeComments(script.data().trim());
                    if (StringUtils.isNotBlank(str)) {
                        haskaifa = true;
                    }

                    String src = Util.null2String(script.attr("src"));
                    if (!"".equals(src)) {
                        haskaifa = true;
                    }
                }
            }
            org.jsoup.select.Elements styles = body.select("style");
            for (int i = 0; i < styles.size(); i++) {
                org.jsoup.nodes.Element style = styles.get(i);
                if (style != null) {
                    String str = removeComments(style.data().trim());
                    if (StringUtils.isNotBlank(str)) {
                        haskaifa = true;
                    }
                    String src = Util.null2String(style.attr("src"));
                    if (!"".equals(src)) {
                        haskaifa = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return haskaifa;
    }

    public static String removeComments(String input) {
        StringBuilder result = new StringBuilder();
        boolean inSingleLineComment = false;
        boolean inMultiLineComment = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            char nextChar = i < input.length() - 1 ? input.charAt(i + 1) : '\0';

            if (!inSingleLineComment && !inMultiLineComment) {
                if (currentChar == '/' && nextChar == '*') {
                    inMultiLineComment = true;
                    i++;
                } else if (currentChar == '/' && nextChar == '/') {
                    inSingleLineComment = true;
                    i++;
                } else {
                    result.append(currentChar);
                }
            } else if (inSingleLineComment) {
                if (currentChar == '\n') {
                    inSingleLineComment = false;
                    result.append(currentChar);
                }
            } else if (inMultiLineComment) {
                if (currentChar == '*' && nextChar == '/') {
                    inMultiLineComment = false;
                    i++;
                }
            }
        }

        return result.toString();
    }
    public static String getDmlSqlContent(String dmlactionid) {
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();

        String sqldml = "select  * from mode_dmlactionset a,mode_dmlactionsqlset b where a.id = ? and a.id=b.ACTIONID";
        rs.executeQuery(sqldml, dmlactionid);


        String sqlContent = "";


        if (rs.next()) {
            String id = Util.null2String(rs.getString("id"));


            String dmlsql = Util.null2String(rs.getString("DMLSQL"));
            String dmlcussql = Util.null2String(rs.getString("DMLCUSSQL"));
            String dmlcuswhere = Util.null2String(rs.getString("dmlcuswhere"));


            String dmlwhere = "";
            if (!"".equals(dmlcuswhere)) {
                dmlwhere += dmlcuswhere + " ";
            }

            rs1.executeQuery("select  * from mode_dmlactionfieldmap where ACTIONSQLSETID =? ", id);
            while (rs1.next()) {
                String fieldname = Util.null2String(rs1.getString("FIELDNAME"));
                String fieldvalue = Util.null2String(rs1.getString("FIELDVALUE"));

                dmlwhere += "and " + fieldname + "='{?" + fieldvalue + "}' ";
            }

            if (dmlwhere.startsWith("and")) dmlwhere = dmlwhere.replaceFirst("and", " ");


            if (!dmlcussql.equals("")) {
                sqlContent = dmlcussql;
            } else {

                if (dmlsql.contains("where")) {
                    dmlsql += dmlwhere;
                } else {
                    dmlsql += " where " + dmlwhere;
                }

                sqlContent = dmlsql;
            }


        }
        return sqlContent;

    }


    public static String getLabelName(String labelId) {

        if (labelId.equals("")) {//id为空无需处理
            return "";
        }
        String labelName = "";
        RecordSet rs = new RecordSet();
        rs.executeQuery("select labelname from htmlLabelinfo where INDEXID = ? and LANGUAGEID = '7' ", labelId);
        if (rs.next()) {
            labelName = rs.getString("labelname");
        }
        return labelName;
    }


    public static String getLayoutType(String type) {


        String layouttype = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 0:
                layouttype = "显示布局";
                break;
            case 1:
                layouttype = "新建布局";
                break;
            case 2:
                layouttype = "编辑布局";
                break;
            case 3:
                layouttype = "监控布局";
                break;
            case 4:
                layouttype = "打印布局";
                break;
        }


        return layouttype;
    }

    public static String getConditionType(String type) {


        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 1:
                text = "普通类型";
                break;
            case 2:
                text = "SQL";
                break;
            case 3:
                text = "JAVA";
                break;

        }


        return text;
    }

    public static String isEnable(String isenable, String enabletag) {

        return enabletag.equals(isenable) ? "是" : "否";


    }


    public static String getHrefType(String hreftype) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(hreftype));

        switch (typeNew) {
            case 1:
                text = "模块";
                break;
            case 2:
                text = "手动输入";
                break;
            case 3:
                text = "模块查询列表";
                break;
            case 4:
                text = "批量修改字段值";
                break;
            case 5:
                text = "回复评论";
                break;
            case 6:
                text = "看板";
                break;
            case 7:
                text = "新建流程";
                break;
            case 8:
                text = "甘特图";
                break;

        }

        return text;
    }


    public static String getInterFaceTypeName(String type) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 1:
                text = "数据审批";
                break;
            case 2:
                text = "集成自定义接口";
                break;
            case 3:
                text = "自定义接口动作";
                break;
            case 4:
                text = "DML接口动作";
                break;
            case 5:
                text = "提醒/即时提醒";
                break;
            case 6:
                text = "ESB接口动作";
                break;

        }

        return text;
    }


    public static String getOpentype(String type) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));


        switch (typeNew) {
            case 1:
                text = "默认窗口";
                break;
            case 2:
                text = "弹出窗口";
                break;
            case 3:
                text = "其它(js)";
                break;
            case 4:
                text = "右侧展开";
                break;
            case 5:
                text = "下方展开列表小卡片";
                break;


        }

        return text;
    }
%>


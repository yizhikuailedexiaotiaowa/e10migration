<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.concurrent.ConcurrentHashMap" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="weaver.formmode.service.CommonConstant" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>

<%!

    public static ConcurrentHashMap<String, String> modeExtendForm = new ConcurrentHashMap<String, String>();//是否虚拟表单， key表单id 值 1 是本地虚拟表单，2是外部虚拟表单  不在这个表里就是实体表单

    public static ConcurrentHashMap<String, String> validApp = new ConcurrentHashMap<String, String>();//有效的应用

    public static ConcurrentHashMap<String, String> validMode = new ConcurrentHashMap<String, String>();//有效的模块

    public static ConcurrentHashMap<String, String> validCustomSearch = new ConcurrentHashMap<String, String>();//有效的查询

    //有效的浏览按钮id mode_browser 缓存
    public static ConcurrentHashMap<String, String> validBrowserId = new ConcurrentHashMap<String, String>();//有效的建模浏览框


    public static ConcurrentHashMap<String, String> repeatFormMode = new ConcurrentHashMap<String, String>();//多个模块引用同一个表单 导致表单名称变化

    public static boolean isinit = false;

    public static HashMap<String, String> modecount = new HashMap<String, String>();
    public static HashMap<String, String> modedate = new HashMap<String, String>();
    public static HashMap<String, String> modetablename = new HashMap<String, String>();


    public static String getModeDataCount(String modeid) {

        if (modecount.containsKey(modeid)) {
            return modecount.get(modeid);
        }
        if (!isValidMode(modeid)) {
            return "";
        }

        RecordSet rs2 = new RecordSet();

        String tablename = getmodetablename(modeid);


        rs2.executeQuery(" select count(*) as count from " + tablename + "   where formmodeid = ? ", modeid);
        String count = "0";
        if (rs2.next()) {
            count = rs2.getString("count");
        }
        modecount.put(modeid, count);
        return count;
    }

    public static String getmodedatacreatedate(String modeid) {
        if (modedate.containsKey(modeid)) {
            return modedate.get(modeid);
        }
        if (!isValidMode(modeid)) {
            return "";
        }
        RecordSet rs2 = new RecordSet();
        String modedatacreatedate = "";
        String tablename = getmodetablename(modeid);

        rs2.executeQuery(" select max(modedatacreatedate) as modedatacreatedate from " + tablename + "  where formmodeid = ?  ", modeid);
        if (rs2.next()) {
            modedatacreatedate = Util.formatMultiLang(rs2.getString("modedatacreatedate"));
        }

        modedate.put(modeid, modedatacreatedate);
        return modedatacreatedate;
    }

    public static String getmodetablename(String modeid) {
        if (modetablename.containsKey(modeid)) {
            return modetablename.get(modeid);
        }

        if (!isValidMode(modeid)) {
            return "";
        }
        RecordSet rs2 = new RecordSet();

        rs2.executeQuery("select workflow_bill.tablename from modeinfo join workflow_bill on  modeinfo.formid = workflow_bill.id and modeinfo.id = ?", modeid);

        String tablename = "";
        if (rs2.next()) {
            tablename = rs2.getString("tablename");
        }


        modetablename.put(modeid, tablename);
        return tablename;
    }

    /**
     * 是否缓存
     *
     * @param
     * @return
     */
    public static boolean isloadCache() {

        if (!isinit) {
            initCache();
        }
        return true;
    }

    /**
     * 初始化建模有效的数据
     *
     * @param
     * @return
     */
    public static synchronized void initCache() {
        if (isinit) {
            return;
        }


        RecordSet rs = new RecordSet();


        rs.executeQuery("select FORMID,VDATASOURCE,vsql from modeformextend WHERE ISVIRTUALFORM = 1");

        while (rs.next()) {

            String formid = rs.getString("FORMID");
            String vdatasource = rs.getString("VDATASOURCE");

            String value = "";

            if (vdatasource.equals("$ECOLOGY_SYS_LOCAL_POOLNAME")) {//本地数据源

            } else {
                value = "2";//外部数据源
            }
            modeExtendForm.put(formid, value);

        }


        rs.executeQuery("select id,treeFieldName from modeTreeField WHERE " + CommonConstant.DB_ISNULL_FUN + "(isdelete,0)!=1  ");

        while (rs.next()) {

            validApp.put(rs.getString("id"), rs.getString("treeFieldName"));

        }


        rs.executeQuery("select id,modetype,formid from modeinfo WHERE " + CommonConstant.DB_ISNULL_FUN + "(isdelete,0)!=1 and formid <0 ");//不是删除的模块，表单id小于0

        while (rs.next()) {

            if (validApp.containsKey(rs.getString("modetype")) && !modeExtendForm.containsKey(rs.getString("formid"))) {//有效应用 && 自定义表单
                validMode.put(rs.getString("id"), rs.getString("formid"));
            }
        }


        rs.executeQuery("select id, appid, modeid, formid from mode_customsearch where modeid > 0");//存在导入失败的模块  modeid = null || modeid = 0

        while (rs.next()) {

            if (validApp.containsKey(rs.getString("appid")) && validMode.containsKey(rs.getString("modeid"))
                    && !modeExtendForm.containsKey(rs.getString("formid"))) {//有效应用 && 自定义表单 && 有效的模块

                validCustomSearch.put(rs.getString("id"), rs.getString("appid"));
            }
        }


        rs.executeQuery("select a.id as modeid ,b.id as formid,b.TABLENAME,a.MODETYPE as appid  " +
                "from modeinfo a join  workflow_bill b on a.FORMID = b.id  where formid in (select formid  " +
                "from modeinfo where   " + getSubINClause(StringUtils.join(validMode.keySet(), ','), "id", "in", 999) + "  group by formid having count(*) >1)");//多个模块引用同一个表单 导致表单名称变化

        while (rs.next()) {

            if (validApp.containsKey(rs.getString("appid")) && validMode.containsKey(rs.getString("modeid"))
                    && !modeExtendForm.containsKey(rs.getString("formid"))) {//有效应用 && 自定义表单 && 有效的模块

                repeatFormMode.put(rs.getString("modeid"), rs.getString("formid"));
            }
        }


        HashSet<String> browserType = new HashSet<String>();
        rs.executeQuery("select id,showname from mode_browser order by id ");

        while (rs.next()) {

            String id = Util.null2String(rs.getString("id"));
            String showname = Util.null2String(rs.getString("showname"));


            if (browserType.contains(showname)) {
                continue;
            } else {
                browserType.add(showname);
            }

            validBrowserId.put(id, showname);
        }

        isinit = true;
    }


    public static boolean isValidCustomSearch(String customid) {

        isloadCache();


        return validCustomSearch.containsKey(customid);

    }

    public static boolean isValidMode(String modeid) {

        isloadCache();


        return validMode.containsKey(modeid);

    }

    public static boolean isValidApp(String appid) {

        isloadCache();


        return validApp.containsKey(appid);

    }

    public static boolean isVirtualForm(String e9_formid) {

        isloadCache();


        return modeExtendForm.containsKey(e9_formid);

    }

    public static boolean isRepeatMode(String modeid) {

        isloadCache();


        return repeatFormMode.containsKey(modeid);

    }


    /**
     * @param browserId mode_browser.id
     * @Author ygd2020
     * @Date 18:55 2023-2-9
     * @Return boolean
     **/
    public static boolean isValidBrowser(String browserId) {
        isloadCache();

        return validBrowserId.containsKey(browserId);

    }

    public static String getSubINClause(String strOrg, String columnName, String instr) {
        return getSubINClause(strOrg, columnName, instr, 999);
    }

    /**
     * 当SQL语句IN查询子句列表超过1000，将会查询报错。故在此将In语句分隔成
     * 多个In子句，并用 Or 来进行关联。
     *
     * @param strOrg     IN语句的列表，以逗号分隔。 如：
     * @param columnName IN语句查询的列名
     * @param instr      IN语句关键字，可以是 IN 或 NOT IN
     * @param len        IN语句支持最大长度列表，一般为999
     * @return
     */
    public static String getSubINClause(String strOrg, String columnName, String instr, int len) {
        String[] orglist = StringUtils.split(strOrg, ",");

        List<String> groupList = new ArrayList<String>();
        List<String> tempList = new ArrayList<String>();
        for (int i = 0; i < orglist.length; i++) {
            if (tempList.size() >= len) {
                String tempGroup = StringUtils.join(tempList, ',');
                groupList.add(columnName + " " + instr + " (" + tempGroup + ")");
                tempList = new ArrayList<String>();
            }

            if (StringUtils.isEmpty(orglist[i])) continue;
            tempList.add(orglist[i]);
        }

        if (tempList.size() > 0) {
            String tempGroup = StringUtils.join(tempList, ',');
            groupList.add(columnName + " " + instr + " (" + tempGroup + ")");
        }

        String orand = "IN".equalsIgnoreCase(instr) ? " OR " : " AND ";
        return "(" + StringUtils.join(groupList, orand) + ")";
    }



%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@include file="util/E10CubeUtil.jsp" %>
<%@include file="util/ValidObjCacheUtil.jsp" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>

<%!


    void appnamesplit() {
        String a = "";

        String[] split = a.split("\n");
        StringBuffer tableString = new StringBuffer();
        tableString.append("<table border='1'>")
                .append("<tr>")
                .append("<td>").append("应用id").append("</td>")
                .append("<td>").append("应用名称").append("</td>")
                .append("<td>").append("所属分部").append("</td>")
                .append("</tr>");

        for (String sp : split) {

            String[] s = sp.split("_");

            int m = s.length;

            if (m == 3) {
                tableString.append("<tr>")
                        .append("<td>").append(s[0]).append("</td>")
                        .append("<td>").append(s[1]).append("</td>")
                        .append("<td>").append(s[2]).append("</td>")
                        .append("</tr>");
            } else {
                ArrayList<String> am = new ArrayList<String>();
                for (int i = 1; i < m; i++) {
                    am.add(s[i]);
                }
                tableString.append("<tr>")
                        .append("<td>").append(s[0]).append("</td>")
                        .append("<td>").append(StringUtils.join("_", am)).append("</td>")
                        .append("<td>").append(s[m - 1]).append("</td>")
                        .append("</tr>");
            }

        }
        tableString.append("</table>");
        System.out.println(tableString);
    }

    public String doCubeAll(String path) {


        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("表单建模_功能维度", path);

        modeData(writeEntity, "表单数据汇总", "表单数据汇总", "");//表单数据汇总
        modeLayoutCode(writeEntity, "布局代码块", "布局代码块", "");//布局代码块
        modeLayoutFieldSql(writeEntity, "模块布局字段属性SQL", "模块布局字段属性SQL", "");//模块布局字段属性SQL
        getVirtualFormMode(writeEntity, "模块虚拟表单", "模块虚拟表单", "");//模块虚拟表单
        modeRightConditionCusSql(writeEntity, "模块_权限_条件_sql", "模块_权限_条件_sql", "");//模块_权限_条件_sql
        modeFieldLinkage(writeEntity, "模块_字段联动", "模块_字段联动", "");//模块_字段联动
        modeWorkflowtoMode(writeEntity, "模块_流程转数据", "模块_流程转数据", "");//模块_流程转数据
        modeBtnLink(writeEntity, "模块_页面扩展链接动作", "模块_页面扩展链接动作", "");//模块_页面扩展链接动作
        modeBtnAction(writeEntity, "模块_页面扩展接口动作", "模块_页面扩展接口动作", "");//模块_页面扩展接口动作
        modeBtnRightConditionCusSql(writeEntity, "模块_页面扩展权限条件SQL", "模块_页面扩展权限条件SQL", "");//模块_页面扩展权限条件SQL
        modeBtnexpendcallbackfn(writeEntity, "模块_页面扩展回调函数", "模块_页面扩展回调函数", "");//模块_页面扩展回调函数
        modeCodeDetail(writeEntity, "模块_编码_自定义属性", "模块_编码_自定义属性", "");//模块_编码_自定义属性
        modeQRCode(writeEntity, "模块_二维码", "模块_二维码", "");//模块_二维码
        modeBARCode(writeEntity, "模块_条形码", "模块_条形码", "");//模块_条形码
        modedataDefine(writeEntity, "模块_浏览数据定义_数据范围设置", "模块_浏览数据定义_数据范围设置", "");//模块_浏览数据定义_数据范围设置
        modeCustomCondition(writeEntity, "查询_固定查询条件", "查询_固定查询条件", "");//查询_固定查询条件
        modeCustomConditionTop(writeEntity, "查询_数据置顶条件", "查询_数据置顶条件", "");//查询_数据置顶条件
        modeCustomCode(writeEntity, "查询_代码块", "查询_代码块", "");//查询_代码块
        modeNoModeCustom(writeEntity, "查询_无模块查询", "查询_无模块查询", "");//查询_无模块查询
        modeCustomRowClick(writeEntity, "查询_行点击事件", "查询_行点击事件", "");//查询_行点击事件
        modeCustomFieldLink(writeEntity, "查询_字段定义_字段链接地址", "查询_字段定义_字段链接地址", "");//查询_字段定义_字段链接地址
        modeCustomfieldshowchange(writeEntity, "查询_字段定义_显示转换", "查询_字段定义_显示转换", "");//查询_字段定义_显示转换
        modeCustomcountset(writeEntity, "查询_统计", "查询_统计", "");//查询_统计
        modeCustombtn(writeEntity, "查询_自定义按钮", "查询_自定义按钮", "");//查询_自定义按钮
        modeCustombtnFilter(writeEntity, "查询_批量操作_页面扩展_数据筛选", "查询_批量操作_页面扩展_数据筛选", "");//查询_批量操作_页面扩展_数据筛选
        CubeInterfaceConfig(writeEntity, "接口管理", "接口管理", "");//接口管理
        remind(writeEntity, "提醒", "提醒", "");//提醒
        gantt(writeEntity, "甘特图", "甘特图", "");//甘特图
        mindmap(writeEntity, "思维导图", "思维导图", "");//思维导图
        board(writeEntity, "看板", "看板", "");//看板
        customResource(writeEntity, "资源面板", "资源面板", "");//资源面板
        customtreefield(writeEntity, "树形浏览框", "树形浏览框", "");//树形浏览框
        customtreeLink(writeEntity, "导航树", "导航树", "");//导航树
        listfield(writeEntity, "列表浏览框", "列表浏览框", "");//列表浏览框

        String filePath = excelWrite_html(writeEntity);
        return filePath;

    }


    public String doCubeApp(String path) {
        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("表单建模_应用维度", path);

        RecordSet rs = new RecordSet();
        rs.executeQuery("select id,treeFieldName,subcompanyid from modeTreeField order by id");

        while (rs.next()) {
            String appid = rs.getString("id");
            String appname = Util.formatMultiLang(rs.getString("treeFieldName"));
            String subcompanyid = rs.getString("subcompanyid");
            if (isValidApp(appid)) {
                doExcel(writeEntity, appid, appname, subcompanyid);
            }
        }

        String filePath = excelWrite_html(writeEntity);

        return filePath;

    }

    String getappname(String appid, String appname, String subcompanyid) {

        String sheetname = appid + "_" + appname;
        if (!subcompanyid.equals("")) {
            RecordSet rs = new RecordSet();
            rs.executeQuery("select subcompanyname from HrmSubCompany where id= ? ", subcompanyid);
            if (rs.next()) {
                sheetname += "_" + rs.getString("subcompanyname");

            } else {
                System.out.println(appid + "     " + "无分权 ");
            }
        }

        return sheetname;
    }

    void doExcel(ExcelWriteEntity excelWrite, String appid, String appname, String subcompanyid) {


        String sheetname = getappname(appid, appname, subcompanyid);

        modeData(excelWrite, "表单数据汇总", sheetname, appid);//表单数据汇总

        modeLayoutCode(excelWrite, "布局代码块", sheetname, appid);//布局代码块
        //modeLayoutLink(excelWrite,"表单数据汇总", sheetname, appid);//布局链接+ifarme
        modeLayoutFieldSql(excelWrite, "模块布局字段属性SQL", sheetname, appid);//模块布局字段属性SQL
        getVirtualFormMode(excelWrite, "模块虚拟表单", sheetname, appid);//模块虚拟表单
        //  modeRightConditionCus(excelWrite,"模块_权限_条件_自定义浏览字", sheetname, appid);//模块_权限_条件_自定义浏览字段  新id 模式这个直接转值
        modeRightConditionCusSql(excelWrite, "模块_权限_条件_sql", sheetname, appid);//模块_权限_条件_sql
        //  modedefaultValue(excelWrite,"默认值", sheetname, appid);//默认值
        modeFieldLinkage(excelWrite, "模块_字段联动", sheetname, appid);//模块_字段联动
        modeWorkflowtoMode(excelWrite, "模块_流程转数据", sheetname, appid);//模块_流程转数据
        modeBtnLink(excelWrite, "模块_页面扩展链接动作", sheetname, appid);//模块_页面扩展链接动作
        modeBtnAction(excelWrite, "模块_页面扩展接口动作", sheetname, appid);//模块_页面扩展接口动作
        // modeBtnRightConditionCus(excelWrite,"模块_页面扩展权限条件自定义浏览字段", sheetname, appid);//模块_页面扩展权限条件自定义浏览字段
        modeBtnRightConditionCusSql(excelWrite, "模块_页面扩展权限条件SQL", sheetname, appid);//模块_页面扩展权限条件SQL
        modeBtnexpendcallbackfn(excelWrite, "模块_页面扩展回调函数", sheetname, appid);//模块_页面扩展回调函数
        modeCodeDetail(excelWrite, "模块_编码_自定义属性", sheetname, appid);//模块_编码_自定义属性
        modeQRCode(excelWrite, "模块_二维码", sheetname, appid);//模块_二维码
        modeBARCode(excelWrite, "模块_条形码", sheetname, appid);//模块_条形码
        modedataDefine(excelWrite, "模块_浏览数据定义_数据范围设置", sheetname, appid);//模块_浏览数据定义_数据范围设置
        modeCustomCondition(excelWrite, "查询_固定查询条件", sheetname, appid);//查询_固定查询条件
        modeCustomConditionTop(excelWrite, "查询_数据置顶条件", sheetname, appid);//查询_数据置顶条件
        modeCustomCode(excelWrite, "查询_代码块", sheetname, appid);//查询_代码块
        modeNoModeCustom(excelWrite, "查询_无模块查询", sheetname, appid);//查询_无模块查询
        modeCustomRowClick(excelWrite, "查询_行点击事件", sheetname, appid);//查询_行点击事件
        modeCustomFieldLink(excelWrite, "查询_字段定义_字段链接地址", sheetname, appid);//查询_字段定义_字段链接地址
        modeCustomfieldshowchange(excelWrite, "查询_字段定义_显示转换", sheetname, appid);//查询_字段定义_显示转换
//        modeCustomdspDefaultValue(excelWrite, "查询_查询条件_自定义浏览框默认值", sheetname, appid);//查询_查询条件_自定义浏览框默认值
        modeCustomcountset(excelWrite, "查询_统计", sheetname, appid);//查询_统计
        modeCustombtn(excelWrite, "查询_自定义按钮", sheetname, appid);//查询_自定义按钮
        modeCustombtnFilter(excelWrite, "查询_批量操作_页面扩展_数据筛选", sheetname, appid);//查询_批量操作_页面扩展_数据筛选
        CubeInterfaceConfig(excelWrite, "接口管理", sheetname, appid);//接口管理
        remind(excelWrite, "提醒", sheetname, appid);//提醒
        gantt(excelWrite, "甘特图", sheetname, appid);//甘特图
        mindmap(excelWrite, "思维导图", sheetname, appid);//思维导图
        board(excelWrite, "看板", sheetname, appid);//看板
        customResource(excelWrite, "资源面板", sheetname, appid);//资源面板
//        littleCardJson(excelWrite, "列表小卡片", sheetname, appid);//列表小卡片
//        searchLineMergel(excelWrite, "列头合并", sheetname, appid);//列头合并
        customtreefield(excelWrite, "树形浏览框", sheetname, appid);//树形浏览框
        customtreeLink(excelWrite, "导航树", sheetname, appid);//导航树
//        customPageLink(excelWrite, "自定义页面", sheetname, appid);//自定义页面
        listfield(excelWrite, "列表浏览框", sheetname, appid);//列表浏览框
    }

    void modeData(ExcelWriteEntity writeEntity, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeDataHeaderNames();
        List<List<String>> values = modeDataValue(appid);
        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }

    void modeLayoutCode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = getModeLayoutCodeHeaderNames();
        List<List<String>> values = getModeLayoutCodeValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);

    }

    void modeLayoutLink(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeLayoutLinkHeaderNames();
        List<List<String>> values = modeLayoutLinkValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeLayoutFieldSql(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = getModeLayoutFieldSqlHeaderNames();
        List<List<String>> values = getModeLayoutFieldSqlValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);


    }

    void getVirtualFormMode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = getVirtualFormModeHeaderNames();
        List<List<String>> values = getVirtualFormModeValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);


    }

    void modeRightConditionCus(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeRightConditionCusHeaderNames();
        List<List<String>> values = modeRightConditionCusValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeRightConditionCusSql(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeRightConditionCusSqlHeaderNames();
        List<List<String>> values = modeRightConditionCusSqlValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modedefaultValue(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modedefaultValueHeaderNames();
        List<List<String>> values = modedefaultValueValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeFieldLinkage(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeFieldLinkageHeaderNames();
        List<List<String>> values = modeFieldLinkageValue(appid);


        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);

    }

    void modeWorkflowtoMode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeWorkflowtoModeHeaderNames();
        List<List<String>> values = modeWorkflowtoModeValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);

    }

    void modeBtnLink(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBtnLinkHeaderNames();
        List<List<String>> values = modeBtnLinkValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }


    void modeBtnAction(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBtnActionHeaderNames();
        List<List<String>> values = modeBtnActionValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeBtnRightConditionCus(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBtnRightConditionCusHeaderNames();
        List<List<String>> values = modeBtnRightConditionCusValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeBtnRightConditionCusSql(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBtnRightConditionCusSqlHeaderNames();
        List<List<String>> values = modeBtnRightConditionCusSqlValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeBtnexpendcallbackfn(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBtnexpendcallbackfnHeaderNames();
        List<List<String>> values = modeBtnexpendcallbackfnValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCodeDetail(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCodeDetailHeaderNames();
        List<List<String>> values = modeCodeDetailValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeQRCode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeQRCodeHeaderNames();
        List<List<String>> values = modeQRCodeValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeBARCode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeBARCodeHeaderNames();
        List<List<String>> values = modeBARCodeValue(appid);

        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }


    void modedataDefine(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modedataDefineHeaderNames();
        List<List<String>> values = modedataDefineValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);

    }

    void modeCustomCondition(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomConditionHeaderNames();
        List<List<String>> values = modeCustomConditionValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomConditionTop(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomConditionTopHeaderNames();
        List<List<String>> values = modeCustomConditionTopValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomCode(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomCodeHeaderNames();
        List<List<String>> values = modeCustomCodeValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeNoModeCustom(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeNoModeCustomHeaderNames();
        List<List<String>> values = modeNoModeCustomValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomRowClick(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomRowClickHeaderNames();
        List<List<String>> values = modeCustomRowClickValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomFieldLink(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomFieldLinkHeaderNames();
        List<List<String>> values = modeCustomFieldLinkValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomfieldshowchange(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomfieldshowchangeHeaderNames();
        List<List<String>> values = modeCustomfieldshowchangeValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomdspDefaultValue(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomdspDefaultValueHeaderNames();
        List<List<String>> values = modeCustomdspDefaultValueValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustomcountset(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustomcountsetHeaderNames();
        List<List<String>> values = modeCustomcountsetValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }


    void modeCustombtn(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustombtnHeaderNames();
        List<List<String>> values = modeCustombtnValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void modeCustombtnFilter(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = modeCustombtnFilterHeaderNames();
        List<List<String>> values = modeCustombtnFilterValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void CubeInterfaceConfig(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = CubeInterfaceConfigHeaderNames();
        List<List<String>> values = CubeInterfaceConfigValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void remind(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = remindHeaderNames();
        List<List<String>> values = remindValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void gantt(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = ganttHeaderNames();
        List<List<String>> values = ganttValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void mindmap(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = mindmapHeaderNames();
        List<List<String>> values = mindmapValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void board(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = boardHeaderNames();
        List<List<String>> values = boardValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void customResource(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = customResourceHeaderNames();
        List<List<String>> values = customResourceValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void littleCardJson(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = littleCardJsonHeaderNames();
        List<List<String>> values = littleCardJsonValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void searchLineMergel(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = searchLineMergelHeaderNames();
        List<List<String>> values = searchLineMergelValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void customtreefield(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = customtreefieldHeaderNames();
        List<List<String>> values = customtreefieldValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void customtreeLink(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = customtreeLinkHeaderNames();
        List<List<String>> values = customtreeLinkValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }

    void customPageLink(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = customPageLinkHeaderNames();
        List<List<String>> values = customPageLinkValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }


    void listfield(ExcelWriteEntity excelWrite, String functionName, String sheetname, String appid) {
        List<String> headerNames = listfieldHeaderNames();
        List<List<String>> values = listfieldValue(appid);
        addExcelModal(excelWrite, sheetname, functionName, headerNames, values);
    }


    List<String> modeDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");

        return headerNames;
    }

    List<String> getModeLayoutCodeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("布局id");
        headerNames.add("布局类型");
        headerNames.add("布局名称");
        headerNames.add("布局代码块");

        return headerNames;
    }

    List<String> modeLayoutLinkHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("布局id");
        headerNames.add("布局名称");
        headerNames.add("布局链接");

        return headerNames;
    }

    List<String> getModeLayoutFieldSqlHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("布局id");
        headerNames.add("布局类型");
        headerNames.add("布局名称");
        headerNames.add("字段id");
        headerNames.add("sql文本");


        return headerNames;
    }

    List<String> getVirtualFormModeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("表单名称");
        headerNames.add("数据源");
        headerNames.add("sql文本");


        return headerNames;
    }

    List<String> modeRightConditionCusHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("权限项");
        headerNames.add("字段名");
        headerNames.add("字段类型");
        headerNames.add("默认值");

        return headerNames;
    }

    List<String> modeRightConditionCusSqlHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("权限项");
        headerNames.add("条件sql");

        return headerNames;
    }

    List<String> modedefaultValueHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("字段");
        headerNames.add("默认值");
        headerNames.add("是否需要处理");

        return headerNames;
    }

    List<String> modeFieldLinkageHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("字段联动名称");
        headerNames.add("是否启用");
        headerNames.add("sql文本");


        return headerNames;
    }

    List<String> modeWorkflowtoModeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("流程转数据名称");
        headerNames.add("是否启用");
        headerNames.add("触发条件");
        headerNames.add("是否需要调整/无需调整需要验证功能");

        return headerNames;
    }

    List<String> modeBtnLinkHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("页面扩展名称");
        headerNames.add("按钮类型");
        headerNames.add("打开方式");
        headerNames.add("链接动作类型");
        headerNames.add("链接/js");

        return headerNames;
    }

    List<String> modeBtnActionHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("页面扩展名称");
        headerNames.add("页面扩展类型");
        headerNames.add("接口动作类型");
        headerNames.add("接口动作名称");
        headerNames.add("是否启用");
        headerNames.add("自定义JAVA接口");
        headerNames.add("sql文本");


        return headerNames;
    }

    List<String> modeBtnRightConditionCusHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("页面扩展名称");
        headerNames.add("页面扩展类型");
        headerNames.add("字段");
        headerNames.add("浏览框标识");
        headerNames.add("默认值");

        return headerNames;
    }

    List<String> modeBtnRightConditionCusSqlHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("页面扩展名称");
        headerNames.add("页面扩展类型");
        headerNames.add("条件sql");

        return headerNames;
    }

    List<String> modeBtnexpendcallbackfnHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("页面扩展名称");
        headerNames.add("页面扩展类型");
        headerNames.add("回调函数");

        return headerNames;
    }

    List<String> modeCodeDetailHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("是否启用");
        headerNames.add("编码字段");
        headerNames.add("流水字段");
        headerNames.add("流水字段属性");
        headerNames.add("是否单独流水");

        return headerNames;
    }

    List<String> modeQRCodeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("是否启用");
        headerNames.add("链接类型");
        headerNames.add("链接");
        headerNames.add("二维码样式");

        return headerNames;
    }

    List<String> modeBARCodeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("是否启用");
        headerNames.add("条形码内容");
        headerNames.add("条形码样式");

        return headerNames;
    }

    List<String> modedataDefineHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("字段");
        headerNames.add("字段标识");
        headerNames.add("条件类型");
        headerNames.add("条件sql");

        return headerNames;
    }


    List<String> modeCustomConditionHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("条件类型");
        headerNames.add("条件sql");
        headerNames.add("java路径");

        return headerNames;
    }


    List<String> modeCustomConditionTopHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("是否启用数据置顶");
        headerNames.add("条件类型");
        headerNames.add("条件sql");
        headerNames.add("条件描述");

        return headerNames;
    }


    List<String> modeCustomCodeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("代码块");

        return headerNames;
    }


    List<String> modeNoModeCustomHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("数据源类型");
        headerNames.add("表单名称");
        headerNames.add("表单类型");
        headerNames.add("是否虚拟表单");
        headerNames.add("sql文本");


        return headerNames;
    }


    List<String> modeCustomRowClickHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("链接类型");
        headerNames.add("打开方式");
        headerNames.add("链接/js");


        return headerNames;
    }

    List<String> modeCustomFieldLinkHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("字段");
        headerNames.add("链接类型");
        headerNames.add("链接");


        return headerNames;
    }


    List<String> modeCustomfieldshowchangeHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("字段");
        headerNames.add("条件类型");
        headerNames.add("转换样式");


        return headerNames;
    }


    List<String> modeCustomdspDefaultValueHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("字段");
        headerNames.add("字段类型");
        headerNames.add("默认值");


        return headerNames;
    }


    List<String> modeCustomcountsetHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("统计名称");
        headerNames.add("是否启用");
        headerNames.add("关联表单类型");
        headerNames.add("统计字段");
        headerNames.add("过滤条件");
        headerNames.add("表单名称");
        headerNames.add("关联条件");
        headerNames.add("是否需要调整");


        return headerNames;
    }

    List<String> modeCustombtnHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("按钮名称");
        headerNames.add("是否启用");
        headerNames.add("按钮类型");
        headerNames.add("链接/js");


        return headerNames;
    }

    List<String> modeCustombtnFilterHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("查询id");
        headerNames.add("查询名称");
        headerNames.add("按钮名称");
        headerNames.add("是否启用");
        headerNames.add("条件类型");
        headerNames.add("条件sql");


        return headerNames;
    }

    List<String> CubeInterfaceConfigHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("接口标识");
        headerNames.add("接口类型");
        headerNames.add("接口名称");


        return headerNames;
    }

    List<String> remindHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("提醒名称");
        headerNames.add("总开关");
        headerNames.add("动作名称");
        headerNames.add("动作是否启用");
        headerNames.add("动作类型");
        headerNames.add("java");
        headerNames.add("sql文本");


        return headerNames;
    }

    List<String> ganttHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("视图id");
        headerNames.add("视图名称");


        return headerNames;
    }

    List<String> mindmapHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("视图id");
        headerNames.add("视图名称");


        return headerNames;
    }


    List<String> boardHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("视图id");
        headerNames.add("视图名称");


        return headerNames;
    }

    List<String> customResourceHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("视图id");
        headerNames.add("视图名称");


        return headerNames;
    }

    List<String> littleCardJsonHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("查询id");
        headerNames.add("查询名称");


        return headerNames;
    }

    List<String> searchLineMergelHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("查询id");
        headerNames.add("查询名称");


        return headerNames;
    }

    List<String> customtreefieldHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("树id");
        headerNames.add("树名称");
        headerNames.add("树节点id");
        headerNames.add("树节点名称");
        headerNames.add("数据源表名");
        headerNames.add("数据源");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("条件");
        headerNames.add("树根节点数据主键");
        headerNames.add("是否需要手动处理");


        return headerNames;
    }

    List<String> customtreeLinkHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("树id");
        headerNames.add("树名称");
        headerNames.add("树节点id");
        headerNames.add("树节点名称");
        headerNames.add("数据源表名");
        headerNames.add("数据源类型");
        headerNames.add("数据源");
        headerNames.add("链接");
        headerNames.add("排序字段");
        headerNames.add("条件");
        headerNames.add("是否需要手动处理");


        return headerNames;
    }

    List<String> customPageLinkHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("页面id");
        headerNames.add("页面名称");
        headerNames.add("tab名称");
        headerNames.add("tab地址");
        headerNames.add("是否需要手动处理");


        return headerNames;
    }

    List<String> listfieldHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("模块id");
        headerNames.add("模块名称");
        headerNames.add("是否建模实体表单");
        headerNames.add("数据量");
        headerNames.add("数据最新时间");
        headerNames.add("浏览框名称");
        headerNames.add("浏览框标识");
        headerNames.add("无条件查询sql");
        headerNames.add("固定条件类型");
        headerNames.add("固定条件sql");
        headerNames.add("固定条件java");

        headerNames.add("是否需要手动处理");


        return headerNames;
    }

    //布局代码块
    List<List<String>> getModeLayoutCodeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,TREEFIELDNAME as appname, mi.id as modeid, mi.MODENAME as modename,wb.TABLENAME as tablename, mh.TYPE as layouttype\n" +
                "     ,mh.LAYOUTNAME as layoutname,scriptstr,mh.id as layoutid\n" +
                "\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "    join workflow_bill wb on wb.id = mi.FORMID\n" +
                "         join modehtmllayout mh on mh.MODEID = mi.ID where scriptstr is not null");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }

        sql += " order by mt.id";

        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String layouttype = getLayoutType(rs.getString("layouttype"));
            String layoutname = Util.formatMultiLang(rs.getString("layoutname"));
            String layoutid = rs.getString("layoutid");
            String scriptstr = rs.getString("scriptstr");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            if (scriptstr.equals("")) {
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(layoutid);
            value.add(layouttype);
            value.add(layoutname);
            String decodeStr = decodeStr(scriptstr);
            if (StringUtils.isBlank(decodeStr)) {

                continue;
            }


            if (!checkCodeHasDevCode(decodeStr)) {
                continue;
            }

            value.add(decodeStr.replace("<", "《").replace(">", "》"));

            values.add(value);

        }

        return values;

    }


    //表单数据汇总
    List<List<String>> modeDataValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,mt.treeFieldName as appname,tablename,mi.id as modeid,modename\n" +
                "from modeTreeField mt\n" +
                "         join modeinfo mi on mi.modetype = mt.id\n" +
                "         join workflow_bill wb on wb.id = mi.formid\n" +
                "  where 1=1  \n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }

        sql += " order by mt.id";

        rs.executeQuery(sql);

        System.out.println(" modeDataValue :" + sql);

        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));


            if (!isValidMode(modeid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));


            values.add(value);

        }

        return values;

    }


    //布局代码块
    List<List<String>> modeLayoutLinkValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,TREEFIELDNAME as appname, mi.id as modeid, mi.MODENAME as modename,wb.TABLENAME as tablename,   mh.TYPE as layouttype\n" +
                "     ,mh.LAYOUTNAME as layoutname,scriptstr,mh.id as layoutid\n" +
                "\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "    join workflow_bill wb on wb.id = mi.FORMID\n" +
                "         join modehtmllayout mh on mh.MODEID = mi.ID where scriptstr is not null");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }

        sql += " order by mt.id";

        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String layouttype = getLayoutType(rs.getString("layouttype"));
            String layoutname = Util.formatMultiLang(rs.getString("layoutname"));
            String layoutid = rs.getString("layoutid");
            String scriptstr = rs.getString("scriptstr");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            if (scriptstr.equals("")) {
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(layoutid);
            value.add(layouttype);
            value.add(layoutname);
            String decodeStr = decodeStr(scriptstr);
            if (StringUtils.isBlank(decodeStr)) {

                continue;
            }
            value.add(decodeStr);

            values.add(value);

        }

        return values;

    }

    //字段sql属性
    List<List<String>> getModeLayoutFieldSqlValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,TREEFIELDNAME as appname, mi.id as modeid, mi.MODENAME as modename,wb.TABLENAME as tablename,   mh.TYPE  as layouttype\n" +
                "     ,mh.LAYOUTNAME as layoutname\n" +
                ",fieldid,attrcontent,mh.id as layoutid\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "    join workflow_bill wb on wb.id = mi.FORMID\n" +
                "         join modehtmllayout mh on mh.MODEID = mi.ID\n" +
                "         join modefieldattr mf on mf.LAYOUTID = mh.ID where attrcontent like '%doFieldSQL(\"%'");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }

        sql += " order by mt.id";

        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String layouttype = getLayoutType(rs.getString("layouttype"));
            String layoutname = Util.formatMultiLang(rs.getString("layoutname"));
            String layoutid = rs.getString("layoutid");
            String fieldid = rs.getString("fieldid");
            String sql_cus = rs.getString("attrcontent");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            int index = sql_cus.indexOf("doFieldSQL(\"");
            if (index > -1) {
                sql_cus = sql_cus.substring(index + 12);
                index = sql_cus.lastIndexOf("\")");
                if (index > -1) {
                    sql_cus = sql_cus.substring(0, index);
                }

                sql_cus = sql_cus.trim();
            }


            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(layoutid);
            value.add(layouttype);
            value.add(layoutname);

            value.add(getFieldText(fieldid));
            value.add(sql_cus);


            values.add(value);

        }

        return values;

    }

    //模块虚拟表单
    List<List<String>> getVirtualFormModeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,TREEFIELDNAME as appname, mi.id as modeid, mi.MODENAME as modename,wb.TABLENAME as tablename\n" +
                "\n" +
                ",wb.ID as formid, LABELNAME as formname\n" +
                "\n" +
                ", VDATASOURCE\n" +
                ",vsql\n" +
                "\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "    join workflow_bill wb on wb.id = mi.FORMID\n" +
                "    left join htmllabelinfo on INDEXID = wb.NAMELABEL\n" +
                "join modeformextend me on me.FORMID = mi.FORMID\n" +
                "WHERE ISVIRTUALFORM = 1   and LANGUAGEID =7");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";


        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String formname = rs.getString("formname");
            String tablename = rs.getString("tablename");
            String vsql = rs.getString("vsql");

            String VDATASOURCE = rs.getString("VDATASOURCE");
            VDATASOURCE = "$ECOLOGY_SYS_LOCAL_POOLNAME".equals(VDATASOURCE) ? "内部数据源" : VDATASOURCE;
            String realFromName = getRealFromName(tablename);
            if (realFromName.equals("cube_vsql")) {
                realFromName = vsql;
            } else {

                realFromName = "select * from " + realFromName;

            }

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(formname);
            value.add(VDATASOURCE);

            value.add(realFromName);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeRightConditionCusValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,\n" +
                "       TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                "     , mr. righttype\n" +
                " , mr.SHARETYPE,fieldvalue\n" +
                " , conditionsql, wb.FIELDNAME,LABELNAME as fieldlabel,wb.FIELDDBTYPE,wl.TABLENAME\n" +
                "from modetreefield mt\n" +
                "    join modeinfo mi\n" +
                "on mi.MODETYPE = mt.id\n" +
                "    join moderightinfo mr on mr.MODEID = mi.ID\n" +
                "    join mode_expressionbase me on me.RIGHTID = mr.id\n" +
                "    join workflow_billfield wb on wb.id = me.FIELDID\n" +
                "    join workflow_bill wl on wl.id = wb.BILLID\n" +
                "    join htmllabelinfo on INDEXID = wb.FIELDLABEL\n" +
                "where mr.CONDITIONTYPE = 1 and wb.FIELDHTMLTYPE=3 and wb.TYPE in (162, 161, 256, 257) and LANGUAGEID = 7\n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";

        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String righttype = rs.getString("righttype");
            String sharetype = rs.getString("sharetype");
            String fieldlabel = rs.getString("fieldlabel");
            String FIELDDBTYPE = rs.getString("FIELDDBTYPE");
            String fieldvalue = rs.getString("fieldvalue");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(getRightTypeText(righttype, sharetype));
            value.add(fieldlabel);
            value.add(FIELDDBTYPE);
            value.add(fieldvalue);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeRightConditionCusSqlValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,\n" +
                "       TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                "     , righttype\n" +
                " , mr.SHARETYPE\n" +
                " , conditionsql\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join moderightinfo mr on mr.MODEID = mi.ID\n" +
                "\n" +
                "where mr.CONDITIONTYPE = 2");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String righttype = rs.getString("righttype");
            String sharetype = rs.getString("sharetype");
            String conditionsql = rs.getString("conditionsql");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(getRightTypeText(righttype, sharetype));
            value.add(conditionsql);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modedefaultValueValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,\n" +
                "       TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                "     , fieldid,customervalue,fieldhtmltype,type\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join DefaultValue dt on dt.MODEID = mi.ID\n" +
                "join workflow_billfield wb on wb.id = fieldid\n" +
                "\n" +
                "where 1=1");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String fieldid = getFieldText(rs.getString("fieldid"));
            String customervalue = rs.getString("customervalue");
            String fieldhtmltype = rs.getString("fieldhtmltype");
            String _type = rs.getString("type");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            String type = "否";

            if (fieldhtmltype.equals("3") && (_type.equals("161") || _type.equals("162") || _type.equals("256") || _type.equals("257"))) {
                type = "是";
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(fieldid);
            value.add(customervalue);
            value.add(type);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeFieldLinkageValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,\n" +
                "       TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                ",md.TRIGGERNAME,isenabled,md.id as entryid\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join modeDataInputentry md on md.MODEID = mi.ID  where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String TRIGGERNAME = rs.getString("TRIGGERNAME");
            String isenabled = rs.getString("isenabled");
            String entryid = rs.getString("entryid");
            String sql_cus = getModeFieldLinkageSql(entryid);

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(TRIGGERNAME);
            value.add(isenabled);
            value.add(sql_cus);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeWorkflowtoModeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid ,\n" +
                "       TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                ",md.workflowtomodename,isenable ,md.id as wfid \n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join mode_workflowtomodeset md on md.MODEID = mi.ID\n" +
                "  where   1=1 \n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String workflowtomodename = rs.getString("workflowtomodename");
            String isenable = rs.getString("isenable");
            String wfid = rs.getString("wfid");

            if (!isValidMode(modeid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(workflowtomodename);
            value.add(isenable.equals("1") ? "是" : "否");
            String workflowtomodeconditiontype = getWorkflowtomodeconditiontype(wfid);
            value.add(workflowtomodeconditiontype);
            value.add(getWorkflowtomodeByhand(wfid, workflowtomodeconditiontype));


            values.add(value);

        }

        return values;

    }

    String getWorkflowtomodeconditiontype(String wfid) {

        String triggertype = "";
        RecordSet rs = new RecordSet();

        rs.executeQuery("select\n" +
                "  conditiontype    as conditiontype,\n" +
                "conditiontext,conditionsql" +
                "    from\n" +
                "    mode_workflowtomodeset where   id = ? and conditiontype is not null and  conditiontype!=1", wfid);

        if (rs.next()) {
            String conditiontype = getConditionType(rs.getString("conditiontype"));


            String conditiontext = "1".equals(rs.getString("conditiontype")) ?
                    rs.getString("conditiontext") :
                    "2".equals(rs.getString("conditiontype")) ? rs.getString("conditionsql") : "无";

            triggertype = conditiontype + " \n" + conditiontext;
        }
        return triggertype;

    }

    String getWorkflowtomodeByhand(String wfid, String workflowtomodeconditiontype) {
        List<String> type = new ArrayList<String>();
        if (!workflowtomodeconditiontype.equals("")) {
            type.add(" 触发条件需要重新设置 ");
        }

        RecordSet rs = new RecordSet();

        rs.executeQuery("select id,MAINTABLEUPDATECONDITION from mode_workflowtomodeset where MAINTABLEOPTTYPE=2 and id = ?", wfid);

        while (rs.next()) {
            String maintableupdatecondition = rs.getString("MAINTABLEUPDATECONDITION");
            if (StringUtils.isNotBlank(maintableupdatecondition)) {
                type.add(" 主表设置更新类型的更新条件: " + maintableupdatecondition);
            }
        }

        rs.executeQuery("select id,MAINTABLEWHERECONDITION from mode_workflowtomodeset where MAINTABLEOPTTYPE=4 and id = ?", wfid);

        while (rs.next()) {
            String maintableupdatecondition = rs.getString("MAINTABLEUPDATECONDITION");
            if (StringUtils.isNotBlank(maintableupdatecondition)) {
                type.add(" 主表设置插入并更新条件: " + maintableupdatecondition);
            }
        }


        rs.executeQuery("select detail.id,detail.UPDATECONDITION from mode_workflowtomodeset main\n" +
                "left join mode_workflowtomodesetopt detail on main.id=detail.mainid where detail.OPTTYPE in(3,4) and main.id=?", wfid);

        while (rs.next()) {
            String UPDATECONDITION = rs.getString("UPDATECONDITION");
            if (StringUtils.isNotBlank(UPDATECONDITION)) {
                type.add(" 主表设置更新类型，明细表设置更新或者更新追加类型，明细表的条件: " + UPDATECONDITION);
            }
        }


        rs.executeQuery("select detail.id,detail.wherecondition from mode_workflowtomodeset main \n" +
                "left join mode_workflowtomodesetopt detail on main.id=detail.mainid where main.maintableopttype =4 and  main.id=?", wfid);

        while (rs.next()) {
            String UPDATECONDITION = rs.getString("wherecondition");
            if (StringUtils.isNotBlank(UPDATECONDITION)) {
                type.add(" 主表设置插入并更新类型，明细表设置流程明细_ID对应存放字段: " + UPDATECONDITION);
            }
        }

        rs.executeQuery("select 1 from cube_fieldexpinfo where wftomodeid = ?", wfid);

        if (rs.next()) {
            type.add(" 重新配置赋值表达式 ");
        }

        return type.isEmpty() ? "否" : StringUtils.join(" \n ", type);


    }

    List<List<String>> modeBtnLinkValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,TREEFIELDNAME                                               as appname\n" +
                "\n" +
                "     , mi.id                                                       as modeid\n" +
                "     , mi.MODENAME                                                 as modename\n" +
                "     , mp.EXPENDNAME\n" +
                "     , mp.SHOWTYPE\n" +
                "     , mp.OPENTYPE\n" +
                "     ,HREFTYPE,\n" +
                "       HREFID,HREFTARGET\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join mode_pageexpand mp on mp.MODEID = mi.ID\n" +
                "where ISSYSTEM !=1 and hreftype not in (1,3,7)");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String EXPENDNAME = Util.formatMultiLang(rs.getString("EXPENDNAME"));

            String showtype = rs.getString("showtype");

            showtype = "2".equals(showtype) ? "鼠标右键按钮" : "tab页面";
            String opentype = rs.getString("opentype");
            if (!opentype.equals("3")) {//不是js打开方式直接跳过吧
                continue;
            }

            opentype = "3".equals(opentype) ? "其他（js打开方式）" : "窗口打开";



            String HREFTYPE = getHrefType(rs.getString("HREFTYPE"));
            String HREFTARGET = rs.getString("HREFTARGET");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(EXPENDNAME);
            value.add(showtype);
            value.add(opentype);
            value.add(HREFTYPE);
            value.add(HREFTARGET);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeBtnActionValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id as appid,\n" +
                "       TREEFIELDNAME                                               as appname\n" +
                "\n" +
                "     , mi.id                                                       as modeid\n" +
                "     , mi.MODENAME                                                 as modename\n" +
                "     , mp.EXPENDNAME\n" +
                "     , mp.SHOWTYPE \n" +
                "\n" +
                "          ,   m.isenable \n" +
                "\n" +
                "     , interfacetype,\n" +
                "m.name,esbid,dmlactionid,javafileAddress,triggerworkflowsetid,taskdetailid\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join mode_pageexpand mp on mp.MODEID = mi.ID\n" +
                "join mode_pageexpanddetail m on mp.id = m.MAINID  where 1=1  \n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String EXPENDNAME = Util.formatMultiLang(rs.getString("EXPENDNAME"));
            String interfacetypename = getInterFaceTypeName(rs.getString("interfacetype"));

            String showtype = rs.getString("showtype");
            showtype = "2".equals(showtype) ? "鼠标右键按钮" : "tab页面";

            String interfacetype = rs.getString("interfacetype");
            String dmlactionid = rs.getString("dmlactionid");
            String triggerworkflowsetid = rs.getString("triggerworkflowsetid");
            String taskdetailid = rs.getString("taskdetailid");
            String isenable = rs.getString("isenable");

            isenable = "1".equals(isenable) ? "是" : "否";
            String javafileAddress = rs.getString("javafileAddress");
            String name = rs.getString("name");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(EXPENDNAME);
            value.add(showtype);
            value.add(interfacetypename);
            value.add(getbtnactionname(interfacetype, name, dmlactionid, triggerworkflowsetid, taskdetailid));
            value.add(isenable);
            value.add(javafileAddress);

            String sql_cus = getDmlSqlContent(dmlactionid);
            value.add(sql_cus);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeBtnRightConditionCusValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename,\n" +
                "       EXPENDNAME\n" +
                "     , mp.SHOWTYPE\n" +
                ",conditionsql, wb.FIELDNAME,LABELNAME as fieldlabel,wb.FIELDDBTYPE,wl.TABLENAME,fieldvalue\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join mode_pageexpand mp on mp.MODEID = mi.id\n" +
                "         join expandBaseRightInfo er on er.EXPANDID = mp.ID\n" +
                "join expandBaseRightExpressionbase me on me.RIGHTID = er.id\n" +
                "join workflow_billfield wb on wb.id = me.FIELDID\n" +
                "join workflow_bill wl on wl.id = wb.BILLID\n" +
                "join htmllabelinfo on INDEXID = wb.FIELDLABEL\n" +
                "where er.CONDITIONTYPE = 1 and wb.FIELDHTMLTYPE=3 and wb.TYPE in (162,161,256,257) and LANGUAGEID = 7");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String EXPENDNAME = Util.formatMultiLang(rs.getString("EXPENDNAME"));

            String showtype = rs.getString("showtype");
            showtype = "2".equals(showtype) ? "鼠标右键按钮" : "tab页面";

            String fieldlabel = rs.getString("fieldlabel");
            String FIELDDBTYPE = rs.getString("FIELDDBTYPE");
            String fieldvalue = rs.getString("fieldvalue");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(EXPENDNAME);
            value.add(showtype);
            value.add(fieldlabel);
            value.add(FIELDDBTYPE);
            value.add(fieldvalue);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeBtnRightConditionCusSqlValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename,\n" +
                "       EXPENDNAME\n" +
                "     ,   mp.SHOWTYPE  \n" +
                ",conditionsql\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join mode_pageexpand mp on mp.MODEID = mi.id\n" +
                "         join expandBaseRightInfo er on er.EXPANDID = mp.ID\n" +
                "where er.CONDITIONTYPE = 2");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";

        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String EXPENDNAME = Util.formatMultiLang(rs.getString("EXPENDNAME"));

            String showtype = rs.getString("showtype");
            showtype = "2".equals(showtype) ? "鼠标右键按钮" : "tab页面";

            String conditionsql = rs.getString("conditionsql");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(EXPENDNAME);
            value.add(showtype);
            value.add(conditionsql);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeBtnexpendcallbackfnValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename,\n" +
                "       EXPENDNAME\n" +
                "\n" +
                "     ,   mp.SHOWTYPE \n" +
                ",expendcallbackfn\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join mode_pageexpand mp on mp.MODEID = mi.id\n" +
                "\n" +
                "where (expendcallbackfn is not null and expendcallbackfn !='')");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String EXPENDNAME = Util.formatMultiLang(rs.getString("EXPENDNAME"));

            String showtype = rs.getString("showtype");
            showtype = "2".equals(showtype) ? "鼠标右键按钮" : "tab页面";

            String expendcallbackfn = rs.getString("expendcallbackfn");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(EXPENDNAME);
            value.add(showtype);
            value.add(expendcallbackfn);

            values.add(value);

        }

        return values;

    }

    List<List<String>> modeCodeDetailValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                ",mc.CODEFIELDID\n" +
                "     ,  mc.ISUSE \n" +
                "     , md.ISSERIAL  ,\n" +
                "md.FIELDNAMESTR,md.SHOWNAMESTR,md.TABLENAME\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join ModeCode mc on mc.MODEID = mi.id\n" +
                "join ModeCodeDetail md on md.CODEMAINID = mc.id\n" +
                "where md.SHOWTYPE in ('4','5')");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));

            String CODEFIELDID = rs.getString("CODEFIELDID");
            String isuse = isEnable(rs.getString("isuse"), "1");
            String SHOWNAMESTR = rs.getString("SHOWNAMESTR");
            String ISSERIAL = isEnable(rs.getString("ISSERIAL"), "1");
            String FIELDNAMESTR = rs.getString("FIELDNAMESTR");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(isuse);
            value.add(getFieldText(CODEFIELDID));
            value.add(SHOWNAMESTR);
            value.add(FIELDNAMESTR);
            value.add(ISSERIAL);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeQRCodeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                ",   mc.ISUSE  \n" +
                "     ,  mc.TARGETTYPE\n" +
                "     , TARGETURL\n" +
                ",mc.ISUSE,QRCODEDESC\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join modeqrcode mc on mc.MODEID = mi.id  where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));

            String isuse = isEnable(rs.getString("isuse"), "1");
            String TARGETTYPE = rs.getString("TARGETTYPE");

            TARGETTYPE = "1".equals(TARGETTYPE) ? "显示" : "2".equals(TARGETTYPE) ? "编辑" : "手动输入";

            String TARGETURL = rs.getString("TARGETURL");
            String QRCODEDESC = rs.getString("QRCODEDESC");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(isuse);
            value.add(TARGETTYPE);
            value.add(TARGETURL);
            value.add(QRCODEDESC);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeBARCodeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                "     ,   mc.ISUSED     as isuse\n" +
                " , CODENUM,INFO\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join mode_barcode mc on mc.MODEID = mi.id where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));

            String isuse = isEnable(rs.getString("isuse"), "1");
            String CODENUM = rs.getString("CODENUM");
            String INFO = rs.getString("INFO");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(isuse);
            value.add(CODENUM);
            value.add(INFO);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modedataDefineValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mt.id         as appid\n" +
                "     , mi.MODENAME   as modename\n" +
                "\n" +
                ",LABELNAME as fieldlabel ,FIELDDBTYPE,TABLENAME\n" +
                ", conditiontype as conditiontype\n" +
                ",FIELDNAME,conditionsql,conditiontext,rootdata\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "join mode_data_definition mc on mc.MODEID = mi.id\n" +
                "join workflow_billfield wb on wb.id = mc.FIELDID\n" +
                "join workflow_bill wl on wl.id = wb.BILLID\n" +
                "join htmllabelinfo on INDEXID = wb.FIELDLABEL where languageid=7\n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));

            String fieldlabel = rs.getString("fieldlabel");
            String FIELDDBTYPE = rs.getString("FIELDDBTYPE");
            String conditiontype = rs.getString("conditiontype");
            conditiontype = "1".equals(conditiontype) ? "普通类型" : "SQL";

            String conditionsql = rs.getString("conditionsql");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(fieldlabel);
            value.add(FIELDDBTYPE);
            value.add(conditiontype);
            value.add(conditionsql);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomConditionValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename\n" +
                "     ,   mc.conditiontype  as conditiontype\n" +
                "     , conditionsql,javafileAddress\n" +
                ",customname,mc.id as customid\n" +
                "from modetreefield mt\n" +
                "\n" +
                "join mode_customsearch mc on mc.appid = mt.id\n" +
                "left join    modeinfo mi on mi.id = mc.modeid\n" +
                " where   1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String conditiontype = getConditionType(rs.getString("conditiontype"));
            String conditionsql = rs.getString("conditionsql");
            String javafileAddress = rs.getString("javafileAddress");

            if (StringUtils.isBlank(conditionsql) || StringUtils.isBlank(javafileAddress)) {//如果没有sql 或者没有java地址就不管了
                continue;
            }

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(conditiontype);
            value.add(conditionsql);
            value.add(javafileAddress);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomConditionTopValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename,customname,mc.id as customid\n" +
                "     ,   mc.conditiontypetop     as conditiontype\n" +
                "     ,conditiontypetop,conditionsqltop,conditiontexttop\n" +
                " ,   mc.enabledtop  as enabledtop\n" +
                "from modetreefield mt\n" +
                "         join mode_customsearch mc on mc.appid = mt.id\n" +
                "left join    modeinfo mi on mi.id = mc.modeid\n" +
                "\n" +
                " where   1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");

            String conditiontype = getConditionType(rs.getString("conditiontype"));
            String conditionsqltop = rs.getString("conditionsqltop");
            String conditiontexttop = rs.getString("conditiontexttop");
            String enabledtop = isEnable(rs.getString("enabledtop"), "1");
            if (StringUtils.isBlank(conditionsqltop)) {
                continue;
            }

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(enabledtop);
            value.add(conditiontype);
            value.add(conditionsqltop);
            value.add(conditiontexttop);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomCodeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , mi.MODENAME   as modename,customname,mc.id as customid,scriptStr\n" +
                "from modetreefield mt\n" +
                "         join mode_customsearch mc on mc.appid = mt.id\n" +
                "left join    modeinfo mi on mi.id = mc.modeid\n" +
                "join mode_jscodearea mj on mj.customid = mc.ID\n" +
                " where   1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String scriptstr = rs.getString("scriptstr");


            if (StringUtils.isBlank(scriptstr)) {

                continue;
            }
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            String decodeStr = decodeStr(scriptstr);
            if (StringUtils.isBlank(decodeStr)) {

                continue;
            }

            if (!checkCodeHasDevCode(decodeStr)) {
                continue;
            }

            value.add(decodeStr.replace("<", "《").replace(">", "》"));


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeNoModeCustomValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,mt.id as appid,wb.TABLENAME as tablename\n" +
                "\n" +
                ",wb.ID as formid, LABELNAME as formname\n" +
                "\n" +
                ",  VDATASOURCE  as VDATASOURCE\n" +
                ", customname,mc.id as customid,  vsql\n" +
                " ,   me.vformtype  as vformtype\n" +
                " ,  me.ISVIRTUALFORM     as ISVIRTUALFORM\n" +
                "\n" +
                "from modetreefield mt\n" +
                "    join mode_customsearch mc on mc.APPID = mt.ID\n" +
                "    join workflow_bill wb on wb.id = mc.FORMID\n" +
                "    join htmllabelinfo on INDEXID = wb.NAMELABEL\n" +
                "  left join modeformextend me on me.FORMID = mc.FORMID\n" +
                "\n" +
                "WHERE (MODEID is null or MODEID ='0'  ) and LANGUAGEID=7");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String VDATASOURCE = rs.getString("VDATASOURCE");
            VDATASOURCE = "$ECOLOGY_SYS_LOCAL_POOLNAME".equals(VDATASOURCE) ? "内部数据源" : VDATASOURCE;

            String formname = rs.getString("formname");
            String vformtype = rs.getString("vformtype");

            vformtype = "2".equals(vformtype) ? "SQL类型表单" : "表单视图";


            String ISVIRTUALFORM = isEnable(rs.getString("ISVIRTUALFORM"), "1");
            String tablename = rs.getString("tablename");
            String vsql = rs.getString("vsql");

            String realFromName = getRealFromName(tablename);
            if (realFromName.equals("cube_vsql")) {
                realFromName = vsql;
            } else {
                realFromName = "select * from " + realFromName;

            }

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(customid);
            value.add(customname);
            value.add(VDATASOURCE);
            value.add(formname);
            value.add(vformtype);
            value.add(ISVIRTUALFORM);
            value.add(realFromName);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomRowClickValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  TREEFIELDNAME as appname,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   , mi.MODENAME as modename,CUSTOMNAME,mc.id as customid\n" +
                ",  hreftype   as hreftype\n" +
                "\n" +
                ",  ms.opentype as opentype\n" +
                ",hrefid,hreftarget\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join mode_searchrowclick ms on ms.customid = mc.ID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String hreftype = getHrefType(rs.getString("hreftype"));


            String opentype = getOpentype(rs.getString("opentype"));
            String hreftarget = rs.getString("hreftarget");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(hreftype);
            value.add(opentype);
            value.add(hreftarget);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeCustomFieldLinkValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("\n" +
                "select  TREEFIELDNAME as appname ,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   , mi.MODENAME as modename,customname,mc.id as customid\n" +
                ",hreflink\n" +
                ",  istitle  as hreftype\n" +
                ",wb.FIELDNAME,LABELNAME\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join mode_customdspfield ms on ms.customid = mc.ID\n" +
                "    join workflow_billfield wb on wb.id = ms.FIELDID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                "left join htmllabelinfo on INDEXID = wb.FIELDLABEL\n" +
                "where istitle in (3,1,2)  and LANGUAGEID = 7");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String LABELNAME = rs.getString("LABELNAME");


            String hreftype = rs.getString("hreftype");
            hreftype = "1".equals(hreftype) ? "表单建模" : "2".equals(hreftype) ? "工作流" : "自定义";


            String hreflink = rs.getString("hreflink");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            HashSet<String> set = new HashSet<String>();
            set.add("");
            set.add("/formmode/view/AddFormMode.jsp?type=$type$&modeId=$modeId$&formId=$formId$&billid=$billid$&opentype=$opentype$&customid=$customid$&viewfrom=$viewfrom$");
            set.add("/spa/cube/index.html#/main/cube/card?type=$type$&modeId=$modeId$&formId=$formId$&billid=$billid$&opentype=$opentype$&customid=$customid$&viewfrom=$viewfrom$");
            set.add("/spa/cube/index.html#/main/cube/card?type=2&modeId=$modeId$&formId=$formId$&billid=$billid$&opentype=$opentype$&customid=$customid$&viewfrom=$viewfrom$");

            if (set.contains(hreflink)) {
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(LABELNAME);
            value.add(hreftype);
            value.add(hreflink);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomfieldshowchangeValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  TREEFIELDNAME as appname,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   , mi.MODENAME as modename,customname,mc.id as customid\n" +
                "     ,  ms.conditiontype  as conditiontype,ms.conditionsql,ms.conditiontext,ms.FIELDSHOWVALUE\n" +
                ",wb.FIELDNAME,LABELNAME\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join customfieldshowchange ms on ms.customid = mc.ID\n" +
                "    join workflow_billfield wb on wb.id = ms.FIELDID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                "left join htmllabelinfo on INDEXID = wb.FIELDLABEL\n" +
                "where  LANGUAGEID = 7\n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String LABELNAME = rs.getString("LABELNAME");


            String conditiontype = getConditionType(rs.getString("conditiontype"));
            String FIELDSHOWVALUE = rs.getString("FIELDSHOWVALUE");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }


            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(LABELNAME);
            value.add(conditiontype);
            value.add(FIELDSHOWVALUE);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustomdspDefaultValueValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("\n" +
                "select  TREEFIELDNAME as appname,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   , mi.MODENAME as modename,CUSTOMNAME,customname,mc.id as customid\n" +
                ",conditionvalue\n" +
                ",wb.FIELDNAME,LABELNAME,FIELDDBTYPE\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join mode_customdspfield ms on ms.customid = mc.ID\n" +
                "    join workflow_billfield wb on wb.id = ms.FIELDID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                "left join htmllabelinfo on INDEXID = wb.FIELDLABEL\n" +
                " where wb.FIELDHTMLTYPE=3 and wb.TYPE in (162,161,256,257) and LANGUAGEID = 7 \n" +
                "\n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            String LABELNAME = rs.getString("LABELNAME");


            String FIELDDBTYPE = rs.getString("FIELDDBTYPE");
            String conditionvalue = rs.getString("conditionvalue");
            if (StringUtils.isBlank(conditionvalue)) {
                continue;
            }

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(LABELNAME);
            value.add(FIELDDBTYPE);
            value.add(conditionvalue);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeCustomcountsetValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select   TREEFIELDNAME as appname ,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   ,   modename,customname,mc.id as customid,name\n" +
                "     ,  ms.isenable   as isenable\n" +
                "     ,  formtype as formtype\n" +
                "     ,countfield,ms.conditionsql,sysbillname,ms.defaultsql\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join mode_customcountset ms on ms.customid = mc.ID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                "\n" +
                "where 1=1");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String type = "是";
            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));
            String name = Util.formatMultiLang(rs.getString("name"));

            String customid = rs.getString("customid");
            String isenable = isEnable(rs.getString("isenable"), "1");


            String formtype = rs.getString("formtype");


            String countfield = rs.getString("countfield");
            String sysbillname = rs.getString("sysbillname");
            String conditionsql = rs.getString("conditionsql");
            String defaultsql = rs.getString("defaultsql");

            if (!formtype.equals("1") && conditionsql.equals("") && defaultsql.equals("")) {
                type = "否";
            }
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(name);
            value.add(isenable);
            value.add("1".equals(formtype) ? "其他表单" : "本表单");
            value.add(countfield);
            value.add(conditionsql);
            value.add(sysbillname);
            value.add(defaultsql);
            value.add(type);


            values.add(value);

        }

        return values;

    }

    List<List<String>> modeCustombtnValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  TREEFIELDNAME as appname ,mt.id as appid\n" +
                "   , mi.id as modeid\n" +
                "   , mi.MODENAME as modename ,customname,mc.id as customid,buttonname as buttonname,\n" +
                "  hreftype  as hreftype\n" +
                "     ,  isshow  as isshow\n" +
                ",  HREFTARGETPARID, HREFTARGETPARVAL  HREFTARGET\n" +
                ",  jsparameter,  jsmethodname,  jsmethodbody\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "join mode_customsearchbutton ms on ms.OBJID = mc.ID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID where 1=1");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));
            String buttonname = Util.formatMultiLang(rs.getString("buttonname"));

            String customid = rs.getString("customid");
            String isshow = isEnable(rs.getString("isshow"), "1");


            String hreftype = rs.getString("hreftype");
            hreftype = "1".equals(hreftype) ? "手动输入(js)" : "2".equals(hreftype) ? "链接" : "";

            String jsparameter = rs.getString("jsparameter");
            String jsmethodbody = rs.getString("jsmethodbody");
            String jsmethodname = rs.getString("jsmethodname");
            String HREFTARGET = rs.getString("HREFTARGET");

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            if (!jsparameter.equals("")) {
                jsparameter = "// " + jsparameter;
            }
            String ecode = jsparameter + "\n " + jsmethodbody + "\n" + jsmethodname;

            if (hreftype.equals("链接")) {
                ecode = HREFTARGET;
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(buttonname);
            value.add(isshow);
            value.add(hreftype);
            value.add(ecode);


            values.add(value);

        }

        return values;

    }


    List<List<String>> modeCustombtnFilterValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname ,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , MODENAME   as modename,customname,mc.id as customid,LISTBATCHNAME as LISTBATCHNAME\n" +
                "     ,   mb.conditiontype   as conditiontype\n" +
                "     ,mb.conditionsql,mb.conditiontext\n" +
                " ,   mb.ISUSE   as ISUSE\n" +
                "from modetreefield mt\n" +
                "\n" +
                "join mode_customsearch mc on mc.appid = mt.id\n" +
                "    join mode_batchset mb on mb.CUSTOMSEARCHID = mc.id\n" +
                " left join modeinfo mi on mi.id = mc.MODEID\n" +
                "where  isfilter =1");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));
            String LISTBATCHNAME = Util.formatMultiLang(rs.getString("LISTBATCHNAME"));

            String customid = rs.getString("customid");
            String ISUSE = isEnable(rs.getString("ISUSE"), "1");
            String conditiontype = getConditionType(rs.getString("conditiontype"));
            String conditionsql = rs.getString("conditionsql");


            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(customid);
            value.add(customname);
            value.add(LISTBATCHNAME);
            value.add(ISUSE);
            value.add(conditiontype);
            value.add(conditionsql);


            values.add(value);

        }

        return values;

    }

    List<List<String>> CubeInterfaceConfigValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("\n" +
                "select TREEFIELDNAME                                           as appname,mt.id as appid\n" +
                "\n" +
                "     , mi.id                                                       as modeid\n" +
                "     , MODENAME                                                as modename\n" +
                "     , cf.interfacePK,cf.interfaceName\n" +
                "     ,   cf.encryptionMode   as encryptionMode\n" +
                " , encryptionPath as encryptionPath\n" +
                "\n" +
                "     ,   cf.interfacetype as INTERFACETYPE\n" +
                "from modetreefield mt\n" +
                "         join modeinfo mi on mi.MODETYPE = mt.id\n" +
                "\n" +
                "         join CubeInterfaceConfig cf on cf.MODEID = mi.ID where 1=1\n");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String interfacePK = Util.formatMultiLang(rs.getString("interfacePK"));
            String interfaceName = Util.formatMultiLang(rs.getString("interfaceName"));

            int INTERFACETYPE = Util.getIntValue(Util.null2String(rs.getString("INTERFACETYPE")), 0);
            String INTERFACETYPENEW = "";
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            switch (INTERFACETYPE) {
                case 0:
                    INTERFACETYPENEW = "保存-更新接口";
                    break;
                case 1:
                    INTERFACETYPENEW = "获取表单内容";
                    break;
                case 2:
                    INTERFACETYPENEW = "根据条件获取表单总数";
                    break;
                case 3:
                    INTERFACETYPENEW = "获取表单数据列表（分页）";
                    break;
                case 4:
                    INTERFACETYPENEW = "删除表单数据";
                    break;
            }


            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(interfacePK);
            value.add(INTERFACETYPENEW);
            value.add(interfaceName);


            values.add(value);

        }

        return values;

    }


    List<List<String>> remindValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mc.name , mti.name as actionname  \n" +
                ",tasktype,javafileaddress,dmlactionid,\n" +
                "         mc.isenable     as isenable\n" +
                "       ,  mti.isenable    as actionenable,dmlactionid\n" +
                "from modetreefield mt\n" +
                "join mode_timedtask mc on mc.appid = mt.ID\n" +
                "left join mode_timedtask_detail mti on mti.taskid = mc.id where 1=1");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String name = Util.formatMultiLang(rs.getString("name"));
            String actionname = Util.formatMultiLang(rs.getString("actionname"));
            String isenable = isEnable(rs.getString("isenable"), "1");
            String actionenable = isEnable(rs.getString("actionenable"), "1");

            String dmlactionid = Util.formatMultiLang(rs.getString("dmlactionid"));


            String tasktype = rs.getString("tasktype");
            String javafileaddress = rs.getString("javafileaddress");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }


            value.add(_appid);
            value.add(appname);
            value.add(name);
            value.add(isenable);
            value.add(actionname);
            value.add(actionenable);
            value.add(tasktype);
            value.add(javafileaddress);


            if (tasktype.equals("dml")) {
                String realFromName = getDmlSqlContent(dmlactionid);
                value.add(realFromName);

            } else {
                value.add("");
            }


            values.add(value);

        }

        return values;

    }

    List<List<String>> ganttValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mc.id as sourceid,mc.ganttname\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_ganttSet mc on mc.appid = mt.ID where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String ganttname = Util.formatMultiLang(rs.getString("ganttname"));
            String sourceid = Util.formatMultiLang(rs.getString("sourceid"));

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(sourceid);
            value.add(ganttname);


            values.add(value);

        }

        return values;

    }

    List<List<String>> mindmapValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mc.id as sourceid,mc.name\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_mindmap mc on mc.appid = mt.ID where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String name = Util.formatMultiLang(rs.getString("name"));
            String sourceid = Util.formatMultiLang(rs.getString("sourceid"));
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(sourceid);
            value.add(name);


            values.add(value);

        }

        return values;

    }

    List<List<String>> boardValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mc.id as sourceid,mc.name\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_board mc on mc.appid = mt.ID where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String name = Util.formatMultiLang(rs.getString("name"));
            String sourceid = Util.formatMultiLang(rs.getString("sourceid"));
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            value.add(_appid);
            value.add(appname);
            value.add(sourceid);
            value.add(name);


            values.add(value);

        }

        return values;

    }


    List<List<String>> customResourceValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mc.id as sourceid,mc.resourceName\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_customResource mc on mc.appid = mt.ID where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();


            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String resourceName = Util.formatMultiLang(rs.getString("resourceName"));
            String sourceid = Util.formatMultiLang(rs.getString("sourceid"));

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(sourceid);
            value.add(resourceName);


            values.add(value);

        }

        return values;

    }

    List<List<String>> littleCardJsonValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mi.id as modeid,\n" +
                "  mi.modename,mc.id as customid,mc.customname,littleCardJson\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                " where   1=1 ");

        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            if (StringUtils.isBlank(rs.getString("littleCardJson"))) {
                continue;

            }
            String customid = rs.getString("customid");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(customid);
            value.add(customname);


            values.add(value);

        }

        return values;

    }


    List<List<String>> searchLineMergelValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select  mt.id as appid\n" +
                "   , TREEFIELDNAME as appname, mi.id as modeid,\n" +
                "  mi.modename,mc.id as customid,mc.customname,mc.linemergel\n" +
                "\n" +
                "from modetreefield mt\n" +
                "join mode_customsearch mc on mc.appid = mt.ID\n" +
                "left join modeinfo mi on mi.id= mc.MODEID\n" +
                "where mc.id in (select customid from mode_searchLineMergel) ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String customname = Util.formatMultiLang(rs.getString("customname"));

            String customid = rs.getString("customid");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(customid);
            value.add(customname);


            values.add(value);

        }

        return values;

    }

    List<List<String>> customtreefieldValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname ,mt.id as appid\n" +
                "\n" +
                "     , mi.id         as modeid\n" +
                "     , MODENAME   as modename,mc.id as treeid,mc.treename,mcd.id as nodeid,mcd.nodename ,mcd.sourceid,mcd.sourcefrom ,datacondition,rootids,mcd.tablename\n" +
                "from modetreefield mt\n" +
                "join\n" +
                "mode_customtree  mc  on mc.appid = mt.id  join mode_customtreedetail mcd on mcd.MAINID=mc.id\n" +
                " left join modeinfo mi on mi.id = mcd.sourceid\n" +
                "where  showtype = 1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String modeid = rs.getString("modeid");
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String treeid = Util.formatMultiLang(rs.getString("treeid"));
            String treename = Util.formatMultiLang(rs.getString("treename"));
            String nodeid = Util.formatMultiLang(rs.getString("nodeid"));
            String nodename = Util.formatMultiLang(rs.getString("nodename"));

            String sourcefrom = rs.getString("sourcefrom");
            String datacondition = rs.getString("datacondition");
            String tablename = rs.getString("tablename");
            String rootids = rs.getString("rootids");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }


            value.add(_appid);
            value.add(appname);
            value.add(treeid);
            value.add(treename);
            value.add(nodeid);
            value.add(nodename);
            value.add(tablename);
            value.add(sourcefrom);
            value.add(modeid);
            value.add(modename);
            value.add(datacondition);
            value.add(rootids);
            value.add(isValidMode(modeid) && rootids.equals("") && datacondition.equals("") ? "否" : "是");


            values.add(value);

        }

        return values;

    }

    List<List<String>> customtreeLinkValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mt.id               as appid,\n" +
                "       mt.TREEFIELDNAME    as appname,\n" +
                "       mct.ID              as treeid,\n" +
                "       mct.TREENAME        as treename,\n" +
                "       mcdtd.id            as nodeid,\n" +
                "       mcdtd.NODENAME      as nodename,tablename,\n" +
                "         mcdtd.SOURCEFROM         as SOURCEFROM,\n" +
                "         mcdtd.HREFTYPE    as HREFTYPE,\n" +
                "       mcdtd.HREFID        as HREFID,\n" +
                "       mcdtd.HREFTARGET    as HREFTARGET,\n" +
                "       mcdtd.DATAORDER     as DATAORDER,\n" +
                "       mcdtd.DATACONDITION as DATACONDITION\n" +
                "from mode_customtree mct\n" +
                "         left join mode_customtreedetail mcdtd\n" +
                "                   on mct.ID = mcdtd.MAINID\n" +
                "         join modetreefield mt\n" +
                "              on mct.APPID = mt.ID\n" +
                "where mct.SHOWTYPE = 0 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String treeid = Util.formatMultiLang(rs.getString("treeid"));
            String treename = Util.formatMultiLang(rs.getString("treename"));
            String nodeid = Util.formatMultiLang(rs.getString("nodeid"));
            String nodename = Util.formatMultiLang(rs.getString("nodename"));

            int sourcefrom = Util.getIntValue(Util.null2String(rs.getString("sourcefrom")));
            String sourcefromNEW = "";

            switch (sourcefrom) {
                case 1:
                    sourcefromNEW = "模块";
                    break;
                case 2:
                    sourcefromNEW = "手动输入";
                    break;
                case 3:
                    sourcefromNEW = "我的下属";
                    break;
                case 4:
                    sourcefromNEW = "组织结构";
                    break;
                case 5:
                    sourcefromNEW = "组织分部";
                    break;
                case 6:
                    sourcefromNEW = "组织部门";
                    break;
            }
            int HREFTYPE = Util.getIntValue(Util.null2String(rs.getString("HREFTYPE")));
            String HREFTYPENEW = "";

            switch (HREFTYPE) {
                case 1:
                    HREFTYPENEW = "模块";
                    break;
                case 2:
                    HREFTYPENEW = "手动输入";
                    break;
                case 3:
                    HREFTYPENEW = "模块查询列表";

            }


            String HREFTARGET = rs.getString("HREFTARGET");
            String tablename = rs.getString("tablename");
            String DATAORDER = rs.getString("DATAORDER");
            String DATACONDITION = rs.getString("DATACONDITION");
            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            String templink = HREFTARGET.replace("&_key", "");
            String type = "是";
            if ((templink.startsWith("/spa/cube/index.html#/main/cube/search?customid=") || templink.startsWith("/formmode/search/CustomSearchBySimple.jsp")) && !templink.contains("&")) {
                type = "否";
            }

            value.add(_appid);
            value.add(appname);
            value.add(treeid);
            value.add(treename);
            value.add(nodeid);
            value.add(nodename);
            value.add(tablename);
            value.add(sourcefromNEW);
            value.add(HREFTYPENEW);
            value.add(HREFTARGET);
            value.add(DATAORDER);
            value.add(DATACONDITION);
            value.add(type);


            values.add(value);

        }

        return values;

    }


    List<List<String>> customPageLinkValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select TREEFIELDNAME as appname,\n" +
                "       mt.id         as appid,\n" +
                "       mc.id         as customid,\n" +
                "       mc.customname,\n" +
                "       mcu.hrefname,\n" +
                "       mcu.hrefaddress\n" +
                "\n" +
                "from modetreefield mt\n" +
                "         join\n" +
                "     mode_custompage mc on mc.appid = mt.id\n" +
                "         left join mode_custompagedetail mcu on mcu.mainid = mc.id where 1=1 ");


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String customid = Util.formatMultiLang(rs.getString("customid"));
            String customname = Util.formatMultiLang(rs.getString("customname"));
            String hrefname = Util.formatMultiLang(rs.getString("hrefname"));
            String hrefaddress = Util.formatMultiLang(rs.getString("hrefaddress"));

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }
            String templink = hrefaddress.replace("&_key", "");
            String type = "是";
            if ((templink.startsWith("/spa/cube/index.html#/main/cube/search?customid=") || templink.startsWith("/formmode/search/CustomSearchBySimple.jsp")) && !templink.contains("&")) {
                type = "否";
            }

            value.add(_appid);
            value.add(appname);
            value.add(customid);
            value.add(customname);
            value.add(hrefname);
            value.add(hrefaddress);
            value.add(type);


            values.add(value);

        }

        return values;

    }


    List<List<String>> listfieldValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = "select TREEFIELDNAME as appname ,mt.id as appid " +
                "   , mi.id         as modeid " +
                "   , MODENAME   as modename,mb.name,mb.showname,mb.id as e9browserid,mc.searchconditiontype " +
                "  as conditiontype,conditionsql,conditiontype,sqltext,javafileAddress " +
                "from modetreefield mt  " +
                "join mode_custombrowser mc on mc.appid = mt.id join mode_browser mb on mb.customid=mc.id " +
                "  left join  modeinfo mi on mi.id = mc.MODEID " +
                "where  1=1 ";


        if (!appid.equals("")) {
            sql += " and mt.id=" + appid;
        }
        sql += " order by mt.id";
        rs.executeQuery(sql);
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));

            String modeid = Util.formatMultiLang(rs.getString("modeid"));
            String modename = Util.formatMultiLang(rs.getString("modename"));
            String name = Util.formatMultiLang(rs.getString("name"));
            String showname = Util.formatMultiLang(rs.getString("showname"));
            String e9browserid = Util.formatMultiLang(rs.getString("e9browserid"));
            String conditiontype = getConditionType(rs.getString("conditiontype"));
            String conditionsql = Util.formatMultiLang(rs.getString("conditionsql"));
            String sqltext = Util.formatMultiLang(rs.getString("sqltext"));
            String javafileAddress = Util.formatMultiLang(rs.getString("javafileAddress"));

            if (!isValidApp(_appid)) {//非有效的应用不需要抓取
                continue;
            }

            if (!isValidBrowser(e9browserid)) {
                continue;
            }

            String type = "是";
            if (isValidMode(modeid) && conditionsql.equals("") && javafileAddress.equals("") && !sqltext.contains("where")) {
                type = "否";
            }

            value.add(_appid);
            value.add(appname);
            value.add(modeid);
            value.add(modename);
            value.add(isValidMode(modeid) ? "是" : "否");

            value.add(getModeDataCount(modeid));
            value.add(getmodedatacreatedate(modeid));
            value.add(name);
            value.add(showname);
            value.add(sqltext);
            value.add(conditiontype);
            value.add(conditionsql);
            value.add(javafileAddress);
            value.add(type);


            values.add(value);

        }

        return values;

    }

    String getbtnactionname(String interfacetype, String name, String dmlactionid, String triggerworkflowsetid, String taskdetailid) {

        RecordSet rs = new RecordSet();
        int intValue = Util.getIntValue(interfacetype);

        switch (intValue) {
            case 1:
                rs.executeQuery("select * from mode_triggerworkflowset where id= ?", triggerworkflowsetid);
                if (rs.next()) {
                    name = rs.getString("triggername");
                }
                break;
            case 4:
                name = getDmlActionName(dmlactionid);
                break;
            case 5:
                rs.executeQuery("select * from mode_timedtask_detail where id= ?", taskdetailid);
                if (rs.next()) {
                        name = rs.getString("name");
                }
                break;

        }

        return name;


    }

    String getRightTypeText(String _righttype, String _sharetype) {


        String type = "";
        int righttype = Util.getIntValue(_righttype);
        int sharetype = Util.getIntValue(_sharetype);

        if (righttype == 0 && (sharetype < 80 || sharetype > 100)) {
            type = "创建权限";
        } else if (sharetype >= 80 && sharetype <= 100) {
            type = "创建人相关权限";
        } else if ((righttype == 1 || righttype == 2 || righttype == 3) && (sharetype < 80 || sharetype > 100)) {
            type = "默认共享权限";
        } else if (righttype == 4 && (sharetype < 80 || sharetype > 100)) {
            type = "监控权限";
        } else if (righttype == 5 && (sharetype < 80 || sharetype > 100)) {
            type = "导出";
        }
        return type;
    }

    String getFieldText(String fieldid) {

        RecordSet rs = new RecordSet();

        rs.executeQuery("select fieldlabel from workflow_billfield where id = ?", fieldid);

        if (rs.next()) {

            fieldid += "_" +
                    getLabelName(rs.getString("fieldlabel"));
        }
        return fieldid;

    }

%>
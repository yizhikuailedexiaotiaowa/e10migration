<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.conn.RecordSet" %>
<%!

    //总数据量
    List<String> tableField1 = new ArrayList<String>(Arrays.asList("总表数量", "数据总量"));
    //top50表info
    List<String> tableField2 = new ArrayList<String>(Arrays.asList("表名", "数据总量", "是否为标准表"));
    //非标准表info
    List<String> tableField3 = new ArrayList<String>(Arrays.asList("表名", "数据总量"));

    String DBTYPE = new RecordSet().getDBType();

    String LIMITCOUNT = "50";

    /**
     * 功能维度
     */
    public String doDataAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("数据量统计", path);
            getAllDataCount(writeEntity, "数据量分析");
            String filePath = excelWrite_html(writeEntity);
            System.out.println("filePath = " + filePath);
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    void getAllDataCount(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        try {
            List<List<String>> values1 = new ArrayList<List<String>>();
            List<List<String>> values2 = new ArrayList<List<String>>();
            List<List<String>> values3 = new ArrayList<List<String>>();
            RecordSet rs1 = new RecordSet();
            RecordSet rs2 = new RecordSet();
            RecordSet rs3 = new RecordSet();
            String countSql1 = "";
            String countSql2 = "";
            String countSql3 = "";

            //数据总量
            List<String> value1 = new ArrayList<String>();
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "SELECT COUNT(table_name) AS tableCount1,SUM(TABLE_ROWS) AS tableDataCount1\n" +
                        "FROM information_schema.TABLES\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ('VIEW','BASE TABLE')";
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "SELECT count(a.name) as tableCount1,\n" +
                        "       sum(b.rows)   as tableDataCount1\n" +
                        "FROM sysobjects AS a\n" +
                        "         INNER JOIN sysindexes AS b ON a.id = b.id\n" +
                        "WHERE (a.type = 'u')\n" +
                        "  AND (b.indid IN (0, 1))";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "select count(u.TABLE_NAME) as tableCount1, sum(u.NUM_ROWS) as tableDataCount1\n" +
                        "from user_tables u\n" +
                        "WHERE u.NUM_ROWS IS not NULL";
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {

            } else {

            }
            rs1.executeQuery(countSql1);
            String tableCount1 = "";
            String tableDataCount1 = "";
            if (rs1.next()) {
                tableCount1 = rs1.getString(1);
                tableDataCount1 = rs1.getString(2);
            }
            value1.add(tableCount1);
            value1.add(tableDataCount1);
            values1.add(value1);


            //top50表数据量分析 不含表单
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TABLE_ROWS , TABLE_NAME \n" +
                        "FROM information_schema.TABLES a\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ('VIEW', 'BASE TABLE')\n" +
                        "  and not EXISTS(select 1 from workflow_bill where a.TABLE_NAME = workflow_bill.tablename or a.TABLE_NAME = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.TABLE_NAME = Workflow_billdetailtable.tablename) " +
                        " order by TABLE_ROWS desc\n" +
                        "limit \n" + LIMITCOUNT;
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TOP " + LIMITCOUNT + " a.name, b.rows \n" +
                        "FROM sysobjects AS a\n" +
                        "         INNER JOIN sysindexes AS b ON a.id = b.id\n" +
                        "WHERE (a.type = 'u')\n" +
                        "  AND (b.indid IN (0, 1))\n" +
                        "  and not EXISTS (select 1 from workflow_bill  where a.name = workflow_bill.tablename  or a.name = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.name = Workflow_billdetailtable.tablename) " +
                        " order by b.rows desc\n";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TABLE_NAME , NUM_ROWS \n" +
                        "FROM (SELECT TABLE_NAME, NUM_ROWS\n" +
                        "      FROM user_tables a\n" +
                        "      WHERE NUM_ROWS IS NOT NULL\n" +
                        "      and not EXISTS(select 1 from workflow_bill where a.TABLE_NAME = workflow_bill.tablename  or a.TABLE_NAME = 'workflow_form')\n" +
                        "      and not EXISTS(select 1 from Workflow_billdetailtable where a.TABLE_NAME = Workflow_billdetailtable.tablename) " +
                        "      ORDER BY NUM_ROWS DESC" +
                        ")\n" +
                        "WHERE ROWNUM <= " + LIMITCOUNT;
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {
            } else {
            }
            rs2.executeQuery(countSql2);
            while (rs2.next()) {
                List<String> value2 = new ArrayList<String>();
                //是否为标准表
                Boolean isStandradTable = false;
                String tableName2 = rs2.getString(1);
                String tableDataCount2 = rs2.getString(2);
                if (containsIgnoreCase(E9_STANDARD_TABLE, tableName2)) {
                    isStandradTable = true;
                }
                //标准表top统计需要跳过无意义的子表 , 表单表和表单明细表已通过sql过滤
                if (containsIgnoreCase(E9_STANDARD_TABLE_FILTER, tableName2)) {
                    continue;
                }
                value2.add(tableName2.toLowerCase());
                value2.add(tableDataCount2);
                value2.add(isStandradTable ? "是" : "否");
                values2.add(value2);
            }


            //非标准表统计
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT TABLE_ROWS, TABLE_NAME\n" +
                        "FROM information_schema.TABLES a\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ('VIEW', 'BASE TABLE')\n" +
                        "order by TABLE_ROWS desc";
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT  a.name, b.rows \n" +
                        "FROM sysobjects AS a\n" +
                        "         INNER JOIN sysindexes AS b ON a.id = b.id\n" +
                        "WHERE (a.type = 'u')\n" +
                        "  AND (b.indid IN (0, 1))\n" +
                        "order by b.rows desc";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT TABLE_NAME, NUM_ROWS\n" +
                        "      FROM user_tables a\n" +
                        "      WHERE NUM_ROWS IS NOT NULL\n" +
                        "      ORDER BY NUM_ROWS DESC\n";
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {
            } else {
            }
            rs3.executeQuery(countSql3);
            while (rs3.next()) {
                List<String> value3 = new ArrayList<String>();
                String tableName3 = rs3.getString(1);
                String tableDataCount3 = rs3.getString(2);
                if (containsIgnoreCase(E9_STANDARD_TABLE, tableName3)) {
                    continue;
                }
                value3.add(tableName3.toLowerCase());
                value3.add(tableDataCount3);
                values3.add(value3);
            }


            addExcelModal(writeEntity, sheetname, "总数据量", tableField1, values1);
            addExcelModal(writeEntity, sheetname, "前50数据的表(不含表单表)", tableField2, values2);
            addExcelModal(writeEntity, sheetname, "非标准表及数据量", tableField3, values3);
        } catch (Exception e) {
            System.out.println("e.getMessage() = " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

    public boolean containsIgnoreCase(String[] array, String target) {
        for (String str : array) {
            //标准表里带*号的
            int indexOfAsterisk = str.indexOf('*');
            if (indexOfAsterisk != -1) {
                str = str.substring(0, indexOfAsterisk);
                if (target.toLowerCase().contains(str.toLowerCase())) {
                    return true;
                }
            } else {
                if (str.equalsIgnoreCase(target)) {
                    return true;
                }
            }
        }
        return false;
    }

%>
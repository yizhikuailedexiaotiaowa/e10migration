<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="weaver.hrm.resource.ResourceComInfo" %>
<%@ page import="com.alibaba.fastjson.JSONObject" %>
<%!

    //流程表单信息
    List<String> portalHead = new ArrayList<String>(Arrays.asList("飘窗名称", "是否启用", "图片链接", "操作说明"));

    /**
     * 功能维度
     */
    public String doPoralAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("门户模块_功能维度", path);
            getHpFloatArea(writeEntity, "门户飘窗");

            String filePath = "";
            if (writeEntity.getSheets().size() > 0) {
                filePath = excelWrite_html(writeEntity);
            }
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void getHpFloatArea(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery(" select * from hpfloatarea ");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String nname = rs.getString("nname");//飘窗名称
            String isuse = rs.getString("isuse");//是否启用
            String detailContent = rs.getString("detailContent");//具体设置
            String picSrc = "";//图片路径
            JSONObject detailContentJson = new JSONObject();
            if (!"".equals(detailContent)) {
                try {
                    detailContentJson = JSON.parseObject(detailContent);
                    picSrc = detailContentJson.getString("picSrc"); //图片路径

                } catch (Exception ex) {
                }
            }

            value.add(nname);
            value.add("1".equals(isuse) ? "启用" : "未启用");
            value.add(picSrc);
            value.add("需要重新上传飘窗图片");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, portalHead, values);
    }


%>
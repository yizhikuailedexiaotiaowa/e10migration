<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="weaver.conn.RecordSetTrans" %>
<%!

    public void createDataSetTable() throws Exception {
        RecordSetTrans rs = new RecordSetTrans();

        String addSql = "";
        String dbType = rs.getDBType().toLowerCase();
        if (dbType.equals("sqlserver")) {
            addSql = "create table dataset_sync (uuid varchar(36), is_valid varchar(5), dataSet_Type varchar(36), dataset_sql text, show_fields text, have_showFields varchar(5), table_form  text)";
        }else if (dbType.equals("mysql")) {

            addSql = "create table dataset_sync (uuid varchar(36), is_valid varchar(5), dataSet_Type varchar(36), dataset_sql longtext, show_fields longtext, have_showFields varchar(5), table_form  longtext)";
        } else if(dbType.equals("postgresql"))
        {
            addSql = "create table dataset_sync (uuid varchar(36), is_valid varchar(5), dataSet_Type varchar(36), dataset_sql text, show_fields text, have_showFields varchar(5), table_form  text)";
        } else if (dbType.equals("oracle") || dbType.equals("dm")) {
            addSql = "create table dataset_sync (uuid varchar(36), is_valid varchar(5), dataSet_Type varchar(36), dataset_sql clob, show_fields clob, have_showFields varchar(5), table_form  clob)";

        } else {
            addSql = "create table dataset_sync (uuid varchar(36), is_valid varchar(5), dataSet_Type varchar(36), dataset_sql text, show_fields text, have_showFields varchar(5), table_form  text)";
        }

        rs.executeSql(addSql);


    }


    public static boolean judgeExistTable(String tablename) throws Exception {
        boolean exist = false;
        RecordSet rs = new RecordSet();
        boolean isoracle = "oracle".equalsIgnoreCase(rs.getDBType());
        boolean isdb2 = "db2".equalsIgnoreCase(rs.getDBType());
        try {
            String sql = "";
            if (isoracle)
                sql = "select 1 from user_tables where table_name = upper('"+tablename+"')";
            else if(isdb2)
                sql = "select 1 from SYSIBM.SYSTABLES where lower(name) = lower('"+tablename+"')";
            else if(rs.getDBType().equals("mysql"))
                sql = "select 1 from information_schema.Tables where table_schema = database() and Table_Name='" + tablename + "' ";
            else if(rs.getDBType().equals("postgresql"))
                sql = "select 1 from information_schema.Tables where table_schema in ('public',current_user) and lower(Table_Name)=lower('" + tablename + "') ";
            else
                sql = "select 1 from sysobjects where name = '"+tablename+"' and objectproperty(id, 'IsUserTable') = 1";
            rs.executeSql(sql);
            if(rs.next())
                exist = true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("判断表"+tablename+"是否存在出现异常", e);
        }
        return exist;
    }

    public static boolean getCheckFieldExistSql(String tablename,String fieldName){

        String  sql = "";
        RecordSet rs = new RecordSet();
        String dbtype= rs.getDBType();

        if ("oracle".equalsIgnoreCase(dbtype)) {
            sql =  "select count(1) as cnt from  USER_TAB_COLUMNS WHERE TABLE_NAME = UPPER('"+tablename+"') AND COLUMN_NAME = '"+fieldName+"' ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {
            sql = "select count(*) as cnt from information_schema.columns " +
                    "        where table_name ='"+tablename+"' and column_name ='"+fieldName+"' and TABLE_SCHEMA = (select database())";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            sql =  "select count(1) as cnt from information_schema.columns WHERE table_schema=(SELECT current_schema()) " +
                    "and table_name = lower('"+tablename+"') and column_name = lower('"+fieldName+"') ";
        } else {
            sql =  "select count(1) as cnt from syscolumns  where id=object_id('"+tablename+"') and name='"+fieldName+"'";
        }


        rs.executeQuery(sql);
        if(rs.next()){
            int cnt=Util.getIntValue(rs.getString("cnt"),-1);
            if(cnt>0){
                return true;
            }
        }
        return false;

    }




    public static HashMap<String, Object> qysNeedUpdate() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        String feibiaoCheckSql = "select distinct t1.id,\n" +
                "                t1.num,\n" +
                "                case\n" +
                "                    when t4.labelname is null then t1.NAME\n" +
                "                    when t4.indexid = 0 then t1.NAME\n" +
                "                    ELSE t4.labelname end as name,\n" +
                "                t1.classpath,\n" +
                "                t2.status\n" +
                "from hp_nonstandard_function_info t1\n" +
                "         LEFT JOIN hp_nonstandard_func_server t2 on t2.funcid = t1.num\n" +
                "         LEFT JOIN hp_server_info t3 on t3.id = t2.serverid\n" +
                "         LEFT JOIN (select INDEXID, labelname from htmllabelinfo where LANGUAGEID = 7) t4 on t4.INDEXID = t1.fieldlabel\n" +
                "where t2.status = 1\n" +
                "and (t1.num = '119' or name = '契约锁集成')\n" +
                "order by t1.num asc";
        RecordSet versionRs = new RecordSet();
        versionRs.executeSql(feibiaoCheckSql);
        if (versionRs.next()) {
            //检查版本
            String versionSql = "select * from wf_qiyuesuoVersion";
            versionRs.executeSql(versionSql);
            versionRs.next();
            try {
                int version = versionRs.getInt("version");
                if (version < 20220930) {
                    result.put("version", version);
                    result.put("shouldUpdate", "1");
                } else {
                    result.put("version", version);
                    result.put("shouldUpdate", "0");
                }
            } catch (Exception e) {
                versionRs.writeLog("e:", e.getMessage());
                throw new RuntimeException(e);
            }

        } else {
            result.put("version", "-1");
            result.put("shouldUpdate", "2");
        }
        return result;
    }

    /**
     * 获取e8升级上来的需要修复流程图节点坐标的流程信息
     * @create 2023/9/25 11:50
     * @author mcd
     *
     * @return java.util.HashMap
     **/
    public List<Map<String, String>> getFixE8ChartInfo() {
        List<Map<String, String>> workflowInfos = new ArrayList();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select id,workflowname from workflow_base where id in (select groupworkflowid from (select  workflowid as groupworkflowid, count(1) as count from workflow_nodelink  where drawstyle is null or drawstyle = '' " +
                "group by workflowid having count(1) > 0) c where count = (select count(1) from workflow_nodelink where workflowid = groupworkflowid))");
        while (rs.next()) {
            Map<String, String> workflowInfo = new HashMap();
            String id = Util.null2String(rs.getString("id"));
            String workflowName = Util.null2String(rs.getString("workflowname"));
            workflowInfo.put("id", id);
            workflowInfo.put("workflowName", workflowName);
            workflowInfos.add(workflowInfo);
        }
        return workflowInfos;
    }


    public  String  getDataSetInfo(){

        String dataSetNum = "";
        RecordSet rs = new RecordSet();


        rs.executeQuery("select count(1)  from  edc_reportdataset");
        while(rs.next()) {
            dataSetNum = rs.getString(1);
        }

        return dataSetNum;

    }


%>
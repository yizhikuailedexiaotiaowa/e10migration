<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="weaver.hrm.company.SubCompanyComInfo" %>
<%@include file="util/E10workflowUtil.jsp" %>
<%@include file="/e10Migration/constant/constant.jsp" %>

<%!


    //流程表单信息
    List<String> formdata = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "表单表名", "数据量", "数据最新时间"));
    //字段联动
    List<String> fdlinkSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "联动名称", "触发字段", "是否启用", "sql文本"));
    //字段属性
    List<String> nodeFdlinkSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "表单名称", "节点名称", "字段名称", "sql文本"));
    //dml附加操作
    List<String> dmlSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "DMl名称", "sql文本"));
    //action附加操作
    List<String> actionSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "class路径", "接口类型"));
    //webservice附加操作
    List<String> webserviceSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "webservice接口名称", "webservice地址", "回写sql"));
    //esb附加操作
    List<String> esbSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "引用esb"));
    List<String> sapActionSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "sap名称", "异构系统", "数据源", "注册服务名"));
    //自定义页面
    List<String> custompageSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "文件路径", "是否启用"));
    //代码块
    List<String> customcodeSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "表单名称", "节点名称", "代码内容"));
    List<String> nodelinkconditionSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "出口名称", "条件内容"));
    List<String> piciconditionSheet = new ArrayList<String>(Arrays.asList("流程名称", "流程状态", "节点名称", "操作组名称", "条件sql", "条件内容"));
    List<String> dubanconditionSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "表单名称", "节点名称", "代码内容"));

    //开启了sap多选按钮的流程,节点
    List<String> sapSheet = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "sap源"));

    // 节点操作组 使用外部接口(自定义java 、自定义sql )
    List<String> groupoutdetail = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "节点名称", "操作组名称", "外部接口类型", "外部接口名称", "外部接口内容"));
    //流程交换的列表
    List<String> workflowChangeSheet = new ArrayList<String>(Arrays.asList("流程交换名称", "交换类型", "流程名称", "所属分部", "交换主表", "是否启用", "说明"));

    //流程创建日程的列表
    List<String> workflowCreatePlanSheet = new ArrayList<String>(Arrays.asList("动作", "流程名称", "触发节点", "说明"));


    /**
     * 功能维度
     */
    public String doWorkFlowAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("工作流程_功能维度", path);
            getFormData(writeEntity, "流程表单信息");
            getFdlinkSheet(writeEntity, "流程字段联动");
            getNodeFdlinkSheet(writeEntity, "流程字段属性sql");
            getWorkflowActionSet(writeEntity, "流程节点附加操作");
            getCustompage(writeEntity, "流程自定义页面");
            getCustomcode(writeEntity, "流程表单代码块");
            getCondition(writeEntity, "流程老规则条件");
            getSap(writeEntity, "SAP关联流程");
            getWorkflowChange(writeEntity, "流程交换列表");
            getWorkflowCreatePlan(writeEntity, "流程创建日程列表");
            getWorkflowgroupoutdetail(writeEntity, "流程节点操作组外部接口");

            //ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
            String filePath = excelWrite_html(writeEntity);
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 获取表单数据信息
     *
     * @param writeEntity
     * @param sheetname
     */
    void getFormData(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values1 = new ArrayList<List<String>>();
        List<List<String>> values2 = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        //老表单
        rs.executeQuery("select * from workflow_base  where  isbill=0 and ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowid = Util.null2String(rs.getString("id"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String totalnum = "";
            String createdate = "";
            rs1.executeQuery("select count(a.REQUESTID) totalnum, max(b.CREATEDATE) createdate from workflow_form a, workflow_requestbase b where a.requestId = b.REQUESTID and b.WORKFLOWID =?", workflowid);
            if (rs1.next()) {
                totalnum = rs1.getString("totalnum");//表单数量
                createdate = rs1.getString("createdate");//最新创建日期
            }
            value.add(workflowname);
            value.add(subCompanyname);
            value.add("workflow_form");
            value.add(totalnum);
            value.add(createdate);

            values1.add(value);
        }
        //新表单
        rs.executeQuery("select a.id,a.WORKFLOWNAME,b.TABLENAME from workflow_base a left join workflow_bill b on a.FORMID = b.id where a.isbill=1 and a.ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String tablename = Util.null2String(rs.getString("TABLENAME"));
            String totalnum = "";
            String createdate = "";
            rs1.executeQuery("select count(a.id) totalnum,max(b.CREATEDATE) createdate from " + tablename + " a,workflow_requestbase b where a.requestId=b.REQUESTID ");
            if (rs1.next()) {
                totalnum = rs1.getString("totalnum");//表单数量
                createdate = rs1.getString("createdate");//最新创建日期
            }
            value.add(workflowname);
            value.add(subCompanyname);
            value.add(tablename);
            value.add(totalnum);
            value.add(createdate);

            values2.add(value);
        }


        addExcelModal(writeEntity, sheetname, "老表单流程", formdata, values1);
        addExcelModal(writeEntity, sheetname, "新表单流程", formdata, values2);


    }

    private void getFdlinkSheet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> values = new ArrayList<List<String>>();

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        RecordSet rs2 = new RecordSet();
        RecordSet rs3 = new RecordSet();
        // 查询当前流程绑定的字段联动
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflow_datainput_entry a,workflow_base b where a.WORKFLOWID=b.id and  b.ISVALID=1 order by a.WORKFLOWID");
        while (rs.next()) {

            String entryid = Util.null2String(rs.getString("id"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String enable = Util.null2String(rs.getString("enable")).equals("0") ? "启用" : "未启用";
            String triggername = Util.null2String(rs.getString("TRIGGERNAME"));
            String triggerfieldid = Util.null2String(rs.getString("TRIGGERFIELDNAME"));
            //0、主字段，1、明细字段
            String detailtype = Util.null2String(rs.getString("type"));
            if (triggerfieldid.startsWith("field")) {
                triggerfieldid = triggerfieldid.substring(5);
            }
            String fieldName = getFieldName(workflowid, triggerfieldid, detailtype);

            rs1.executeQuery("select * from workflow_datainput_main where ENTRYID=?", entryid);
            while (rs1.next()) {
                String backfields = "";
                String sqlfrom = "";
                String sqlwhere = "";

                String mainId = Util.null2String(rs1.getString("id"));
                rs2.executeQuery("select * from workflow_datainput_table  where DATAINPUTID=?", mainId);
                while (rs2.next()) {
                    String tableid = Util.null2String(rs2.getString("id"));
                    String tablename = Util.null2String(rs2.getString("TABLENAME"));
                    String alias = Util.null2String(rs2.getString("ALIAS"));
                    if ("".equals(alias)) {
                        alias = tablename;
                        sqlfrom = sqlfrom + tablename + ",";
                    } else {
                        sqlfrom = sqlfrom + tablename + " " + alias + ",";
                    }


                    rs3.executeQuery("select * from workflow_datainput_field  where DATAINPUTID=? and TABLEID=?", mainId, tableid);
                    while (rs3.next()) {
                        String type = Util.null2String(rs3.getString("type"));// 1为条件字段，2为查询字段
                        String dbfieldname = Util.null2String(rs3.getString("DBFIELDNAME"));
                        String pagefieldname = Util.null2String(rs3.getString("PAGEFIELDNAME"));
                        if ("1".equals(type)) {// 拼接条件
                            sqlwhere = sqlwhere + alias + "." + dbfieldname + "='" + pagefieldname + "'" + " and ";
                        }
                        if ("2".equals(type)) {
                            backfields = backfields + alias + "." + dbfieldname + ",";
                        }
                    }
                }
                backfields = backfields.trim();
                sqlfrom = sqlfrom.trim();
                sqlwhere = sqlwhere.trim();
                if (backfields.endsWith(",")) {
                    backfields = backfields.substring(0, backfields.length() - 1);
                }
                if (sqlfrom.endsWith(",")) {
                    sqlfrom = sqlfrom.substring(0, sqlfrom.length() - 1);
                }
                if (sqlwhere.endsWith("and")) {
                    sqlwhere = sqlwhere.substring(0, sqlwhere.length() - 3);
                }

                String whereclause = Util.null2String(rs1.getString("WHERECLAUSE")).trim();
                if (whereclause.toLowerCase().startsWith("where")) {
                    whereclause.substring(5);
                }
                String finalSql = "select ";
                if ("".equals(backfields)) {
                    backfields = "*";
                }
                finalSql = finalSql + backfields + " from " + sqlfrom + " where 1=1 ";
                if (!"".equals(whereclause)) {
                    finalSql = finalSql + " and " + whereclause;
                }
                if (!"".equals(sqlwhere)) {
                    finalSql = finalSql + " and " + sqlwhere;
                }
                List<String> value = new ArrayList<String>();
                value.add(workflowname);
                value.add(subCompanyname);
                value.add(triggername);
                value.add(fieldName);
                value.add(enable);
                value.add(finalSql);


                values.add(value);
            }

        }
        addExcelModal(writeEntity, sheetname, sheetname, fdlinkSheet, values);

    }


    private void getNodeFdlinkSheet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();


        String nodesql = "select b.*, a.* " +
                " from workflow_nodefieldattr a " +
                "         left join " +
                "     (select c.WORKFLOWID, d.SUBCOMPANYID, d.WORKFLOWNAME, d.ISVALID, c.NODEID " +
                "      from workflow_flownode c " +
                "               left join workflow_base d on c.WORKFLOWID = d.id) b " +
                "     on a.NODEID = b.NODEID " +
                " where b.ISVALID = 1 " +
                "  and b.WORKFLOWID > 0  and a.attrcontent like '%doFieldSQL%'";

        rs.executeQuery(nodesql);
        while (rs.next()) {
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String workflowid = Util.null2String(rs.getString("WORKFLOWID"));
            String nodeid = Util.null2String(rs.getString("nodeid"));
            String fieldid = Util.null2String(rs.getString("FIELDID"));
            String formid = Util.null2String(rs.getString("FORMID"));
            String sql_cus = Util.null2String(rs.getString("ATTRCONTENT"));

            int index = sql_cus.indexOf("doFieldSQL(\"");
            if (index > -1) {
                sql_cus = sql_cus.substring(index + 12);
                index = sql_cus.lastIndexOf("\")");
                if (index > -1) {
                    sql_cus = sql_cus.substring(0, index);
                }

                sql_cus = sql_cus.trim();
            }

            List<String> value = new ArrayList<String>();
            value.add(workflowname);
            value.add(subCompanyname);
            value.add(getFormName(workflowid, formid));
            value.add(getNodeName(nodeid));
            value.add(getFieldName(workflowid, fieldid, "0"));
            value.add(sql_cus);

            values.add(value);
        }
        addExcelModal(writeEntity, sheetname, sheetname, nodeFdlinkSheet, values);
    }

    private void getWorkflowActionSet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> actionValues = new ArrayList<List<String>>();
        List<List<String>> dmlValues = new ArrayList<List<String>>();
        List<List<String>> wsValues = new ArrayList<List<String>>();
        List<List<String>> esbValues = new ArrayList<List<String>>();
        List<List<String>> sapValues = new ArrayList<List<String>>();
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();

        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();

        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflowactionset a , workflow_base b where a.WORKFLOWID=b.id  and b.ISVALID=1  order by a.WORKFLOWID");
        while (rs.next()) {
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String actionname = Util.null2String(rs.getString("actionname"));
            int nodeid = Util.getIntValue(rs.getString("NODEID"), 0);
            int nodelinkid = Util.getIntValue(rs.getString("NODELINKID"), 0);
            String interfaceid = Util.null2String(rs.getString("INTERFACEID"));
            int isused = Util.getIntValue(Util.null2String(rs.getString("ISUSED")), 0);
            int interfacetype = Util.getIntValue(Util.null2String(rs.getString("interfacetype")));// 动作流类型 1.dml 2.webservice 3.action 6.esb


            switch (interfacetype) {
                case 1: //dml

                    List<String> dmlValue = new ArrayList<String>();
                    String sqldml = "select  * from formactionset a,formactionsqlset b where a.id = ? and a.id=b.ACTIONID";
                    rs1.executeQuery(sqldml, interfaceid);
                    if (rs1.next()) {
                        String dmlname = Util.null2String(rs1.getString("DMLACTIONNAME"));
                        String dmlsql = Util.null2String(rs1.getString("DMLSQL"));
                        String dmlcussql = Util.null2String(rs1.getString("DMLCUSSQL"));

                        dmlValue.add(workflowname);
                        dmlValue.add(subCompanyname);
                        dmlValue.add(actionname);
                        dmlValue.add(nodeid > 0 ? "节点" : "出口");
                        dmlValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        dmlValue.add(isused == 1 ? "启用" : "未启用");
                        dmlValue.add(dmlname);
                        dmlValue.add("".equals(dmlsql) ? dmlcussql : dmlsql);

                        dmlValues.add(dmlValue);
                    }
                    break;
                case 2://webservice
                    List<String> wsValue = new ArrayList<String>();
                    String wsaction = "select b.CUSTOMNAME,b.WEBSERVICEURL,a.* from wsformactionset a ,wsregiste b where a.WSURL = b.id and a.id = ?";
                    rs1.executeQuery(wsaction, interfaceid);
                    if (rs1.next()) {
                        String wsactionname = Util.null2String(rs1.getString("ACTIONNAME"));
                        String customname = Util.null2String(rs1.getString("CUSTOMNAME"));
                        String webserviceurl = Util.null2String(rs1.getString("WEBSERVICEURL"));
                        String retstr = Util.null2String(rs1.getString("RETSTR"));

                        wsValue.add(workflowname);
                        wsValue.add(subCompanyname);
                        wsValue.add(actionname);
                        wsValue.add(nodeid > 0 ? "节点" : "出口");
                        wsValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        wsValue.add(isused == 1 ? "启用" : "未启用");
                        wsValue.add(wsactionname);
                        wsValue.add(customname);
                        wsValue.add(webserviceurl);
                        wsValue.add(retstr);


                        wsValues.add(wsValue);

                    }

                    break;
                case 3://action
                    List<String> actionValue = new ArrayList<String>();
                    String sqlaction = "select  * from actionsetting where actionname = ?";
                    rs1.executeQuery(sqlaction, interfaceid);
                    if (rs1.next()) {
                        String actionshowname = Util.null2String(rs1.getString("ACTIONSHOWNAME"));
                        String actionclass = Util.null2String(rs1.getString("ACTIONCLASS"));
                        boolean isStandarAction = false;
                        List<String> list = Arrays.asList(STANDARACTION);
                        if (list.contains(actionclass)) {
                            isStandarAction = true;
                        }
                        actionValue.add(workflowname);
                        actionValue.add(subCompanyname);
                        actionValue.add(actionname);
                        actionValue.add(nodeid > 0 ? "节点" : "出口");
                        actionValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        actionValue.add(isused == 1 ? "启用" : "未启用");
                        actionValue.add(Util.formatMultiLang(actionshowname, "7"));
                        actionValue.add(actionclass);
                        actionValue.add(isStandarAction ? "标准接口" : "自定义接口");

                        actionValues.add(actionValue);
                    }
                    break;
                case 6://esb
                    List<String> esbValue = new ArrayList<String>();
                    String sqlesb = "select  * from esb_actionset where actionname = ?";
                    rs1.executeQuery(sqlesb, interfaceid);
                    if (rs1.next()) {
                        String esbid = Util.null2String(rs1.getString("esbid"));
                        esbValue.add(workflowname);
                        esbValue.add(subCompanyname);
                        esbValue.add(actionname);
                        esbValue.add(nodeid > 0 ? "节点" : "出口");
                        esbValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        esbValue.add(isused == 1 ? "启用" : "未启用");
                        esbValue.add(Util.formatMultiLang(interfaceid, "7"));
                        esbValue.add(esbid);

                        esbValues.add(esbValue);
                    }
                    break;

                case 4://sap动作
                    List<String> sapValue = new ArrayList<String>();
                    String sqlsap = "select * from int_BrowserbaseInfo where MARK = ?";
                    rs1.executeQuery(sqlsap, interfaceid);
                    if (rs1.next()) {

                        String regservice = Util.null2String(rs1.getString("REGSERVICE"));//所属服务
                        int w_enable = Util.getIntValue(rs1.getString("w_enable"));//是否启用

                        sapValue.add(workflowname);
                        sapValue.add(subCompanyname);
                        sapValue.add(actionname);
                        sapValue.add(nodeid > 0 ? "节点" : "出口");
                        sapValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        sapValue.add(w_enable == 1 ? "启用" : "未启用");
                        sapValue.add(Util.formatMultiLang(interfaceid, "7"));
                        String sql = "select b.HETENAME,c.POOLNAME, a.* from  sap_service a " +
                                "left join int_heteproducts b on  a.HPID=b.id   " +
                                "left join sap_datasource c on a.POOLID=c.id  " +
                                "where a.id=?";
                        rs1.executeQuery(sql, regservice);
                        if (rs1.next()) {
                            sapValue.add(Util.null2String(rs1.getString("HETENAME")));
                            sapValue.add(Util.null2String(rs1.getString("POOLNAME")));
                            sapValue.add(Util.null2String(rs1.getString("REGNAME")));
                        }

                        sapValues.add(sapValue);
                    }
                    break;
                default:
                    break;

            }
        }

        addExcelModal(writeEntity, sheetname, "节点/出口action", actionSheet, actionValues);
        addExcelModal(writeEntity, sheetname, "节点/出口dml", dmlSheet, dmlValues);
        addExcelModal(writeEntity, sheetname, "节点/出口webservice", webserviceSheet, wsValues);
        addExcelModal(writeEntity, sheetname, "节点/出口esb", esbSheet, esbValues);
        addExcelModal(writeEntity, sheetname, "节点/出口sap动作流", sapActionSheet, sapValues);

    }

    private void getCustompage(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from workflow_base where ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String custompage = Util.null2String(rs.getString("custompage")).trim();

            List<String> list = Arrays.asList(CUSTOMPAGE);
            boolean isbiaozhun = false;
            for (String page : list) {
                if (custompage.indexOf(page) > -1) {
                    isbiaozhun = true;
                    break;
                }
            }
            if (isbiaozhun) {
                continue;
            }

            if (!"".equals(custompage)) {
                value.add(workflowname);
                value.add(subCompanyname);
                value.add(custompage);
                value.add("启用");

                values.add(value);
            }
        }

        rs.executeQuery("select  b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflow_custompageconfig a,workflow_base b where a.workflowid=b.id and b.ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String custompage = Util.null2String(rs.getString("url"));
            int enable = Util.getIntValue(rs.getString("enable"), 0);

            List<String> list = Arrays.asList(CUSTOMPAGE);
            boolean isbiaozhun = false;
            for (String page : list) {
                if (custompage.indexOf(page) > -1) {
                    isbiaozhun = true;
                    break;
                }
            }
            if (isbiaozhun) {
                continue;
            }

            value.add(workflowname);
            value.add(subCompanyname);
            value.add(custompage);
            value.add(enable == 1 ? "启用" : "未启用");

            values.add(value);

        }

        addExcelModal(writeEntity, sheetname, sheetname, custompageSheet, values);

    }

    private void getCustomcode(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.*  from workflow_nodehtmllayout a,workflow_base b where a.WORKFLOWID=b.id and b.ISVALID=1   order by  WORKFLOWID,NODEID");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String formid = Util.null2String(rs.getString("FORMID"));
            String nodeid = Util.null2String(rs.getString("NODEID"));
            String scriptstr = Util.null2String(rs.getString("scriptstr"));
            System.out.println("scriptstr==================" + scriptstr);
            String deScriptstr = decodeStr(scriptstr);

            if ("".equals(deScriptstr)) {
                continue;
            }

            boolean haskaifa = false;
            try {
                org.jsoup.nodes.Element body = org.jsoup.Jsoup.parse(deScriptstr);
                org.jsoup.select.Elements scripts = body.select("script");
                for (int i = 0; i < scripts.size(); i++) {
                    org.jsoup.nodes.Element script = scripts.get(i);
                    if (script != null) {
                        String str = script.data().trim();
                        if (!"".equals(str)) {
                            haskaifa = true;
                        }
                        String src = Util.null2String(script.attr("src"));
                        if (!"".equals(src)) {
                            haskaifa = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!haskaifa) {
                continue;
            }

            value.add(workflowname);
            value.add(subCompanyname);
            value.add(getFormName(workflowid, formid));
            value.add(getNodeName(nodeid));
            value.add(deScriptstr.replace("<", "《").replace(">", "》"));


            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, customcodeSheet, values);

    }

    private void getCondition(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> nodelinkValues = new ArrayList<List<String>>();
        List<List<String>> piciValues = new ArrayList<List<String>>();
        List<List<String>> dubanValues = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();

        //出口条件老规则
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflow_nodelink a,workflow_base b where a.WORKFLOWID=b.id and a.CONDITION like '%(%' and b.ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String linkname = Util.formatMultiLang(Util.null2String(rs.getString("LINKNAME")), "7");
            String condition = Util.null2String(rs.getString("CONDITION"));


            value.add(workflowname);
            value.add(subCompanyname);
            value.add(linkname);
            value.add(condition);

            nodelinkValues.add(value);
        }

        //批次条件老规则
        rs.executeQuery("select (select  workflowname from workflow_base where workflow_base.id = t3.workflowid) as workflowname, " +
                "       (select case isvalid " +
                "                   when '1' then '有效' " +
                "                   when '0' then '无效' " +
                "                   when '2' then '测试' " +
                "                   when '3' then '历史版本' " +
                "                   else '未知' end\n" +
                "        from workflow_base " +
                "        where workflow_base.id = t3.workflowid)                                                  as isvalid, " +
                "       t2.groupname                                                                              as groupname, " +
                "       (select  nodename  from workflow_nodebase where workflow_nodebase.id = t2.nodeid) as nodename, " +
                "       conditions                                                                                as conditions, " +
                "       conditioncn                                                                               as conditioncn " +
                "from workflow_groupdetail t, " +
                "     workflow_nodegroup t2, " +
                "     workflow_flownode t3 " +
                "where t2.id = t.groupid " +
                "  and t3.nodeid = t2.nodeid " +
                "  and conditions like '%(%'");

        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String isvalid = Util.null2String(rs.getString("isvalid"));
            String nodename = Util.processBody(Util.null2String(rs.getString("nodename")), "7");
            String groupname = Util.null2String(rs.getString("groupname"));
            String conditions = Util.null2String(rs.getString("conditions"));
            String conditioncn = Util.null2String(rs.getString("conditioncn"));


            value.add(workflowname);
            value.add(isvalid);
            value.add(nodename);
            value.add(groupname);
            value.add(conditions);
            value.add(conditioncn);

            piciValues.add(value);
        }
        rs.executeQuery("select (select  workflowname  " +
                "        from workflow_base " +
                "        where workflow_base.id = workflow_urgerdetail.workflowid) as workflowname, " +
                "       (select case isvalid " +
                "                   when '1' then '有效' " +
                "                   when '0' then '无效' " +
                "                   when '2' then '测试' " +
                "                   when '3' then '历史版本' " +
                "                   else '未知' end " +
                "        from workflow_base " +
                "        where workflow_base.id = workflow_urgerdetail.workflowid) as isvaid, " +
                "       conditions                                                 as conditions, " +
                "       conditioncn                                                as conditioncn " +
                "from workflow_urgerdetail " +
                "where exists (select 1 from workflow_base where workflow_base.id = workflow_urgerdetail.workflowid) " +
                "  and conditions like '%(%' ");
        //督办条件老规则
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String isvaid = Util.null2String(rs.getString("isvaid"));
            String conditions = Util.null2String(rs.getString("conditions"));
            String conditioncn = Util.null2String(rs.getString("conditioncn"));


            value.add(workflowname);
            value.add(isvaid);
            value.add(conditions);
            value.add(conditioncn);

            dubanValues.add(value);
        }


        addExcelModal(writeEntity, sheetname, "出口条件老规则", nodelinkconditionSheet, nodelinkValues);
        addExcelModal(writeEntity, sheetname, "批次条件老规则", piciconditionSheet, piciValues);
        addExcelModal(writeEntity, sheetname, "督办条件老规则", dubanconditionSheet, dubanValues);


    }

    private void getSap(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        rs.executeQuery("select * from  workflow_base  where SAPSource <>'' and ISVALID=1");
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String sapSource = Util.null2String(rs.getString("SAPSource"));
            rs1.executeQuery("select * from sap_datasource where id=?", sapSource);
            if (rs1.next()) {
                String poolname = Util.null2String(rs1.getString("POOLNAME"));

                value.add(workflowname);
                value.add(subCompanyname);
                value.add(poolname);


                values.add(value);
            }
        }

        addExcelModal(writeEntity, sheetname, sheetname, sapSheet, values);

    }

    private void getWorkflowChange(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,a.* from wfec_indatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String name = Util.processBody(Util.null2String(rs.getString("NAME")), "7");
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String outermaintable = Util.null2String(rs.getString("OUTERMAINTABLE"));
            int STATUS = Util.getIntValue(Util.null2String(rs.getString("STATUS")));

            value.add(name);
            value.add("推送");
            value.add(workflowname);
            value.add(subCompanyname);
            value.add(outermaintable);
            value.add(STATUS == 1 ? "启用" : "未启用");
            value.add("请根据实际业务逻辑，使用ESB动作流重新搭建出使用场景，搭建过程中有疑问可以咨询卢震昊！");

            values.add(value);

        }

        rs.executeQuery("select b.WORKFLOWNAME,a.* from wfec_outdatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String name = Util.processBody(Util.null2String(rs.getString("NAME")), "7");
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String outermaintable = Util.null2String(rs.getString("OUTERMAINTABLE"));
            int STATUS = Util.getIntValue(Util.null2String(rs.getString("STATUS")));

            value.add(name);
            value.add("接收");
            value.add(workflowname);
            value.add(subCompanyname);
            value.add(outermaintable);
            value.add(STATUS == 1 ? "启用" : "未启用");
            value.add("请根据实际业务逻辑，使用ESB动作流重新搭建出使用场景，搭建过程中有疑问可以咨询卢震昊！");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, workflowChangeSheet, values);
    }

    private void getWorkflowCreatePlan(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,a.* from wfec_indatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String nodeid = Util.null2String(rs.getString("NODEID"));

            value.add("流程创建日程");
            value.add(workflowname);
            value.add(getNodeName(nodeid));
            value.add("创建日程主体可迁移，具体创建，提醒细节需手动调整设置！过程中有疑问可以咨询卢震昊！");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, workflowCreatePlanSheet, values);
    }

    private void getWorkflowgroupoutdetail(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();


        RecordSet rs = new RecordSet();
        rs.executeQuery("select t3.workflowid,\n" +
                "       workflowname,\n" +
                "       subcompanyid,\n" +
                "       t2.nodeid,\n" +
                "       t2.groupname,\n" +
                "       t.type,\n" +
                "       t.jobobj,\n" +
                "       t.jobfield,\n" +
                "       t.zdysqlname\n" +
                "from workflow_groupdetail t,\n" +
                "     workflow_nodegroup t2,\n" +
                "     workflow_flownode t3,\n" +
                "     workflow_base t4\n" +
                "where t.groupid = t2.id and t4.id = t3.workflowid and t4.isvalid =1\n" +
                "  and t2.nodeid = t3.nodeid\n" +
                "  and t.type in (97, 98)");

        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String nodeid = Util.null2String(rs.getString("nodeid"));
            String groupname = Util.null2String(rs.getString("groupname"));
            String jobfield = Util.null2String(rs.getString("jobfield"));
            String type = Util.null2String(rs.getString("type"));// 97 java 98 sql
            String typename = type.equals("97") ? Util.null2String(rs.getString("jobobj")) : Util.null2String(rs.getString("zdysqlname"));
            type = type.equals("97") ? "java" : "sql";

            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));

            value.add(workflowname);
            value.add(subCompanyname);
            value.add(getNodeName(nodeid));
            value.add(groupname);
            value.add(type);
            value.add(typename);
            value.add(jobfield);


            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, groupoutdetail, values);
    }

%>
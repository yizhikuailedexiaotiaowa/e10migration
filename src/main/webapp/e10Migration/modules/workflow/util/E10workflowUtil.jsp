<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.systeminfo.SystemEnv" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="weaver.conn.RecordSet" %>
<%!
    public static String getFieldName(String wfid, String fieldid, String isdetail) {
        String name = "";
        RecordSet rs = new RecordSet();
        String isbill = "";
        String formid = "";
        rs.executeQuery("select isbill ,formid from workflow_base where id=?", wfid);
        if (rs.next()) {
            isbill = Util.null2String(rs.getString("isbill"));
            formid = Util.null2String(rs.getString("formid"));
        }
        String sql = "";
        if (isbill.equals("1")) {
            sql = "select t2.labelname as name from workflow_billfield t, htmllabelinfo t2 where t.fieldlabel=t2.indexid and t2.languageid=7 and t.id='" + Util.getIntValue(fieldid, -1) + "'";
        } else {
            sql = "select fieldlable as name from workflow_fieldlable where formid='" + formid + "'  and langurageid=7 and fieldid='" + Util.getIntValue(fieldid, -1) + "'";
        }
        rs.executeQuery(sql);
        if (rs.next()) {
            name = rs.getString("name");
        }
        return Util.formatMultiLang(name, "7");
    }

    public static String getNodeName(String nodeid) {
        RecordSet rs = new RecordSet();
        rs.executeQuery("select nodename from workflow_nodebase where id = ?", nodeid);
        String nodename = "";
        if (rs.next()) {
            nodename = Util.null2String(rs.getString("nodename"));
        }
        return Util.formatMultiLang(nodename, "7");
    }

    public static String getNodeLinkName(String nodelinkid) {
        RecordSet rs = new RecordSet();
        rs.executeQuery("select linkname from workflow_nodelink where id = ?", nodelinkid);
        String linkname = "";
        if (rs.next()) {
            linkname = Util.null2String(rs.getString("linkname"));
        }
        return Util.formatMultiLang(linkname, "7");
    }

    public static String getFormName(String workflowid, String formid) {
        RecordSet rs = new RecordSet();
        int isbill = 0;//0:老表单   1:新表单
        String formname = "";
        rs.executeQuery("select isbill from workflow_base where id=?", workflowid);
        if (rs.next()) {
            isbill = Util.getIntValue(Util.null2String(rs.getString("isbill")));
        }
        if (isbill == 0) {
            //老表单
            rs.executeQuery("select formname from workflow_formbase where id = ?", formid);
            if (rs.next()) {
                formname = Util.null2String(rs.getString("formname"));
            }
        } else {
            //新表单
            rs.executeQuery("select * from workflow_bill where id=?", formid);
            if (rs.next()) {
                int namelabel = Util.getIntValue(rs.getString("NAMELABEL"));
                formname = SystemEnv.getHtmlLabelName(namelabel, 7);
            }
        }

        return Util.formatMultiLang(formname);
    }
%>
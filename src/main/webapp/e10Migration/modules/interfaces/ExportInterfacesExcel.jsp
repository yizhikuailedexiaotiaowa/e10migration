<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.conn.RecordSet" %>

<%!


    private static List<String> INT_webserviceSheet = new ArrayList<String>(Arrays.asList("按钮名称", "按钮标识", "ws标识", "ws地址", "ws方法", "ws命名空间", "转换字段"));
    private static List<String> INT_sqlSheet = new ArrayList<String>(Arrays.asList("按钮名称", "按钮标识", "数据源", "sql文本", "转换字段"));
    private static List<String> INT_customerurlSheet = new ArrayList<String>(Arrays.asList("按钮名称", "按钮标识", "页面地址"));
    private static List<String> INT_esbSheet = new ArrayList<String>(Arrays.asList("按钮名称", "按钮标识", "esb名称"));

    private static List<String> cus_classSheet = new ArrayList<String>(Arrays.asList("所属功能", "名称", "class路径", "备注"));
    private static List<String> scheduleSheet = new ArrayList<String>(Arrays.asList("计划任务名称", "计划任务class", "执行周期", "是否开启"));
    private static List<String> ofsSendSheet = new ArrayList<String>(Arrays.asList("系统标识", "系统地址", "class路径", "是否开启", "描述"));
    private static List<String> outerdatawfsetSheet = new ArrayList<String>(Arrays.asList("触发名称", "相关流程", "所属分部", "触发主表", "状态", "说明"));
    private static List<String> sapBrowserSheet = new ArrayList<String>(Arrays.asList("系统名称", "数据源名称", "接口名称", "按钮标识", "说明"));

    public String doInterFaceAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("系统集成功能清单", path);
            getCustomBrowser(writeEntity, "自定义浏览按钮信息");
            getCustomTransClass(writeEntity, "自定义接口||转换类");
            getCustomSchedule(writeEntity, "自定义计划任务");
            getCustomOfssend(writeEntity, "自定义统一待办推送");
            getOuterdatawfset(writeEntity, "流程触发集成清单");
            getSapBrowser(writeEntity, "SAP浏览按钮信息");

            String filePath = excelWrite_html(writeEntity);
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void getCustomBrowser(ExcelWriteEntity writeEntity, String sheetname) {

        List<List<String>> sqlvalues = new ArrayList<List<String>>();//普通浏览按钮(只有sql,不包含别的)
        List<List<String>> customurlvalues = new ArrayList<List<String>>();//自定义地址
        List<List<String>> wsvalues = new ArrayList<List<String>>();//webservice
        List<List<String>> esbvalues = new ArrayList<List<String>>();//esb服务


        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        rs.executeQuery("select * from  datashowset");
        while (rs.next()) {
            String id = Util.null2String(rs.getString("id"));
            String name = Util.formatMultiLang(Util.null2String(rs.getString("NAME")));
            String showname = Util.null2String(rs.getString("SHOWNAME"));
            int showtype = Util.getIntValue(Util.null2String(rs.getString("showtype")), 1);

            switch (showtype) {
                case 1://列表
                case 2://树形

                    int datafrom = Util.getIntValue(Util.null2String(rs.getString("datafrom")));
                    switch (datafrom) {
                        case 0://webservice
                            List<String> wsvalue = new ArrayList<String>();//webservice

                            String wsurl = Util.null2String(rs.getString("WSURL"));
                            String wsoperation = Util.null2String(rs.getString("WSOPERATION"));
                            String wsworkname = Util.null2String(rs.getString("WSWORKNAME"));
                            rs1.executeQuery("select * from wsregiste a,wsregistemethod b  where a.id=b.MAINID and a.id=? and b.id=?", wsurl, wsoperation);
                            if (rs1.next()) {
                                String customname = Util.null2String(rs1.getString("CUSTOMNAME"));
                                String webserviceurl = Util.null2String(rs1.getString("WEBSERVICEURL"));
                                String methodname = Util.null2String(rs1.getString("METHODNAME"));

                                wsvalue.add(name);
                                wsvalue.add(showname);
                                wsvalue.add(customname);
                                wsvalue.add(webserviceurl);
                                wsvalue.add(methodname);
                                wsvalue.add(wsworkname);
                                wsvalue.add(getbrowserparam(id));


                                wsvalues.add(wsvalue);
                            }

                            break;
                        case 1://数据库
                        case 3://存储过程
                            List<String> sqlvalue = new ArrayList<String>();
                            String datasourceid = Util.null2String(rs.getString("DATASOURCEID"));
                            String sqltext = Util.null2String(rs.getString("SQLTEXT"));
                            sqlvalue.add(name);
                            sqlvalue.add(showname);
                            sqlvalue.add(datasourceid);
                            sqlvalue.add(sqltext);
                            sqlvalue.add(getbrowserparam(id));

                            sqlvalues.add(sqlvalue);
                            break;
                        case 2://自定义地址
                            List<String> customurlvalue = new ArrayList<String>();
                            String customhref = Util.null2String(rs.getString("customhref"));
                            customurlvalue.add(name);
                            customurlvalue.add(showname);
                            customurlvalue.add(customhref);

                            customurlvalues.add(customurlvalue);
                            break;
                        case 4://esb服务
                            break;
                    }
                    break;
                case 3://自定义页面
                    List<String> customurlvalue = new ArrayList<String>();
                    String customhref = Util.null2String(rs.getString("customhref"));
                    customurlvalue.add(name);
                    customurlvalue.add(showname);
                    customurlvalue.add(customhref);

                    customurlvalues.add(customurlvalue);
                    break;

            }

        }

        //创建一个模块 功能项
        ExcelFunctionBean wsBean = new ExcelFunctionBean("webservices数据来源");
        ExcelFunctionBean sqlBean = new ExcelFunctionBean("sql数据来源");
        ExcelFunctionBean cusurlBean = new ExcelFunctionBean("自定义页面");
        ExcelFunctionBean esbBean = new ExcelFunctionBean("esb数据来源");

        //功能有多少列，指定每个列的列名
        wsBean.setHeaderNames(INT_webserviceSheet);
        sqlBean.setHeaderNames(INT_sqlSheet);
        cusurlBean.setHeaderNames(INT_customerurlSheet);
        esbBean.setHeaderNames(INT_esbSheet);

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        wsBean.setValues(wsvalues);
        sqlBean.setValues(sqlvalues);
        cusurlBean.setValues(customurlvalues);
        esbBean.setValues(esbvalues);


        List<ExcelFunctionBean> list = new ArrayList<ExcelFunctionBean>();//一个sheet页有多少功能项
        list.add(wsBean);
        list.add(sqlBean);
        list.add(cusurlBean);
        list.add(esbBean);

        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            sheets.get(sheetname).addAll(list);
        } else { //不存在当前sheet页，维护新的
            sheets.put(sheetname, list);
        }
        writeEntity.setSheets(sheets);

    }


    private void getCustomTransClass(ExcelWriteEntity writeEntity, String sheetname) {

        List<List<String>> cus_classValues = new ArrayList<List<String>>();//集成登录

        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from outter_encryptclass");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String encryptname = Util.null2String(rs.getString("encryptname"));
            String encryptclass = Util.null2String(rs.getString("ENCRYPTCLASS"));

            value.add("集成登录自定义加密");
            value.add(encryptname);
            value.add(encryptclass);
            value.add("");

            cus_classValues.add(value);
        }

        rs.executeQuery("select * from LDAP_FORMART  where FORMARTLEVEL=2");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String formartname = Util.null2String(rs.getString("FORMARTNAME"));
            String formartclass = Util.null2String(rs.getString("FORMARTCLASS"));
            String formarttype = Util.null2String(rs.getString("FORMARTTYPE"));

            value.add("LDAP自定义接口");
            value.add(formartname);
            value.add(formartclass);
            value.add("1".equals(formarttype) ? "正向同步转换接口" : "反向同步转换接口");

            cus_classValues.add(value);

        }

        rs.executeQuery("select * from hrsyncset where intetype=3");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String custominterface = Util.null2String(rs.getString("custominterface"));

            value.add("HR同步自定义同步接口");
            value.add("HR同步自定义同步接口");
            value.add(custominterface);
            value.add("请按照E10的对接方式重新开发");

            cus_classValues.add(value);
        }

        rs.executeQuery("select * from hrsync_formart where FORMARTLEVEL=2");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String formartname = Util.null2String(rs.getString("FORMARTNAME"));
            String formartclass = Util.null2String(rs.getString("FORMARTCLASS"));
            String formartparams = Util.null2String(rs.getString("FORMARTPARAMS"));

            value.add("HR同步自定义转换接口");
            value.add(formartname);
            value.add(formartclass);
            value.add(formartparams);

            cus_classValues.add(value);

        }


        addExcelModal(writeEntity, sheetname, sheetname, cus_classSheet, cus_classValues);
    }

    private void getCustomSchedule(ExcelWriteEntity writeEntity, String sheetname) {
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from schedulesetting");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String pointid = Util.null2String(rs.getString("POINTID"));
            String classpath = Util.null2String(rs.getString("CLASSPATH"));
            String cronexpr = Util.null2String(rs.getString("CRONEXPR"));
            int runstatus = Util.getIntValue(Util.null2String(rs.getString("runstatus")));

            List<String> list = Arrays.asList(STANDARSCHEDULER);
            if (list.contains(classpath)) {
                continue;
            }

            value.add(pointid);
            value.add(classpath);
            value.add(cronexpr);
            value.add(runstatus == 0 ? "正常" : "禁用");


            values.add(value);

        }
        addExcelModal(writeEntity, sheetname, sheetname, scheduleSheet, values);


    }

    private void getCustomOfssend(ExcelWriteEntity writeEntity, String sheetname) {

        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from ofs_sendinfo");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String syscode = Util.null2String(rs.getString("syscode"));
            String serverurl = Util.null2String(rs.getString("serverurl"));
            String classimpl = Util.null2String(rs.getString("classimpl"));
            String isvalid = Util.null2String(rs.getString("isvalid"));
            String sysdesc = Util.null2String(rs.getString("sysdesc"));

            if ("weaver.ofs.push.imp.SendRequestForE10".equals(classimpl) || "weaver.ofs.interfaces.SendRequestStatusDataImplForE9".equals(classimpl)) {
                continue;
            }

            value.add(syscode);
            value.add(serverurl);
            value.add(classimpl);
            value.add("1".equals(isvalid) ? "开启" : "关闭");
            value.add(sysdesc);

            values.add(value);

        }

        addExcelModal(writeEntity, sheetname, sheetname, ofsSendSheet, values);
    }

    private void getOuterdatawfset(ExcelWriteEntity writeEntity, String sheetname) {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,a.* from outerdatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String id = Util.null2String(rs.getString("id"));
            String name = Util.processBody(Util.null2String(rs.getString("SETNAME")), "7");
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("subcompanyid"))));
            String outermaintable = Util.null2String(rs.getString("OUTERMAINTABLE"));
            String status = "";
            rs1.executeQuery("select * from outerdatawfperiodset where scope=?", id);
            if (rs1.next()) {
                //0,不触发;1,按分钟;2,按小时;3,按每天;4,按每周;5,按每月;6,为空;
                int type = Util.getIntValue(Util.null2String(rs1.getString("type")), -1);
                if ("6".equals(type)) {//6的情况要找-1的数据
                    rs1.executeQuery("select * from outerdatawfperiodset where scope=?", "-1");
                    if (rs1.next()) {
                        type = Util.getIntValue(Util.null2String(rs1.getString("type")));
                    }
                }
                switch (type) {
                    case -1:
                    case 0:
                    case 6:
                        status = "不触发";
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        status = "定时触发";
                        break;
                }
            }
            value.add(name);
            value.add(workflowname);
            value.add(subCompanyname);
            value.add(outermaintable);
            value.add(status);
            value.add("请根据实际业务逻辑，使用ESB动作流重新搭建出使用场景，核心使用组件为：定时触发+流程创建组件+dml执行组件等，搭建过程中有疑问可以咨询卢震昊！");

            values.add(value);
        }
        addExcelModal(writeEntity, sheetname, sheetname, outerdatawfsetSheet, values);
    }

    private String getbrowserparam(String mainid) {
        String result = "";
        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from datashowparam  where MAINID=? and TRANSQL is not null and TRANSQL<>''", mainid);
        while (rs.next()) {
            String fieldname = Util.formatMultiLang(Util.null2String(rs.getString("FIELDNAME")));
            String searchname = Util.null2String(rs.getString("SEARCHNAME"));
            String transql = Util.null2String(rs.getString("TRANSQL"));

            result += fieldname + "||" + searchname + "||" + transql + "\n";

        }
        return result;

    }

    private void getSapBrowser(ExcelWriteEntity writeEntity, String sheetname) {
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select (select hetename from int_heteproducts where int_heteproducts.id = a.HPID) hetename,\n" +
                "       (select POOLNAME from sap_datasource where sap_datasource.id = a.POOLID) poolname,\n" +
                "       (select REGNAME from sap_service where sap_service.id = a.REGSERVICE) regname,\n" +
                "       a.*\n" +
                "from int_BrowserbaseInfo a");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String wType = Util.null2String(rs.getString("W_TYPE"));
            String browserType = Util.null2String(rs.getString("BROWSERTYPE"));//Sap浏览按钮针对不同的入口，进行分配的ID 226:集成单选, 227:集成多选, 228:流程创建
            if ("228".equals(browserType) || ("226".equals(browserType) && !"".equals(wType))) {
                continue;
            }

            String mark = Util.null2String(rs.getString("MARK"));
            String hetename = Util.null2String(rs.getString("hetename"));
            String poolname = Util.null2String(rs.getString("poolname"));
            String regname = Util.null2String(rs.getString("regname"));

            value.add(hetename);
            value.add(poolname);
            value.add(regname);
            value.add(mark);
            value.add("sap按钮调整难度较大，请咨询卢震昊相关事项");

            values.add(value);

        }

        addExcelModal(writeEntity, sheetname, sheetname, sapBrowserSheet, values);
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.conn.RecordSet" %>
<%@ page import="weaver.hrm.resource.ResourceComInfo" %>
<%@ page import="com.alibaba.fastjson.JSONObject" %>
<%!


    public static String getDataSetType(String datasetId){
        String dataSetType = "";

        RecordSet rs = new RecordSet();

        List<String> edcList = new ArrayList<String>();
        List<String>  ufList = new ArrayList<String>();
        List<String>  formTableList = new ArrayList<String>();



        int i = 0;
        rs.executeQuery("select uuid,tableName,dataSetId  from  edc_reportdstable where dataSetId = '"+datasetId+"'");
        while(rs.next()){
            String tableName = rs.getString("tableName");

            if(tableName != null && !"".equals(tableName)){
                i++;

                if( !tableName.contains("select ") && !tableName.contains("SELECT ")){ //不是sql
                    if (tableName.contains("uf_") || tableName.contains("edc_uf_") || tableName.contains("formtable_main")){
                        if(tableName.contains("formtable_main")){
                            formTableList.add(tableName);
                        }else if(tableName.contains("edc_uf_")){
                            edcList.add(tableName);
                        }else{
                            ufList.add(tableName);
                        }
                    }
                }

            }

        }

        if(formTableList.size() == i  ){
            dataSetType = "1";
        } else if(ufList.size() == i){
            dataSetType = "2";
        } else if(edcList.size() == i){
            dataSetType = "3";
        } else if(formTableList.size()+edcList.size()+ufList.size() == i){  //直连数仓
            dataSetType = "999";
        } else { //sql数据集
            dataSetType = "-100";
        }

        return dataSetType;

    }

    //导出主方法
    public String doExportEdcDetail(String path) {
        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("数据中心配置清单", path);  //excel文件名称

        RecordSet rs = new RecordSet();
        rs.executeQuery("select *  from  edc_reportdataset");
        String datasetIds = "";

        String dirConnDatasetIds = "";

            while (rs.next()) {
                String datasetid = rs.getString("uuid");
                if("-100".equals(getDataSetType(datasetid))) {
                    datasetIds += "'" + datasetid + "'" + ",";
                } else {
                    dirConnDatasetIds += "'" + datasetid + "'" + ",";
                }






            }


            if(StringUtils.isNotBlank(datasetIds)) {
                datasetIds = datasetIds.substring(0, datasetIds.length()-1);

                getEdcReport(writeEntity,"数据集和报表 (因数据集和报表有回收站功能, 所以将已删除数据也拉出来)","数据集和报表", datasetIds);  //sheet页

                getEdcBoard(writeEntity, "数据集和看板", "数据集和看板", datasetIds); //sheet页


            }

        if(StringUtils.isNotBlank(dirConnDatasetIds)) {
            dirConnDatasetIds = dirConnDatasetIds.substring(0, dirConnDatasetIds.length() - 1);
        }





        getEdcFormApp( writeEntity, "",  "自由填报");

        getExcelReport( writeEntity, "",  "多级填报");

        //getEdcDataSet( writeEntity, "注意:  版本升级开发看使用数量,  项目人员无需关注此项",  "数据集", datasetIds, dirConnDatasetIds);

        String filePath = excelWrite_html(writeEntity);

        return filePath;

    }

    //functionName:为excel表格顶部的区域说明,    sheetname: sheet页名称
    void getEdcReport(ExcelWriteEntity writeEntity, String functionName, String sheetname, String datasetIds) {
        List<String> headerNames = reportDataHeaderNames();



        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();

        String sql = "select ds.name, ds.creator, ds.isdeleted,   es.name,es.creator,ds.uuid  from edc_reportsheetds  esds inner join  edc_reportdataset ds  on esds.dataSetId=ds.uuid   left join edc_reportsheet es on  esds.sheetId = es.uuid\n" +
                "where ds.uuid in ("+datasetIds+") and  es.quickType <> 'GroupCross'  order by es.isdeleted, es.uuid";

        rs.executeQuery(sql);

         while(rs.next()) {
             List<String> value = new ArrayList<String>();



            String sheetCreaterName = "";
             String dataSetCreaterName = "";

             try{
                 ResourceComInfo rci = new ResourceComInfo();

                 dataSetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(2)), "7");  //多语言
                 sheetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(5)),"7");


             } catch (Exception e){
                 e.printStackTrace();
             }


             value.add(rs.getString(6)); //数据集id
             value.add(rs.getString(1)); //数据集名称
             value.add(dataSetCreaterName); //数据集创建人
             value.add("调整sql");
             value.add("1".equals(rs.getString(3)) ? "是":"否"); //数据集已删除
             value.add(rs.getString(4));  //报表名称
             value.add(sheetCreaterName); //报表创建人
             value.add("1".equals(rs.getString(6)) ? "是":"否"); //报表已删除
             value.add("因数据集sql要调整,报表重点测试功能,若字段设置出问题,手动调整 (不是一定要调)");





             values.add(value);

         }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


    List<String> reportDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集id");
        headerNames.add("数据集名称");
        headerNames.add("数据集创建人");
        headerNames.add("调整内容");
        headerNames.add("数据集已删除");
        headerNames.add("报表名称");
        headerNames.add("报表创建人");
        headerNames.add("报表已删除");
        headerNames.add("调整内容");

        return headerNames;
    }


    void getEdcBoard(ExcelWriteEntity writeEntity, String functionName, String sheetname, String datasetIds) {
        List<String> headerNames = boardDataHeaderNames();



        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select  edc_reportdataset.name, edc_reportdataset.creator, edc_board_dashboard.name, edc_board_dashboard.creator,  edc_board_widget.name, edc_reportdataset.uuid   from edc_board_widget, edc_board_dashboard,edc_reportdataset  where edc_board_widget.board = edc_board_dashboard.id  \n" +
                "    and edc_reportdataset.uuid = edc_board_widget.datamodel  and    edc_reportdataset.uuid in ("+datasetIds+")   order by  edc_board_dashboard.id");

        while(rs.next()) {
            List<String> value = new ArrayList<String>();

            String dataSetCreaterName = "";
            String boardCreaterName = "";

            try{
                ResourceComInfo rci = new ResourceComInfo();

                dataSetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(2)), "7");  //多语言
                boardCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(4)),"7");


            } catch (Exception e){
                e.printStackTrace();
            }


            value.add(rs.getString(6)); //数据集id
            value.add(rs.getString(1)); //数据集名称
            value.add(dataSetCreaterName);  //数据集创建人
            value.add("调整sql");
            value.add(rs.getString(3)); //看板名称
            value.add(boardCreaterName); //看板创建人
            value.add(rs.getString(5)); //图表名称
            value.add("因数据集sql要调整, 看板重点测试功能, 若字段设置出问题,手动调整 (不是一定要调)");

            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


    List<String> boardDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集id");
        headerNames.add("数据集名称");
        headerNames.add("数据集创建人");
        headerNames.add("调整内容");
        headerNames.add("看板名称");
        headerNames.add("看板创建人");
        headerNames.add("图表名称");
        headerNames.add("调整内容");
        return headerNames;
    }



    void getEdcFormApp(ExcelWriteEntity writeEntity, String functionName, String sheetname) {
        List<String> headerNames = edcAppDataHeaderNames();



        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();



        Map<String,String> edcCubeMap = new HashMap<String,String>();
        RecordSet rs1 = new RecordSet();
        rs1.executeQuery("select  formId,formmodeId,nodeid from  edc_joinCubeSetting  where isUsed = '1' and formmodeId is not null and formmodeId <> ''");
        while(rs1.next()){
           String   formmodeId = rs1.getString("formmodeId");
            String   formId = rs1.getString("formId");
            String   nodeId = rs1.getString("nodeId");
            if(StringUtils.isBlank(nodeId)){
                edcCubeMap.put(formId, formmodeId);
            }


        }




        rs.executeQuery("select edc_app.id , edc_app.name , edc_app.creator , edc_app.createdate, edc_form_page.codeblock,edc_app.cubeAppId,edc_form_page.formid   from edc_app  left join  edc_form_page  on  edc_app.id = edc_form_page.appid    where edc_app.displaytype = 'FORM' and \n" +
                "    (edc_app.isdeleted <> 1 or edc_app.isdeleted is null or edc_app.isdeleted='')");

        while(rs.next()) {

            String codeblock = rs.getString("codeblock");

            String jsContent = "";
            String cssContent = "";

             if(StringUtils.isNotBlank(codeblock)) {
                 JSONObject codeJson = JSONObject.parseObject(codeblock);
                 jsContent = codeJson.getString("JS");
                 cssContent = codeJson.getString("CSS");
             }



            String formid = rs.getString("formid");

            String cubeAppId  = edcCubeMap.get(formid);


            if((jsContent != null && !"".equals(jsContent) ) || (cssContent != null && !"".equals(cssContent)) || (cubeAppId != null && !"".equals(cubeAppId))) {

                List<String> value = new ArrayList<String>();

                String edcAppCreaterName = "";


                try{
                    ResourceComInfo rci = new ResourceComInfo();

                    edcAppCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(3)), "7");  //多语言


                } catch (Exception e){
                    e.printStackTrace();
                }



                value.add(rs.getString(1)); //应用ID
                value.add(rs.getString(2));  //应用名称
                value.add(edcAppCreaterName); //创建人
                value.add(rs.getString(4)); //创建日期
                value.add(jsContent); //JS代码块
                value.add(cssContent); //CSS代码块
                value.add(cubeAppId == null ? "": cubeAppId); //关联建模应用ID
                value.add("若js和css有数据重新开发代码块,若关联建模应用ID,与版本升级部讨论具体处理方案"); //调整内容

                values.add(value);


            }



        }

        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);

    }



    List<String> edcAppDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用ID");
        headerNames.add("应用名称");
        headerNames.add("创建人");
        headerNames.add("创建日期");
        headerNames.add("JS代码块");
        headerNames.add("CSS代码块");
        headerNames.add("关联建模应用ID");
        headerNames.add("调整内容");
        return headerNames;
    }





    void getExcelReport(ExcelWriteEntity writeEntity, String functionName, String sheetname) {
        List<String> headerNames = edcExcelReportHeaderNames();



        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from edc_app  where  displaytype = 'EXCEL'");

        while(rs.next()) {

            String edc_app_id = rs.getString("id");

            String name = rs.getString("name");

            String isdeleted =  "1".equals(rs.getString("isdeleted")) ? "是 (无回收站,咨询是否使用)" : "否";
            String cubeAppId = rs.getString("cubeAppId");


            List<String> value = new ArrayList<String>();

            value.add(edc_app_id); //应用ID
            value.add(name);  //应用名称
            value.add(isdeleted); //是否删除
            value.add(cubeAppId); //关联建模应用ID
            value.add("E10标准不支持功能很多,请尽快跟版本升级部确认");
            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);

    }


    List<String> edcExcelReportHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用ID");
        headerNames.add("应用名称");
        headerNames.add("是否删除");
        headerNames.add("关联建模应用ID");
        headerNames.add("调整内容");
        return headerNames;
    }



    void getEdcDataSet(ExcelWriteEntity writeEntity, String functionName, String sheetname,  String datasetIds, String dirConnDatasetIds) {
        List<String> headerNames = edcDataSetHeaderNames();



        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();

        if(StringUtils.isBlank(datasetIds)){
            datasetIds = "''";
        }

        if(StringUtils.isBlank(dirConnDatasetIds)){
            dirConnDatasetIds = "''";
        }



        rs.executeQuery("select *   from edc_reportdataset   where uuid in ("+datasetIds+") or uuid in ("+dirConnDatasetIds+")");

        while(rs.next()) {

            String dataSetId = rs.getString("uuid");

            String name = rs.getString("name");

            String dataSetType = "";
            if(datasetIds.contains("'"+dataSetId+"'")){
                dataSetType = "sql";
            } else if(dirConnDatasetIds.contains("'"+dataSetId+"'")){
                dataSetType = "直连";
            }

            String isdeleted = "1".equals(rs.getString("isdeleted")) ? "是" : "否";


            List<String> value = new ArrayList<String>();


            value.add(dataSetId); //数据集ID
            value.add(name);  //数据集名称
            value.add(dataSetType); //数据集类型
            value.add(isdeleted); //是否删除

            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);


    }


    List<String> edcDataSetHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集ID");
        headerNames.add("数据集名称");
        headerNames.add("数据集类型");
        headerNames.add("是否删除");
        return headerNames;
    }



%>
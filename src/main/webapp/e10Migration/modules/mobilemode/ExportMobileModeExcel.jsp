<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%@ page import="weaver.general.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="weaver.general.BaseBean" %>
<%@ page import="com.alibaba.fastjson.JSON" %>


<%!



    public String domobilemodeApp(String path) {
        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("移动建模_应用清单", path);

        RecordSet rs = new RecordSet();
        rs.executeQuery("select id,subcompanyid,appname  from mobileappbaseinfo where ISDELETE = 0 order by id ");

        while (rs.next()) {
            String appid = rs.getString("id");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String subcompanyid = rs.getString("subcompanyid");
            doMobileModeExcel(writeEntity, appid, appname, subcompanyid);

        }

        new BaseBean().writeLog(JSON.toJSONString(writeEntity));

        String filePath = excelWrite_html(writeEntity);

        return filePath;

    }


    void doMobileModeExcel(ExcelWriteEntity writeEntity, String appid, String appname, String subcompanyid) {


        String sheetname = getappname(appid, appname, subcompanyid);

        pageCompList(writeEntity, "页面组件清单", sheetname, appid);//页面组件清单


    }

    void pageCompList(ExcelWriteEntity writeEntity, String functionName, String sheetname, String appid) {
        List<String> headerNames = pageCompListNames();
        List<List<String>> values = pageCompListValue(appid);
        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);


    }


    List<String> pageCompListNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("页面id");
        headerNames.add("页面名称");
        headerNames.add("组件类型");
        headerNames.add("组件名称");

        return headerNames;
    }
    public static final List<Map<String, String>>
            plugins = JSON.parseObject("[{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Navigation\",\"text\":\"128104\",\"type\":\"nav\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Slide\",\"text\":\"128105\",\"type\":\"basic\",\"order\":\"2\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"List\",\"text\":\"128106\",\"type\":\"list\",\"order\":\"3\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Html\",\"text\":\"128107\",\"type\":\"basic\",\"order\":\"4\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Toolbar\",\"text\":\"128108\",\"type\":\"nav\",\"order\":\"5\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Chart\",\"text\":\"128109\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Map\",\"text\":\"128111\",\"type\":\"other\",\"order\":\"7\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Timelinr\",\"text\":\"128112\",\"type\":\"list\",\"order\":\"8\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NavPanel\",\"text\":\"128114\",\"type\":\"nav\",\"order\":\"10\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Tree\",\"text\":\"128116\",\"type\":\"other\",\"order\":\"11\"},{\"isEnabled\":\"0\",\"unique\":\"1\",\"id\":\"TouchButton\",\"text\":\"128170\",\"type\":\"nav\",\"order\":\"12\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Countdown\",\"text\":\"128118\",\"type\":\"other\",\"order\":\"13\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"UrlList\",\"text\":\"128119\",\"type\":\"list\",\"order\":\"14\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Form\",\"text\":\"128145\",\"type\":\"form\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FInputText\",\"text\":\"128146\",\"type\":\"form\",\"order\":\"16\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FHidden\",\"text\":\"128147\",\"type\":\"form\",\"order\":\"17\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FTextarea\",\"text\":\"128148\",\"type\":\"form\",\"order\":\"18\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FSelect\",\"text\":\"128149\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"FButton\",\"text\":\"128171\",\"type\":\"form\",\"order\":\"49\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FSound\",\"text\":\"128154\",\"type\":\"form\",\"order\":\"21\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"FLbs\",\"text\":\"128155\",\"type\":\"form\",\"order\":\"22\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FPhoto\",\"text\":\"128156\",\"type\":\"form\",\"order\":\"23\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"QRCode\",\"text\":\"128122\",\"type\":\"other\",\"order\":\"24\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"Reply\",\"text\":\"128123\",\"type\":\"other\",\"order\":\"25\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Tab\",\"text\":\"128127\",\"type\":\"nav\",\"order\":\"28\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"UserAvatar\",\"text\":\"128129\",\"type\":\"other\",\"order\":\"29\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NavHeader\",\"text\":\"128130\",\"type\":\"nav\",\"order\":\"30\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FCheckbox\",\"text\":\"128150\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DataSet\",\"text\":\"128131\",\"type\":\"data\",\"order\":\"32\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"SegControl\",\"text\":\"128133\",\"type\":\"nav\",\"order\":\"46\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FCheck\",\"text\":\"128151\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FBrowser\",\"text\":\"128152\",\"type\":\"form\",\"order\":\"20\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"GridTable\",\"text\":\"128134\",\"type\":\"list\",\"order\":\"36\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"Calendar\",\"text\":\"128135\",\"type\":\"other\",\"order\":\"37\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Button\",\"text\":\"128142\",\"type\":\"basic\",\"order\":\"48\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Chart2\",\"text\":\"128110\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DataDetail\",\"text\":\"128132\",\"type\":\"data\",\"order\":\"32\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"TabBar\",\"text\":\"128136\",\"type\":\"nav\",\"order\":\"47\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"HoriList\",\"text\":\"128137\",\"type\":\"list\",\"order\":\"15\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Weather\",\"text\":\"128138\",\"type\":\"other\",\"order\":\"43\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ProgressBar\",\"text\":\"128139\",\"type\":\"other\",\"order\":\"44\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ColumnBreak\",\"text\":\"128140\",\"type\":\"nav\",\"order\":\"45\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FFile\",\"text\":\"128158\",\"type\":\"form\",\"order\":\"45\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Picture\",\"text\":\"128141\",\"type\":\"other\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Video\",\"text\":\"128143\",\"type\":\"other\",\"order\":\"49\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Iframe\",\"text\":\"128144\",\"type\":\"other\",\"order\":\"50\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FHandwriting\",\"text\":\"128157\",\"type\":\"form\",\"order\":\"24\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"UrlGridTable\",\"text\":\"128756\",\"type\":\"list\",\"order\":\"52\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"BarChart\",\"text\":\"128820\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"PieChart\",\"text\":\"128841\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"LineChart\",\"text\":\"32615\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DetailTable\",\"text\":\"130658\",\"type\":\"form\",\"order\":\"2\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FunnelChart\",\"text\":\"131032\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FScores\",\"text\":\"131418\",\"type\":\"form\",\"order\":\"20\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"CountPanel\",\"text\":\"381999\",\"type\":\"data\",\"order\":\"33\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"RSSList\",\"text\":\"382128\",\"type\":\"list\",\"order\":\"33\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FRange\",\"text\":\"382243\",\"type\":\"form\",\"order\":\"46\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FDateTime\",\"text\":\"382272\",\"type\":\"form\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"RadarChart\",\"text\":\"382297\",\"type\":\"chart\",\"order\":\"7\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"FloatButton\",\"text\":\"128170\",\"type\":\"nav\",\"order\":\"12\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"GaugeChart\",\"text\":\"382317\",\"type\":\"chart\",\"order\":\"8\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"LargeList\",\"text\":\"383006\",\"type\":\"list\",\"order\":\"16\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"APIList\",\"text\":\"386407\",\"type\":\"list\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FAPIBrowser\",\"text\":\"388150\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"AMap\",\"text\":\"128111\",\"type\":\"other\",\"order\":\"7\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FLbs4amap\",\"text\":\"128155\",\"type\":\"form\",\"order\":\"22\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"TipPanel\",\"text\":\"500722\",\"type\":\"other\",\"order\":\"51\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"SearchBox\",\"text\":\"502101\",\"type\":\"basic\",\"order\":\"52\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Steps\",\"text\":\"503558\",\"type\":\"other\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NoticeBar\",\"text\":\"503937\",\"type\":\"other\",\"order\":\"54\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NList\",\"text\":\"128106\",\"type\":\"list\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"RichText\",\"text\":\"507147\",\"type\":\"basic\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"TopSearch\",\"text\":\"507676\",\"type\":\"list\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NGridTable\",\"text\":\"128134\",\"type\":\"list\",\"order\":\"2\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DynamicForm\",\"text\":\"512437\",\"type\":\"form\",\"order\":\"3\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NTimeline\",\"text\":\"128112\",\"type\":\"list\",\"order\":\"8\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NHoriList\",\"text\":\"128137\",\"type\":\"list\",\"order\":\"15\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NLargeList\",\"text\":\"383006\",\"type\":\"list\",\"order\":\"16\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FAddress\",\"text\":\"516534\",\"type\":\"form\",\"order\":\"48\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ChartBoard\",\"text\":\"521227\",\"type\":\"chart\",\"order\":\"9\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FVideo\",\"text\":\"523351\",\"type\":\"form\",\"order\":\"25\"}]", List.class);

    List<List<String>> pageCompListValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();
        new BaseBean().writeLog(JSON.toJSONString(plugins));
        String sql = ("select mobileappbaseinfo.id as appid ,mobileappbaseinfo.appname,apphomepage.ID as pageid,pagename,mectype\n" +
                "from mobileappbaseinfo join apphomepage on mobileappbaseinfo.ID = APPID join mobileextendcomponent on apphomepage.ID = OBJID\n" +
                " where apphomepage.isdelete = 0 and mobileappbaseinfo.ISDELETE =0 ");


        if (!appid.equals("")) {
            sql += " and mobileappbaseinfo.id=" + appid;
        }

        sql += " order by mobileappbaseinfo.id,apphomepage.ID";

        rs.executeQuery(sql);

        new BaseBean().writeLog(" pageCompListValue :" + sql);

        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String pageid = rs.getString("pageid");
            String mectype = rs.getString("mectype");
            String pagename = Util.formatMultiLang(rs.getString("pagename"));


            value.add(_appid);
            value.add(appname);
            value.add(pageid);
            value.add(pagename);

            value.add(mectype);
            // todo  判断mectype 后可以判断是否需要检查
            String mecTypeDesc = getMecTypeDesc(plugins, mectype);


            value.add(mecTypeDesc);
            // todo  判断mectype 后解析mecparam判断其中有多少项需要 检查/重新设置

            values.add(value);

        }

        return values;

    }

    public String getText(String type,Map<String, String> plugin){
        if("custom".equals(type)){
            return Util.null2String(plugin.get("text"));
        }else{
            return SystemEnv.getHtmlLabelName(Util.getIntValue(plugin.get("text")), 7);
        }
    }
    private String getMecTypeDesc(List<Map<String, String>> plugins, String mectype) {
        for (Map<String, String> plugin : plugins) {
            String id = Util.null2String(plugin.get("id"));

            if (id.equals(mectype)) {
                return getText(Util.null2String(plugin.get("type")), plugin);
            }

        }
        return "组件未识别";
    }


%>
<%@ page import="java.util.LinkedHashMap" %>

<%@ page import="java.io.*" %>
<%@ page import="weaver.e10Migration.excel.file.bean.ExcelWriteEntity" %>
<%@ page import="weaver.e10Migration.excel.file.bean.ExcelFunctionBean" %>
<%@ page import="weaver.e10Migration.excel.file.ExcelWrite" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%!

    /**
     * 统一在这个方法里面封装excel需要的table
     * @param writeEntity
     * @return
     */
    public String excelWrite_html(ExcelWriteEntity writeEntity) {
        ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
        String filePath = excelWrite.excelWrite(writeEntity);

        return filePath;
    }
    public void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }



%>
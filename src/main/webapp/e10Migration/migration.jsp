<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<jsp:useBean id="rs" class="weaver.conn.RecordSet" scope="page"/>

<%@include file="/e10Migration/modules/qiyuesuo/qiyuesuoCheck.jsp" %>

<%
    String title = "E9升级E10清单获取";

%>

<%


    HashMap<String, Object> qysCheck = qysNeedUpdate();
    String version = Util.null2String(qysCheck.get("version"));
    String shouldUpdate = Util.null2String(qysCheck.get("shouldUpdate"));

    List<Map<String, String>> workflowInfos = getFixE8ChartInfo();
    String workflowDesign = "";
    if (workflowInfos.size() > 0) {
        workflowDesign = "客户存在" + workflowInfos.size() + "条E8流程图数据,需执行初始化接口,初始化流程图的E9坐标,请在迁移之前点击按钮先执行初始化逻辑!!";
    }

    //数据中心

    //1. 创建表
    if(!judgeExistTable("dataset_sync")){
        createDataSetTable();
    }

    /*if(!getCheckFieldExistSql("dataset_sync", "table_form")){
        rs.executeUpdate("alter table dataset_sync add table_form varchar(500)");
    }*/

    String dataSetDesign = "";
    int  dataSetNum = Util.getIntValue(getDataSetInfo(), 0);
    if (dataSetNum > 0) {
        dataSetDesign = "客户存在" + dataSetNum + "条数据集,需点击按钮初始化数据!!";
    }




%>
<HTML>
<HEAD>

    <!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="/js/jquery.min.js"></script>

    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="/js/popper.min.js"></script>


    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="/js/bootstrap.min.js"></script>

    <script type="text/javascript">


    </script>

</head>
<BODY scroll="no">
<div class="row">
    <div class="col-8">
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1"><%=title%></span>
        </nav>
    </div>
    <div class="col-4">
        <button type="button" class="btn btn-success"  onclick="javascript:dosubmit();">生成清单</button>
    </div>
</div>


<div class="container-fluid">
    <div>
        <div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="loadingModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div id="loadingimage">
                            <div>
                                <img src="/images/loading2_wev8.gif" style="vertical-align: middle"><label
                                    id="loadingMsg">正在执行,请稍后!
                            </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <iframe name="downiframe" id="downiframe" src="" style="display:none"></iframe>
        <FORM id=weaver name="frmain" action="/e10Migration/migrationOperation.jsp" method=post>
            <input type="hidden" name="operation" value="add"/>
            <input type="hidden" name="checkvaule" id="checkvaule"/>

            <div class="alert alert-success" role="alert">
                清单获取
            </div>
            <div class="form-group row">
                <label for="dimension" class="col-sm-2 col-form-label">获取维度</label>
                <div class="col-sm-10">
                    <select id="dimension" name="dimension" style="width:136px;margin-left:2px;">
                        <option value="0">功能维度</option>
                        <%--<option value="1">流程/应用维度</option>--%>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="modulesid" class="col-sm-2 col-form-label">获取模块</label>
                <div class="col-sm-10">
                    <div id="modulesid" >

                        <label><input name="modules" type="checkbox" checked class="checkbox" value="all"/>数据量分析 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="workflow"/>流程 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="cube"/>表单建模 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="interfaces"/>集成 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="portal"/>门户 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="edc"/>数据中心 </label>
                        <label><input name="modules" type="checkbox" checked class="checkbox" value="mobilemode"/>移动建模 </label>
                    </div>
                </div>
            </div>


            <div class="form-group row">
                <label for="desc" class="col-sm-2 col-form-label">相关说明</label>
                <div class="col-sm-10">
                    <div id="desc" >

                        <label>1.以功能维度导出各个模块需要重新开发或者收到调整的清单 </label></br>
                        <label>2.调整内容和调整方式可以参考以下相关文档 </label></br>
                        <a href="https://www.e-cology.com.cn/sp/doc/docDetail/100500240013091325" target="_blank">E9迁移10实施人员配置说明文档</a>

                        </label>
                    </div>
                </div>
            </div>
            <div class="alert alert-warning" role="alert">
                迁移前置检查(若仅是评估，该步骤可忽略)
            </div>

            <div class="form-group row">
                <label for="qiyuesuo" class="col-sm-2 col-form-label">契约锁非标版本</label>
                <div class="col-sm-10">
                    <div id="qiyuesuo" >

                        <INPUT class=inputStyle name="version" readonly value="<%=version%>"></br>
                        <%
                            if ("1".equals(shouldUpdate)) {
                        %>
                        <label style='color: red'>当前契约锁非标版本为<%=version%>
                            ,不满足迁移版本的要求20220930,请在迁移前先升级契约锁版本至最新!!!!!</label>
                        <%
                        } else if ("0".equals(shouldUpdate)) {
                        %>
                        <label>当前契约锁非标版本为<%=version%>,满足迁移版本要求.</label>
                        <%
                        } else {
                        %>
                        <label>客户未部署契约锁非标,可忽略.</label>
                        <%
                            }
                        %>

                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="workflowchart" class="col-sm-2 col-form-label">流程图数据检查</label>
                <div class="col-sm-10">
                    <div id="workflowchart" >

                        <%
                            if ("".equals(workflowDesign)) {
                        %>
                        <label>客户不存在E8流程图数据,可忽略.</label>
                        <%
                        } else {
                        %>
                        <input type="button" class="e8_btn_top" value="初始化数据" onclick="fixData()"></br>
                        <label><%=workflowDesign%>
                        </label>

                        <%
                            }
                        %>

                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="datasetcheck" class="col-sm-2 col-form-label">数据集检查</label>
                <div class="col-sm-10">
                    <div id="datasetcheck" >

                        <%
                            if ("".equals(dataSetDesign)) {
                        %>
                        <label>客户不存在数据集数据,可忽略.</label>
                        <%
                        } else {
                        %>
                        <input type="button" class="e8_btn_top" value="初始化数据" onclick="fixDataSet()"></br>
                        <label><div style="color:#F00"><%=dataSetDesign%></div>
                        </label>

                        <%
                            }
                        %>


                    </div>
                </div>
            </div>


        </FORM>
    </div>
</div>
<style>
    .flowFrame {
        /*float:left;*/
        margin: 0px;
        padding: 0px;
        width: 100%;
        height: 500px;
        border: 0px;
    }
</style>
<style>
    #loading {
        position: absolute;
        left: 45%;
        background: #ffffff;
        top: 40%;
        padding: 8px;
        z-index: 20001;
        height: auto;
        border: 1px solid #ccc;
        display: none;
    }
</style>
</body>

<script type="text/javascript">
    function fixData() {
        $("#loading").show();
        $.ajax({
            url: '/e10Migration/modules/workflow/util/fixChart.jsp',
            type: 'get',
            dataType: "text",
            success: function (data) {
                $("#loading").hide();
                alert("初始化成功!!" + data);
                location.reload();
            },
            error: function () {
                $("#loading").hide();
                alert("初始化失败!!");
            }
        });
    }

    //初始化数据集
    function fixDataSet() {
        $("#loading").show();
        $.ajax({
            url: '/e10Migration/modules/edc/dataSetSync.jsp',
            type: 'post',
            dataType: "json",
            success: function (data) {
                $("#loading").hide();

                var syncNum = parseInt(data.syncNum);

                var validNum = parseInt(data.validNum);

                var sqlNum = parseInt(data.sqlNum);

                if(syncNum > 0 ){
                    alert("初始化成功" + syncNum+" 条数据集!! 其中有效数据:"+validNum+"条, 生成sql:"+sqlNum+"条!");
                } else {
                    alert("初始化失败!!");
                }

                location.reload();
            },
            error: function () {
                $("#loading").hide();
                alert("初始化失败!!");
            }
        });
    }



    function dosubmit() {
        $("#loading").show();
        var checkvaule = "";
        var dimension = $("#dimension").val();
        $("[name='modules']:checked").each(function () {
            checkvaule += $(this).val() + ",";
        })
        if (checkvaule != "") {
            checkvaule = checkvaule.substring(0, checkvaule.length - 1);
            $("#checkvaule").val(checkvaule);
        } else {
            $("#loading").hide();
            alert("请选择获取模块！");
            return;
        }

        $.ajax({
            url: '/e10Migration/migrationOperation.jsp',
            type: 'post',
            data: {
                checkvaule: checkvaule,
                dimension: dimension,
            },
            dataType: "json",
            success: function (data) {
                var success = data.success;
                if (success != 'undefined' && success != '0') {
                    document.getElementById("downiframe").src = "/downloadExcel";
                } else {
                    alert("下载失败");//下载失败
                }
                $("#loading").hide();
            },
            error: function () {
                $("#loading").hide();
                alert("下载失败!!");
            }
        });

    }
</script>
</html>
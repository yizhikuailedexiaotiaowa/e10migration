<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="weaver.general.Util" %>

<%@ page import="weaver.hrm.*" %>


<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.*" %>
<%@ page import="com.weaver.versionupgrade.customRest.util.ConfigUtil" %>
<%@include file="modules/all/ExportAllDataExcel.jsp" %>
<%@include file="modules/cube/ExportModeExcel.jsp" %>
<%@include file="modules/interfaces/ExportInterfacesExcel.jsp" %>
<%@include file="modules/workflow/ExportWorkflowExcel.jsp" %>
<%@include file="modules/mobilemode/ExportMobileModeExcel.jsp" %>
<%@include file="modules/edc/ExportEdcExcel.jsp" %>
<%@include file="modules/portal/ExportPortalExcel.jsp" %>

<%@include file="/e10Migration/excel/SB2ExcelWritr.jsp" %>

<%!
    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }

%>
<%

    String operation = Util.null2String(request.getParameter("operation"), "");
    String dimension = Util.null2String(request.getParameter("dimension"), "");
    String checkvaule = Util.null2String(request.getParameter("checkvaule"), "");
    List<String> list = Arrays.asList(checkvaule.split(","));
    List<String> fileList = new ArrayList<String>();


    String PATH = ConfigUtil.getExcelDir() + File.separatorChar;

    File file = new File(PATH);
    if (!file.exists()) {
        file.mkdirs();
    }
//总量分析
    /*
    1.获取总数据量
    2.前50数据的表
    3.所有非标准表以及非表单表的清单以及数据量
     */
    if (list.contains("all")) {
        String filepath = doDataAll(PATH);
        if (!"".equals(filepath)) {
            fileList.add(filepath);
        }
    }


    //建模,流程分为功能维度和应用维度,集成一个维度就可以了  ,总共生成3篇文档
    if ("0".equals(dimension)) {//功能维度
        if (list.contains("workflow")) {
            String filepath = doWorkFlowAll(PATH);
            if (!"".equals(filepath)) {
                fileList.add(filepath);
            }
        }
        if (list.contains("cube")) {
            String filepath = doCubeAll(PATH);

            if (!"".equals(filepath)) {
                fileList.add(filepath);
            }
        }

    } else if ("1".equals(dimension)) {//流程,应用维度
        if (list.contains("workflow")) {
//            ExportWorkflowExcel exportWorkflowExcel = new ExportWorkflowExcel();
//            String filepath = exportModeExcel.doall(PATH);
//            if (!"".equals(filepath)) {
//                fileList.add(filepath);
//            }
        }
        if (list.contains("cube")) {
            String filepath = doCubeApp(PATH);
            if (!"".equals(filepath)) {
                fileList.add(filepath);
            }
        }
    }


    //集成模块的清单
    if (list.contains("interfaces")) {

        String filepath = doInterFaceAll(PATH);
        if (!"".equals(filepath)) {
            fileList.add(filepath);
        }
    }
    //门户的清单
    if (list.contains("portal")) {
        String filepath = doPoralAll(PATH);
        if (!"".equals(filepath)) {
            fileList.add(filepath);
        }
    }
    //数据中心的清单
    if (list.contains("edc")) {
        String filepath = doExportEdcDetail(PATH);
        if (!"".equals(filepath)) {
            fileList.add(filepath);
        }
    }

    //移动建模的清单
    if (list.contains("mobilemode")) {
        String filepath = domobilemodeApp(PATH);
        if (!"".equals(filepath)) {
            fileList.add(filepath);
        }
    }

    File allecologyzip = new File(PATH + ConfigUtil.excelname);
    if (allecologyzip.exists() && allecologyzip.isFile()) {
        allecologyzip.delete();
    }

    //开始打包
    wscheck.ZipUtils PackFileUtil = new wscheck.ZipUtils();
    wscheck.ZipUtils.execute(fileList, PATH + ConfigUtil.excelname, PATH, "ecology");


    if (allecologyzip.exists()) {
        out.print("{\"success\":\"" + 1 + "\"}");
    }


%>
